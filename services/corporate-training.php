<?php include '../includes/session.php'; ?>
<!DOCTYPE html>
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
	<!--<![endif]-->
        <head>
            <meta charset="utf-8">
            <title>PrwaTech | Corporate Training </title>
            <meta name="title" content="Big Data Hadoop Online Training | Hadoop Certification Course | Prwatech" />
            <meta name="keywords" content="hadoop training, online hadoop training, hadoop training classes, hadoop course online, big data training, big data course, big data online course, hadoop tutorial, HDFS training, Yarn training, MapReduce training, Pig training, Hive training, HBase training" />
            <meta name="description" content="Our Big Data Hadoop Certification Training helps you master HDFS, Yarn, MapReduce, Pig, Hive, HBase with use cases on Retail, Social Media, Aviation, Finance, Tourism domain" />
            <meta name="description" content="PrwaTech provides big data Hadoop Training Classes for beginners and developers with versatile carrier options. Know more about courses visit our website.">
            <meta name="author" content="prwatech">
            <meta name="google-site-verification" content="fSzBVSEUMu0l5lnD0qBVGv5F_16zaI6xiUZFMm-iMqQ" /> 
            <?php include '../includes/csslinks.php'; ?>
        </head>

	<!-- body classes:  -->
	<!-- "boxed": boxed layout mode e.g. <body class="boxed"> -->
	<!-- "pattern-1 ... pattern-9": background patterns for boxed layout mode e.g. <body class="boxed pattern-1"> -->
	<!-- "transparent-header": makes the header transparent and pulls the banner to top -->
	<body class="no-trans front-page transparent-header ">
            <!-- scrollToTop -->
		<!-- ================ -->
		<div class="scrollToTop circle"><i class="icon-up-open-big"></i></div>
		
		<!-- page wrapper start -->
		<!-- ================ -->
		<div class="page-wrapper">
		
			<!-- header-container start -->
			<?php include '../includes/header.php' ;  ?>
			<!-- header-container end -->
		
			<!-- banner start -->
			<!-- ================ -->
			<div class="banner dark-translucent-bg" style="background-image:url('../images/page-about-banner-4.png'); background-position: 50% 27%;">
				<!-- breadcrumb start -->
				<!-- ================ -->
				<div class="breadcrumb-container object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
					<div class="container">
						<ol class="breadcrumb">
							<li><i class="fa fa-home pr-10"></i><a class="link-dark" href="index.html">Home</a></li>
							<li class="active">Services</li>
							<li class="active">Corporate Training</li>
							
						</ol>
					</div>
				</div>
				<!-- breadcrumb end -->
				<div class="container">
					
				</div>
			</div>
			<!-- banner end -->
			
			<div id="page-start"></div>

			<!-- section start -->
			<!-- ================ -->
                        <section class="light-gray-bg pv-30 clearfix"  style="background:#f8f8f8 url('../images/bg/bigbg2.PNG') no-repeat;background-position:right center;box-shadow:inset 0 2px 7px rgba(0,0,0, 0.25);">
				<div class="container">
					<div class="row">
                                            <div class="col-md-8">
                                                <h2 class="text-center heading-font" style="text-transform:none;"><strong> Corporate </strong> Training </h2>
                                                <div class="separator"></div>
                                                <h5 class="heading-font">Big data analytics and business intelligence</h5>
                                                <p class="cpara2">One among many skills that is overwhelming the technical industry is Big Data Analytics also knows as business intelligence. Big data analytics / business intelligence courses play a crucial role in establishing the foundations for every enthusiastic leaner who aspires to climb up the career ladder as a Data Analyst or a Business Intelligence expert.</p>
                                                <p class="large cpara2">The Course also includes:</p>
                                                <ul class="list-icons cpara2">
                                                    <li class="object-non-visible" data-animation-effect="fadeInUpSmall" data-effect-delay="100"><i class="icon-check-1"></i>Big Data Hadoop</li>
                                                    <li class="object-non-visible" data-animation-effect="fadeInUpSmall" data-effect-delay="150"><i class="icon-check-1"></i>Hadoop Admin</li>
                                                    <li class="object-non-visible" data-animation-effect="fadeInUpSmall" data-effect-delay="200"><i class="icon-check-1"></i>Spark & Scala</li>
                                                    <li class="object-non-visible" data-animation-effect="fadeInUpSmall" data-effect-delay="250"><i class="icon-check-1"></i>Python</li>
                                                    <li class="object-non-visible" data-animation-effect="fadeInUpSmall" data-effect-delay="250"><i class="icon-check-1"></i>Tableau</li>
                                                </ul>
                                            </div>
						<div class="col-md-4">
                                                    <p>&nbsp;</p>
                                                    <div class="overlay-container overlay-visible" style="box-shadow:0 6px 10px rgba(0,0,0, 0.25);">
                                                        <img src="../images/training/aboutSmall1.png" />
                                                        
                                                        <a href="../images/training/aboutBig1.jpg" class="popup-img overlay-link" ><i class="icon-plus-1"></i></a>
                                                    </div>
                                                  
                                                    
                                               </div>
						
					</div>
                                    <p>&nbsp;</p>
                                   
                        <div class="separator"></div>
                        <h2 class="text-center heading-font object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100" style="text-transform:none;">Contact Us <strong>+91 8147111254 </strong></h2>
                        <div class="separator"></div>
                        <p>&nbsp;</p>
                                 </div>
                        </section>
			   <section class="pv-30 clearfix aboutrow2" id="row1" >
					<h2 class="text-center heading-font" style="text-transform:none;">Glimpses of <strong>Corporate Training</strong>  </h2>
				         <div class="separator"></div>
                                         <p>&nbsp;</p>
					<div class="owl-carousel carousel-autoplay">
						<div class="image-box shadow text-center">
							<div class="overlay-container overlay-visible" style="box-shadow:0 6px 10px rgba(0,0,0, 0.25);">
                                                            <img src="../images/training/glimpses/g31.jpg" />
                                                        
                                                        <a href="../images/training/glimpses/g31.jpg" class="popup-img overlay-link" ><i class="icon-plus-1"></i></a>
                                                    </div>
						</div>
						<div class="image-box shadow text-center">
							<div class="overlay-container overlay-visible" style="box-shadow:0 6px 10px rgba(0,0,0, 0.25);">
                                                            <img src="../images/training/glimpses/g1.jpg" />
                                                        
                                                        <a href="../images/training/glimpses/g1.jpg" class="popup-img overlay-link" ><i class="icon-plus-1"></i></a>
                                                    </div>
						</div>
						<div class="image-box shadow text-center">
							<div class="overlay-container overlay-visible" style="box-shadow:0 6px 10px rgba(0,0,0, 0.25);">
                                                            <img src="../images/training/glimpses/g2.jpg" />
                                                        
                                                        <a href="../images/training/glimpses/g2.jpg" class="popup-img overlay-link" ><i class="icon-plus-1"></i></a>
                                                    </div>
						</div>
						<div class="image-box shadow text-center">
							<div class="overlay-container overlay-visible" style="box-shadow:0 6px 10px rgba(0,0,0, 0.25);">
                                                            <img src="../images/training/glimpses/g3.jpg" />
                                                        
                                                        <a href="../images/training/glimpses/g3.jpg" class="popup-img overlay-link" ><i class="icon-plus-1"></i></a>
                                                    </div>
						</div>
						<div class="image-box shadow text-center">
							<div class="overlay-container overlay-visible" style="box-shadow:0 6px 10px rgba(0,0,0, 0.25);">
                                                            <img src="../images/training/glimpses/g4.jpg" />
                                                        
                                                        <a href="../images/training/glimpses/g4.jpg" class="popup-img overlay-link" ><i class="icon-plus-1"></i></a>
                                                    </div>
						</div>
						<div class="image-box shadow text-center">
							<div class="overlay-container overlay-visible" style="box-shadow:0 6px 10px rgba(0,0,0, 0.25);">
                                                            <img src="../images/training/glimpses/g5.jpg" />
                                                        
                                                        <a href="../images/training/glimpses/g5.jpg" class="popup-img overlay-link" ><i class="icon-plus-1"></i></a>
                                                    </div>
						</div>
						<div class="image-box shadow text-center">
							<div class="overlay-container overlay-visible" style="box-shadow:0 6px 10px rgba(0,0,0, 0.25);">
                                                            <img src="../images/training/glimpses/g6.jpg" />
                                                        
                                                        <a href="../images/training/glimpses/g6.jpg" class="popup-img overlay-link" ><i class="icon-plus-1"></i></a>
                                                    </div>
						</div>
						<div class="image-box shadow text-center">
							<div class="overlay-container overlay-visible" style="box-shadow:0 6px 10px rgba(0,0,0, 0.25);">
                                                            <img src="../images/training/glimpses/g7.jpg" />
                                                        
                                                        <a href="../images/training/glimpses/g7.jpg" class="popup-img overlay-link" ><i class="icon-plus-1"></i></a>
                                                    </div>
						</div>
						<div class="image-box shadow text-center">
							<div class="overlay-container overlay-visible" style="box-shadow:0 6px 10px rgba(0,0,0, 0.25);">
                                                            <img src="../images/training/glimpses/g8.jpg" />
                                                        
                                                        <a href="../images/training/glimpses/g8.jpg" class="popup-img overlay-link" ><i class="icon-plus-1"></i></a>
                                                    </div>
						</div>
						<div class="image-box shadow text-center">
							<div class="overlay-container overlay-visible" style="box-shadow:0 6px 10px rgba(0,0,0, 0.25);">
                                                            <img src="../images/training/glimpses/g9.jpg" />
                                                        
                                                        <a href="../images/training/glimpses/g9.jpg" class="popup-img overlay-link" ><i class="icon-plus-1"></i></a>
                                                    </div>
						</div>
						<div class="image-box shadow text-center">
							<div class="overlay-container overlay-visible" style="box-shadow:0 6px 10px rgba(0,0,0, 0.25);">
                                                            <img src="../images/training/glimpses/g10.jpg" />
                                                        
                                                        <a href="../images/training/glimpses/g10.jpg" class="popup-img overlay-link" ><i class="icon-plus-1"></i></a>
                                                    </div>
						</div>
						<div class="image-box shadow text-center">
							<div class="overlay-container overlay-visible" style="box-shadow:0 6px 10px rgba(0,0,0, 0.25);">
                                                            <img src="../images/training/glimpses/g11.jpg" />
                                                        
                                                        <a href="../images/training/glimpses/g11.jpg" class="popup-img overlay-link" ><i class="icon-plus-1"></i></a>
                                                    </div>
						</div>
						<div class="image-box shadow text-center">
							<div class="overlay-container overlay-visible" style="box-shadow:0 6px 10px rgba(0,0,0, 0.25);">
                                                            <img src="../images/training/glimpses/g12.jpg" />
                                                        
                                                        <a href="../images/training/glimpses/g12.jpg" class="popup-img overlay-link" ><i class="icon-plus-1"></i></a>
                                                    </div>
						</div>
						<div class="image-box shadow text-center">
							<div class="overlay-container overlay-visible" style="box-shadow:0 6px 10px rgba(0,0,0, 0.25);">
                                                            <img src="../images/training/glimpses/g13.jpg" />
                                                        
                                                        <a href="../images/training/glimpses/g13.jpg" class="popup-img overlay-link" ><i class="icon-plus-1"></i></a>
                                                    </div>
						</div>
						<div class="image-box shadow text-center">
							<div class="overlay-container overlay-visible" style="box-shadow:0 6px 10px rgba(0,0,0, 0.25);">
                                                            <img src="../images/training/glimpses/g14.jpg" />
                                                        
                                                        <a href="../images/training/glimpses/g14.jpg" class="popup-img overlay-link" ><i class="icon-plus-1"></i></a>
                                                    </div>
						</div>
						<div class="image-box shadow text-center">
							<div class="overlay-container overlay-visible" style="box-shadow:0 6px 10px rgba(0,0,0, 0.25);">
                                                            <img src="../images/training/glimpses/g15.jpg" />
                                                        
                                                        <a href="../images/training/glimpses/g15.jpg" class="popup-img overlay-link" ><i class="icon-plus-1"></i></a>
                                                    </div>
						</div>
						<div class="image-box shadow text-center">
							<div class="overlay-container overlay-visible" style="box-shadow:0 6px 10px rgba(0,0,0, 0.25);">
                                                            <img src="../images/training/glimpses/g16.jpg" />
                                                        
                                                        <a href="../images/training/glimpses/g16.jpg" class="popup-img overlay-link" ><i class="icon-plus-1"></i></a>
                                                    </div>
						</div>
						<div class="image-box shadow text-center">
							<div class="overlay-container overlay-visible" style="box-shadow:0 6px 10px rgba(0,0,0, 0.25);">
                                                            <img src="../images/training/glimpses/g17.jpg" />
                                                        
                                                        <a href="../images/training/glimpses/g17.jpg" class="popup-img overlay-link" ><i class="icon-plus-1"></i></a>
                                                    </div>
						</div>
						<div class="image-box shadow text-center">
							<div class="overlay-container overlay-visible" style="box-shadow:0 6px 10px rgba(0,0,0, 0.25);">
                                                            <img src="../images/training/glimpses/g18.jpg" />
                                                        
                                                        <a href="../images/training/glimpses/g18.jpg" class="popup-img overlay-link" ><i class="icon-plus-1"></i></a>
                                                    </div>
						</div>
						<div class="image-box shadow text-center">
							<div class="overlay-container overlay-visible" style="box-shadow:0 6px 10px rgba(0,0,0, 0.25);">
                                                            <img src="../images/training/glimpses/g19.jpg" />
                                                        
                                                        <a href="../images/training/glimpses/g19.jpg" class="popup-img overlay-link" ><i class="icon-plus-1"></i></a>
                                                    </div>
						</div>
						<div class="image-box shadow text-center">
							<div class="overlay-container overlay-visible" style="box-shadow:0 6px 10px rgba(0,0,0, 0.25);">
                                                            <img src="../images/training/glimpses/g20.jpg" />
                                                        
                                                        <a href="../images/training/glimpses/g20.jpg" class="popup-img overlay-link" ><i class="icon-plus-1"></i></a>
                                                    </div>
						</div>
						<div class="image-box shadow text-center">
							<div class="overlay-container overlay-visible" style="box-shadow:0 6px 10px rgba(0,0,0, 0.25);">
                                                            <img src="../images/training/glimpses/g21.jpg" />
                                                        
                                                        <a href="../images/training/glimpses/g21.jpg" class="popup-img overlay-link" ><i class="icon-plus-1"></i></a>
                                                    </div>
						</div>
						<div class="image-box shadow text-center">
							<div class="overlay-container overlay-visible" style="box-shadow:0 6px 10px rgba(0,0,0, 0.25);">
                                                            <img src="../images/training/glimpses/g22.jpg" />
                                                        
                                                        <a href="../images/training/glimpses/g22.jpg" class="popup-img overlay-link" ><i class="icon-plus-1"></i></a>
                                                    </div>
						</div>
						<div class="image-box shadow text-center">
							<div class="overlay-container overlay-visible" style="box-shadow:0 6px 10px rgba(0,0,0, 0.25);">
                                                            <img src="../images/training/glimpses/g23.jpg" />
                                                        
                                                        <a href="../images/training/glimpses/g23.jpg" class="popup-img overlay-link" ><i class="icon-plus-1"></i></a>
                                                    </div>
						</div>
						
						
					</div>
				
			
                        </section>
                   
                             <section class="light-gray-bg pv-30 clearfix"  style="background:#f8f8f8 url('../images/bg/overlay.png') repeat;box-shadow:inset 0 2px 7px rgba(0,0,0, 0.25);">
				<div class="container">
					<div class="row">
                                            <div class="col-md-10 col-md-offset-1">
                                                <h2 class="text-center heading-font" style="text-transform:none">Our <strong>Clients</strong></h2>
                                                <div class="separator"></div>
                                                  <p class="cpara2">PrwaTech provides training to the best industry professionals working at IBM, HCL, ITC and Accenture and many more.</p>   
                                                <br>
                                            
                                            <p>&nbsp;</p>
						<div class="col-md-3">
                                                    <img src="../images/client/ibm.png" style="box-shadow:0 6px 10px rgba(0,0,0, 0.25);"/>
                                                </div>
						<div class="col-md-3">
                                                    <img src="../images/client/amdocs.png" style="box-shadow:0 6px 10px rgba(0,0,0, 0.25);"/>
                                                </div>
						<div class="col-md-3">
                                                    <img src="../images/client/capgimini.png" style="box-shadow:0 6px 10px rgba(0,0,0, 0.25);"/>
                                                </div>
						<div class="col-md-3">
                                                    <img src="../images/client/flipkart.png" style="box-shadow:0 6px 10px rgba(0,0,0, 0.25);"/>
                                                </div>
                                            <p>&nbsp;</p>
						<div class="col-md-3">
                                                    <img src="../images/client/hcl.png" style="box-shadow:0 6px 10px rgba(0,0,0, 0.25);"/>
                                                </div>
						<div class="col-md-3">
                                                    <img src="../images/client/syntel.png" style="box-shadow:0 6px 10px rgba(0,0,0, 0.25);"/>
                                                </div>
						
						<div class="col-md-3">
                                                    <img src="../images/client/ciber.png" style="box-shadow:0 6px 10px rgba(0,0,0, 0.25);"/>
                                                </div>
						<div class="col-md-3">
                                                    <img src="../images/client/sungaurd.png" style="box-shadow:0 6px 10px rgba(0,0,0, 0.25);"/>
                                                </div>
						<p>&nbsp;</p>
                                                
						<div class="col-md-3">
                                                    <img src="../images/client/synchron.png" style="box-shadow:0 6px 10px rgba(0,0,0, 0.25);"/>
                                                </div>
						<div class="col-md-3">
                                                    <img src="../images/client/yodlee.png" style="box-shadow:0 6px 10px rgba(0,0,0, 0.25);"/>
                                                </div>
						<div class="col-md-3">
                                                    <img src="../images/client/uhg.png" style="box-shadow:0 6px 10px rgba(0,0,0, 0.25);"/>
                                                </div>
						<div class="col-md-3">
                                                    <img src="../images/client/appliedmaterial.png" style="box-shadow:0 6px 10px rgba(0,0,0, 0.25);"/>
                                                </div>
                                               <p>&nbsp;</p>
                                                <div class="col-md-3">
                                                    <img src="../images/client/symphony.png" style="box-shadow:0 6px 10px rgba(0,0,0, 0.25);"/>
                                                </div>
                                               <div class="col-md-3">
                                                   <img src="../images/client/johnson.png" style="box-shadow:0 6px 10px rgba(0,0,0, 0.25);"/>
                                                </div>
						
					</div>
                                    <p>&nbsp;</p>
                                 </div>
                        </section>
                         <section class="pv-30  padding-bottom-clear dark-translucent-bg parallax" id="row-testimonials"style="background:url('../images/bg/451.jpg'); box-shadow:inset 0 2px 7px rgba(0,0,0, 0.25);">
                            
                            <div class="space-bottom">
                                <div class="owl-carousel content-slider">
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-md-8 col-md-offset-2">
                                                <div class="testimonial text-center">
                                                    <div class="testimonial-image">
                                                        <img src="../images/testimonial-1.png" alt="" title="" class="img-circle">
                                                    </div>
                                                        
                                                    <div class="testimonial-body">
                                                        <blockquote>
                                                            <p class="cpara3">It was a nice learning experience with prwatech. The classes were well scheduled and managed.
                                                                Verma has good understanding of the topics taught and catered to our problems and doubts very patiently. The best thing about him was that he handled the situations accordingly, when needed a friend, he became one and also a teacher to always guide us.</p>
                                                        </blockquote>
                                                        <div class="cpara3">Anukanksha Garg</div>
                                                        <div class="testimonial-info-2 cpara3">B.TECH- CS</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-md-8 col-md-offset-2">
                                                <div class="testimonial text-center">
                                                    <div class="testimonial-image">
                                                        <img src="../images/testimonial-2.jpg" alt="" title="" class="img-circle">
                                                    </div>
                                                        
                                                    <div class="testimonial-body">
                                                        <blockquote>
                                                            <p class="cpara3">“Excellent course and instructor. I learnt a lot in a short period. Good value for money. Instructor took us through Advanced Hadoop Development in depth.”</p>
                                                        </blockquote>
                                                        <div class="testimonial-info-1 cpara3">Anupam Khamparia</div>
                                                        <div class="testimonial-info-2 cpara3">Consultant, Cognizant Technology Solutions, Bangalore</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-md-8 col-md-offset-2">
                                                <div class="testimonial text-center">
                                                    <div class="testimonial-image">
                                                        <img src="../images/testimonial-3.jpg" alt="" title="" class="img-circle">
                                                    </div>
                                                        
                                                    <div class="testimonial-body">
                                                        <blockquote>
                                                            <p class="cpara3">“Excellent course and instructor. I learnt a lot in a short period. Good value for money. Instructor took us through Advanced Hadoop Development in depth.”</p>
                                                        </blockquote>
                                                        <div class="testimonial-info-1 cpara3">Kumar Waibhav</div>
                                                        <div class="testimonial-info-2 cpara3">Software Engineer, Cognizant Technology Solutions,<br> Philadelphia, Pennsylvania, USA</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                    
                            </div>
                        </section> 
			<!-- section end -->

			<!-- section -->
			<!-- ================ -->
		
			<!-- section end -->

			<!-- section start -->
			<!-- ================ -->
                        
			                   
			<!-- section end -->
			<!-- footer start (Add "dark" class to #footer in order to enable dark footer) -->
			<!-- ================ -->
			<?php include './../includes/footer.php'; ?>
			<!-- footer end -->
			
		</div>
		<!-- page-wrapper end -->
<script>
            $.get('../plugins/magnific-popup/jquery.magnific-popup.min.js').done(function (){
                                        $.get('../js/template.js').done(function (){
                                        
                                        })
                                    }) // For Late Binding 
                    </script>
		<?php include './../includes/jslinks.php';?>
		<!-- Color Switcher End -->
                <?php include './../includes/quick-query.php';?>
                 <?php include './../includes/UserSignup.php';?>
<!--                 <script type="text/javascript" src="plugins/magnific-popup/jquery.magnific-popup.min.js"></script>-->
	</body>
        

</html>

