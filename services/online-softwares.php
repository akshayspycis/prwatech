<?php include '../includes/session.php'; ?>
<!DOCTYPE html>
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
	<!--<![endif]-->
        <head>
            <meta charset="utf-8">
            <title>PrwaTech | Online Softwares | IT training & placements for students by Prwatech </title>
            <meta name="title" content="Big Data Hadoop Online Training | Hadoop Certification Course | Prwatech" />
                <meta name="keywords" content="career placement assistance by aptech, job placement assistance, career guidance, it training & placements, jobs in it industry, major it companies" />
               <meta name="description" content="PrwaTech has a dedicated placement team to help students and get placement in various IT job roles with major companies. Students get a foothold in the booming IT &amp; ITeS Industry.">
                <meta name="description" content="PrwaTech provides big data Hadoop Training Classes for beginners and developers with versatile carrier options. Know more about courses visit our website.">
                <meta name="author" content="prwatech">
                <meta name="google-site-verification" content="fSzBVSEUMu0l5lnD0qBVGv5F_16zaI6xiUZFMm-iMqQ" /> 
                <?php include '../includes/csslinks.php'; ?>
                <style>
                    #softwares-box{
                        background:#f8f8f8;border:1px solid gray;padding-top:30px;padding-bottom:30px;margin-bottom:30px;box-shadow:0 6px 10px rgba(0,0,0, 0.25);
                    }
                </style>
        </head>

	<!-- body classes:  -->
	<!-- "boxed": boxed layout mode e.g. <body class="boxed"> -->
	<!-- "pattern-1 ... pattern-9": background patterns for boxed layout mode e.g. <body class="boxed pattern-1"> -->
	<!-- "transparent-header": makes the header transparent and pulls the banner to top -->
	<body class="no-trans front-page transparent-header ">
            <!-- scrollToTop -->
		<!-- ================ -->
		<div class="scrollToTop circle"><i class="icon-up-open-big"></i></div>
		
		<!-- page wrapper start -->
		<!-- ================ -->
		<div class="page-wrapper">
		
			<!-- header-container start -->
			<?php include '../includes/header.php' ;  ?>
			<!-- header-container end -->
		
			<!-- banner start -->
			<!-- ================ -->
			<div class="banner dark-translucent-bg" style="background-image:url('../images/page-about-banner-3.jpg'); background-position: 50% 27%;">
				<!-- breadcrumb start -->
				<!-- ================ -->
				<div class="breadcrumb-container object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
					<div class="container">
						<ol class="breadcrumb">
							<li><i class="fa fa-home pr-10"></i><a class="link-dark" href="index.html">Home</a></li>
							<li class="active">Online Softwares</li>
							
						</ol>
					</div>
				</div>
				<!-- breadcrumb end -->
				<div class="container">
					
				</div>
			</div>
			<!-- banner end -->
			
			<div id="page-start"></div>

			<!-- section start -->
			<!-- ================ -->
                         
                         <section class="light-gray-bg pv-30 clearfix"  style="background:#f8f8f8 url('../images/bg/overlay.png') repeat;box-shadow:inset 0 2px 7px rgba(0,0,0, 0.25);">
                                      <div class="container">
						 <h2 class="text-center heading-font" style="text-transform:none;">Download<strong>Softwares</strong></h2>
                                                <div class="separator"></div>
                                                <div class="row" id="softwares-box">
                                                    
                                                    <div class="col-md-10" >
                                                        <h3 class="heading-font" style="font-weight:bold">Download the image of ubuntu 12.04:</h3>
                                                
                                                    </div>
                                                    <div class="col-md-2">
                                                        
                                                        <a href="http://www.traffictool.net/vmware/ubuntu1204t.html" class="btn btn-success">URL 1</a>
                                                    </div>
                                                    <div class="clearfix"></div>
                                            
                                                </div>
                                                 <div class="row" id="softwares-box">
                                                    
                                                    <div class="col-md-10" >
                                                        <h3 class="heading-font" style="font-weight:bold">Download the image of Hadoop:</h3>
                                                
                                                    </div>
                                                    <div class="col-md-2">
                                                        
                                                        <a href="http://archive.apache.org/dist/hadoop/core/hadoop-1.2.0/hadoop-1.2.0.tar.gz" class="btn btn-success">URL 1</a>
                                                    </div>
                                                    <div class="clearfix"></div>
                                            
                                                </div>
                                                <div class="row" id="softwares-box">
                                                    
                                                    <div class="col-md-10" >
                                                        <h3 class="heading-font" style="font-weight:bold">Download the image of Vmplayer:</h3>
                                                
                                                    </div>
                                                    <div class="col-md-2">
                                                        <a href="https://www.vmware.com/tryvmware/?p=player" class="btn btn-success">URL 1</a>
                                                        <a href="https://drive.google.com/file/d/0ByQlo7MGiNReWFlkS1JNR3JnWEU/edit?usp=sharing" class="btn btn-success">URL 2</a>
                                                    </div>
                                                    <div class="clearfix"></div>
                                            
                                                </div>
                                                 <div class="row" id="softwares-box">
                                                    
                                                    <div class="col-md-10" >
                                                        <h3 class="heading-font" style="font-weight:bold">Download the image of Cloudera-demo-0.3.7.vmwarevm.tar:</h3>
                                                
                                                    </div>
                                                    <div class="col-md-2">
                                                        <a href="https://drive.google.com/file/d/0ByQlo7MGiNReRXRNdHhnckpINVU/edit?usp=sharing" class="btn btn-success">URL 1</a>
                                                       
                                                    </div>
                                                    <div class="clearfix"></div>
                                            
                                                </div>
                                                 <div class="row" id="softwares-box">
                                                    
                                                    <div class="col-md-10" >
                                                        <h3 class="heading-font" style="font-weight:bold">Download the image of Cloudera-quickstart-vm-4.4.0-1-vmware.7z:</h3>
                                                
                                                    </div>
                                                    <div class="col-md-2">
                                                        <a href="https://drive.google.com/file/d/0ByQlo7MGiNReTExLNmd1Zk5SV2M/edit?usp=sharing" class="btn btn-success">URL 1</a>
                                                       
                                                    </div>
                                                    <div class="clearfix"></div>
                                            
                                                </div>
                                                 <div class="row" id="softwares-box">
                                                    
                                                    <div class="col-md-10" >
                                                        <h3 class="heading-font" style="font-weight:bold">Download the image of Eclipse indigo:</h3>
                                                
                                                    </div>
                                                    <div class="col-md-2">
                                                        <a href="https://drive.google.com/file/d/0B_NodZGbjhaVVlNYRS1zVGJfb00/edit?usp=sharing" class="btn btn-success">URL 1</a>
                                                       
                                                    </div>
                                                    <div class="clearfix"></div>
                                            
                                                </div>
                                                 <div class="row" id="softwares-box">
                                                    
                                                    <div class="col-md-10" >
                                                        <h3 class="heading-font" style="font-weight:bold">Download the image of Filzila (ftp tool)</h3>
                                                
                                                    </div>
                                                    <div class="col-md-2">
                                                        <a href="https://drive.google.com/file/d/0B_NodZGbjhaVODFEUXVsNTc5VmM/edit?usp=sharing" class="btn btn-success">URL 1</a>
                                                       
                                                    </div>
                                                    <div class="clearfix"></div>
                                            
                                                </div>
                                                 <div class="row" id="softwares-box">
                                                    
                                                    <div class="col-md-10" >
                                                        <h3 class="heading-font" style="font-weight:bold">Download the image of Java 1.6:</h3>
                                                
                                                    </div>
                                                    <div class="col-md-2">
                                                        <a href="https://drive.google.com/file/d/0B_NodZGbjhaVODFEUXVsNTc5VmM/edit?usp=sharing" class="btn btn-success">URL 1</a>
                                                       
                                                    </div>
                                                    <div class="clearfix"></div>
                                            
                                                </div>
                                                 <div class="row" id="softwares-box">
                                                    
                                                    <div class="col-md-10" >
                                                        <h3 class="heading-font" style="font-weight:bold">Download the image of Filzila (ftp tool)</h3>
                                                
                                                    </div>
                                                    <div class="col-md-2">
                                                        <a href="https://drive.google.com/file/d/0B_NodZGbjhaVODFEUXVsNTc5VmM/edit?usp=sharing" class="btn btn-success">URL 1</a>
                                                       
                                                    </div>
                                                    <div class="clearfix"></div>
                                            
                                                </div>
                                                 <div class="row" id="softwares-box">
                                                    
                                                    <div class="col-md-10" >
                                                        <h3 class="heading-font" style="font-weight:bold">Download the image of Filzila (ftp tool)</h3>
                                                
                                                    </div>
                                                    <div class="col-md-2">
                                                        <a href="https://drive.google.com/file/d/0BypjD9NJHlr9RVlXVHZET3pBMzg/edit?usp=sharing" class="btn btn-success">URL 1</a>
                                                        <a href="http://www.gtlib.gatech.edu/pub/apache/pig/pig-0.11.0/pig-0.11.0.tar.gz" class="btn btn-success">URL 2</a>
                                                       
                                                    </div>
                                                    <div class="clearfix"></div>
                                            
                                                </div>
                                                <div class="row" id="softwares-box">
                                                    
                                                    <div class="col-md-10" >
                                                        <h3 class="heading-font" style="font-weight:bold">Download the image of Hive</h3>
                                                
                                                    </div>
                                                    <div class="col-md-2">
                                                        <a href="https://drive.google.com/file/d/0BypjD9NJHlr9bnI0dDl3SGEyM1U/edit?usp=sharing" class="btn btn-success">URL 1</a>
                                                       
                                                       
                                                    </div>
                                                    <div class="clearfix"></div>
                                            
                                                </div>
                                                <div class="row" id="softwares-box">
                                                    
                                                    <div class="col-md-10" >
                                                        <h3 class="heading-font" style="font-weight:bold">Download the image of Hive</h3>
                                                
                                                    </div>
                                                    <div class="col-md-2">
                                                        <a href="https://drive.google.com/file/d/0BypjD9NJHlr9bnI0dDl3SGEyM1U/edit?usp=sharing" class="btn btn-success">URL 1</a>
                                                       
                                                       
                                                    </div>
                                                    <div class="clearfix"></div>
                                            
                                                </div>
                                                <div class="row" id="softwares-box">
                                                    
                                                    <div class="col-md-10" >
                                                        <h3 class="heading-font" style="font-weight:bold">Download the image of Hbase</h3>
                                                
                                                    </div>
                                                    <div class="col-md-2">
                                                        <a href="https://drive.google.com/file/d/0BypjD9NJHlr9SWNGbTdpMEZQeUU/edit?usp=sharing" class="btn btn-success">URL 1</a>
                                                       
                                                       
                                                    </div>
                                                    <div class="clearfix"></div>
                                            
                                                </div>
                                                <div class="row" id="softwares-box">
                                                    
                                                    <div class="col-md-10" >
                                                        <h3 class="heading-font" style="font-weight:bold">Download the image of Zookeeper</h3>
                                                
                                                    </div>
                                                    <div class="col-md-2">
                                                        <a href="https://drive.google.com/file/d/0BypjD9NJHlr9OHZkb1c0ajgzQnM/edit?usp=sharing" class="btn btn-success">URL 1</a>
                                                       
                                                       
                                                    </div>
                                                    <div class="clearfix"></div>
                                            
                                                </div>
                                                <div class="row" id="softwares-box">
                                                    
                                                    <div class="col-md-10" >
                                                        <h3 class="heading-font" style="font-weight:bold">Download the image of Sqoop tool:</h3>
                                                
                                                    </div>
                                                    <div class="col-md-2">
                                                        <a href="https://drive.google.com/file/d/0BypjD9NJHlr9Q01VQVJqc2FKV1E/edit?usp=sharing" class="btn btn-success">URL 1</a>
                                                       
                                                       
                                                    </div>
                                                    <div class="clearfix"></div>
                                            
                                                </div>
                                                 <div class="row" id="softwares-box">
                                                    
                                                    <div class="col-md-10" >
                                                        <h3 class="heading-font" style="font-weight:bold">Download the image of MySQL Installer and Unzip it:</h3>
                                                
                                                    </div>
                                                    <div class="col-md-2">
                                                        <a href="https://drive.google.com/file/d/0B2-rlCGKD40NangwRGdLUXg2REE/edit?usp=sharing" class="btn btn-success">URL 1</a>
                                                        <a href="http://dev.mysql.com/downloads/connector/j/" class="btn btn-success">URL 2</a>
                                                       
                                                       
                                                    </div>
                                                    <div class="clearfix"></div>
                                            
                                                </div>
                                                 <div class="row" id="softwares-box">
                                                    
                                                    <div class="col-md-10" >
                                                        <h3 class="heading-font" style="font-weight:bold">Download the image of Mysql-connector</h3>
                                                
                                                    </div>
                                                    <div class="col-md-2">
                                                        <a href="https://drive.google.com/file/d/0B2-rlCGKD40NSVJ3QlpWSk95OFE/edit" class="btn btn-success">URL 1</a>
                                                        
                                                       
                                                       
                                                    </div>
                                                    <div class="clearfix"></div>
                                            
                                                </div>
                                                 <div class="row" id="softwares-box">
                                                    
                                                    <div class="col-md-10" >
                                                        <h3 class="heading-font" style="font-weight:bold">Download the image of Flume-1.3.1</h3>
                                                
                                                    </div>
                                                    <div class="col-md-2">
                                                        <a href="https://drive.google.com/file/d/0BypjD9NJHlr9enlfVTdheXpZbVk/edit?usp=sharing" class="btn btn-success">URL 1</a>
                                                    </div>
                                                    <div class="clearfix"></div>
                                            
                                                </div>
                                                 <div class="row" id="softwares-box">
                                                    
                                                    <div class="col-md-10" >
                                                        <h3 class="heading-font" style="font-weight:bold">Download the image of Oracle Connector For Oracle to HDFS Using Sqoop</h3>
                                                
                                                    </div>
                                                    <div class="col-md-2">
                                                        <a href="https://drive.google.com/file/d/0BypjD9NJHlr9Y05NSHBjcmxJeUk/view?usp=sharing" class="btn btn-success">URL 1</a>
                                                    </div>
                                                    <div class="clearfix"></div>
                                            
                                                </div>
                                                 <div class="row" id="softwares-box">
                                                    
                                                    <div class="col-md-10" >
                                                        <h3 class="heading-font" style="font-weight:bold">Download the image of Flume-1.3.1</h3>
                                                
                                                    </div>
                                                    <div class="col-md-2">
                                                        <a href="https://drive.google.com/file/d/0BypjD9NJHlr9enlfVTdheXpZbVk/edit?usp=sharing" class="btn btn-success">URL 1</a>
                                                    </div>
                                                    <div class="clearfix"></div>
                                            
                                                </div>
                                                <div class="row" id="softwares-box">
                                                    
                                                    <div class="col-md-10" >
                                                        <h3 class="heading-font" style="font-weight:bold">Download the image of Flume Snapshot.jar</h3>
                                                
                                                    </div>
                                                    <div class="col-md-2">
                                                        <a href="https://drive.google.com/file/d/0BypjD9NJHlr9RF91cEVKdGY3NEU/edit?usp=sharing" class="btn btn-success">URL 1</a>
                                                    </div>
                                                    <div class="clearfix"></div>
                                            
                                                </div>
                                                <div class="row" id="softwares-box">
                                                    
                                                    <div class="col-md-10" >
                                                        <h3 class="heading-font" style="font-weight:bold">Download the image of Oracle Connector For Oracle to HDFS Using Sqoop</h3>
                                                
                                                    </div>
                                                    <div class="col-md-2">
                                                        <a href="https://drive.google.com/file/d/0BypjD9NJHlr9Y05NSHBjcmxJeUk/view?usp=sharing" class="btn btn-success">URL 1</a>
                                                    </div>
                                                    <div class="clearfix"></div>
                                            
                                                </div>
                                                <div class="row" id="softwares-box">
                                                    
                                                    <div class="col-md-10" >
                                                        <h3 class="heading-font" style="font-weight:bold">WINSCP</h3>
                                                
                                                    </div>
                                                    <div class="col-md-2">
                                                        <a href="https://drive.google.com/file/d/0B1WXBTTt-8M_VG1mS05iTFJHR0E/view?usp=sharing" class="btn btn-success">URL 1</a>
                                                    </div>
                                                    <div class="clearfix"></div>
                                            
                                                </div>
                                                <div class="row" id="softwares-box">
                                                    
                                                    <div class="col-md-10" >
                                                        <h3 class="heading-font" style="font-weight:bold">VMware version 7</h3>
                                                
                                                    </div>
                                                    <div class="col-md-2">
                                                        <a href="https://drive.google.com/file/d/0B1WXBTTt-8M_Qm93T2JKMUowTHM/view?usp=sharing" class="btn btn-success">URL 1</a>
                                                    </div>
                                                    <div class="clearfix"></div>
                                            
                                                </div>
                                                
                                      </div>
                                    </section> 
                         
		
			<!-- section end -->

			<!-- section start -->
			<!-- ================ -->
                        
			                   
			<!-- section end -->
			<!-- footer start (Add "dark" class to #footer in order to enable dark footer) -->
			<!-- ================ -->
			<?php include '../includes/footer.php'; ?>
			<!-- footer end -->
			
		</div>
		<!-- page-wrapper end -->

		<?php include '../includes/jslinks.php';?>
		<!-- Color Switcher End -->
              <?php include '../includes/quick-query.php';?>
              <?php include '../includes/UserSignup.php';?>
	</body>


</html>



