<?php include '../includes/session.php'; ?><!DOCTYPE html>
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
	<!--<![endif]-->
<head>
		<meta charset="utf-8">
                <title>PrwaTech | Workshop Bigdata and Hadoop </title>
		<meta name="title" content="Big Data Hadoop Online Training | Hadoop Certification Course | Prwatech" />
                <meta name="keywords" content="hadoop training, online hadoop training, hadoop training classes, hadoop course online, big data training, big data course, big data online course, hadoop tutorial, HDFS training, Yarn training, MapReduce training, Pig training, Hive training, HBase training" />
                <meta name="description" content="Our Big Data Hadoop Certification Training helps you master HDFS, Yarn, MapReduce, Pig, Hive, HBase with use cases on Retail, Social Media, Aviation, Finance, Tourism domain" />
                <meta name="description" content="PrwaTech provides big data Hadoop Training Classes for beginners and developers with versatile carrier options. Know more about courses visit our website.">
                <meta name="author" content="prwatech">
                <meta name="google-site-verification" content="fSzBVSEUMu0l5lnD0qBVGv5F_16zaI6xiUZFMm-iMqQ" />  
                <?php include '../includes/csslinks.php'; ?>
        </head>

	<!-- body classes:  -->
	<!-- "boxed": boxed layout mode e.g. <body class="boxed"> -->
	<!-- "pattern-1 ... pattern-9": background patterns for boxed layout mode e.g. <body class="boxed pattern-1"> -->
	<!-- "transparent-header": makes the header transparent and pulls the banner to top -->
	<body class="no-trans front-page transparent-header ">
            <!-- scrollToTop -->
		<!-- ================ -->
		<div class="scrollToTop circle"><i class="icon-up-open-big"></i></div>
		
		<!-- page wrapper start -->
		<!-- ================ -->
		<div class="page-wrapper">
		
			<!-- header-container start -->
			<?php include '../includes/header.php' ;  ?>
			<!-- header-container end -->
		
			<!-- banner start -->
			<!-- ================ -->
			<div class="banner dark-translucent-bg" style="background-image:url('../images/page-course-banner-1.jpg'); background-position: 50% 27%;">
				<!-- breadcrumb start -->
				<!-- ================ -->
				<div class="breadcrumb-container object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
					<div class="container">
						<ol class="breadcrumb">
							<li><i class="fa fa-home pr-10"></i><a class="link-dark" href="index.html">Home</a></li>
							<li class="active">All Courses</li>
							<li class="active">Workshop Bigdata and Hadoop</li>
						</ol>
					</div>
				</div>
				<!-- breadcrumb end -->
				
			</div>
			<!-- banner end -->
			
			<div id="page-start"></div>

			<!-- section start -->
			<!-- ================ -->
                         <section class="light-gray-bg pv-30 clearfix"  style="background:#f8f8f8 url('../images/bg/overlay.png') repeat;box-shadow:inset 0 2px 7px rgba(0,0,0, 0.25);">
				<div class="container">
					<div class="row">
						<div class="col-md-8">
							 <h2 class="text-center heading-font" style="text-transform:none;">Workshop on <strong> Bigdata and Hadoop </strong></h2>
                                                    <div class="separator"></div>
                                                         <p class="cpara2">In PRWATECH we make sure that real time experience is bieng given to students and professional , through workshop in advance fields like BIG Data & Hadoop, Business Intelligence & Business Analytics.Presently above mentioned fields are the hot cake in the market and persons having a hands on experience on these fields are among the most sought after professionals .With reference to the same, we wish to start training programs in this field to their own place in a shorter duration.</p>
                                                </div>
						<div class="col-md-4">
                                                    <p>&nbsp;</p>
                                                    <div class="overlay-container overlay-visible" style="box-shadow:0 6px 10px rgba(0,0,0, 0.25);">
                                                        <img src="../images/training/aboutSmall1.png" />
                                                        
                                                        <a href="../images/training/aboutBig1.jpg" class="popup-img overlay-link" ><i class="icon-plus-1"></i></a>
                                                    </div>
                                                  
                                                    
                                               </div>
						
					</div>
                                     <p>&nbsp;</p>
                                 </div>
                         </section>
                        
                        <section class="light-gray-bg pv-30 clearfix"  style="background:#f8f8f8 url('../images/bg/bigbg4.png') no-repeat;background-position:left center;box-shadow:inset 0 2px 7px rgba(0,0,0, 0.25);">
				<div class="container">
					<div class="row">
					<div class="col-md-4">
                                                    <p>&nbsp;</p>
                                                    <div class="overlay-container overlay-visible" style="box-shadow:0 6px 10px rgba(0,0,0, 0.25);">
                                                        <img src="../images/training/IBM-small1.png" />
                                                        
                                                        <a href="../images/training/IBM-big1.jpg" class="popup-img overlay-link" ><i class="icon-plus-1"></i></a>
                                                    </div>
                                                  
                                                    
                                               </div>	
                                            <div class="col-md-8">
                                                <p>&nbsp;</p>
                                                         <p class="cpara2">Our main focus is that through this workshop students gain valuable knowledge about BIG DATA & HADOOP, Business Intelligence & Business Analytics and also keep them updated about the devlopments in BIGDATA & Analytics segment .In each workshop we send experts for the training in thier own place , we shall work in juxtapostion with new and challenging problems or promising problem solution technique whose realm has not been explored well. Various groups have had a prolonged impact on subsequent research and practice through the publications, software,and data that they created. For most of the participants, the biggest benefit is the interaction with other members, and extremely enriching interactions. </p>
                                                         <p class="cpara2"> The workshops also contribute to the pool of trained specialists in the fields of BIG DATA & HADOOP, Business Intelligence and Business Analytics by providing immersive training to corporate professionals and students, allowing participants from different backgrounds to learn from one another, and educating all workshop participants through guest lectures and research updates.</p>
						</div>
						
						
					</div>
                                    <p>&nbsp;</p>
                                 </div>
                        </section>
                         <section class="light-gray-bg pv-30 clearfix"  style="background:#f8f8f8 url('../images/bg/overlay.png') repeat;box-shadow:inset 0 2px 7px rgba(0,0,0, 0.25);">
				<div class="container">
					<div class="row">
						<p>&nbsp;</p>
						
						<div class="col-md-8">
						 <p>&nbsp;</p>
                                                     <h3 class="heading-font">Workshop topics include:</h3>
                                                 
                                                 <ul class="list-icons" id="clist">
                                                        <li><i class="icon-check pr-10"></i> Introduction of Big Data and Hadoop</li>
                                                        <li><i class="icon-check pr-10"></i> Playing around with Cluster (Hadoop Cluster)</li>
                                                        <li><i class="icon-check pr-10"></i> Map-Reduce Basics and Implementation</li>
                                                        <li><i class="icon-check pr-10"></i> PIG (Analytics using Pig)</li>
                                                        <li><i class="icon-check pr-10"></i> HIVE(Understanding and Analytics on HIVE)</li>
                                                        <li><i class="icon-check pr-10"></i> Hbase(Database Techniques and Loading the data)</li>
                                                        <li><i class="icon-check pr-10"></i> Real Time training programme</li>
                                                            
                                                    </ul>
                                                </div>
                                                 <div class="col-md-4">
                                                    <div class="overlay-container overlay-visible" style="box-shadow:0 6px 10px rgba(0,0,0, 0.25);">
                                                        <img src="../images/training/gallery2/IBM4.jpg"  width="100%"/>
                                                        
                                                        <a href="../images/training/gallery2/IBM4.jpg" class="popup-img overlay-link" ><i class="icon-plus-1"></i></a>
                                                    </div>
                                               </div>
						</div>
                                               
					</div>
                                    <p>&nbsp;</p>
                                 </div>
                        </section>
			
                         <div class="section pv-40 parallax dark-translucent-bg background-img-7">
                            <div class="container">
                                
                                <h2 class="text-center heading-font object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100" style="text-transform:none;">Our  <strong>Workshops</strong>.</h2>
                                <p>&nbsp;</p>
                                <div class="col-md-6">
                                    <div class="owl-carousel content-slider" style="width:550px;height:400px;border:10px solid rgba(0,0,0,0.3)">
								<div class="overlay-container overlay-visible">
                                                                    <img src="../images/workshop/6.JPG"  style="width:550px;height: 380px;" alt="">
									<div class="overlay-bottom hidden-xs">
										<div class="text">
											<h3 class="title">Bigdata Hadoop</h3>
											Let the undisciplined thinking of Data get replaces by awareness; let them work as we want. We have come with Bigdata Hadoop Workshop based on 100% Practical Session.
										</div>
									</div>
									<a href="../images/workshop/6.JPG" class="popup-img overlay-link" title="image title"><i class="icon-plus-1"></i></a>
								</div>
<!--								<div class="overlay-container overlay-visible">
                                                                    <img src="../images/workshop/2.JPG"  style="width:550px;height: 380px;" alt="">
									<div class="overlay-bottom hidden-xs">
										<div class="text">
											<h3 class="title">Bigdata Hadoop</h3>
											Let the undisciplined thinking of Data get replaces by awareness; let them work as we want. We have come with Bigdata Hadoop Workshop based on 100% Practical Session.
										</div>
									</div>
									<a href="../images/workshop/2.JPG" class="popup-img overlay-link" title="image title"><i class="icon-plus-1"></i></a>
								</div>
								<div class="overlay-container overlay-visible">
                                                                    <img src="../images/workshop/3.JPG"  style="width:550px;height: 380px;" alt="">
									<div class="overlay-bottom hidden-xs">
										<div class="text">
											<h3 class="title">Bigdata Hadoop</h3>
											Let the undisciplined thinking of Data get replaces by awareness; let them work as we want. We have come with Bigdata Hadoop Workshop based on 100% Practical Session.
										</div>
									</div>
									<a href="../images/workshop/3.JPG" class="popup-img overlay-link" title="image title"><i class="icon-plus-1"></i></a>
								</div>-->
							</div>
							<br>
                                </div>
                                <div class="col-md-3">
                                    <div  style="" class="service-box ph-20 feature-box  object-non-visible animated object-visible fadeInDownSmall" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
                                        <center> <span class="icon circle" style="background: rgba(0,0,0,0.9)"><i class="fa fa-umbrella" style="color:orange"></i></span>
                                       
                                       <h3 class="heading-font">Course Highlights</h3></center>
                                                 
                                                 <ul class="list-icons" id="clist">
                                                     <li><i class="icon-check pr-10"></i> Extensive Classroom &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Training</li>
                                                        <li><i class="icon-check pr-10"></i> Live Project</li>
                                                        <li><i class="icon-check pr-10"></i> Online Materials</li>
                                                        <li><i class="icon-check pr-10"></i> Complimentary Course</li>
                                                        <li><i class="icon-check pr-10"></i> 24x7 Online Access</li>
                                                        
                                   </ul>
                                        <p>&nbsp;</p>
                                       
                                    </div>
                                </div>
                                 <div class="col-md-3">
                                    <div  style="" class="service-box ph-20 feature-box  object-non-visible animated object-visible fadeInDownSmall" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
                                        <center> <span class="icon circle" style="background: rgba(0,0,0,0.9)"><i class="fa fa-credit-card" style="color:orange"></i></span>
                                            <h3 class="heading-font">Attractions</h3></center>
                                                
                                            <ul class="list-icons" id="clist">
                                                <li><i class="icon-check pr-10"></i> Free tutorials access</li>
                                                <li><i class="icon-check pr-10"></i> Certificate</li>
                                                <li><i class="icon-check pr-10"></i> Interviews Questions</li>
                                                <li><i class="icon-check pr-10"></i>  On Spot Cash Prizes </li>
                                                    
                                            </ul><p>&nbsp;</p><p>&nbsp;</p><br>
                                    </div>
                                </div>
                             
                            </div>
                        </div>	
                       
                        
			<!-- section end -->
                    
			<div class="clearfix"></div>
			
			<!-- ================ -->
			
			<!-- section end -->
                     
			
			<!-- section end -->

			<!-- section -->
			<!-- ================ -->
			 <section class="pv-30  padding-bottom-clear dark-translucent-bg parallax" id="row-testimonials"style="background:url('../images/bg/451.jpg'); box-shadow:inset 0 2px 7px rgba(0,0,0, 0.25);">
				
				<div class="space-bottom">
					
					<div class="owl-carousel content-slider">
						<div class="container">
							<div class="row">
								<div class="col-md-8 col-md-offset-2">
									<div class="testimonial text-center">
										<div class="testimonial-image">
                                                                                    <img src="../images/testimonial-1.png" alt="" title="" class="img-circle">
										</div>
										
										<div class="testimonial-body">
                                                                                    <p>&nbsp;</p>
												<p id="cpara">It was a nice learning experience with prwatech. The classes were well scheduled and managed.
Verma has good understanding of the topics taught and catered to our problems and doubts very patiently. The best thing about him was that he handled the situations accordingly, when needed a friend, he became one and also a teacher to always guide us.</p>
											
											<div class="testimonial-info-1">Anukanksha Garg</div>
											<div class="testimonial-info-2">B.TECH- CS</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="container">
							<div class="row">
								<div class="col-md-8 col-md-offset-2">
									<div class="testimonial text-center">
										<div class="testimonial-image">
											<img src="../images/testimonial-2.jpg" alt="" title="" class="img-circle">
										</div>
										
										<div class="testimonial-body">
											 <p>&nbsp;</p>
												<p id="cpara">Excellent course and instructor. I learnt a lot in a short period. Good value for money. Instructor took us through Advanced Hadoop Development in depth.</p>
											
											<div class="testimonial-info-1">Anupam Khamparia</div>
											<div class="testimonial-info-2">Consultant, Cognizant Technology Solutions, Bangalore</div>
										</div>
									</div>
								</div>
							</div>
						</div>
                                            <div class="container">
							<div class="row">
								<div class="col-md-8 col-md-offset-2">
									<div class="testimonial text-center">
										<div class="testimonial-image">
											<img src="../images/testimonial-3.jpg" alt="" title="" class="img-circle">
										</div>
										
										<div class="testimonial-body">
											<p>&nbsp;</p>
												<p id="cpara">Excellent course and instructor. I learnt a lot in a short period. Good value for money. Instructor took us through Advanced Hadoop Development in depth.</p>
											<div class="testimonial-info-1">Kumar Waibhav</div>
                                                                                        <div class="testimonial-info-2">Software Engineer, Cognizant Technology Solutions,<br> Philadelphia, Pennsylvania, USA</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					
				</div>
			</section>
			<!-- section end -->

			<!-- section start -->
			<!-- ================ -->
                        
			                   
			<!-- section end -->
			<!-- footer start (Add "dark" class to #footer in order to enable dark footer) -->
			<!-- ================ -->
			<?php include '../includes/footer.php'; ?>
			<!-- footer end -->
			
		</div>
		<!-- page-wrapper end -->

		<?php include '../includes/jslinks.php';?>
                <div class="modal fade" id="demoVideoPopUp" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header ">
                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                <h4 class="modal-title" id="myModalLabel">Demo Session at Prwatech</h4>
                            </div>
                            <div class="modal-body">
                               <iframe width="570" height="350" src="https://www.youtube.com/embed/xA5TAalZt40" frameborder="0" allowfullscreen></iframe> 
                              
                            </div>
                            <div class="modal-footer">
                                
                            </div>
                        </div>
                    </div>
                </div>
		<!-- Color Switcher End -->
               <?php include '../includes/quick-query.php';?>
               <?php include '../includes/UserSignup.php';?>
	</body>


</html>
