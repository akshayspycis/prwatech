<?php include '../includes/session.php'; ?>
<!DOCTYPE html>
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
	<!--<![endif]-->
        <head>
            <meta charset="utf-8">
            <title>Python Training Courses Institute In Bangalore BTM & Pune</title>
            <meta name="description" content="We have best Python training institutes in Bangalore & Pune with expert instructors for python online courses and training classes, Contact us for demo classes." />
            <meta property="og:type" content="article" />
            <meta property="og:title" content="Python Online Course" />
            <meta property="og:url" content="http://prwatech.in/python-online-course/" />
            <meta property="og:site_name" content="BigData &amp; HADOOP" />
                <meta name="google-site-verification" content="fSzBVSEUMu0l5lnD0qBVGv5F_16zaI6xiUZFMm-iMqQ" />  
                <?php include '../includes/csslinks.php'; ?>
        </head>

	<!-- body classes:  -->
	<!-- "boxed": boxed layout mode e.g. <body class="boxed"> -->
	<!-- "pattern-1 ... pattern-9": background patterns for boxed layout mode e.g. <body class="boxed pattern-1"> -->
	<!-- "transparent-header": makes the header transparent and pulls the banner to top -->
	<body class="no-trans front-page transparent-header ">
            <!-- scrollToTop -->
		<!-- ================ -->
		<div class="scrollToTop circle"><i class="icon-up-open-big"></i></div>
		
		<!-- page wrapper start -->
		<!-- ================ -->
		<div class="page-wrapper">
		
			<!-- header-container start -->
			<?php include '../includes/header.php' ;  ?>
			<!-- header-container end -->
		
			<!-- banner start -->
			<!-- ================ -->
			<div class="banner dark-translucent-bg" style="background-image:url('../images/page-course-banner-7.jpg'); background-position: 50% 27%;">
				<!-- breadcrumb start -->
				<!-- ================ -->
				<div class="breadcrumb-container object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
					<div class="container">
						<ol class="breadcrumb">
							<li><i class="fa fa-home pr-10"></i><a class="link-dark" href="index.html">Home</a></li>
							<li class="active">All Courses</li>
							<li class="active">Python Training</li>
						</ol>
					</div>
				</div>
				<!-- breadcrumb end -->
				<div class="container">
					
				</div>
			</div>
			<!-- banner end -->
			
			<div id="page-start"></div>

			<!-- section start -->
			<!-- ================ -->
			<section class="light-gray-bg pv-30 clearfix object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100" id="cdrow1">
				<div class="container">
					<div class="row">
						<div class="col-md-8">
							 <h1 class="text-center heading-font" style="text-transform:none;"> <strong>Python </strong> training Institute Pune & Bangalore </h1>
                                                  
							<div class="separator"></div>
                                                        <p class="large text-justify heading-font">Python training courses at PrwaTech will provide you in-depth knowledge in Python by industry expert trainers in Pune & Bangalore who will help you to boost your knowledge in this domain. Python is one of the leading, powerful and flexible open-source languages that is easy to use, easy to learn and has prevailing libraries for data analysis and manipulation. For years, Python is being used in scientific-computing and highly quantitative fields like oil and gas, finance, physics and signal processing. We also have python training institute in pune and <span id="highlighter">Best Python Training Institute In Bangalore </span>.
                                                            <br>
<span id="highlighter">The best python classes in Bangalore</span> training program cover both basic and advanced concepts of <span id="highlighter">Python training </span>such as sequence and file operations in python, writing python scripts, Web Scraping, Machine Learning in Python, Map Reduce in Python, Python UDF for Pig and Hive and Hadoop Streaming. Participants will also undergo essential and widely used packages like pandas, pydoop, numpy scipy, scikit etc. For best python training in Bangalore & pune enroll today in our training course.
.</p>
						</div>
						<div class="col-md-4">
                                                    <img src="../images/por-7.png"/>
                                                </div>
						
					</div>
                                    <p>&nbsp;</p>
                                    <div class="separator"></div>
                                    <h3 class="text-center heading-font object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100" style="text-transform:none;">Contact Us <strong>+91 8147111254 </strong></h3>
                                    <div class="separator"></div>
                                    <p>&nbsp;</p>
                                </div>
                                <div class="container">
                                <div class="row" id="batch_details">
                                    <div class="col-md-3">
                                        <div class="col-md-3">
                                            <div class="row">
                                                <p id ="date-time">21 <br><span id="month">NOV</span></p>
                                            </div>
                                       </div>
                                        <div class="col-md-9" id="upcomming-batch">
                                            <div class="row">
                                                <div class="col-md-12"><p id="dt-content"><b> Mon-Wed (3 Days) <br><small>&nbsp; <br> Rs. 14000/- <a href="#" class="" data-toggle="modal" data-target="#demoPopUp">Enroll Now</a></small></b></p></div>
                                                
                                            </div>
                                       </div>
                                    </div>
                                  <div class="col-md-3">
                                        <div class="col-md-3">
                                            <div class="row">
                                                <p id ="date-time">24 <br><span id="month">NOV</span></p>
                                            </div>
                                       </div>
                                        <div class="col-md-9" id="upcomming-batch">
                                            <div class="row">
                                                <div class="col-md-12"><p id="dt-content"><b> Mon-Wed (3 Days) <br><small>&nbsp; <br> Rs. 14000/- <a href="#" class="" data-toggle="modal" data-target="#demoPopUp">Enroll Now</a></small></b></p></div>
                                                
                                            </div>
                                       </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="col-md-3">
                                            <div class="row">
                                                <p id ="date-time">27 <br><span id="month">NOV</span></p>
                                            </div>
                                       </div>
                                        <div class="col-md-9" id="upcomming-batch">
                                            <div class="row">
                                                <div class="col-md-12"><p id="dt-content"><b> Mon-Wed (3 Days) <br><small>&nbsp; <br> Rs. 14000/- <a href="#" class="" data-toggle="modal" data-target="#demoPopUp">Enroll Now</a></small></b></p></div>
                                                
                                            </div>
                                       </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="col-md-3">
                                            <div class="row">
                                                <p id ="date-time">30 <br><span id="month">NOV</span></p>
                                            </div>
                                       </div>
                                        <div class="col-md-9" id="upcomming-batch">
                                            <div class="row">
                                                <div class="col-md-12"><p id="dt-content"><b> Mon-Wed (3 Days) <br><small>&nbsp; <br> Rs. 14000/- <a href="#" class="" data-toggle="modal" data-target="#demoPopUp">Enroll Now</a></small></b></p></div>
                                                
                                            </div>
                                       </div>
                                    </div>
                                </div>
                            </div>
			</section>
                          <section id="coursed-row2" class="object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
                            <div class="container">
                                
                                 <h3 class="text-center heading-font object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100" style="text-transform:none;"> <strong>Course</strong> Details</h3>
                                 <div id="vseparator" class="object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
                                     <div class="vhr"></div>
                                </div>
                                <div class="row col-md-9" >
                               <!-- tabs start -->
                                <!-- ================ -->
                                <!-- Nav tabs -->
                                <ul class="nav nav-tabs style-3" id="tab-course" role="tablist">
                                    <li class="active"><a href="#h3tab1" role="tab" data-toggle="tab">Overview</a></li>
                                    <li><a href="#h3tab2" role="tab" data-toggle="tab">Course Curriculum</a></li>
                                    <li><a href="#h3tab3" role="tab" data-toggle="tab">FAQ's</a></li>
                                    <li><a href="#h3tab4" role="tab" data-toggle="tab">Download Course Outline</a></li>
                                    <li><a href="#h3tab5" role="tab" data-toggle="tab">Reviews</a></li>
                                   
                                </ul>
                                <!-- Tab panes -->
                                <div class="tab-content">
                                    <div class="tab-pane in active " id="h3tab1">
                                        <div class="row">
                                            <div class="col-md-12 object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
                                               
                                                <h2 class="heading-font"> Python training courses objectives: </h2>
                                                <p id="cpara">
                                                <br>
•	To throw light on programming and enjoy python scripting.<br>
•	Learn python lexical features as well as syntax.<br>
•	Discover core python structures and flow control.<br>
•	Create and execute python functions.<br>
•	Work with python execution environment.<br>
•	Explore python library.<br>
•	Handle file-system using python scripts.<br>
•	Learn object oriented programming.<br>
•	Understand and handle errors and exceptions.<br>
•	Learn python document programming.<br>
•	Learn python web programming.<br>
•	Manage NoSQL and SQL databases with python.<br>
	<br>
        </p>
<h2 class="heading-font"> Why to go for Python <span id="highlighter">Online </span>Training ? </h2><br>
                                    <p id="cpara">
•	Our Python training classes <span id="highlighter">in Bangalore & Pune </span>will help those who want to excel in the field of information technology.<br>

•	Our training tutorials for <span id="highlighter">Python Online Training In Bangalore </span>will help you move forward in your career by setting you apart from your competitors.<br>
•	There is a great demand for Python in the today’s job market and the participants can easily learn theoretical as well as practical knowledge at our python training institute in Pune.
<br>
<br>
Anyone who’s interested in learning Programming Languages and searching for <span id="highlighter">Python Course In Bangalore Btm, python training in Pune & Bangalore </span>and anyone interested in learning IT courses and looking for best python training institutes in Bangalore <span id="highlighter">BTM </span>must enroll in our Python training courses.<br>
                                    </p>
                                    <p id="cpara"><h2 class="heading-font"> Prerequisite for Python <span id="highlighter">Online </span>Courses: </h2><br>
•	<span id="highlighter">Python Online Courses In Bangalore </span>available for  basic understanding of web design or fundamental programming skills<br>
</p>
<h2 class="heading-font">Why Python over other languages ? </h2><br>
<p id="cpara">
Easy to learn syntax of python <span id="highlighter">course </span>helps new programmers to code efficiently. By decreasing and simplifying the debugging process, productivity from the programmers’ end in improved. Effective reusability of code is integrated in the python language allowing even small programmer to perform huge tasks efficiently. Python is gaining momentum over java, Objective C etc, owing to its wide fields of application.<br>
</p>
<h2 class="heading-font">Python market and future </h2><br>
<p id="cpara">
Python is a multi-platform language used for desktop application development. Python provides a number of choices for web developers in the form of frameworks such as Pyramid and Django. It supports standard internet protocols such as html, xml etc
If you are looking for <span id="highlighter">Best </span>Python training in Bangalore <span id="highlighter">BTM </span>and interested in learning this powerful scripting language from <span id="highlighter">Best Python Institute In Bangalore BTM </span>then enroll yourself for Python training course and get trained with certified experts.
</p>

                                                    
                                            </div>
                                            
                                        </div>
                                            
                                    </div>
                                    <div class="tab-pane" id="h3tab2">
                                          <div class="row">
                                            <div class="col-md-12 ">
                                                  <div class="panel-group collapse-style-2" id="accordion-2">
								<div class="panel panel-default">
									<div class="panel-heading">
										<h4 class="panel-title">
											<a data-toggle="collapse" data-parent="#accordion-2" href="#collapseOne-2">
												<i class="fa fa-arrow-right pr-10"></i>Module 1
											</a>
										</h4>
									</div>
                                                                    <div id="collapseOne-2" class="panel-collapse collapse in">
                                                                        <div class="panel-body">
                                                                                   
                                                                                    <ul class="list-icons" id="clist">
                                                                                        <li><i class="icon-check pr-10"></i>Introduction </li>
                                                                                        <li><i class="icon-check pr-10"></i>Working with data</li>
                                                                                        <li><i class="icon-check pr-10"></i>module</li>
                                                                                        <li><i class="icon-check pr-10"></i>Object Oriented programming</li>
                                                                                        <li><i class="icon-check pr-10"></i>File handling</li>
                                                                                        <li><i class="icon-check pr-10"></i>Regular Expressions</li>
                                                                                        <li><i class="icon-check pr-10"></i>Iterators and Generators</li>
                                                                                        <li><i class="icon-check pr-10"></i>Unit testing</li>
                                                                                     </ul>
                                                                        </div>
                                                                    </div>
								</div>
								<div class="panel panel-default">
									<div class="panel-heading">
										<h4 class="panel-title">
											<a data-toggle="collapse" data-parent="#accordion-2" href="#collapseTwo-2" class="collapsed">
												<i class="fa fa-arrow-right pr-10"></i>Module 2
											</a>
										</h4>
									</div>
									<div id="collapseTwo-2" class="panel-collapse collapse">
										<div class="panel-body">
                                                                                    <p id="cpara">In this module,you will understand what is python,how to install and use it including with some basic concept of python to understandit more.</p>
                                                                                    <ul class="list-icons" id="clist">
                                                                                        <li><i class="icon-check pr-10"></i> Installing Python (By default installed in linux system)</li>
                                                                                        <li><i class="icon-check pr-10"></i>formal and natural language </li>
                                                                                        <li><i class="icon-check pr-10"></i> Python Interpreter</li>
                                                                                        <li><i class="icon-check pr-10"></i>Python Script</li>
                                                                                        <li><i class="icon-check pr-10"></i>Assignment</li>
                                                                                        <li><i class="icon-check pr-10"></i>Number</li>
                                                                                        <li><i class="icon-check pr-10"></i>Strings</li>
                                                                                        <li><i class="icon-check pr-10"></i>Function</li>
                                                                                        <li><i class="icon-check pr-10"></i>Condition Expressions</li>
                                                                                        <li><i class="icon-check pr-10"></i>Errors and Debuggers (Syntax errors, Runtime errors,Semantic errors)</li>
                                                                                    </ul> 
                                                                                    <p id ="cpara">Working With data</p>
                                                                                     <ul class="list-icons" id="clist">
                                                                                        <li><i class="icon-check pr-10"></i> This module is for descrbing different data type and function in python and how to use them .</li>
                                                                                        <li><i class="icon-check pr-10"></i>Dictionaries (Dict)</li>
                                                                                        <li><i class="icon-check pr-10"></i> Lists</li>
                                                                                        <li><i class="icon-check pr-10"></i>Tuples</li>
                                                                                        <li><i class="icon-check pr-10"></i>Sets</li>
                                                                                        <li><i class="icon-check pr-10"></i>Strings</li>
                                                                                        <li><i class="icon-check pr-10"></i>List â€“ dictionaries C omprehensions (List of list, list of dict, dict of list,dictof dict etc.)</li>
                                                                                        <li><i class="icon-check pr-10"></i>Built in functions (str, type,dir)</li>
                                                                                        <li><i class="icon-check pr-10"></i>Lambda Function  </li>
                                                                                       
                                                                                    </ul> 
                                                                                </div>
									</div>
								</div>
                                                      <div class="panel panel-default">
                                                          <div class="panel-heading">
                                                              <h4 class="panel-title">
                                                                  <a data-toggle="collapse" data-parent="#accordion-2" href="#collapseThree-2" class="collapsed">
                                                                      <i class="fa fa-arrow-right pr-10"></i>Module 3 
                                                                  </a>
                                                              </h4>
                                                          </div>
                                                          <div id="collapseThree-2" class="panel-collapse collapse">
                                                              <div class="panel-body">
                                                                  <p id="cpara">This module contains standard libraries which are mostly used in python and third party tools .
Standard Libraries (Urllib , re ,os, sys, strings, math, random etc)
Installing third party tool (Virtual env, Beatiful Soup etc)</p>
                                                              </div>
                                                          </div>
                                                      </div>
                                                      <div class="panel panel-default">
                                                          <div class="panel-heading">
                                                              <h4 class="panel-title">
                                                                  <a data-toggle="collapse" data-parent="#accordion-2" href="#collapseFour-2" class="collapsed">
                                                                      <i class="fa fa-arrow-right pr-10"></i>Module 4
                                                                  </a>
                                                              </h4>
                                                          </div>
                                                          <div id="collapseFour-2" class="panel-collapse collapse">
                                                              <div class="panel-body">
                                                                  <h3 class="heading-font">Object Oriented Programming</h3>
                                                                  <p id="cpara">This module deals with object oriented concepts of python programming. state</p>
                                                                   <ul class="list-icons" id="clist">
                                                                    <li><i class="icon-check pr-10"></i> classes and object</li>
                                                                    <li><i class="icon-check pr-10"></i> Inheritance </li>
                                                                    <li><i class="icon-check pr-10"></i> special class methods </li>
                                                                    <li><i class="icon-check pr-10"></i> Error and exception handling</li>
                                                                
                                                            </ul>  
                                                              </div>
                                                          </div>
                                                      </div>
                                                      <div class="panel panel-default">
                                                          <div class="panel-heading">
                                                              <h4 class="panel-title">
                                                                  <a data-toggle="collapse" data-parent="#accordion-2" href="#collapseFive-2" class="collapsed">
                                                                      <i class="fa fa-arrow-right pr-10"></i>Module 5
                                                                  </a>
                                                              </h4>
                                                          </div>
                                                          <div id="collapseFive-2" class="panel-collapse collapse">
                                                              <div class="panel-body">
                                                                       <h3 class="heading-font">File handling</h3>
                                                                  <p id="cpara">This modeaul deal file handling in python programming such that what are different use cases we can apply on files.</p>
                                                                  
                                                                  <ul class="list-icons" id="clist">
                                                                      <li><i class="icon-check pr-10"></i>Reading file</li>
                                                                      <li><i class="icon-check pr-10"></i>Opening File </li>
                                                                      <li><i class="icon-check pr-10"></i>Closeing file  </li>
                                                                      <li><i class="icon-check pr-10"></i> Error and exception handling</li>
                                                                     
                                                                  </ul>
                                                              </div>
                                                          </div>
                                                      </div>
                                                      <div class="panel panel-default">
                                                          <div class="panel-heading">
                                                              <h4 class="panel-title">
                                                                  <a data-toggle="collapse" data-parent="#accordion-2" href="#collapseSix-2" class="collapsed">
                                                                      <i class="fa fa-arrow-right pr-10"></i>Module 6
                                                                  </a>
                                                              </h4>
                                                          </div>
                                                          <div id="collapseSix-2" class="panel-collapse collapse">
                                                              <div class="panel-body">
                                                                      <h3 class="heading-font">Regular Expressions </h3>
                                                                  <p id="cpara">Regular expressions are a powerful and standardized way of searching replacing , and parsing text from files/string etc. if you`ve used regular expression in other lan-guages the syntax will be very familiar, and you get by just reading the summary of the  re module </p>
                                                                  
                                                                  <ul class="list-icons" id="clist">
                                                                      <li><i class="icon-check pr-10"></i>pattern and rules for them</li>
                                                                      <li><i class="icon-check pr-10"></i>sub function </li>
                                                                      <li><i class="icon-check pr-10"></i>search function </li>
                                                                      <li><i class="icon-check pr-10"></i> compile function </li>
                                                                     
                                                                        
                                                                  </ul>  
                                                              </div>
                                                          </div>
                                                      </div>
                                                       <div class="panel panel-default">
                                                          <div class="panel-heading">
                                                              <h4 class="panel-title">
                                                                  <a data-toggle="collapse" data-parent="#accordion-2" href="#collapseSeven-2" class="collapsed">
                                                                      <i class="fa fa-arrow-right pr-10"></i>Module 7
                                                                  </a>
                                                              </h4>
                                                          </div>
                                                          <div id="collapseSeven-2" class="panel-collapse collapse">
                                                              <div class="panel-body">
                                                                  <ul class="list-icons" id="clist">
                                                                         <h3 class="heading-font">Iterators and Generator  </h3>
                                                                  <p id="cpara">These are objects which use some logic/methods  to get 
next value of sequence .</p>
                                                                  
                                                                      <li><i class="icon-check pr-10"></i> Terators</li>
                                                                      <li><i class="icon-check pr-10"></i> General expressions</li>
                                                                      <li><i class="icon-check pr-10"></i> Itertools</li>
                                                                     
                                                                  </ul> 
                                                              </div>
                                                          </div>
                                                      </div>
                                                       <div class="panel panel-default">
                                                          <div class="panel-heading">
                                                              <h4 class="panel-title">
                                                                  <a data-toggle="collapse" data-parent="#accordion-2" href="#collapseEight-2" class="collapsed">
                                                                      <i class="fa fa-arrow-right pr-10"></i>Module 8
                                                                  </a>
                                                              </h4>
                                                          </div>
                                                          <div id="collapseEight-2" class="panel-collapse collapse">
                                                              <div class="panel-body">
                                                                  <h3 class="heading-font">Unit testing  </h3>
                                                                  <p id="cpara">This is very important for sale- testing  purpose of your project/program. this is useful for checking expected result , failed and success  scenarios.</p>
                                                                  <ul class="list-icons" id="clist">
                                                                      <li><i class="icon-check pr-10"></i> Purpose and use of it </li>
                                                                      <li><i class="icon-check pr-10"></i> How to write test case </li>
                                                                      <li><i class="icon-check pr-10"></i> test case for success , failures</li>
                                                                      <li><i class="icon-check pr-10"></i> Test case for expected  result </li>
                                                                     
                                                                        
                                                                  </ul>
                                                              </div>
                                                          </div>
                                                      </div>
                                                    </div>
                                                
                                                </div>

                                        </div>
                                       
                                    </div>
                                          <div class="tab-pane" id="h3tab3">
                                        <div class="row">
                                            <div class="col-md-12 ">
                                                   	<div class="panel-group collapse-style-3" id="accordion-3">
								<div class="panel panel-default">
									<div class="panel-heading">
										<h4 class="panel-title">
											<a data-toggle="collapse" data-parent="#accordion-3" href="#collapseOne-3">
												1.	What are the benefits of Python course?
											</a>
										</h4>
									</div>
									<div id="collapseOne-3" class="panel-collapse collapse in">
										<div class="panel-body">
                                                                                    <p id="cpara" class="text-justify">
                                                                                        High readability, powerful library and clear syntax are the important features of this high level language. It is now considered one of the must- know languages. It is an open- source language that is powerful and easy to learn. Python is used in science, signal processing, scientific-computing, finance and highly quantitative fields. Python is a demand of every IT company. It is an easy to learn syntax language. It is beneficial for new programmers and for all those who switch to Python course. New joining can use the codes efficiently and decrease the debugging process. Reusability of codes is integrated in python that allows performing the huge tasks even small programs efficiently. Python is gaining momentum over languages such as java, C, C++ etc.
                                                                                    </p>
                                                                                   
										</div>
									</div>
								</div>
								<div class="panel panel-default">
									<div class="panel-heading">
										<h4 class="panel-title">
											<a data-toggle="collapse" data-parent="#accordion-3" href="#collapseTwo-3" class="collapsed">
												2.	Why should I join PrwaTech?
											</a>
										</h4>
									</div>
									<div id="collapseTwo-3" class="panel-collapse collapse">
										<div class="panel-body">
                                                                                    <p id="cpara" class="text-justify">
                                                                                        Understanding the accurate problem is far more important than actually solving it. PrwaTech highly skilled professional team of software engineers and solution providers strive to help individual by proving the accurate technology. Our highly skilled team works together to understand the problem and provide the best effective business offering in any circumstances. We at PrwaTech help and support the upcoming business entrepreneurs. We provide the most optimized solution for the business not only on a large scale but also at the small scale. 
                                                                                    </p>
                                                                                    
										</div>
									</div>
								</div>
								<div class="panel panel-default">
									<div class="panel-heading">
										<h4 class="panel-title">
											<a data-toggle="collapse" data-parent="#accordion-3" href="#collapseThree-3" class="collapsed">
												3.	What are the pre-requisites of this course?
											</a>
										</h4>
									</div>
									<div id="collapseThree-3" class="panel-collapse collapse">
										<div class="panel-body">
                                                                                    <p id="cpara" class="text-justify">
                                                                                          If you are not from programming background you can still join our Python programming course. PrwaTech has many well designed courses like Java, Dotnet, C, C++ and linux. You can join these programs. These programs not only help you in Python programming training instead it will definitely help you to boost your career.
                                                                                    </p>
                                                                                    
										</div>
									</div>
								</div>
                                                            <div class="panel panel-default">
									<div class="panel-heading">
										<h4 class="panel-title">
											<a data-toggle="collapse" data-parent="#accordion-3" href="#collapseFour-3" class="collapsed">
												4.	Who will be my training instructor?
											</a>
										</h4>
									</div>
									<div id="collapseFour-3" class="panel-collapse collapse">
										<div class="panel-body">
                                                                                    <p id="cpara" class="text-justify">
                                                                                         We have well trained faculty of members with more than 5 years of experience in the same. PrwaTech highly skilled professional team of software engineers and solution providers strive to help individual by providing the accurate technology. Our highly skilled team works together to understand the problem and provide the best effective solution. Before considering any assignment PrwaTech thoroughly screen the professional. Verify all the documents, educational qualifications and work experience.
                                                                                    </p>
                                                                                    
										</div>
									</div>
								</div>
                                                            <div class="panel panel-default">
									<div class="panel-heading">
										<h4 class="panel-title">
											<a data-toggle="collapse" data-parent="#accordion-3" href="#collapseFive-3" class="collapsed">
												5.	What is the placement criterion?
											</a>
										</h4>
									</div>
									<div id="collapseFive-3" class="panel-collapse collapse">
										<div class="panel-body">
                                                                                    <p id="cpara" class="text-justify">
                                                                                        PrwaTech provides 100% placement assistance. We have 95% of placement record in reputed companies and have organized 2000+ interviews in HCL, Infosys, IBM, Accenture etc. We have trained 2000+ students. Our professionals will provide you with several questions along with solutions that you might face in your interview.
                                                                                    </p>
                                                                                    
										</div>
									</div>
								</div>
                                                       
							</div>
                                                
                                                </div>

                                        </div>
                                       
                                    </div>
                                    <div class="tab-pane" id="h3tab4">
                                        <div class="row object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
                                            <div class="col-md-4">
                                                <p id="cpara" class="text-default"><a href="">Click Here to Download</a></p>
                                                
                                            </div>
                                            <div class="col-md-8">
                                                 <h4 class="title text-default" class="heading-font">Bigdata and Hadoop Services</h4>
                                                <ul class="list-icons">
                                                    <li><i class="icon-check pr-10"></i> PowerPoint Presentation covering all classes</li>
                                                    <li><i class="icon-check pr-10"></i> Recorded Videos Sessions On Bigdata and Hadoop with LMS Access.(lifetime support)</li>
                                                    <li><i class="icon-check pr-10"></i> Quiz , Assignment & POC.</li>
                                                    <li><i class="icon-check pr-10"></i> On Demand Online Support .</li>
                                                    <li><i class="icon-check pr-10"></i> Discussion Forum.</li>
                                                    <li><i class="icon-check pr-10"></i> Material
                                                        <ul class="list-icons" style="margin-left:50px;">
                                                            <li>a. Sample Question papers of Cloudera Certification.</li>
                                                            <li>b. Technical Notes & Study Material.</li>
                                                        </ul>
                                                    
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        
                                    </div>
                                    <div class="tab-pane" id="h3tab5">
                                        <div class="row object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
                                            <div class="col-md-6 ">
                                                <div class="pv-20 ph-20 feature-box-2 boxed shadow object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
                                                    <div class="col-md-3 testimonial-image"><img src="../images/testimonial-2.jpg" /></div> 
                                                    <div class="body">
                                                        <h4 class="title">Anupam Khamparia 
                                                            <br>
                                                            <span style="font-size:11px;">Consultant, Cognizant Technology Solutions, Bangalore</span>
                                                        </h4>
                                                        <p class ="text-justify">“Excellent course and instructor. I learnt a lot in a short period. Good value for money. Instructor took us through Advanced Hadoop Development in depth.”</p>
                                                       
                                                    </div>
                                                </div>
                                            </div>
                                          <div class="col-md-6 ">
                                                <div class="pv-20 ph-20 feature-box-2 boxed shadow object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
                                                    <div class="col-md-3 testimonial-image"><img src="../images/testimonial-1.png" /></div> 
                                                    <div class="body">
                                                        <h4 class="title">Anukanksha Garg
                                                          <br>
                                                            <span style="font-size:11px;">B.Tech. CSE</span>
                                                        </h4>
                                                        <p class ="text-justify">It was a nice learning experience with prwatech. The classes were well scheduled and managed.
Verma has good understanding of the topics taught and catered to our problems and doubts very patiently. The best thing about him was that he handled the situations accordingly, when needed a friend, he became one and also a teacher to always guide us.</p>
                                                       
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6 ">
                                                <div class="pv-20 ph-20 feature-box-2 boxed shadow object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
                                                    <div class="col-md-3 testimonial-image"><img src="../images/testimonial-4.PNG" /></div> 
                                                    <div class="body">
                                                        <h4 class="title">Varun Shivashanmugum
                                                        
                                                        <br>
                                                            <span style="font-size:11px;">Associate Consultant, ITC Infotech Ltd</span>
                                                        </h4>
                                                        
                                                        <p class ="text-justify">“Faculty is good,Verma takes keen interest and personnel care in improvising skills in students. And most importantly,Verma will be available and clears doubts at any time apart from class hours. And he always keeps boosting and trying to increase confidence in his students which adds extra attribute to him and organization as well. and organization as well.</p>
                                                       
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6 ">
                                                <div class="pv-20 ph-20 feature-box-2 boxed shadow object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
                                                    <div class="col-md-3 testimonial-image"><img src="../images/testimonial-5.jpg" /></div> 
                                                    <div class="body">
                                                        <h4 class="title">Mayank Srivastava <br>
                                                        <span style="font-size:11px;">Hadoop Developer, L&T, Bangalore</span>
                                                        </h4>
                                                        <p class ="text-justify">“Really good course content and labs, patient enthusiastic instructor. Good instructor, with in depth skills…Very relevant practicals allowed me to put theory into practice.”</p>
                                                       
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                                 <div class="col-md-3" style="position:relative;top:-10px;">
                                     <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center" >
                                         <div class="overlay-container">
                                             <img src="../images/icon/curse-d1.png" alt="">
                                         </div>
                                         <div class="body">
                                          <h3 class="heading-font">Rs. 14,000 + Tax </h3>
                                             <span class="small-font">per 2 weeks</span>
                                             <div class="separator"></div>
                                             <p class="heading-font"></p>
                                             <table class="table table-striped ">
                                                 <tbody>
                                                     <tr><th><center>35 Hours</center></th></tr>
                                                 <tr><th><center>15 Seats</center></th></tr>
                                                 <tr><th><center>Course Badge</center></th></tr>
                                                 <tr><th><center>Course Certificate</center></th></tr>
                                                 <tr><th><center> <a  onclick="openNav()" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Enroll Now<i class="fa fa-arrow-right pl-10"></i></a></center></th></tr>
                                                 </tbody>
                                             </table>
                                            <?php include '../includes/demo-popup.php';?>
                                           
                                         </div>
                                     </div>
                                      <h3 class="heading-font text-center" style="background:#4c4c4c;color:white;padding-top:10px;padding-bottom:10px">Suggested Courses</h3> 
                                      <div class="owl-carousel content-slider">
                                          <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
                                              <div class="overlay-container">
                                                  <img src="../images/por-1.png" alt="">
                                                  <div class="overlay-to-top">
                                                      <p class="small margin-clear">&nbsp;</p>
                                                  </div>
                                              </div>
                                              <div class="body">
                                                  <h3 class="heading-font">Big Data Hadoop Training</h3>
                                                  <div class="separator"></div>
                                                  <p class="heading-font">Advanced Program for Top-notch Developers </p>
                                                  <a href="big-data-hadoop-training.php"  target="_blank" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read More<i class="fa fa-arrow-right pl-10"></i></a>												
                                              </div>
                                          </div>
                                          <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
                                              <div class="overlay-container">
                                                  <img src="../images/por-2.png" alt="">
                                                  <div class="overlay-to-top">
                                                      <p class="small margin-clear">&nbsp;</p>
                                                  </div>
                                              </div>
                                              <div class="body">
                                                  <h3 class="heading-font">Hadoop Admin Training</h3>
                                                  <div class="separator"></div>
                                                  <p class="heading-font">Advanced Program for Top-notch Developers </p>
                                                  <a href="hadoop-admin.php" target="_blank" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read More<i class="fa fa-arrow-right pl-10"></i></a>												
                                              </div>
                                          </div>
                                          <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
                                              <div class="overlay-container">
                                                  <img src="../images/por-3.png" alt="">
                                                  <div class="overlay-to-top">
                                                      <p class="small margin-clear">&nbsp;</p>
                                                  </div>
                                              </div>
                                              <div class="body">
                                                  <h3 class="heading-font">Apache Spark Scala Training</h3>
                                                  <div class="separator"></div>
                                                  <p class="heading-font">Apache Spark Scala Combo Training </p>
                                                  <a href="apache-spark-scala-training.php" target="_blank" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read More<i class="fa fa-arrow-right pl-10"></i></a>												
                                              </div>
                                          </div>
                                          <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
                                              <div class="overlay-container">
                                                  <img src="../images/por-4.png" alt="">
                                                  <div class="overlay-to-top">
                                                      <p class="small margin-clear">&nbsp;</p>
                                                  </div>
                                              </div>
                                              <div class="body">
                                                  <h3 class="heading-font">Data Science Training</h3>
                                                  <div class="separator"></div>
                                                  <p class="heading-font">Find out the truth about what Data Science is. </p>
                                                  <a href="data-science-course-training-bangalore.php" target="_blank" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read More<i class="fa fa-arrow-right pl-10"></i></a>												
                                              </div>
                                          </div>
                                          <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
                                              <div class="overlay-container">
                                                  <img src="../images/por-5.png" alt="">
                                                  <div class="overlay-to-top">
                                                      <p class="small margin-clear">&nbsp;</p>
                                                  </div>
                                              </div>
                                              <div class="body">
                                                  <h3 class="heading-font">R Programming Training</h3>
                                                  <div class="separator"></div>
                                                  <p class="heading-font">Business Analytics With R Training</p>
                                                  <a href="r-programing.php" target="_blank" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read More<i class="fa fa-arrow-right pl-10"></i></a>												
                                              </div>
                                          </div>
                                          <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
                                              <div class="overlay-container">
                                                  <img src="../images/por-6.png" alt="">
                                                  <div class="overlay-to-top">
                                                      <p class="small margin-clear">&nbsp;</p>
                                                  </div>
                                              </div>
                                              <div class="body">
                                                  <h3 class="heading-font"> Tableau Training & Certification</h3>
                                                  <div class="separator"></div>
                                                  <p class="heading-font">Speed on concepts of data visualization</p>
                                                  <a href="tableau.php" target="_blank" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read More<i class="fa fa-arrow-right pl-10"></i></a>												
                                              </div>
                                          </div>
                                          <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
                                              <div class="overlay-container">
                                                  <img src="../images/por-7.png" alt="">
                                                  <div class="overlay-to-top">
                                                      <p class="small margin-clear">&nbsp;</p>
                                                  </div>
                                              </div>
                                              <div class="body">
                                                  <h3 class="heading-font">Python Programming</h3>
                                                  <div class="separator"></div>
                                                  <p class="heading-font">Advanced Concepts for Top-notch Developers  </p>
                                                  <a href="python.php" target="_blank" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read More<i class="fa fa-arrow-right pl-10"></i></a>												
                                              </div>
                                          </div>
                                      </div> 
                                      <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center" >
                                         <div class="body">
                                             <div itemscope itemtype="http://schema.org/Product"style="text-align: left">
                                                <span itemprop="name">Python Training</span><br>
                                                <div itemprop="aggregateRating" itemscope itemtype="http://schema.org/AggregateRating">
                                                    Average Rating: <span itemprop="ratingValue">4.7</span><br>
                                                    Votes: <span itemprop="ratingCount">867</span><br>
                                                    Reviews: <span itemprop="reviewCount">867</span>
                                                </div>
                                            </div>
                                         </div>
                                     </div>
                                 </div> 
                            </div> 
                           
                           
                            <p>&nbsp;</p>
                            
                        </section>
                        <!-- section end -->
                       <div class="clearfix"></div>
			
			<!-- ================ -->
			
			<!-- section end -->
                        <section class="pv-30 dark-bg padding-bottom-clear clearfix object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
					<br>
					<div class="container">
						<div class="row">
							<div class="col-md-3 ">
								<div class="ph-20 feature-box text-center object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
									<span class="icon default-bg circle"><i class="fa fa-institution"></i></span>
									<h3>Live classes</h3>
									<div class="separator clearfix"></div>
                                                                        <p class="heading-font ">Live online and interactive classes conducted by instructor</p>
									
								</div>
							</div>
							<div class="col-md-3 ">
								<div class="ph-20 feature-box text-center object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
									<span class="icon default-bg circle"><i class="fa fa-user"></i></span>
									<h3>Expert instructions</h3>
									<div class="separator clearfix"></div>
									<p class="heading-font">Learn from our Experts and get Real-Time Guidance</p>
									
								</div>
							</div>
							<div class="col-md-3 ">
								<div class="ph-20 feature-box text-center object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
									<span class="icon default-bg circle"><i class="icon-signal"></i></span>
									<h3>24 X 7 Support</h3>
									<div class="separator clearfix"></div>
									<p class="heading-font">Personalized Guidance from our 24X7 Support Team</p>
									
								</div>
							</div>
							<div class="col-md-3 ">
								<div class="ph-20 feature-box text-center object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
									<span class="icon default-bg circle"><i class="icon-calendar"></i></span>
									<h3>Flexible schedule</h3>
									<div class="separator clearfix"></div>
									<p class="heading-font">Reschedule your Batch/Class at Your Convenience</p>
									
								</div>
							</div>
						</div>
					</div>
					<br>
				</section>
			
			<!-- section end -->

			<!-- section -->
			<!-- ================ -->
			 <section class="pv-30   padding-bottom-clear dark-translucent-bg parallax padding-bottom-clear" style="background: url('../images/bg/451.jpg') 50% -9px; box-shadow: rgba(0, 0, 0, 0.247059) 0px 2px 7px inset;">
				
				<div class="space-bottom">
					
					<div class="owl-carousel content-slider">
						<div class="container">
							<div class="row">
								<div class="col-md-8 col-md-offset-2">
									<div class="testimonial text-center">
										<div class="testimonial-image">
                                                                                    <img src="../images/testimonial-1.png" alt="" title="" class="img-circle">
										</div>
										
										<div class="testimonial-body">
                                                                                    <p>&nbsp;</p>
												<p id="cpara">It was a nice learning experience with prwatech. The classes were well scheduled and managed.
Verma has good understanding of the topics taught and catered to our problems and doubts very patiently. The best thing about him was that he handled the situations accordingly, when needed a friend, he became one and also a teacher to always guide us.</p>
											
											<div class="testimonial-info-1">Anukanksha Garg</div>
											<div class="testimonial-info-2">B.TECH- CS</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="container">
							<div class="row">
								<div class="col-md-8 col-md-offset-2">
									<div class="testimonial text-center">
										<div class="testimonial-image">
											<img src="../images/testimonial-2.jpg" alt="" title="" class="img-circle">
										</div>
										
										<div class="testimonial-body">
											 <p>&nbsp;</p>
												<p id="cpara">Excellent course and instructor. I learnt a lot in a short period. Good value for money. Instructor took us through Advanced Hadoop Development in depth.</p>
											
											<div class="testimonial-info-1">Anupam Khamparia</div>
											<div class="testimonial-info-2">Consultant, Cognizant Technology Solutions, Bangalore</div>
										</div>
									</div>
								</div>
							</div>
						</div>
                                            <div class="container">
							<div class="row">
								<div class="col-md-8 col-md-offset-2">
									<div class="testimonial text-center">
										<div class="testimonial-image">
											<img src="../images/testimonial-3.jpg" alt="" title="" class="img-circle">
										</div>
										
										<div class="testimonial-body">
											<p>&nbsp;</p>
												<p id="cpara">Excellent course and instructor. I learnt a lot in a short period. Good value for money. Instructor took us through Advanced Hadoop Development in depth.</p>
											<div class="testimonial-info-1">Kumar Waibhav</div>
                                                                                        <div class="testimonial-info-2">Software Engineer, Cognizant Technology Solutions,<br> Philadelphia, Pennsylvania, USA</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					
				</div>
			</section>
			<!-- section end -->

			<!-- section start -->
			<!-- ================ -->
                        
			                   
			<!-- section end -->
			<!-- footer start (Add "dark" class to #footer in order to enable dark footer) -->
			<!-- ================ -->
			<?php include './../includes/footer.php'; ?>
			<!-- footer end -->
			
		</div>
		<!-- page-wrapper end -->

		<?php include './../includes/jslinks.php';?>
		<!-- Color Switcher End -->
                    <?php include './../includes/quick-query.php';?>
                 <?php include './../includes/UserSignup.php';?>
	</body>

        <script>
            $(document).ready(function(){
                onlad(7);
            });  
        </script>

</html>

