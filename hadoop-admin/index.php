<?php include '../includes/session.php'; ?>
<!DOCTYPE html>
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
	<!--<![endif]-->
<head>
		<meta charset="utf-8">
                <title>Hadoop Admin Training Institute </title>
                    <meta name="description" content="Prwatech is leading Hadoop training institute for Hadoop Cluster Admin courses in Bangalore &amp; Pune. We provide practical &amp; focused training for learners." />
                    <meta property="og:type" content="article" />
                    <meta property="og:title" content="Hadoop Admin" />
                    <meta property="og:description" content="Prwatech is leading Hadoop training institute for Hadoop Cluster Admin courses in Bangalore &amp; Pune. We provide practical &amp; focused training for learners." />
                    <meta property="og:url" content="http://prwatech.in/hadoop-admin/" />
                    <meta property="og:site_name" content="BigData &amp; HADOOP" />
                    <meta name="google-site-verification" content="fSzBVSEUMu0l5lnD0qBVGv5F_16zaI6xiUZFMm-iMqQ" />  
                <?php include '../includes/csslinks.php'; ?>
        </head>

	<!-- body classes:  -->
	<!-- "boxed": boxed layout mode e.g. <body class="boxed"> -->
	<!-- "pattern-1 ... pattern-9": background patterns for boxed layout mode e.g. <body class="boxed pattern-1"> -->
	<!-- "transparent-header": makes the header transparent and pulls the banner to top -->
	<body class="no-trans front-page transparent-header ">
            <!-- scrollToTop -->
		<!-- ================ -->
		<div class="scrollToTop circle"><i class="icon-up-open-big"></i></div>
		
		<!-- page wrapper start -->
		<!-- ================ -->
		<div class="page-wrapper">
		
			<!-- header-container start -->
			<?php include '../includes/header.php' ;  ?>
			<!-- header-container end -->
		
			<!-- banner start -->
			<!-- ================ -->
			<div class="banner dark-translucent-bg" style="background-image:url('../images/page-course-banner-2.jpg'); background-position: 50% 27%;">
				<!-- breadcrumb start -->
				<!-- ================ -->
				<div class="breadcrumb-container object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
					<div class="container">
						<ol class="breadcrumb">
							<li><i class="fa fa-home pr-10"></i><a class="link-dark" href="index.html">Home</a></li>
							<li class="active">All Courses</li>
							<li class="active">Hadoop Admin</li>
						</ol>
					</div>
				</div>
				<!-- breadcrumb end -->
				<div class="container">
					
				</div>
			</div>
			<!-- banner end -->
			
			<div id="page-start"></div>

			<!-- section start -->
			<!-- ================ -->
			<section class="light-gray-bg pv-30 clearfix object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100" id="cdrow1">
				<div class="container">
					<div class="row">
						<div class="col-md-8">
							 <h1 class="text-center heading-font" style="text-transform:none;"> <strong>Hadoop Admin</strong> Training</h1>
                                                  
							<div class="separator"></div>
                                                        <p class="large text-justify heading-font">PrwaTech invites you to enroll in our Hadoop Admin Training and become a first rate Hadoop Administrator. This training Course is designed by professionals to provide practical skills and knowledge to become a successful Hadoop Administrator. Learn from experts, get all your queries answered and enjoy 24/7 support and get unlimited access to tutorials.</p>
						</div>
						<div class="col-md-4">
                                                    <img src="../images/por-2.png"/>
                                                </div>
						
					</div>
                                     <p>&nbsp;</p>
                                    <div class="separator"></div>
                                    <h3 class="text-center heading-font object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100" style="text-transform:none;">Contact Us <strong>+91 8147111254 </strong></h3>
                                     <div class="separator"></div>
                                      <p>&nbsp;</p>
                            
				</div>
                               <div class="container">
                                <div class="row" id="batch_details">
                                    <div class="col-md-3">
                                        <div class="col-md-3">
                                            <div class="row">
                                                <p id ="date-time">21 <br><span id="month">NOV</span></p>
                                            </div>
                                       </div>
                                        <div class="col-md-9" id="upcomming-batch">
                                            <div class="row">
                                                <div class="col-md-12"><p id="dt-content"><b> Mon-Wed (3 Days) <br><small>&nbsp; <br> Rs. 14000/- <a href="#" class="" data-toggle="modal" data-target="#demoPopUp">Enroll Now</a></small></b></p></div>
                                                
                                            </div>
                                       </div>
                                    </div>
                                  <div class="col-md-3">
                                        <div class="col-md-3">
                                            <div class="row">
                                                <p id ="date-time">24 <br><span id="month">NOV</span></p>
                                            </div>
                                       </div>
                                        <div class="col-md-9" id="upcomming-batch">
                                            <div class="row">
                                                <div class="col-md-12"><p id="dt-content"><b> Mon-Wed (3 Days) <br><small>&nbsp; <br> Rs. 14000/- <a href="#" class="" data-toggle="modal" data-target="#demoPopUp">Enroll Now</a></small></b></p></div>
                                                
                                            </div>
                                       </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="col-md-3">
                                            <div class="row">
                                                <p id ="date-time">27 <br><span id="month">NOV</span></p>
                                            </div>
                                       </div>
                                        <div class="col-md-9" id="upcomming-batch">
                                            <div class="row">
                                                <div class="col-md-12"><p id="dt-content"><b> Mon-Wed (3 Days) <br><small>&nbsp; <br> Rs. 14000/- <a href="#" class="" data-toggle="modal" data-target="#demoPopUp">Enroll Now</a></small></b></p></div>
                                                
                                            </div>
                                       </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="col-md-3">
                                            <div class="row">
                                                <p id ="date-time">30 <br><span id="month">NOV</span></p>
                                            </div>
                                       </div>
                                        <div class="col-md-9" id="upcomming-batch">
                                            <div class="row">
                                                <div class="col-md-12"><p id="dt-content"><b> Mon-Wed (3 Days) <br><small>&nbsp; <br> Rs. 14000/- <a href="#" class="" data-toggle="modal" data-target="#demoPopUp">Enroll Now</a></small></b></p></div>
                                                
                                            </div>
                                       </div>
                                    </div>
                                </div>
                            </div>
			</section>
                         <section id="coursed-row2" class="object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
                            <div class="container">
                                
                                 <h3 class="text-center heading-font object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100" style="text-transform:none;"> <strong>Course</strong> Details</h3>
                                 <div id="vseparator" class="object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
                                     <div class="vhr"></div>
                                </div>
                                <div class="row col-md-9" >
                               <!-- tabs start -->
                                <!-- ================ -->
                                <!-- Nav tabs -->
                                <ul class="nav nav-tabs style-3" id="tab-course" role="tablist">
                                    <li class="active"><a href="#h3tab1" role="tab" data-toggle="tab">Overview</a></li>
                                    <li><a href="#h3tab2" role="tab" data-toggle="tab">Course Curriculum</a></li>
                                    <li><a href="#h3tab3" role="tab" data-toggle="tab">FAQ's</a></li>
                                    <li><a href="#h3tab4" role="tab" data-toggle="tab">Download Course Outline</a></li>
                                    <li><a href="#h3tab5" role="tab" data-toggle="tab">Reviews</a></li>
                                   
                                </ul>
                                <!-- Tab panes -->
                                <div class="tab-content">
                                    <div class="tab-pane in active " id="h3tab1">
                                         <div class="row">
                                            <div class="col-md-12 object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
                                                
                                                <p id="cpara">
                                                   PrwaTech invites you to enroll in our Hadoop Admin Training and become a first rate Hadoop Administrator. This training Course is designed by professionals to provide practical skills and knowledge to become a successful Hadoop Administrator. Learn from experts, get all your queries answered and enjoy 24/7 support and get unlimited access to tutorials.
                                                </p>
                                               
                                                <h2 class="heading-font">About The Training:</h2>
                                                <p id="cpara">
                                                The Hadoop Admin training course is designed to offer knowledge and skills which will make you a successful Hadoop Architect. Our training program starts with elementary concepts of Hadoop Cluster and Apache Hadoop. The training program covers topics to configure, manage, deploy, secure and monitor a Hadoop Cluster. Training program includes various challenging and practical exercises for trainees.</p>
    
                                                <h2 class="heading-font">Training Highlights:</h2>
                                                <ul class="list-icons" id="clist">
                                                    <li><i class="icon-check pr-10"></i> Role Specific Training</li>
                                                    <li><i class="icon-check pr-10"></i> Hands-on Exercises</li>
                                                    <li><i class="icon-check pr-10"></i> Introduction to BIG DATA and HADOOP</li>
                                                    <li><i class="icon-check pr-10"></i> Resume preparation, Interview question answers are also provided.</li>
                                                    <li><i class="icon-check pr-10"></i> Covers all significant Hadoop system products</li>
                                                    
                                                </ul>
                                                    
                                                <h2 class="heading-font">Benefits of Hadoop Admin training:</h2>
                                                <ul class="list-icons" id="clist">
                                                    <li><i class="icon-check pr-10"></i> Unlimited access to Training material</li>
                                                    <li><i class="icon-check pr-10"></i> Assignments for every training session</li>
                                                    <li><i class="icon-check pr-10"></i> Excellent Course Material and E-books</li>
                                                    <li><i class="icon-check pr-10"></i> Excellent Lab Material</li>
                                                    <li><i class="icon-check pr-10"></i> 24/7 contact with trainers for doubt clarification</li>
                                                    <li><i class="icon-check pr-10"></i> Certificate of Completion at completion</li>
                                                        
                                                </ul>
                                                <h2 class="heading-font">After the completion of Hadoop Admin Training course at PrwaTech, you will have the knowledge of :</h2>
                                                <ul class="list-icons" id="clist">
                                                    <li><i class="icon-check pr-10"></i> Apache Hadoop</li>
                                                    <li><i class="icon-check pr-10"></i> Hadoop Cluster</li>
                                                    <li><i class="icon-check pr-10"></i> Hadoop Administration</li>
                                                    <li><i class="icon-check pr-10"></i> Insight on Hadoop 2.0</li>
                                                    <li><i class="icon-check pr-10"></i> HDFS Federation</li>
                                                    <li><i class="icon-check pr-10"></i> Name Node High Availability</li>
                                                    <li><i class="icon-check pr-10"></i> YARN</li>
                                                    <li><i class="icon-check pr-10"></i> Hadoop Cluster Installation</li>
                                                    <li><i class="icon-check pr-10"></i> Load Data and Run applications</li>
                                                    <li><i class="icon-check pr-10"></i> Troubleshoot Hadoop Cluster</li>
                                                    <li><i class="icon-check pr-10"></i> Backup and Recovery</li>
                                                    <li><i class="icon-check pr-10"></i> Hcatalog/Hive Oozie</li>
                                                    <li><i class="icon-check pr-10"></i> HBase Administration</li>
                                                        
                                                </ul>
                                                
                                                <div class="separator"></div>
                                                <h2 class="heading-font">Who should go for this training?</h2>
                                                <p id = "cpara">Systems administrators, linux administrators, windows administrators, Infrastructure engineers, Big Data Architects, DB Administrators, IT managers and Mainframe Professionals.</p>
                                                  
                                                <h2 class="heading-font">Pre-requisites for this training.</h2>
                                                <p id = "cpara">This course requires no prior knowledge of Java, Hadoop Cluster Administration or Apache Hadoop. Fundamental knowledge of Linux basics is necessary as Hadoop runs on Linux.</p>
                                            </div>
                                                
                                        </div>
                                            
                                    </div>
                                    <div class="tab-pane" id="h3tab2">
                                          <div class="row">
                                            <div class="col-md-12 ">
                                                  <div class="panel-group collapse-style-2" id="accordion-2">
								<div class="panel panel-default">
									<div class="panel-heading">
										<h4 class="panel-title">
											<a data-toggle="collapse" data-parent="#accordion-2" href="#collapseOne-2">
												<i class="fa fa-arrow-right pr-10"></i>Introduction to Hadoop
											</a>
										</h4>
									</div>
                                                                    <div id="collapseOne-2" class="panel-collapse collapse in">
                                                                        <div class="panel-body">
                                                                            
                                                                            <ul class="list-icons" id="clist">
                                                                                <li><i class="icon-check pr-10"></i>The amount of data processing in today’s life</li>
                                                                                <li><i class="icon-check pr-10"></i>What Hadoop is why it is important?</li>
                                                                                <li><i class="icon-check pr-10"></i>Hadoop comparison with traditional systems</li>
                                                                                <li><i class="icon-check pr-10"></i>Hadoop history</li>
                                                                                <li><i class="icon-check pr-10"></i>Hadoop main components and architecture</li>
                                                                              
                                                                            </ul>
                                                                        </div>
                                                                    </div>
								</div>
								<div class="panel panel-default">
									<div class="panel-heading">
										<h4 class="panel-title">
											<a data-toggle="collapse" data-parent="#accordion-2" href="#collapseTwo-2" class="collapsed">
												<i class="fa fa-arrow-right pr-10"></i>Hadoop Distributed File System (HDFS)
											</a>
										</h4>
									</div>
									<div id="collapseTwo-2" class="panel-collapse collapse">
										<div class="panel-body">
                                                                                    <ul class="list-icons" id="clist">
                                                                                        <li><i class="icon-check pr-10"></i>HDFS overview and design</li>
                                                                                        <li><i class="icon-check pr-10"></i>HDFS architecture</li>
                                                                                        <li><i class="icon-check pr-10"></i>HDFS file storage</li>
                                                                                        <li><i class="icon-check pr-10"></i>Component failures and recoveries</li>
                                                                                        <li><i class="icon-check pr-10"></i>Block placement</li>
                                                                                        <li><i class="icon-check pr-10"></i>Balancing the Hadoop cluster</li>
                                                                                            
                                                                                    </ul>  
                                                                                </div>
									</div>
								</div>
                                                      <div class="panel panel-default">
                                                          <div class="panel-heading">
                                                              <h4 class="panel-title">
                                                                  <a data-toggle="collapse" data-parent="#accordion-2" href="#collapseThree-2" class="collapsed">
                                                                      <i class="fa fa-arrow-right pr-10"></i>Planning your Hadoop cluster
                                                                  </a>
                                                              </h4>
                                                          </div>
                                                          <div id="collapseThree-2" class="panel-collapse collapse">
                                                              <div class="panel-body">
                                                                 <ul class="list-icons" id="clist">
                                                                                        <li><i class="icon-check pr-10"></i>Planning a Hadoop cluster and its capacity</li>
                                                                                        <li><i class="icon-check pr-10"></i>Hadoop software and hardware configuration</li>
                                                                                        <li><i class="icon-check pr-10"></i>HDFS Block replication and rack awareness</li>
                                                                                        <li><i class="icon-check pr-10"></i>Network topology for Hadoop cluster</li>
                                                                                        
                                                                </ul>      
                                                              </div>
                                                          </div>
                                                      </div>
                                                      <div class="panel panel-default">
                                                          <div class="panel-heading">
                                                              <h4 class="panel-title">
                                                                  <a data-toggle="collapse" data-parent="#accordion-2" href="#collapseFour-2" class="collapsed">
                                                                      <i class="fa fa-arrow-right pr-10"></i>Hadoop Deployment
                                                                  </a>
                                                              </h4>
                                                          </div>
                                                          <div id="collapseFour-2" class="panel-collapse collapse">
                                                              <div class="panel-body">
                                                                  <ul class="list-icons" id="clist">
                                                                                        <li><i class="icon-check pr-10"></i>Different Hadoop deployment types</li>
                                                                                        <li><i class="icon-check pr-10"></i>Hadoop distribution options</li>
                                                                                        <li><i class="icon-check pr-10"></i>Hadoop competitors</li>
                                                                                        <li><i class="icon-check pr-10"></i>Hadoop installation procedure</li>
                                                                                        <li><i class="icon-check pr-10"></i>Distributed cluster architecture</li>
                                                                                        
                                                                </ul>    
                                                              </div>
                                                          </div>
                                                      </div>
                                                      <div class="panel panel-default">
                                                          <div class="panel-heading">
                                                              <h4 class="panel-title">
                                                                  <a data-toggle="collapse" data-parent="#accordion-2" href="#collapseFive-2" class="collapsed">
                                                                      <i class="fa fa-arrow-right pr-10"></i>Hadoop Cluster Configuration
                                                                  </a>
                                                              </h4>
                                                          </div>
                                                          <div id="collapseFive-2" class="panel-collapse collapse">
                                                              <div class="panel-body">
                                                                     <ul class="list-icons" id="clist">
                                                                                        <li><i class="icon-check pr-10"></i>Hadoop configuration overview and important configuration file</li>
                                                                                        <li><i class="icon-check pr-10"></i>Configuration parameters and values</li>
                                                                                        <li><i class="icon-check pr-10"></i>HDFS parameters MapReduce parameters</li>
                                                                                        <li><i class="icon-check pr-10"></i>Hadoop environment setup</li>
                                                                                        <li><i class="icon-check pr-10"></i>‘Include’ and ‘Exclude’ configuration files</li>
                                                                                        <li><i class="icon-check pr-10"></i>Preparing HDFS</li>
                                                                                        <li><i class="icon-check pr-10"></i>MULTINODE CLUSTER</li>
                                                                                        <li><i class="icon-check pr-10"></i>Setting basic configuration parameters</li>
                                                                                        <li><i class="icon-check pr-10"></i>Configuring block allocation, redundancy and replication</li>
                                                                                        <li><i class="icon-check pr-10"></i>Installing and setting up the MapReduce environment</li>
                                                                                        <li><i class="icon-check pr-10"></i>Delivering redundant load balancing via Rack Awareness</li>
                                                                                      
                                                                                        
                                                                </ul>  
                                                              </div>
                                                          </div>
                                                      </div>
                                                      <div class="panel panel-default">
                                                          <div class="panel-heading">
                                                              <h4 class="panel-title">
                                                                  <a data-toggle="collapse" data-parent="#accordion-2" href="#collapseSix-2" class="collapsed">
                                                                      <i class="fa fa-arrow-right pr-10"></i>MANAGING RESOURCES AND CLUSTER HEALTH
                                                                  </a>
                                                              </h4>
                                                          </div>
                                                          <div id="collapseSix-2" class="panel-collapse collapse">
                                                              <div class="panel-body">
                                                                  <ul class="list-icons" id="clist">
                                                                                        <li><i class="icon-check pr-10"></i>Allocating resources</li>
                                                                                        <li><i class="icon-check pr-10"></i>CLOUDERA MANAGER</li>
                                                                                        <li><i class="icon-check pr-10"></i>Setting quotas to constrain HDFS utilization</li>
                                                                                        <li><i class="icon-check pr-10"></i>Prioritizing access to MapReduce using schedulers</li>
                                                                                        <li><i class="icon-check pr-10"></i>Maintaining HDFS</li>
                                                                                        <li><i class="icon-check pr-10"></i>Starting and stopping Hadoop daemons</li>
                                                                                        <li><i class="icon-check pr-10"></i>Monitoring HDFS status</li>
                                                                                        <li><i class="icon-check pr-10"></i>Adding and removing data nodes</li>
                                                                                        <li><i class="icon-check pr-10"></i>Administering MapReduce</li>
                                                                                        <li><i class="icon-check pr-10"></i>Managing MapReduce jobs</li>
                                                                                        <li><i class="icon-check pr-10"></i>Tracking progress with monitoring tools</li>
                                                                                        <li><i class="icon-check pr-10"></i>Commissioning and decommissioning compute nodes</li>
                                                                                        
                                                                                      
                                                                                        
                                                                </ul>      
                                                              </div>
                                                          </div>
                                                      </div>
                                                       <div class="panel panel-default">
                                                          <div class="panel-heading">
                                                              <h4 class="panel-title">
                                                                  <a data-toggle="collapse" data-parent="#accordion-2" href="#collapseSeven-2" class="collapsed">
                                                                      <i class="fa fa-arrow-right pr-10"></i>Hadoop Administration and Maintenance
                                                                  </a>
                                                              </h4>
                                                          </div>
                                                          <div id="collapseSeven-2" class="panel-collapse collapse">
                                                              <div class="panel-body">
                                                                        <ul class="list-icons" id="clist">
                                                                                        <li><i class="icon-check pr-10"></i>Namenode/Datanode directory structures and files</li>
                                                                                        <li><i class="icon-check pr-10"></i>File system image and Edit log</li>
                                                                                        <li><i class="icon-check pr-10"></i>The Checkpoint Procedure</li>
                                                                                        <li><i class="icon-check pr-10"></i>Namenode failure and recovery procedure</li>
                                                                                        <li><i class="icon-check pr-10"></i>Safe Mode</li>
                                                                                        <li><i class="icon-check pr-10"></i>Metadata and Data backup</li>
                                                                                        <li><i class="icon-check pr-10"></i>Employing the standard built–in tools</li>
                                                                                        <li><i class="icon-check pr-10"></i>Employing the standard built–in tools</li>
                                                                                        <li><i class="icon-check pr-10"></i>Managing and debugging processes using JVM metrics</li>
                                                                                        <li><i class="icon-check pr-10"></i>Performing Hadoop status checks</li>
                                                                                        <li><i class="icon-check pr-10"></i>Tuning with supplementary tools</li>
                                                                                        <li><i class="icon-check pr-10"></i>Assessing performance with Ganglia</li>
                                                                                        <li><i class="icon-check pr-10"></i>Benchmarking to ensure continued performance</li>
                                                                                        <li><i class="icon-check pr-10"></i>YARN,</li>
                                                                                        <li><i class="icon-check pr-10"></i>MRv2</li>
                                                                                        <li><i class="icon-check pr-10"></i>Job Failure</li>
                                                                                        <li><i class="icon-check pr-10"></i>RunningMRv1 inYARN</li>
                                                                                        <li><i class="icon-check pr-10"></i>Upgrade yourexistingMRv1 code to MRv2</li>
                                                                                        <li><i class="icon-check pr-10"></i>Programmingin YARNframework.</li>
                                                                 </ul> 
                                                              </div>
                                                          </div>
                                                      </div>
                                                       <div class="panel panel-default">
                                                          <div class="panel-heading">
                                                              <h4 class="panel-title">
                                                                  <a data-toggle="collapse" data-parent="#accordion-2" href="#collapseEight-2" class="collapsed">
                                                                      <i class="fa fa-arrow-right pr-10"></i>EXTENDING HADOOP
                                                                  </a>
                                                              </h4>
                                                          </div>
                                                          <div id="collapseEight-2" class="panel-collapse collapse">
                                                              <div class="panel-body">
                                                                  <ul class="list-icons" id="clist">
                                                                                        <li><i class="icon-check pr-10"></i>Simplifying information access</li>
                                                                                        <li><i class="icon-check pr-10"></i>Enabling SQL–like querying with Hive</li>
                                                                                        <li><i class="icon-check pr-10"></i>Installing Pig to create MapReduce jobs</li>
                                                                                        <li><i class="icon-check pr-10"></i>Integrating additional elements of the ecosystem</li>
                                                                                        <li><i class="icon-check pr-10"></i>Imposing a tabular view on HDFS with HBase</li>
                                                                                        <li><i class="icon-check pr-10"></i>Configuring Oozie to schedule workflows</li>
                                                                                        
                                                                                        
                                                                 </ul> 
                                                              </div>
                                                          </div>
                                                      </div>
                                                       <div class="panel panel-default">
                                                          <div class="panel-heading">
                                                              <h4 class="panel-title">
                                                                  <a data-toggle="collapse" data-parent="#accordion-2" href="#collapseNine-2" class="collapsed">
                                                                      <i class="fa fa-arrow-right pr-10"></i>Sqoop (Real world datasets and analysis)
                                                                  </a>
                                                              </h4>
                                                          </div>
                                                          <div id="collapseNine-2" class="panel-collapse collapse">
                                                              <div class="panel-body">
                                                                  <p id="cpara">Learning Objectives- This Module will cover to Import & Export Data from RDBMS(MySql,Oracle) to HDFS & Vice Versa</p>
                                                                  <ul class="list-icons" id="clist">
                                                                                        <li><i class="icon-check pr-10"></i>What is Sqoop?</li>
                                                                                        <li><i class="icon-check pr-10"></i>Why Sqoop?</li>
                                                                                        <li><i class="icon-check pr-10"></i>Importing and exporting data using Sqoop</li>
                                                                                        <li><i class="icon-check pr-10"></i>Provisioning Hive Metastore</li>
                                                                                        <li><i class="icon-check pr-10"></i>Sqoop Connectors</li>
                                                                                        <li><i class="icon-check pr-10"></i>What are the features of Sqoop?</li>
                                                                                        <li><i class="icon-check pr-10"></i>What are the performance banchmarks in our cluster for Sqoop?</li>
                                                                                        <li><i class="icon-check pr-10"></i>Multiple Case with Hands On.</li>
                                                                                        
                                                                 </ul> 
                                                              </div>
                                                          </div>
                                                      </div>
                                                       <div class="panel panel-default">
                                                          <div class="panel-heading">
                                                              <h4 class="panel-title">
                                                                  <a data-toggle="collapse" data-parent="#accordion-2" href="#collapseTen-2" class="collapsed">
                                                                      <i class="fa fa-arrow-right pr-10"></i>Flume (Twitter Datasets )
                                                                  </a>
                                                              </h4>
                                                          </div>
                                                          <div id="collapseTen-2" class="panel-collapse collapse">
                                                              <div class="panel-body">
                                                                
                                                                  <ul class="list-icons" id="clist">
                                                                                        <li><i class="icon-check pr-10"></i>What is Flume?</li>
                                                                                        <li><i class="icon-check pr-10"></i>Why Flume?</li>
                                                                                        <li><i class="icon-check pr-10"></i>Importing data using Flume</li>
                                                                                        
                                                                                        
                                                                 </ul> 
                                                              </div>
                                                          </div>
                                                      </div>
                                                       <div class="panel panel-default">
                                                          <div class="panel-heading">
                                                              <h4 class="panel-title">
                                                                  <a data-toggle="collapse" data-parent="#accordion-2" href="#collapseEleven-2" class="collapsed">
                                                                      <i class="fa fa-arrow-right pr-10"></i>Benchmarking
                                                                  </a>
                                                              </h4>
                                                          </div>
                                                          <div id="collapseEleven-2" class="panel-collapse collapse">
                                                              <div class="panel-body">
                                                                 
                                                                  <ul class="list-icons" id="clist">
                                                                                        <li><i class="icon-check pr-10"></i>Allocating Resources</li>
                                                                                        <li><i class="icon-check pr-10"></i>Defining Instance</li>
                                                                                        <li><i class="icon-check pr-10"></i>Cloudera Benchmark</li>
                                                                                        <li><i class="icon-check pr-10"></i>Pricing & Cost Effectiveness</li>
                                                                                        <li><i class="icon-check pr-10"></i>AWS</li>
                                                                                        
                                                                                        
                                                                 </ul> 
                                                              </div>
                                                          </div>
                                                      </div>
                                                    </div>
                                                
                                                </div>

                                        </div>
                                       
                                    </div>
                                          <div class="tab-pane" id="h3tab3">
                                        <div class="row">
                                            <div class="col-md-12 ">
                                                   	<div class="panel-group collapse-style-3" id="accordion-3">
								<div class="panel panel-default">
									<div class="panel-heading">
										<h4 class="panel-title">
											<a data-toggle="collapse" data-parent="#accordion-3" href="#collapseOne-3">
												Why Should I Learn Hadoop From Prwatech?
											</a>
										</h4>
									</div>
									<div id="collapseOne-3" class="panel-collapse collapse in">
										<div class="panel-body">
                                                                                    <p id="cpara" class="text-justify">
                                                                                        Prwatech is the pioneer of Hadoop training in India. As you know today the demand for Hadoop professionals far exceeds the supply. So it pays to be with the market leader like Prwatech when it comes to learning Hadoop in order to command top salaries. As part of the training you will learn about the various components of Hadoop like MapReduce, HDFS, HBase, Hive, Pig, Sqoop, Flume, Oozie among others. You will get an in-depth understanding of the entire Hadoop framework for processing huge volumes of data in real world scenarios.
                                                                                    </p>
                                                                                    <p id="cpara" class="text-justify">
                                                                                       The Prwatech training is the most comprehensive course, designed by industry experts keeping in mind the job scenario and corporate requirements. We also provide lifetime access to videos, course materials, 24/7 Support, and free course material upgrade. Hence it is a one-time investment. 
                                                                                    </p>
										</div>
									</div>
								</div>
								<div class="panel panel-default">
									<div class="panel-heading">
										<h4 class="panel-title">
											<a data-toggle="collapse" data-parent="#accordion-3" href="#collapseTwo-3" class="collapsed">
												What Are The Various Modes Of Training That Prwatech Offers?
											</a>
										</h4>
									</div>
									<div id="collapseTwo-3" class="panel-collapse collapse">
										<div class="panel-body">
                                                                                    <p id="cpara" class="text-justify">
                                                                                        Prwatech basically offers the self-paced training and online instructor-led training. Apart from that we also provide corporate training for enterprises. All our trainers come with over 5 years of industry experience in relevant technologies and also they are subject matter experts working as consultants. You can check about the quality of our trainers in the sample videos provided.
                                                                                    </p>
                                                                                    
										</div>
									</div>
								</div>
								<div class="panel panel-default">
									<div class="panel-heading">
										<h4 class="panel-title">
											<a data-toggle="collapse" data-parent="#accordion-3" href="#collapseThree-3" class="collapsed">
												Can I Request For A Support Session If I Find Difficulty In Grasping Topics?
											</a>
										</h4>
									</div>
									<div id="collapseThree-3" class="panel-collapse collapse">
										<div class="panel-body">
                                                                                    <p id="cpara" class="text-justify">
                                                                                          If you have any queries you can contact our 24/7 dedicated support to raise a ticket. We provide you email support and solution to your queries. If the query is not resolved by email we can arrange for a one-on-one session with our trainers. The best part is that you can contact Prwatech even after completion of training to get support and assistance. There is also no limit on the number of queries you can raise when it comes to doubt clearance and query resolution.
                                                                                    </p>
                                                                                    
										</div>
									</div>
								</div>
                                                            <div class="panel panel-default">
									<div class="panel-heading">
										<h4 class="panel-title">
											<a data-toggle="collapse" data-parent="#accordion-3" href="#collapseFour-3" class="collapsed">
												If I Am Not From A Programming Background But Have A Basic Knowledge Of Programming Can I Still Learn Hadoop?
											</a>
										</h4>
									</div>
									<div id="collapseFour-3" class="panel-collapse collapse">
										<div class="panel-body">
                                                                                    <p id="cpara" class="text-justify">
                                                                                         Yes, you can learn Hadoop without being from a software background. We provide complimentary courses in Java and Linux so that you can brush up on your programming skills. This will help you in learning Hadoop technologies better and faster.
                                                                                    </p>
                                                                                    
										</div>
									</div>
								</div>
                                                            <div class="panel panel-default">
									<div class="panel-heading">
										<h4 class="panel-title">
											<a data-toggle="collapse" data-parent="#accordion-3" href="#collapseFive-3" class="collapsed">
												What Kind Of Projects Will I Be Working On As Part Of The Training?
											</a>
										</h4>
									</div>
									<div id="collapseFive-3" class="panel-collapse collapse">
										<div class="panel-body">
                                                                                    <p id="cpara" class="text-justify">
                                                                                        We provide you with the opportunity to work on real world projects wherein you can apply your knowledge and skills that you acquired through our training. We have multiple projects that thoroughly test your skills and knowledge of various Hadoop components making you perfectly industry-ready. These projects could be in exciting and challenging fields like banking, insurance, retail, social networking, high technology and so on. The Prwatech projects are equivalent to six months of relevant experience in the corporate world.
                                                                                    </p>
                                                                                    
										</div>
									</div>
								</div>
                                                            <div class="panel panel-default">
									<div class="panel-heading">
										<h4 class="panel-title">
											<a data-toggle="collapse" data-parent="#accordion-3" href="#collapseSix-3" class="collapsed">
												Do You Provide Placement Assistance?
											</a>
										</h4>
									</div>
									<div id="collapseSix-3" class="panel-collapse collapse">
										<div class="panel-body">
                                                                                    <p id="cpara" class="text-justify">
                                                                                        Yes, Prwatech does provide you with placement assistance. We have tie-ups with 80+ organizations including Ericsson, Cisco, Cognizant, TCS, among others that are looking for Hadoop professionals and we would be happy to assist you with the process of preparing yourself for the interview and the job.
                                                                                    </p>
                                                                                    
										</div>
									</div>
								</div>
							</div>
                                                
                                                </div>

                                        </div>
                                       
                                    </div>
                                    <div class="tab-pane" id="h3tab4">
                                        <div class="row object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
                                            <div class="col-md-4">
                                                <p id="cpara" class="text-default"><a href="">Click Here to Download</a></p>
                                                
                                            </div>
                                            <div class="col-md-8">
                                                 <h4 class="title text-default" class="heading-font">Bigdata and Hadoop Services</h4>
                                                <ul class="list-icons">
                                                    <li><i class="icon-check pr-10"></i> PowerPoint Presentation covering all classes</li>
                                                    <li><i class="icon-check pr-10"></i> Recorded Videos Sessions On Bigdata and Hadoop with LMS Access.(lifetime support)</li>
                                                    <li><i class="icon-check pr-10"></i> Quiz , Assignment & POC.</li>
                                                    <li><i class="icon-check pr-10"></i> On Demand Online Support .</li>
                                                    <li><i class="icon-check pr-10"></i> Discussion Forum.</li>
                                                    <li><i class="icon-check pr-10"></i> Material
                                                        <ul class="list-icons" style="margin-left:50px;">
                                                            <li>a. Sample Question papers of Cloudera Certification.</li>
                                                            <li>b. Technical Notes & Study Material.</li>
                                                        </ul>
                                                    
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        
                                    </div>
                                    <div class="tab-pane" id="h3tab5">
                                        <div class="row object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
                                            <div class="col-md-6 ">
                                                <div class="pv-20 ph-20 feature-box-2 boxed shadow object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
                                                    <div class="col-md-3 testimonial-image"><img src="../images/testimonial-2.jpg" /></div> 
                                                    <div class="body">
                                                        <h4 class="title">Anupam Khamparia 
                                                            <br>
                                                            <span style="font-size:11px;">Consultant, Cognizant Technology Solutions, Bangalore</span>
                                                        </h4>
                                                        <p class ="text-justify">“Excellent course and instructor. I learnt a lot in a short period. Good value for money. Instructor took us through Advanced Hadoop Development in depth.”</p>
                                                       
                                                    </div>
                                                </div>
                                            </div>
                                          <div class="col-md-6 ">
                                                <div class="pv-20 ph-20 feature-box-2 boxed shadow object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
                                                    <div class="col-md-3 testimonial-image"><img src="../images/testimonial-1.png" /></div> 
                                                    <div class="body">
                                                        <h4 class="title">Anukanksha Garg
                                                          <br>
                                                            <span style="font-size:11px;">B.Tech. CSE</span>
                                                        </h4>
                                                        <p class ="text-justify">It was a nice learning experience with prwatech. The classes were well scheduled and managed.
Verma has good understanding of the topics taught and catered to our problems and doubts very patiently. The best thing about him was that he handled the situations accordingly, when needed a friend, he became one and also a teacher to always guide us.</p>
                                                       
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6 ">
                                                <div class="pv-20 ph-20 feature-box-2 boxed shadow object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
                                                    <div class="col-md-3 testimonial-image"><img src="../images/testimonial-4.PNG" /></div> 
                                                    <div class="body">
                                                        <h4 class="title">Varun Shivashanmugum
                                                        
                                                        <br>
                                                            <span style="font-size:11px;">Associate Consultant, ITC Infotech Ltd</span>
                                                        </h4>
                                                        
                                                        <p class ="text-justify">“Faculty is good,Verma takes keen interest and personnel care in improvising skills in students. And most importantly,Verma will be available and clears doubts at any time apart from class hours. And he always keeps boosting and trying to increase confidence in his students which adds extra attribute to him and organization as well. and organization as well.</p>
                                                       
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6 ">
                                                <div class="pv-20 ph-20 feature-box-2 boxed shadow object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
                                                    <div class="col-md-3 testimonial-image"><img src="../images/testimonial-5.jpg" /></div> 
                                                    <div class="body">
                                                        <h4 class="title">Mayank Srivastava <br>
                                                        <span style="font-size:11px;">Hadoop Developer, L&T, Bangalore</span>
                                                        </h4>
                                                        <p class ="text-justify">“Really good course content and labs, patient enthusiastic instructor. Good instructor, with in depth skills…Very relevant practicals allowed me to put theory into practice.”</p>
                                                       
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                                 <div class="col-md-3" style="position:relative;top:-10px;">
                                     <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center" >
                                         <div class="overlay-container">
                                             <img src="../images/icon/curse-d1.png" alt="">
                                         </div>
                                           <div class="body">
                                             <h3 class="heading-font">Rs. 14,000 + Tax </h3>
                                             <span class="small-font">per 2 weeks</span>
                                             <div class="separator"></div>
                                             <p class="heading-font"></p>
                                             <table class="table table-striped ">
                                                 <tbody>
                                                     <tr><th><center>35 Hours</center></th></tr>
                                                 <tr><th><center>15 Seats</center></th></tr>
                                                 <tr><th><center>Course Badge</center></th></tr>
                                                 <tr><th><center>Course Certificate</center></th></tr>
                                                 <tr><th><center> <a  onclick="openNav()" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Enroll Now<i class="fa fa-arrow-right pl-10"></i></a></center></th></tr>
                                                 </tbody>
                                             </table>
                                            <?php include '../includes/demo-popup.php';?>
                                                
                                         </div>
                                     </div>
                                      <h3 class="heading-font text-center" style="background:#4c4c4c;color:white;padding-top:10px;padding-bottom:10px">Suggested Courses</h3> 
                                      <div class="owl-carousel content-slider">
                                          <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
                                              <div class="overlay-container">
                                                  <img src="../images/por-1.png" alt="">
                                                  <div class="overlay-to-top">
                                                      <p class="small margin-clear">&nbsp;</p>
                                                  </div>
                                              </div>
                                              <div class="body">
                                                  <h3 class="heading-font">Big Data Hadoop Training</h3>
                                                  <div class="separator"></div>
                                                  <p class="heading-font">Advanced Program for Top-notch Developers </p>
                                                  <a href="big-data-hadoop-training.php"  target="_blank" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read More<i class="fa fa-arrow-right pl-10"></i></a>												
                                              </div>
                                          </div>
                                          <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
                                              <div class="overlay-container">
                                                  <img src="../images/por-2.png" alt="">
                                                  <div class="overlay-to-top">
                                                      <p class="small margin-clear">&nbsp;</p>
                                                  </div>
                                              </div>
                                              <div class="body">
                                                  <h3 class="heading-font">Hadoop Admin Training</h3>
                                                  <div class="separator"></div>
                                                  <p class="heading-font">Advanced Program for Top-notch Developers </p>
                                                  <a href="hadoop-admin.php" target="_blank" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read More<i class="fa fa-arrow-right pl-10"></i></a>												
                                              </div>
                                          </div>
                                          <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
                                              <div class="overlay-container">
                                                  <img src="../images/por-3.png" alt="">
                                                  <div class="overlay-to-top">
                                                      <p class="small margin-clear">&nbsp;</p>
                                                  </div>
                                              </div>
                                              <div class="body">
                                                  <h3 class="heading-font">Apache Spark Scala Training</h3>
                                                  <div class="separator"></div>
                                                  <p class="heading-font">Apache Spark Scala Combo Training </p>
                                                  <a href="apache-spark-scala-training.php" target="_blank" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read More<i class="fa fa-arrow-right pl-10"></i></a>												
                                              </div>
                                          </div>
                                          <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
                                              <div class="overlay-container">
                                                  <img src="../images/por-4.png" alt="">
                                                  <div class="overlay-to-top">
                                                      <p class="small margin-clear">&nbsp;</p>
                                                  </div>
                                              </div>
                                              <div class="body">
                                                  <h3 class="heading-font">Data Science Training</h3>
                                                  <div class="separator"></div>
                                                  <p class="heading-font">Find out the truth about what Data Science is. </p>
                                                  <a href="data-science-course-training-bangalore.php" target="_blank" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read More<i class="fa fa-arrow-right pl-10"></i></a>												
                                              </div>
                                          </div>
                                          <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
                                              <div class="overlay-container">
                                                  <img src="../images/por-5.png" alt="">
                                                  <div class="overlay-to-top">
                                                      <p class="small margin-clear">&nbsp;</p>
                                                  </div>
                                              </div>
                                              <div class="body">
                                                  <h3 class="heading-font">R Programming Training</h3>
                                                  <div class="separator"></div>
                                                  <p class="heading-font">Business Analytics With R Training</p>
                                                  <a href="r-programing.php" target="_blank" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read More<i class="fa fa-arrow-right pl-10"></i></a>												
                                              </div>
                                          </div>
                                          <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
                                              <div class="overlay-container">
                                                  <img src="../images/por-6.png" alt="">
                                                  <div class="overlay-to-top">
                                                      <p class="small margin-clear">&nbsp;</p>
                                                  </div>
                                              </div>
                                              <div class="body">
                                                  <h3 class="heading-font"> Tableau Training & Certification</h3>
                                                  <div class="separator"></div>
                                                  <p class="heading-font">Speed on concepts of data visualization</p>
                                                  <a href="tableau.php" target="_blank" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read More<i class="fa fa-arrow-right pl-10"></i></a>												
                                              </div>
                                          </div>
                                          <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
                                              <div class="overlay-container">
                                                  <img src="../images/por-7.png" alt="">
                                                  <div class="overlay-to-top">
                                                      <p class="small margin-clear">&nbsp;</p>
                                                  </div>
                                              </div>
                                              <div class="body">
                                                  <h3 class="heading-font">Python Programming</h3>
                                                  <div class="separator"></div>
                                                  <p class="heading-font">Advanced Concepts for Top-notch Developers  </p>
                                                  <a href="python.php" target="_blank" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read More<i class="fa fa-arrow-right pl-10"></i></a>												
                                              </div>
                                          </div>
                                      </div> 
                                      <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center" >
                                         <div class="body">
                                             <div itemscope itemtype="http://schema.org/Product"style="text-align: left">
                                                            <span itemprop="name">Hadoop Admin</span><br>
                                                            <div itemprop="aggregateRating" itemscope itemtype="http://schema.org/AggregateRating">
                                                                Average Rating: <span itemprop="ratingValue">4.73</span><br>
                                                                Votes: <span itemprop="ratingCount">855</span><br>
                                                                Reviews: <span itemprop="reviewCount">855</span>
                                                            </div>
                                            </div>
                                         </div>
                                     </div>
                                 </div> 
                            </div> 
                           
                           
                            <p>&nbsp;</p>
                            
                        </section>
                     
                        
			<!-- section end -->
                    
			<div class="clearfix"></div>
			
			<!-- ================ -->
			
			<!-- section end -->
                        <section class="pv-30 dark-bg padding-bottom-clear clearfix object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
					<br>
					<div class="container">
						<div class="row">
							<div class="col-md-3 ">
								<div class="ph-20 feature-box text-center object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
									<span class="icon default-bg circle"><i class="fa fa-institution"></i></span>
									<h3>Live classes</h3>
									<div class="separator clearfix"></div>
                                                                        <p class="heading-font ">Live online and interactive classes conducted by instructor</p>
									
								</div>
							</div>
							<div class="col-md-3 ">
								<div class="ph-20 feature-box text-center object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
									<span class="icon default-bg circle"><i class="fa fa-user"></i></span>
									<h3>Expert instructions</h3>
									<div class="separator clearfix"></div>
									<p class="heading-font">Learn from our Experts and get Real-Time Guidance</p>
									
								</div>
							</div>
							<div class="col-md-3 ">
								<div class="ph-20 feature-box text-center object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
									<span class="icon default-bg circle"><i class="icon-signal"></i></span>
									<h3>24 X 7 Support</h3>
									<div class="separator clearfix"></div>
									<p class="heading-font">Personalized Guidance from our 24X7 Support Team</p>
									
								</div>
							</div>
							<div class="col-md-3 ">
								<div class="ph-20 feature-box text-center object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
									<span class="icon default-bg circle"><i class="icon-calendar"></i></span>
									<h3>Flexible schedule</h3>
									<div class="separator clearfix"></div>
									<p class="heading-font">Reschedule your Batch/Class at Your Convenience</p>
									
								</div>
							</div>
						</div>
					</div>
					<br>
				</section>
			
			<!-- section end -->

			<!-- section -->
			<!-- ================ -->
			 <section class="pv-30   padding-bottom-clear dark-translucent-bg parallax padding-bottom-clear" style="background: url('../images/bg/451.jpg') 50% -9px; box-shadow: rgba(0, 0, 0, 0.247059) 0px 2px 7px inset;">
				
				<div class="space-bottom">
					
					<div class="owl-carousel content-slider">
						<div class="container">
							<div class="row">
								<div class="col-md-8 col-md-offset-2">
									<div class="testimonial text-center">
										<div class="testimonial-image">
                                                                                    <img src="../images/testimonial-1.png" alt="" title="" class="img-circle">
										</div>
										
										<div class="testimonial-body">
                                                                                    <p>&nbsp;</p>
												<p id="cpara">It was a nice learning experience with prwatech. The classes were well scheduled and managed.
Verma has good understanding of the topics taught and catered to our problems and doubts very patiently. The best thing about him was that he handled the situations accordingly, when needed a friend, he became one and also a teacher to always guide us.</p>
											
											<div class="testimonial-info-1">Anukanksha Garg</div>
											<div class="testimonial-info-2">B.TECH- CS</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="container">
							<div class="row">
								<div class="col-md-8 col-md-offset-2">
									<div class="testimonial text-center">
										<div class="testimonial-image">
											<img src="../images/testimonial-2.jpg" alt="" title="" class="img-circle">
										</div>
										
										<div class="testimonial-body">
											 <p>&nbsp;</p>
												<p id="cpara">Excellent course and instructor. I learnt a lot in a short period. Good value for money. Instructor took us through Advanced Hadoop Development in depth.</p>
											
											<div class="testimonial-info-1">Anupam Khamparia</div>
											<div class="testimonial-info-2">Consultant, Cognizant Technology Solutions, Bangalore</div>
										</div>
									</div>
								</div>
							</div>
						</div>
                                            <div class="container">
							<div class="row">
								<div class="col-md-8 col-md-offset-2">
									<div class="testimonial text-center">
										<div class="testimonial-image">
											<img src="../images/testimonial-3.jpg" alt="" title="" class="img-circle">
										</div>
										
										<div class="testimonial-body">
											<p>&nbsp;</p>
												<p id="cpara">Excellent course and instructor. I learnt a lot in a short period. Good value for money. Instructor took us through Advanced Hadoop Development in depth.</p>
											<div class="testimonial-info-1">Kumar Waibhav</div>
                                                                                        <div class="testimonial-info-2">Software Engineer, Cognizant Technology Solutions,<br> Philadelphia, Pennsylvania, USA</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					
				</div>
			</section>
			<!-- section end -->

			<!-- section start -->
			<!-- ================ -->
                        
			                   
			<!-- section end -->
			<!-- footer start (Add "dark" class to #footer in order to enable dark footer) -->
			<!-- ================ -->
			<?php include './../includes/footer.php'; ?>
			<!-- footer end -->
			
		</div>
		<!-- page-wrapper end -->

		<?php include './../includes/jslinks.php';?>
                <!-- Color Switcher End -->
                    <?php include './../includes/quick-query.php';?>
                     <?php include './../includes/UserSignup.php';?>
	</body>

        <script>
            $(document).ready(function(){
                onlad(2);
            });  
        </script>

</html>
