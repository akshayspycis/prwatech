<?php include '../includes/session.php'; ?>
<!DOCTYPE html>
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
	<!--<![endif]-->
        <head>
            <meta charset="utf-8">
            <title>Data Science Course Training &amp; Certification Bangalore India</title>
            <meta name="description" content="Prwatech provides data science Training in Bangalore &amp; course classes for fresher and developers. Know more about our data science certification programs." />
	<meta property="og:type" content="article" />
	<meta property="og:title" content="Data Science Course Training Bangalore" />
	<meta property="og:description" content="Prwatech provides data science Training in Bangalore &amp; course classes for fresher and developers. Know more about our data science certification programs." />
	<meta property="og:url" content="http://prwatech.in/data-science-course-training-bangalore/" />
	<meta property="og:site_name" content="BigData &amp; HADOOP" />
            <meta name="google-site-verification" content="fSzBVSEUMu0l5lnD0qBVGv5F_16zaI6xiUZFMm-iMqQ" /> 
                <?php include '../includes/csslinks.php'; ?>
        </head>

	<!-- body classes:  -->
	<!-- "boxed": boxed layout mode e.g. <body class="boxed"> -->
	<!-- "pattern-1 ... pattern-9": background patterns for boxed layout mode e.g. <body class="boxed pattern-1"> -->
	<!-- "transparent-header": makes the header transparent and pulls the banner to top -->
	<body class="no-trans front-page transparent-header ">
            <!-- scrollToTop -->
		<!-- ================ -->
		<div class="scrollToTop circle"><i class="icon-up-open-big"></i></div>
		
		<!-- page wrapper start -->
		<!-- ================ -->
		<div class="page-wrapper">
		
			<!-- header-container start -->
			<?php include '../includes/header.php' ;  ?>
			<!-- header-container end -->
		
			<!-- banner start -->
			<!-- ================ -->
			<div class="banner dark-translucent-bg" style="background-image:url('../images/page-course-banner-4.jpg'); background-position: 50% 27%;">
				<!-- breadcrumb start -->
				<!-- ================ -->
				<div class="breadcrumb-container object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
					<div class="container">
						<ol class="breadcrumb">
							<li><i class="fa fa-home pr-10"></i><a class="link-dark" href="index.html">Home</a></li>
							<li class="active">All Courses</li>
							<li class="active">Data Science Course Training</li>
						</ol>
					</div>
				</div>
				<!-- breadcrumb end -->
				<div class="container">
					
				</div>
			</div>
			<!-- banner end -->
			
			<div id="page-start"></div>

			<!-- section start -->
			<!-- ================ -->
			<section class="light-gray-bg pv-30 clearfix object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100" id="cdrow1">
				<div class="container">
					<div class="row">
						<div class="col-md-8">
							 <h1 class="text-center heading-font" style="text-transform:none;"> <strong>Data Science</strong> Course Training Bangalore</h1>
                                                  
							<div class="separator"></div>
                                                        <p class="large text-justify heading-font">Data science is an interdisciplinary field about processes and systems to extract knowledge or insights from data in various forms, either structured or unstructured, which is a continuation of some of the data analysis fields such as statistics, machine learning, data mining, and predictive analytics.</p>
						</div>
						<div class="col-md-4">
                                                    <img src="../images/por-4.png"/>
                                                 </div>
						
					</div>
                                    <p>&nbsp;</p>
                                    <div class="separator"></div>
                                    <h3 class="text-center heading-font object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100" style="text-transform:none;">Contact Us <strong>+91 8147111254 </strong></h3>
                                    <div class="separator"></div>
                                    <p>&nbsp;</p>
                                    <div class="container">
                                               <div class="row" id="batch_details">
                                              <div class="col-md-3">    
                                                  <div class="col-md-3">
                                                      <div class="row">
                                                          <p id ="date-time">21 <br><span id="month">NOV</span></p>
                                                      </div>
                                                  </div>
                                                  <div class="col-md-9" id="upcomming-batch">
                                                      <div class="row">
                                                          <div class="col-md-12"><p id="dt-content"><b> Mon-Wed (3 Days) <br><small>&nbsp; <br> Rs. 14000/- <a href="#" class="" data-toggle="modal" data-target="#demoPopUp">Enroll Now</a></small></b></p></div>
                                                    
                                                      </div>
                                                  </div>
                                              </div>
                                              <div class="col-md-3">
                                                  <div class="col-md-3">
                                                      <div class="row">
                                                          <p id ="date-time">24 <br><span id="month">NOV</span></p>
                                                      </div>
                                                  </div>
                                                  <div class="col-md-9" id="upcomming-batch">
                                                      <div class="row">
                                                          <div class="col-md-12"><p id="dt-content"><b> Mon-Wed (3 Days) <br><small>&nbsp; <br> Rs. 14000/- <a href="#" class="" data-toggle="modal" data-target="#demoPopUp">Enroll Now</a></small></b></p></div>
                                                    
                                                      </div>
                                                  </div>
                                              </div>
                                              <div class="col-md-3">
                                                  <div class="col-md-3">
                                                      <div class="row">
                                                          <p id ="date-time">27 <br><span id="month">NOV</span></p>
                                                      </div>
                                                  </div>
                                                  <div class="col-md-9" id="upcomming-batch">
                                                      <div class="row">
                                                          <div class="col-md-12"><p id="dt-content"><b> Mon-Wed (3 Days) <br><small>&nbsp; <br> Rs. 14000/- <a href="#" class="" data-toggle="modal" data-target="#demoPopUp">Enroll Now</a></small></b></p></div>
                                                    
                                                      </div>
                                                  </div>
                                              </div>
                                              <div class="col-md-3">
                                                  <div class="col-md-3">
                                                      <div class="row">
                                                          <p id ="date-time">30 <br><span id="month">NOV</span></p>
                                                      </div>
                                                  </div>
                                                  <div class="col-md-9" id="upcomming-batch">
                                                      <div class="row">
                                                          <div class="col-md-12"><p id="dt-content"><b> Mon-Wed (3 Days) <br><small>&nbsp; <br> Rs. 14000/- <a href="#" class="" data-toggle="modal" data-target="#demoPopUp">Enroll Now</a></small></b></p></div>
                                                    
                                                      </div>
                                                  </div>
                                              </div>
                                          </div>
                                      </div>
				</div>
                            
			</section>
                          <section id="coursed-row2" class="object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
                            <div class="container">
                                
                                 <h3 class="text-center heading-font object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100" style="text-transform:none;"> <strong>Course</strong> Details</h3>
                                 <div id="vseparator" class="object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
                                     <div class="vhr"></div>
                                </div>
                                <div class="row col-md-9" >
                               <!-- tabs start -->
                                <!-- ================ -->
                                <!-- Nav tabs -->
                                <ul class="nav nav-tabs style-3" id="tab-course" role="tablist">
                                    <li class="active"><a href="#h3tab1" role="tab" data-toggle="tab">Overview</a></li>
                                    <li><a href="#h3tab2" role="tab" data-toggle="tab">Course Curriculum</a></li>
                                    <li><a href="#h3tab3" role="tab" data-toggle="tab">FAQ's</a></li>
                                    <li><a href="#h3tab4" role="tab" data-toggle="tab">Download Course Outline</a></li>
                                    <li><a href="#h3tab5" role="tab" data-toggle="tab">Reviews</a></li>
                                   
                                </ul>
                                <!-- Tab panes -->
                                <div class="tab-content">
                                    <div class="tab-pane in active " id="h3tab1">
                                        <div class="row">
                                           <div class="col-md-12 object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
                                              <h2 class="heading-font">Who is data scientist?</h2>
                                                <p id="cpara">
                                                 Data Scientist is a person who analyses entire accumulated data from various sources, across various machines concerned with the subject or a product in order to provide suitable insights to his company. The growth of the company depends upon the strategical plans drawn by forecasting marketing changes depending upon the insights provided by their data science. The study of the accumulated data across from all the sources, consumer network, competitors’ information, sales records, brand image inputs, consumer reviews, political conditions, financial conditions, everything related to the product defines specific status of the product in the market, thus enable the data scientist to provide suitable analysis report to his company. Information explosion in these days due to the invention of the internet made it extremely necessary for the business organizations to equip themselves with an expert and efficient data analyst in their organization to thrive ahead.
                                                </p>
                                                <h2 class="heading-font">Why it is necessary to learn Data Science?</h2>
                                                <p id="cpara">
                                                It is necessary to learn the data science is more than ever in the present business scenario. The need to fill the void for an able data scientist is necessary ever than before. Gone are the days of brick and mortar business. Present business need to have powerful tools to meet the global competition. Unknown challenges, unexpected changes in political and economic status in the global market need to be met suitably by effective business strategies. It is necessary to learn the data science for the following reasons explained below for the benefit of those who wish to take up this course.
                                                </p>
                                                <p id="cpara">
                                                Connect with consumers effectively, knowing them directly help to develop higher end products that are beneficial both the ways.
                                                </p>
                                              <ul class="list-icons" id="clist">
                                                    <li><i class="icon-check pr-10"></i> De-redesign the products to meet consumer expectations</li>
                                                    <li><i class="icon-check pr-10"></i>Allows performing risk analysis</li>
                                                    <li><i class="icon-check pr-10"></i>Helps to keep your data safe and secure</li>
                                                    <li><i class="icon-check pr-10"></i> Explore new revenues based on the data analysis</li>
                                                    <li><i class="icon-check pr-10"></i> Customize all the transactions by making real time analysis</li>
                                                    <li><i class="icon-check pr-10"></i> Build Spark Applications using Java, Python and Scala</li>
                                                    <li><i class="icon-check pr-10"></i> By applying suitable strategies, it will allow organizations to reduce maintenance cost</li>
                                                    <li><i class="icon-check pr-10"></i> Allows business organizations to get wider prospective of the concerned business</li>
                                                </ul>
                                                 <p id="cpara">
                                                Connect with consumers effectively, knowing them directly help to develop higher end products that are beneficial both the ways.
                                                </p>  
                                                <h2 class="heading-font">Who is best fit to take up Data Science Training Online ? </h2>
                                                <ul class="list-icons" id="clist">
                                                    <li><i class="icon-check pr-10"></i> IT professionals</li>
                                                    <li><i class="icon-check pr-10"></i> Business Analysts</li>
                                                    <li><i class="icon-check pr-10"></i> Data Analysts</li>
                                                    <li><i class="icon-check pr-10"></i> Ware house managers</li>
                                                    <li><i class="icon-check pr-10"></i> Application Developers</li>
                                                    <li><i class="icon-check pr-10"></i> Job seekers in IT industry</li>
                                                    <li><i class="icon-check pr-10"></i> Business operators</li>
                                                </ul>
                                                <h2 class="heading-font">What are the course objectives of Data Science?</h2>
                                                <ul class="list-icons" id="clist">
                                                    <li><i class="icon-check pr-10"></i> Learners are taught to understand business intelligence and business analytics</li>
                                                    <li><i class="icon-check pr-10"></i> To understand the business data analysis through the powerful tools of data application</li>
                                                    <li><i class="icon-check pr-10"></i> Learn how to apply Tableau, MapReduce, and get introduced in to R and R+</li>
                                                    <li><i class="icon-check pr-10"></i> Understand the methods of data mining and creation of decision tree</li>
                                                    <li><i class="icon-check pr-10"></i> Explore different aspects of Big Data Technologies</li>
                                                    <li><i class="icon-check pr-10"></i> Learn the concepts of loop functions and debugging tools</li>
                                                   
                                               </ul>
                                              
                                                <div class="separator"></div>
                                                <h2 class="heading-font">What are the pre-requisites for the course?</h2>
                                                <p id ="cpara">Learners should have an aptitude for analytical thinking. It is necessary to have quantitative skills. Technical background and programming knowledge will be an added advantage to join this course. Anybody who is interested in the field of data analytics can join the course to hone their skills.</p>
                                            
                                            </div>
                                                
                                        </div>
                                            
                                    </div>
                                    <div class="tab-pane" id="h3tab2">
                                        <div class="row">
                                            <div class="col-md-12 ">
                                                  <div class="panel-group collapse-style-2" id="accordion-2">
								<div class="panel panel-default">
									<div class="panel-heading">
										<h4 class="panel-title">
											<a data-toggle="collapse" data-parent="#accordion-2" href="#collapseOne-2">
												<i class="fa fa-arrow-right pr-10"></i>Module 1
											</a>
										</h4>
									</div>
                                                                    <div id="collapseOne-2" class="panel-collapse collapse in">
                                                                        <div class="panel-body">
                                                                            <h3 class="heading-font">Introduction</h3>
                                                                            <p id="cpara"><b>Learning Objectives </b>– Data scientist certification in India module one starts with the basics of Business Analytics, R basic, R programming, R’s role in solving analytical problems, R’s popularity in tech giants like- Facebook, Google, Finance etc.</p>
                                                                            <p id="cpara"><b>Topics </b> – Business Analytics, R, R language and programming, Ecosystem, Uses of R, Data types in R, subsetting methods, R comparison with other software’s, R installation, operations in R, useful packages, IDER, GUI, using functions like- length(), str(), ncol() etc, summarize data.</p>
                                                                        </div>
                                                                    </div>
								</div>
								<div class="panel panel-default">
									<div class="panel-heading">
										<h4 class="panel-title">
											<a data-toggle="collapse" data-parent="#accordion-2" href="#collapseTwo-2" class="collapsed">
												<i class="fa fa-arrow-right pr-10"></i>Module 2
											</a>
										</h4>
									</div>
									<div id="collapseTwo-2" class="panel-collapse collapse">
										<div class="panel-body">
                                                                                    <h3 class="heading-font">Data Manipulation and Data Import Techniques in R</h3>
                                                                                    <p id="cpara"><b>Learning Objectives </b> – In this module, we will discuss about dirty data set, data cleaning, which results in data set, and ready for analysis, exploring functions, versatility of R, robustness of R. This module helps you to understand various importing techniques in R</p>
                                                                                    <p id="cpara"><b>Topics </b> – Data Cleaning, Data Inspection, Troubleshooting the problems, Function uses, grepl(), sub(), grep(), Data coerce, apply() functions uses, Import data from spreadsheets into R, Importing text files into R, import data, installation of packages. RDBMS from R, using ODBC, SQL queries, Web Scraping.</p>
                                                                                        
                                                                                </div>
									</div>
								</div>
                                                      <div class="panel panel-default">
                                                          <div class="panel-heading">
                                                              <h4 class="panel-title">
                                                                  <a data-toggle="collapse" data-parent="#accordion-2" href="#collapseThree-2" class="collapsed">
                                                                      <i class="fa fa-arrow-right pr-10"></i>Module 3 
                                                                  </a>
                                                              </h4>
                                                          </div>
                                                          <div id="collapseThree-2" class="panel-collapse collapse">
                                                              <div class="panel-body">
                                                                  <h3 class="heading-font">Exploratory Data Analysis</h3>
                                                                  <p id="cpara"><b>Learning Objectives </b>– Data scientist certification in Bangalore module 3 includes information about exploratory data analysis, EDA- for observing what data tell us beyond the formal hypothesis. Typical EDA process.</p>
                                                                  <p id="cpara"><b>Topics </b> – Exploratory Data Analysis, Implementing EDA on datasets, Box plots, cor() in R, EDA functions, list(), summarize(), Multiple packages in R, various plots, Segment plot, HC plot in R.</p>
                                                                      
                                                              </div>
                                                          </div>
                                                      </div>
                                                      <div class="panel panel-default">
                                                          <div class="panel-heading">
                                                              <h4 class="panel-title">
                                                                  <a data-toggle="collapse" data-parent="#accordion-2" href="#collapseFour-2" class="collapsed">
                                                                      <i class="fa fa-arrow-right pr-10"></i>Module 4
                                                                  </a>
                                                              </h4>
                                                          </div>
                                                          <div id="collapseFour-2" class="panel-collapse collapse">
                                                              <div class="panel-body">
                                                                  <h3 class="heading-font">Data Visualization in R</h3>
                                                                  <p id="cpara"><b>Learning Objectives </b>– Leaning Objectives – In this module you will learn about- visualization in USP of R, creating simple visualization in R, complex visualization in R.</p>
                                                                  <p id="cpara"><b>Topics </b> – Data Visualization, Graphical representations, Functions in R, Plotting graphs like box plot, table plot and histogram, Improvising plots, GUIs, Deducer, R Commander and Spatial Analysis.</p>
                                                                      
                                                              </div>
                                                          </div>
                                                      </div>
                                                      <div class="panel panel-default">
                                                          <div class="panel-heading">
                                                              <h4 class="panel-title">
                                                                  <a data-toggle="collapse" data-parent="#accordion-2" href="#collapseFive-2" class="collapsed">
                                                                      <i class="fa fa-arrow-right pr-10"></i>Module 5
                                                                  </a>
                                                              </h4>
                                                          </div>
                                                          <div id="collapseFive-2" class="panel-collapse collapse">
                                                              <div class="panel-body">
                                                                  <h3 class="heading-font">Data Mining: Clustering Techniques, Decision Trees and Random Forest</h3>
                                                                  <p id="cpara"><b>Learning Objectives </b>–  In this module we will discuss about various Machine Learning algorithms, Machine Learning types- Unsupervised Learning and Supervised Learning, Difference between Unsupervised Learning and Supervised Learning. We will also learn about K-means Clustering and implementing them, Concepts of Decision trees and Random forest, Algorithm creation of forests and trees, LMS.</p>
                                                                  <p id="cpara"><b>Topics </b> – Data Mining, Machine Learning algorithms, Unsupervised and Supervised Machine, K-means clustering, Decision Trees, Random forest Algorithm creation, Entropy, features and working of Random forest.</p>
                                                             </div>
                                                          </div>
                                                      </div>
                                                      <div class="panel panel-default">
                                                          <div class="panel-heading">
                                                              <h4 class="panel-title">
                                                                  <a data-toggle="collapse" data-parent="#accordion-2" href="#collapseSix-2" class="collapsed">
                                                                      <i class="fa fa-arrow-right pr-10"></i>Module 6
                                                                  </a>
                                                              </h4>
                                                          </div>
                                                          <div id="collapseSix-2" class="panel-collapse collapse">
                                                              <div class="panel-body">
                                                                  <h3 class="heading-font">Linear and Logistic Regression</h3>
                                                                  <p id="cpara"><b>Learning Objectives </b>– Data scientist training in Bangalore, module 6, we will introduce- Regression Techniques, Linear and logistic regression basics, implementation in R.</p>
                                                                  <p id="cpara"><b>Topics </b> – Simple and Multiple Linear Regressions, Simple and Multiple Logistic Regression, Log Linear Model.</p>
                                                                      
                                                              </div>
                                                          </div>
                                                      </div>
                                                       <div class="panel panel-default">
                                                          <div class="panel-heading">
                                                              <h4 class="panel-title">
                                                                  <a data-toggle="collapse" data-parent="#accordion-2" href="#collapseSeven-2" class="collapsed">
                                                                      <i class="fa fa-arrow-right pr-10"></i>Module 7
                                                                  </a>
                                                              </h4>
                                                          </div>
                                                          <div id="collapseSeven-2" class="panel-collapse collapse">
                                                              <div class="panel-body">
                                                                  <h3 class="heading-font">Anova and Predictive Analysis</h3>
                                                                  <p id="cpara"><b>Learning Objectives </b>–  In this module we will discuss about the Analysis of Variance, Anova Technique, Predictive Analysis.</p>
                                                                  <p id="cpara"><b>Topics </b> – Anova Technique, Sample testing, One and Two way Anova, Predictive Analysis.</p>
                                                                      
                                                              </div>
                                                          </div>
                                                      </div>
                                                       <div class="panel panel-default">
                                                          <div class="panel-heading">
                                                              <h4 class="panel-title">
                                                                  <a data-toggle="collapse" data-parent="#accordion-2" href="#collapseEight-2" class="collapsed">
                                                                      <i class="fa fa-arrow-right pr-10"></i>Module 8
                                                                  </a>
                                                              </h4>
                                                          </div>
                                                          <div id="collapseEight-2" class="panel-collapse collapse">
                                                              <div class="panel-body">
                                                                  <h3 class="heading-font">Project Work:</h3>
                                                                  <p id="cpara">You will be given a problem statement and in this module our qualified professionals will help you on that statement.</p>
                                                                  <p id="cpara">Perform Exploratory Data Analysis</p>
                                                                  <p id="cpara">Establish hypothesis of data.</p>
                                                                  <ul class="list-icons" id="clist">
                                                                      <li><i class="icon-check pr-10"></i> Handle outliers</li>
                                                                      <li><i class="icon-check pr-10"></i> Troubleshooting missing data.</li>
                                                                      <li><i class="icon-check pr-10"></i> Validation datasets.</li>
                                                                      <li><i class="icon-check pr-10"></i> Logistic Regression</li>
                                                                      <li><i class="icon-check pr-10"></i> Confusion Matrix</li>
                                                                      <li><i class="icon-check pr-10"></i> Final model</li>
                                                                          
                                                                  </ul>
                                                              </div>
                                                          </div>
                                                      </div>
                                                    </div>
                                                
                                                </div>

                                        </div>
                                       
                                    </div>
                                    <div class="tab-pane" id="h3tab3">
                                        <div class="row">
                                            <div class="col-md-12 ">
                                                <div class="panel-group collapse-style-3" id="accordion-3">
                                                    <div class="panel panel-default">
                                                        <div class="panel-heading">
                                                            <h4 class="panel-title">
                                                                <a data-toggle="collapse" data-parent="#accordion-3" href="#collapseOne-3">
                                                                        Who are the Instructors?            
                                                                </a>
                                                            </h4>
                                                        </div>
                                                        <div id="collapseOne-3" class="panel-collapse collapse in">
                                                            <div class="panel-body">
                                                                <p id="cpara" class="text-justify">
                                                                    All our instructors are working professionals from the Industry and have at least 5-8 yrs of relevant experience in various domains. They are subject matter experts and are trained by Prwatech for providing online training so that participants get a great learning experience.
                                                                </p>
                                                                
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="panel panel-default">
                                                        <div class="panel-heading">
                                                            <h4 class="panel-title">
                                                                <a data-toggle="collapse" data-parent="#accordion-3" href="#collapseTwo-3" class="collapsed">
                                                                       How will I execute the Practicals?   
                                                                </a>
                                                            </h4>
                                                        </div>
                                                        <div id="collapseTwo-3" class="panel-collapse collapse">
                                                            <div class="panel-body">
                                                                <p id="cpara" class="text-justify">
                                                                       For your practical work, we will help you set up a Virtual Machine on your system with IDE for Scala. This will be local access for you. The detailed step-wise installation guides are present in your LMS which will help you to install and set-up the environment for Spark and Scala. In case you come across any doubt, the 24*7 support team will promptly assist you.            
                                                                </p>
                                                                                        
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="panel panel-default">
                                                        <div class="panel-heading">
                                                            <h4 class="panel-title">
                                                                <a data-toggle="collapse" data-parent="#accordion-3" href="#collapseThree-3" class="collapsed">
                                                                     What if I miss a class?       
                                                                </a>
                                                            </h4>
                                                        </div>
                                                        <div id="collapseThree-3" class="panel-collapse collapse">
                                                            <div class="panel-body">
                                                                <p id="cpara" class="text-justify">
                                                                    You will never lose any lecture. You can choose either of the two options:
                                                                    1. View the recorded session of the class available in your LMS.
                                                                    2. You can attend the missed session, in any other live batch.            
                                                                </p>
                                                                                        
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="panel panel-default">
                                                        <div class="panel-heading">
                                                            <h4 class="panel-title">
                                                                <a data-toggle="collapse" data-parent="#accordion-3" href="#collapseFour-3" class="collapsed">
                                                                    What if I have queries after I complete this course?    
                                                                </a>
                                                            </h4>
                                                        </div>
                                                        <div id="collapseFour-3" class="panel-collapse collapse">
                                                            <div class="panel-body">
                                                                <p id="cpara" class="text-justify">
                                                                    Yes, you can learn Hadoop without being from a software background. We provide complimentary courses in Java and Linux so that you can brush up on your programming skills. This will help you in learning Hadoop technologies better and faster.
                                                                </p>
                                                                                        
                                                            </div>
                                                        </div>
                                                    </div>
                                                  
                                                    <div class="panel panel-default">
                                                        <div class="panel-heading">
                                                            <h4 class="panel-title">
                                                                <a data-toggle="collapse" data-parent="#accordion-3" href="#collapseSix-3" class="collapsed">
                                                                    Do You Provide Placement Assistance?
                                                                </a>
                                                            </h4>
                                                        </div>
                                                        <div id="collapseSix-3" class="panel-collapse collapse">
                                                            <div class="panel-body">
                                                                <p id="cpara" class="text-justify">
                                                                    Yes, Prwatech does provide you with placement assistance. We have tie-ups with 80+ organizations including Ericsson, Cisco, Cognizant, TCS, among others that are looking for Hadoop professionals and we would be happy to assist you with the process of preparing yourself for the interview and the job.
                                                                </p>
                                                                                        
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                            
                                            </div>
                                                    
                                        </div>
                                            
                                    </div>
                                    <div class="tab-pane" id="h3tab4">
                                        <div class="row object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
                                            <div class="col-md-4">
                                                <p id="cpara" class="text-default"><a href="">Click Here to Download</a></p>
                                                
                                            </div>
                                            <div class="col-md-8">
                                                 <h4 class="title text-default" class="heading-font">Services</h4>
                                                <ul class="list-icons">
                                                    <li><i class="icon-check pr-10"></i> PowerPoint Presentation covering all classes</li>
                                                    <li><i class="icon-check pr-10"></i> Recorded Videos Sessions On Bigdata and Hadoop with LMS Access.<br>
                                                                       &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(lifetime support)</li>
                                                    <li><i class="icon-check pr-10"></i> Quiz , Assignment & POC.</li>
                                                    <li><i class="icon-check pr-10"></i> On Demand Online Support .</li>
                                                    <li><i class="icon-check pr-10"></i> Discussion Forum.</li>
                                                    <li><i class="icon-check pr-10"></i> Material
                                                        <ul class="list-icons" style="margin-left:50px;">
                                                            <li>a. Sample Question papers of Cloudera Certification.</li>
                                                            <li>b. Technical Notes & Study Material.</li>
                                                        </ul>
                                                    
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        
                                    </div>
                                    <div class="tab-pane" id="h3tab5">
                                        <div class="row object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
                                            <div class="col-md-6 ">
                                                <div class="pv-20 ph-20 feature-box-2 boxed shadow object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
                                                    <div class="col-md-3 testimonial-image"><img src="../images/testimonial-2.jpg" /></div> 
                                                    <div class="body">
                                                        <h4 class="title">Anupam Khamparia 
                                                            <br>
                                                            <span style="font-size:11px;">Consultant, Cognizant Technology Solutions, Bangalore</span>
                                                        </h4>
                                                        <p class ="text-justify">“Excellent course and instructor. I learnt a lot in a short period. Good value for money. Instructor took us through Advanced Hadoop Development in depth.”</p>
                                                       
                                                    </div>
                                                </div>
                                            </div>
                                          <div class="col-md-6 ">
                                                <div class="pv-20 ph-20 feature-box-2 boxed shadow object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
                                                    <div class="col-md-3 testimonial-image"><img src="../images/testimonial-1.png" /></div> 
                                                    <div class="body">
                                                        <h4 class="title">Anukanksha Garg
                                                          <br>
                                                            <span style="font-size:11px;">B.Tech. CSE</span>
                                                        </h4>
                                                        <p class ="text-justify">It was a nice learning experience with prwatech. The classes were well scheduled and managed.
Verma has good understanding of the topics taught and catered to our problems and doubts very patiently. The best thing about him was that he handled the situations accordingly, when needed a friend, he became one and also a teacher to always guide us.</p>
                                                       
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6 ">
                                                <div class="pv-20 ph-20 feature-box-2 boxed shadow object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
                                                    <div class="col-md-3 testimonial-image"><img src="../images/testimonial-4.PNG" /></div> 
                                                    <div class="body">
                                                        <h4 class="title">Varun Shivashanmugum
                                                        
                                                        <br>
                                                            <span style="font-size:11px;">Associate Consultant, ITC Infotech Ltd</span>
                                                        </h4>
                                                        
                                                        <p class ="text-justify">“Faculty is good,Verma takes keen interest and personnel care in improvising skills in students. And most importantly,Verma will be available and clears doubts at any time apart from class hours. And he always keeps boosting and trying to increase confidence in his students which adds extra attribute to him and organization as well. and organization as well.</p>
                                                       
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6 ">
                                                <div class="pv-20 ph-20 feature-box-2 boxed shadow object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
                                                    <div class="col-md-3 testimonial-image"><img src="../images/testimonial-5.jpg" /></div> 
                                                    <div class="body">
                                                        <h4 class="title">Mayank Srivastava <br>
                                                        <span style="font-size:11px;">Hadoop Developer, L&T, Bangalore</span>
                                                        </h4>
                                                        <p class ="text-justify">“Really good course content and labs, patient enthusiastic instructor. Good instructor, with in depth skills…Very relevant practicals allowed me to put theory into practice.”</p>
                                                       
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                                 <div class="col-md-3" style="position:relative;top:-10px;">
                                     <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center" >
                                         <div class="overlay-container">
                                             <img src="../images/icon/curse-d1.png" alt="">
                                         </div>
                                         <div class="body">
                                              <h3 class="heading-font">Rs. 14,000 + Tax </h3>
                                             <span class="small-font">per 2 weeks</span>
                                             <div class="separator"></div>
                                             <p class="heading-font"></p>
                                             <table class="table table-striped ">
                                                 <tbody>
                                                     <tr><th><center>35 Hours</center></th></tr>
                                                 <tr><th><center>15 Seats</center></th></tr>
                                                 <tr><th><center>Course Badge</center></th></tr>
                                                 <tr><th><center>Course Certificate</center></th></tr>
                                                 <tr><th><center> <a  onclick="openNav()" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Enroll Now<i class="fa fa-arrow-right pl-10"></i></a></center></th></tr>
                                                 </tbody>
                                             </table>
                                            <?php include '../includes/demo-popup.php';?>
                                           
                                         </div>
                                     </div>
                                      <h3 class="heading-font text-center" style="background:#4c4c4c;color:white;padding-top:10px;padding-bottom:10px">Suggested Courses</h3> 
                                      <div class="owl-carousel content-slider">
                                          <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
                                              <div class="overlay-container">
                                                  <img src="../images/por-1.png" alt="">
                                                  <div class="overlay-to-top">
                                                      <p class="small margin-clear">&nbsp;</p>
                                                  </div>
                                              </div>
                                              <div class="body">
                                                  <h3 class="heading-font">Big Data Hadoop Training</h3>
                                                  <div class="separator"></div>
                                                  <p class="heading-font">Advanced Program for Top-notch Developers </p>
                                                  <a href="big-data-hadoop-training.php"  target="_blank" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read More<i class="fa fa-arrow-right pl-10"></i></a>												
                                              </div>
                                          </div>
                                          <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
                                              <div class="overlay-container">
                                                  <img src="../images/por-2.png" alt="">
                                                  <div class="overlay-to-top">
                                                      <p class="small margin-clear">&nbsp;</p>
                                                  </div>
                                              </div>
                                              <div class="body">
                                                  <h3 class="heading-font">Hadoop Admin Training</h3>
                                                  <div class="separator"></div>
                                                  <p class="heading-font">Advanced Program for Top-notch Developers </p>
                                                  <a href="hadoop-admin.php" target="_blank" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read More<i class="fa fa-arrow-right pl-10"></i></a>												
                                              </div>
                                          </div>
                                          <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
                                              <div class="overlay-container">
                                                  <img src="../images/por-3.png" alt="">
                                                  <div class="overlay-to-top">
                                                      <p class="small margin-clear">&nbsp;</p>
                                                  </div>
                                              </div>
                                              <div class="body">
                                                  <h3 class="heading-font">Apache Spark Scala Training</h3>
                                                  <div class="separator"></div>
                                                  <p class="heading-font">Apache Spark Scala Combo Training </p>
                                                  <a href="apache-spark-scala-training.php" target="_blank" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read More<i class="fa fa-arrow-right pl-10"></i></a>												
                                              </div>
                                          </div>
                                          <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
                                              <div class="overlay-container">
                                                  <img src="../images/por-4.png" alt="">
                                                  <div class="overlay-to-top">
                                                      <p class="small margin-clear">&nbsp;</p>
                                                  </div>
                                              </div>
                                              <div class="body">
                                                  <h3 class="heading-font">Data Science Training</h3>
                                                  <div class="separator"></div>
                                                  <p class="heading-font">Find out the truth about what Data Science is. </p>
                                                  <a href="data-science-course-training-bangalore.php" target="_blank" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read More<i class="fa fa-arrow-right pl-10"></i></a>												
                                              </div>
                                          </div>
                                          <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
                                              <div class="overlay-container">
                                                  <img src="../images/por-5.png" alt="">
                                                  <div class="overlay-to-top">
                                                      <p class="small margin-clear">&nbsp;</p>
                                                  </div>
                                              </div>
                                              <div class="body">
                                                  <h3 class="heading-font">R Programming Training</h3>
                                                  <div class="separator"></div>
                                                  <p class="heading-font">Business Analytics With R Training</p>
                                                  <a href="r-programing.php" target="_blank" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read More<i class="fa fa-arrow-right pl-10"></i></a>												
                                              </div>
                                          </div>
                                          <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
                                              <div class="overlay-container">
                                                  <img src="../images/por-6.png" alt="">
                                                  <div class="overlay-to-top">
                                                      <p class="small margin-clear">&nbsp;</p>
                                                  </div>
                                              </div>
                                              <div class="body">
                                                  <h3 class="heading-font"> Tableau Training & Certification</h3>
                                                  <div class="separator"></div>
                                                  <p class="heading-font">Speed on concepts of data visualization</p>
                                                  <a href="tableau.php" target="_blank" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read More<i class="fa fa-arrow-right pl-10"></i></a>												
                                              </div>
                                          </div>
                                          <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
                                              <div class="overlay-container">
                                                  <img src="../images/por-7.png" alt="">
                                                  <div class="overlay-to-top">
                                                      <p class="small margin-clear">&nbsp;</p>
                                                  </div>
                                              </div>
                                              <div class="body">
                                                  <h3 class="heading-font">Python Programming</h3>
                                                  <div class="separator"></div>
                                                  <p class="heading-font">Advanced Concepts for Top-notch Developers  </p>
                                                  <a href="python.php" target="_blank" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read More<i class="fa fa-arrow-right pl-10"></i></a>												
                                              </div>
                                          </div>
                                      </div> 
                                      <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center" >
                                         <div class="body">
                                             <div itemscope itemtype="http://schema.org/Product"style="text-align: left">
                                                    <span itemprop="name">Data Science Course</span><br>
                                                    <div itemprop="aggregateRating" itemscope itemtype="http://schema.org/AggregateRating">
                                                        Average Rating: <span itemprop="ratingValue">4.74</span><br>
                                                        Votes: <span itemprop="ratingCount">856</span><br>
                                                        Reviews: <span itemprop="reviewCount">856</span>
                                                    </div>

                                            </div>
                                         </div>
                                     </div>
                                 </div> 
                            </div> 
                           
                           
                            <p>&nbsp;</p>
                            
                        </section>
                      
                        
			<!-- section end -->
                    
			<div class="clearfix"></div>
			
			<!-- ================ -->
			
			<!-- section end -->
                        <section class="pv-30 dark-bg padding-bottom-clear clearfix object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
					<br>
					<div class="container">
						<div class="row">
							<div class="col-md-3 ">
								<div class="ph-20 feature-box text-center object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
									<span class="icon default-bg circle"><i class="fa fa-institution"></i></span>
									<h3>Live classes</h3>
									<div class="separator clearfix"></div>
                                                                        <p class="heading-font ">Live online and interactive classes conducted by instructor</p>
									
								</div>
							</div>
							<div class="col-md-3 ">
								<div class="ph-20 feature-box text-center object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
									<span class="icon default-bg circle"><i class="fa fa-user"></i></span>
									<h3>Expert instructions</h3>
									<div class="separator clearfix"></div>
									<p class="heading-font">Learn from our Experts and get Real-Time Guidance</p>
									
								</div>
							</div>
							<div class="col-md-3 ">
								<div class="ph-20 feature-box text-center object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
									<span class="icon default-bg circle"><i class="icon-signal"></i></span>
									<h3>24 X 7 Support</h3>
									<div class="separator clearfix"></div>
									<p class="heading-font">Personalized Guidance from our 24X7 Support Team</p>
									
								</div>
							</div>
							<div class="col-md-3 ">
								<div class="ph-20 feature-box text-center object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
									<span class="icon default-bg circle"><i class="icon-calendar"></i></span>
									<h3>Flexible schedule</h3>
									<div class="separator clearfix"></div>
									<p class="heading-font">Reschedule your Batch/Class at Your Convenience</p>
									
								</div>
							</div>
						</div>
					</div>
					<br>
				</section>
			
			<!-- section end -->

			<!-- section -->
			<!-- ================ -->
			 <section class="pv-30   padding-bottom-clear dark-translucent-bg parallax padding-bottom-clear" style="background: url('../images/bg/451.jpg') 50% -9px; box-shadow: rgba(0, 0, 0, 0.247059) 0px 2px 7px inset;">
				
				<div class="space-bottom">
					
					<div class="owl-carousel content-slider">
						<div class="container">
							<div class="row">
								<div class="col-md-8 col-md-offset-2">
									<div class="testimonial text-center">
										<div class="testimonial-image">
                                                                                    <img src="../images/testimonial-1.png" alt="" title="" class="img-circle">
										</div>
										
										<div class="testimonial-body">
                                                                                    <p>&nbsp;</p>
												<p id="cpara">It was a nice learning experience with prwatech. The classes were well scheduled and managed.
Verma has good understanding of the topics taught and catered to our problems and doubts very patiently. The best thing about him was that he handled the situations accordingly, when needed a friend, he became one and also a teacher to always guide us.</p>
											
											<div class="testimonial-info-1">Anukanksha Garg</div>
											<div class="testimonial-info-2">B.TECH- CS</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="container">
							<div class="row">
								<div class="col-md-8 col-md-offset-2">
									<div class="testimonial text-center">
										<div class="testimonial-image">
											<img src="../images/testimonial-2.jpg" alt="" title="" class="img-circle">
										</div>
										
										<div class="testimonial-body">
											 <p>&nbsp;</p>
												<p id="cpara">Excellent course and instructor. I learnt a lot in a short period. Good value for money. Instructor took us through Advanced Hadoop Development in depth.</p>
											
											<div class="testimonial-info-1">Anupam Khamparia</div>
											<div class="testimonial-info-2">Consultant, Cognizant Technology Solutions, Bangalore</div>
										</div>
									</div>
								</div>
							</div>
						</div>
                                            <div class="container">
							<div class="row">
								<div class="col-md-8 col-md-offset-2">
									<div class="testimonial text-center">
										<div class="testimonial-image">
											<img src="../images/testimonial-3.jpg" alt="" title="" class="img-circle">
										</div>
										
										<div class="testimonial-body">
											<p>&nbsp;</p>
												<p id="cpara">Excellent course and instructor. I learnt a lot in a short period. Good value for money. Instructor took us through Advanced Hadoop Development in depth.</p>
											<div class="testimonial-info-1">Kumar Waibhav</div>
                                                                                        <div class="testimonial-info-2">Software Engineer, Cognizant Technology Solutions,<br> Philadelphia, Pennsylvania, USA</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					
				</div>
			</section>
			<!-- section end -->

			<!-- section start -->
			<!-- ================ -->
                        
			                   
			<!-- section end -->
			<!-- footer start (Add "dark" class to #footer in order to enable dark footer) -->
			<!-- ================ -->
			<?php include './../includes/footer.php'; ?>
			<!-- footer end -->
			
		</div>
		<!-- page-wrapper end -->

		<?php include './../includes/jslinks.php';?>
		<!-- Color Switcher End -->
                <?php include './../includes/quick-query.php';?>
                <?php include './../includes/UserSignup.php';?>
	</body>
        <script>
            $(document).ready(function(){
                onlad(4);
            });  
        </script>


</html>
