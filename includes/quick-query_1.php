<style>
    .sidenav {
        height:100%;
        width: 20%;
        position: fixed;
        z-index:99999;
        top:0;
        right: 0;
        background-color: #111;
        overflow-x: hidden;
        transition: 0.5s;
        padding-top: 60px;
        
    }
    
    .sidenav a {
        padding: 8px 8px 8px 32px;
        text-decoration: none;
        font-size: 25px;
        color: #818181;
        display: block;
        transition: 0.3s
    }
    
    .sidenav a:hover, .offcanvas a:focus{
        color: #f1f1f1;
    }
    
    .sidenav .closebtn {
        position: absolute;
        top: 0;
        right: 25px;
        font-size: 36px;
        margin-left: 50px;
    }
    
    @media screen and (max-height: 450px) {
        .sidenav {padding-top: 15px;}
        .sidenav a {font-size: 18px;}
    }
    #quick-query{
        font-family:century gothic;
        font-weight:bold;
      
    }
</style>
</style>
<button class="btn btn-default" style=" position:fixed;top:200px;right:0px;box-shadow:-5px 3px 2px 0 rgba(0,0,0,0.2);font-family:century gothic;color:white;font-size:16px" onclick="openNav()"><b>Q<br>u<br>e<br>r<br>y</b></button>
<div id="mySidenav" class="sidenav" style="display:none">
    <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
    <h4 class="heading-font" style="margin-left:30px;margin-bottom:0px;color:white;font-weight:bold">Drop a Query</h4>
    <form role="form" class="p-30" method="post" id="quick-query">
        <div class="form-group">
            <input type="text" class="form-control" id="name" name="name" placeholder="Name">
            <input type="hidden" class="form-control" id="course_id" name="course_id" placeholder="Name">
        </div>
        <div class="form-group">
            <input type="text" class="form-control" id="email" name="email" placeholder="Email">
        </div>
        <div class="form-group">
            
            <input type="text" class="form-control" id="contact" name="contact" placeholder="Contact">
        </div>
        <div class="form-group">
            
            <input type="text" class="form-control" id="subject" name="subject" placeholder="Subject">
        </div>
        
        <div class="form-group">
            
            <textarea class="form-control" rows="3" id="message" name="message" placeholder="Message"></textarea>
        </div>
        <input type="submit" name="submit" id="submit" class="btn btn-default" value="Send"/>
    </form>
   

<!-- Modal -->
<div id="warningmsg" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
        <div class="modal-header" style="background:orangered;">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Warning</h4>
      </div>
      <div class="modal-body" id="errormsg">
          <p id="msg" style="color:green;font-family:century gothic;font-size:18px;font-weight:bold"></p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

</div> 



<div id="enquirysuccess" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
        <div class="modal-header" style="background:green;">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Success</h4>
      </div>
      <div class="modal-body" id="errormsg">
          <p id="msg" style="color:gray;font-family:verdana;font-size:14px;line-height:22px">Thank you for getting in touch! We appreciate you contacting us. We try to respond as soon as possible. Have a great day ahead!</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
<script>
    function openNav() {
        document.getElementById("mySidenav").style.display = "block";
        document.getElementById("mySidenav").style.width = "300px";
    }
    
    function closeNav() {
        document.getElementById("mySidenav").style.width = "0";
    }
    

</script>  
<script type="text/javascript" src="ajax/QuickQuery_1.js"></script>
<!-- Trigger the modal with a button -->
