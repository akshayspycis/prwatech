<section class="section parallax background-img-3 dark-translucent-bg">
					<div class="container">
						<div class="row">
							<div class="col-md-12">
								<div class="call-to-action text-center">
									<div class="row">
										<div class="col-sm-8">
											<h1 class="title " >Build your Skills. Rediscover Learning!</h1>
											<p class="heading-font">Live online and interactive classes conducted by instructors.</p>
										</div>
										<div class="col-sm-4">
											<br>
											<p><a href="#" class="btn btn-lg btn-gray-transparent btn-animated" data-toggle="modal" data-target="#contactPopUp" >Drop a Query<i class="fa fa-dedent pl-20"></i></a></p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
</section>
<div class="modal fade" id="contactPopUp" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel">Drop a Query</h4>
            </div>
            <div class="modal-body">
                <form role="form">
                    <div class="form-group">
                        <label for="name">Name</label>
                        <input type="text" class="form-control" id="" placeholder="">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Email address</label>
                        <input type="email" class="form-control" id="exampleInputEmail1" placeholder="">
                    </div>
                    
                    <div class="form-group">
                        <label for="subject">Subject</label>
                        <input type="text" class="form-control" id="exampleInputEmail1" placeholder="">
                    </div>
                    <div class="form-group">
                        <label for="phone">Phone</label>
                        <input type="text" class="form-control" id="exampleInputEmail1" placeholder="">
                    </div>
                    <div class="form-group">
                        <label>Message</label>
                        <textarea class="form-control" rows="3"></textarea>
                    </div>
                    
                    
                    <button type="submit" class="btn btn-default">Send</button>
                </form>
            </div>
            <div class="modal-footer">
                
            </div>
        </div>
    </div>
</div>