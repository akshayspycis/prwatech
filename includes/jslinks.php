<!-- JavaScript files placed at the end of the document so the pages load faster -->
		<!-- ================================================== -->
		<!-- Jquery and Bootstap core js files -->
		<script type="text/javascript" src="../plugins/jquery.min.js"></script>
                <script type="text/javascript" src="../bootstrap/js/bootstrap.min.js"></script>

		<!-- Modernizr javascript -->
		<script type="text/javascript" src="../plugins/modernizr.js"></script>

		<!-- jQuery Revolution Slider  -->
		<script type="text/javascript" src="../plugins/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
		<script type="text/javascript" src="../plugins/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
                 <!-- Isotope javascript -->
		<script type="text/javascript" src="../plugins/isotope/isotope.pkgd.min.js"></script>
		
		<!-- Magnific Popup javascript -->
<!--		<script type="text/javascript" src="../plugins/magnific-popup/jquery.magnific-popup.min.js"></script>-->
		
		<!-- Appear javascript -->
		<script type="text/javascript" src="../plugins/waypoints/jquery.waypoints.min.js"></script>

		<!-- Count To javascript -->
		<script type="text/javascript" src="../plugins/jquery.countTo.js"></script>
		
		<!-- Parallax javascript -->
		<script src="../plugins/jquery.parallax-1.1.3.js"></script>

		<!-- Contact form -->
		<script src="../plugins/jquery.validate.js"></script>
                    <!-- Background Video -->
		<script src="../plugins/vide/jquery.vide.js"></script>

		<!-- Owl carousel javascript -->
		<script type="text/javascript" src="../plugins/owl-carousel/owl.carousel.js"></script>
		<!-- SmoothScroll javascript -->
		<script type="text/javascript" src="../plugins/jquery.browser.js"></script>
		<script type="text/javascript" src="../plugins/SmoothScroll.js"></script>
<!-- Initialization of Plugins -->
		<script type="text/javascript" src="../js/template.js"></script>

		<!-- Custom Scripts -->
		<script type="text/javascript" src="../js/custom.js"></script>
		<!-- Color Switcher (Remove these lines) -->
		<script type="text/javascript" src="../style-switcher/style-switcher.js"></script>
                
               
		<script type="text/javascript">
          course_batch_details={}
          
            function onlad(course_id){
                /* Ajax call for Product Display*/
                    $.ajax({
                    type:"post",
                    url:"../server/controller/SelCourseBatchDetails.php",
                    data:{'course_id':course_id},
                    success: function(data) {
            
                      var duce = jQuery.parseJSON(data);
                        $("#batch_details").empty();
                        $.each(duce, function (index, article) {
                            course_batch_details[article.course_batch_details_id]={};
                            course_batch_details[article.course_batch_details_id]["course_id"]=article.course_id;
                            course_batch_details[article.course_batch_details_id]["date"]=article.date;
                            course_batch_details[article.course_batch_details_id]["day_type"]=article.day_type;
                            course_batch_details[article.course_batch_details_id]["day"]=article.day;
                            course_batch_details[article.course_batch_details_id]["time"]=article.time;
                            course_batch_details[article.course_batch_details_id]["fees"]=article.fees;
                            course_batch_details[article.course_batch_details_id]["discount"]=article.discount;
                            var btn_class="btn-danger";
                           if(article.status=="Enable"){
                               btn_class="btn-success";
                           }
                           var d = new Date(article.date);
                           var n = d.getDate();
                           var m = d.getMonth();
                           var month_name=["","Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"]
                          $("#batch_details").append($("<div>").addClass("col-md-3")
                                  .append($("<div>").addClass("col-md-3")
                                        .append($("<div>").addClass("row")
                                            .append($("<p>").attr({'id':'date-time'}).append(n)
                                                .append($("<br>"))
                                                .append($("<span>").attr({'id':'month'}).append(month_name[m].toUpperCase()))
                                            )
                                        )
                                   )
                                  .append($("<div>").addClass("col-md-9").attr({'id':'upcomming-batch'})
                                    .append($("<div>").addClass("row")
                                        .append($("<div>").addClass("col-md-12")
                                            .append($("<p>").attr({'id':'dt-content'})
                                                .append($("<b>").append(article.day_type+" - "+article.day)
                                                    .append($("<br>"))
                                                    .append($("<small>").append(article.time+" IST" +" (30 Hrs.)")
                                                        .append($("<br>"))
                                                        .append("Rs. "+article.fees+" ")
                                                        .append($("<a>").css({'cursor':'pointer'}).append(" Enroll Now").click(function (){
                                                            showDemoModel(course_id)
                                                        }))
                                                    )
                                                )
                                           )
                                        )
                                    )
                                 ));
                          
                          
                        });
                    }
                });
                }
                function showDemoModel (course_id){
                     openNav();
                    $("#mySidenav").find("form").each(function(){
                            this.reset();
                    });
                    $("#mySidenav").find("#course_id").val(course_id);
                }
                
      </script>
      
    