    
<div class="header-container">
                
    <!-- ================ -->
    <div class="header-top" id="bg-blue">
        <div class="bg-fullscreen">
            <div class="container">
                <div class="row">
                    <div class="col-xs-3 col-sm-6 col-md-8">
                        <!-- header-top-first start -->
                        <!-- ================ -->
                        <div class="header-top-first clearfix">
                            <ul class="list-inline hidden-sm hidden-xs contact-detail">
                                <li><i class="fa fa-phone pr-5 pl-10"></i> +91 8147111254 </li>
                                <li><i class="fa fa-envelope-o pr-5 pl-10"></i> info@prwatech.in</li>
                                <li><i class="fa fa-desktop pr-5 pl-10"></i><a href="../blog/">Blog</a></li>
                            </ul>
                        </div>
                        <!-- header-top-first end -->
                    </div>
                    <div class="col-xs-9 col-sm-6 col-md-4">
                
                        <!-- header-top-second start -->
                        <!-- ================ -->
                        <div id="header-top-second"  class="clearfix">
                
                            <!-- header top dropdowns start -->
                            <!-- ================ -->
            <?php
            if(isset($_SESSION['user_id'])){
            if($_SESSION['user_type']==='normal'){
            ?>
                            <div class="header-top-dropdown text-right">
                                <div class="btn-group ">
                                    <p style="position:relative;top:8px;right:5px;font-family:verdana;"><?php echo ""." "."<b>".$_SESSION['user_name']."</b>"; ?><p>
                                </div>  
                                <div class="btn-group">
                                    <a href="../profile-page" class="btn  btn-login btn-sm"><b>Profile</b></a>
                                </div>  
                                <div class="btn-group">
                                    <a href="../logout" class="btn  btn-login btn-sm"><b>Sign Out</b></a>
                                </div>  
                            </div>
                
            <?php
                
            }else{
            header("Location:../iiadmin/home");
            }
            }else{
            ?>
                            <div class="header-top-dropdown text-right">
                                <div class="btn-group">
                                    <a  class="btn btn-signup btn-sm"  data-toggle="modal" data-target="#mySignUp"><i class="fa fa-user pr-10"></i> Sign Up</a>
                                </div>
                                <div class="btn-group dropdown">
                                    <button type="button" class="btn  btn-login btn-sm" data-toggle="modal" data-target="#mySignIn"><i class="fa fa-lock pr-10"></i> Login</button>
                
                                </div>
                            </div>
                
            <?php
            }
            ?>
                            <!--  header top dropdowns end -->
                        </div>
                        <!-- header-top-second end -->
                    </div>
                </div>
            </div>
        </div>
                
    </div>
                
    <!-- ================ --> 
    <header class="header  fixed   clearfix" style="box-shadow: 0 4px 10px rgba(0,0,0, 0.50);">
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <!-- header-left start -->
                    <!-- ================ -->
                    <div class="header-left clearfix">
                
                        <!-- logo -->
                        <div id="logo" class="logo">
                            <a href="index.php"><img id="logo_img" src="../images/logo_light_blue.png" alt="The Project"></a>
                        </div>
                        <div class="site-slogan" style="font-family:century gothic;">
                            &nbsp;&nbsp;Share Ideas, Start Something Good.
                        </div>
                
                    </div>
                    <!-- header-left end -->
                
                </div>
                <div class="col-md-9">
                
                    <!-- header-right start -->
                    <!-- ================ -->
                    <div class="header-right clearfix">
                
                        <!-- main-navigation start -->
                        <!-- classes: -->
                        <!-- "onclick": Makes the dropdowns open on click, this the default bootstrap behavior e.g. class="main-navigation onclick" -->
                        <!-- "animated": Enables animations on dropdowns opening e.g. class="main-navigation animated" -->
                        <!-- "with-dropdown-buttons": Mandatory class that adds extra space, to the main navigation, for the search and cart dropdowns -->
                        <!-- ================ -->
                        <div class="main-navigation  animated with-dropdown-buttons">
                
                            <!-- navbar start -->
                            <!-- ================ -->
                            <nav class="navbar navbar-default" role="navigation">
                                <div class="container-fluid">
                
                                    <!-- Toggle get grouped for better mobile display -->
                                    <div class="navbar-header">
                                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse-1">
                                            <span class="sr-only">Toggle navigation</span>
                                            <span class="icon-bar"></span>
                                            <span class="icon-bar"></span>
                                            <span class="icon-bar"></span>
                                        </button>
                
                                    </div>
                
                                    <!-- Collect the nav links, forms, and other content for toggling -->
                                    <div class="collapse navbar-collapse" id="navbar-collapse-1">
                                        <!-- main-menu -->
                                        <ul class="nav navbar-nav " style="font-family:century gothic">
                                            <li>
                                                <a  href="http://infoparkinnovations.in/prwatech/">Home</a>
                                            </li>
                                            <li>
                                                <a  href="../about/">About Us</a>
                                            </li>
                
                                            <!-- mega-menu start -->
                
                                            <!-- mega-menu end -->
                                            <li class="dropdown ">
                                                <a class="dropdown-toggle" data-toggle="dropdown" href="#">Courses</a>
                                                <ul class="dropdown-menu">
                                                    <li class="dropdown ">
                                                        <a  class="dropdown-toggle" data-toggle="dropdown" href="../big-data-hadoop-training/">Big Data Hadoop Training </a>
                                                        <ul class="dropdown-menu">
                                                            <li ><a href="../big-data-hadoop-training/">Bigdata Hadoop Training </a></li>
                                                            <li ><a href="../big-data-hadoop-training-in-pune/">Bigdata Hadoop Training in Pune</a></li>
                                                            <li ><a href="../big-data-hadoop-training/">Bigdata Hadoop Online Training </a></li>
                                                            <li ><a href="../big-data-hadoop-training/">Bigdata Hadoop Online self Training</a></li>
                                                        </ul>
                                                    </li>
                                                    <li class="dropdown ">
                                                        <a  class="dropdown-toggle" data-toggle="dropdown" href="../hadoop-admin/">Hadoop Admin</a>
                                                        <ul class="dropdown-menu">
                                                            <li ><a href="../hadoop-admin/">Class Room Training</a></li>
                                                            <li ><a href="../hadoop-admin-online-course/">Hadoop Admin Online Course</a></li>
                                                            <li ><a href="../hadoop-admin/">Hadoop Admin Online Self Training</a></li>
                
                                                        </ul>
                                                    </li>
                                                    <li><a href="../apache-spark-scala-training-2/">Apache Spark Scala Training</a></li>
                                                    <li><a href="../data-science-course-training-bangalore/">Data Science Course</a></li>
                                                    <li><a href="../r-programing/">R Programming</a></li>
                                                    <li><a href="../tableau/">Tableau</a></li>
                                                    <li><a href="../python-online-course/">Python</a></li>
                                                    <li><a href="../sastraining/">SAS Training</a></li>
                                                </ul>
                                            </li>
                                            <!-- mega-menu start -->													
                
                                            <!-- mega-menu end -->
                                            <li class="dropdown ">
                                                <a href="../services/corporate-training.php" class="dropdown-toggle" data-toggle="dropdown">Services</a>
                                                <ul class="dropdown-menu">
                                                    <li><a  href="../services/corporate-training">Corporate Training</a></li>
                                                    <li><a  href='../services/workshop-on-bigdata-hadoop'>Workshop on Bigdata & Hadoop</a></li>
                                                    <li ><a href="#">LMS</a></li>
                                                    <li ><a href="../services/placement-assistance">Placement Assistance</a></li>
                                                </ul>
                                            </li>
                                            <li ><a href="../careers/">Career</a></li>
                
                                            <li ><a href="../gallery/">Gallery</a></li>
                                            <li ><a href="../contact/">Contact</a></li>
                
                                        </ul>
                                        <!-- main-menu end -->
                
                                        <!-- header dropdown buttons -->
                                        <div class="header-dropdown-buttons hidden-xs ">
                                            <div class="btn-group dropdown">
                                                <button type="button" class="btn dropdown-toggle" data-toggle="dropdown"><i class="icon-search"></i></button>
                                                <ul class="dropdown-menu dropdown-menu-right dropdown-animation" id="search-form">
                                                    <li>
                                                        <form role="search" class="search-box margin-clear">
                                                            <div class="form-group has-feedback">
                                                                <input type="text" class="form-control" placeholder="Search" style="background:rgba(0,0,0,0.7);color:white">
                                                                <i class="icon-search form-control-feedback"></i>
                                                            </div>
                                                        </form>
                                                    </li>
                                                </ul>
                                            </div>
                
                                        </div>
                                        <!-- header dropdown buttons end-->
                
                                    </div>
                
                                </div>
                            </nav>
                            <!-- navbar end -->
                
                        </div>
                        <!-- main-navigation end -->	
                    </div>
                    <!-- header-right end -->
                
                </div>
            </div>
        </div>
                
    </header>
    <!-- header end -->
</div>
                        
                        