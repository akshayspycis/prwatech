<footer id="footer" class="clearfix ">
<!--                            <div class="footer-upper-top">
                                
                            </div>-->
                            <div class="footer-upper">
                                
                            </div>
<!--                            <div id="footer-bottom" >
                                
                            </div>-->
				<!-- .footer start -->
				<!-- ================ -->
				<div class="footer" style="background:url('../images/bg/3a.png');background-position:bottom center; box-shadow:inset 0 6px 10px rgba(0,0,0, 0.25);">
					<div class="container">
						<div class="footer-inner">
							<div class="row">
								<div class="col-md-3">
									<div class="footer-content">
                                                                            <div class="logo-footer"><img id="logo-footer" src="../images/logo_light_blue.png" alt=""/></div>
                                                                            <p class="heading-font"style="text-align:justify">Training lays the foundation for an engineer.
                                                                                It provides a strong platform to build ones perception and implementation by mastering a wide range of skills .
                                                                                One of India’s leading and largest training provider for Big Data and Hadoop Corporate training programs is the prestigious PrwaTech. <a href="#">Learn More<i class="fa fa-long-arrow-right pl-5"></i></a></p>
										<div class="separator-2"></div>
										<nav>
										 <a href="../terms-and-conditions">Terms, Conditions & Privacy</a>	
										</nav>
									</div>
								</div>
								<div class="col-md-3">
									<div class="footer-content">
										<h3 class="title">Latest From Blog</h3>
										<div class="separator-2"></div>
                                                                                <div id="blog_details_client_in_footer">
<!--										<div class="media margin-clear">
											<div class="media-left">
												<div class="overlay-container">
                                                                                                    <img class="media-object" src="../images/por-1.png" alt="blog-thumb">
													<a href="blog-post.html" class="overlay-link small"><i class="fa fa-link"></i></a>
												</div>
											</div>
											<div class="media-body">
												<h6 class="media-heading heading-font"><a href="blog-post.html">Learn about the Fundamentals of Hadoop Development</a></h6>
												<p class="small margin-clear"><i class="fa fa-calendar pr-10"></i>Mar 23, 2015</p>
											</div>
											<hr>
										</div>
										<div class="media margin-clear">
											<div class="media-left">
												<div class="overlay-container">
                                                                                                    <img class="media-object" src="../images/por-1.png" alt="blog-thumb">
													<a href="blog-post.html" class="overlay-link small"><i class="fa fa-link"></i></a>
												</div>
											</div>
											<div class="media-body">
												<h6 class="media-heading heading-font"><a href="blog-post.html">Learn about the Fundamentals of Hadoop Development</a></h6>
												<p class="small margin-clear"><i class="fa fa-calendar pr-10"></i>Mar 23, 2015</p>
											</div>
											<hr>
										</div>
										<div class="media margin-clear">
											<div class="media-left">
												<div class="overlay-container">
                                                                                                    <img class="media-object" src="../images/por-1.png" alt="blog-thumb">
													<a href="blog-post.html" class="overlay-link small"><i class="fa fa-link"></i></a>
												</div>
											</div>
											<div class="media-body">
												<h6 class="media-heading heading-font"><a href="blog-post.html">Learn about the Fundamentals of Hadoop Development</a></h6>
												<p class="small margin-clear"><i class="fa fa-calendar pr-10"></i>Mar 23, 2015</p>
											</div>
											<hr>
										</div>-->
										</div>
										<div class="text-right space-top">
											<a href="../blog/" class="link-dark"><i class="fa fa-plus-circle pl-5 pr-5"></i>More</a>	
										</div>
									</div>
								</div>
								<div class="col-md-3">
									<div class="footer-content">
										<h3 class="title">Find Us In Bangalore</h3>
										<div class="separator-2"></div>
                                                                                    <div class="heading-font" itemscope itemtype="http://schema.org/PostalAddress"> 
<span itemprop="name">PRWATECH</span><br> 
<span itemprop="streetAddress">No.14, 29th Main , 2nd Cross, V.P road BTM-1st Stage, Behind AXA building, Land Mark : Vijaya Bank ATM</span><br> 
<span itemprop="addressLocality">Bangalore</span>, 
<span itemprop="addressRegion"> Karnataka </span> 
<span itemprop="postalCode">560 068</span><br> 
<span itemprop="addressCountry">India</span> 
</div>

									
										<div class="separator-2"></div>
											<ul class="social-links circle animated-effect-1">
											<li class="facebook"><a target="_blank" href="https://www.facebook.com/JustHadoop"><i class="fa fa-facebook"></i></a></li>
											<li class="twitter"><a target="_blank" href=" https://twitter.com/prwatech"><i class="fa fa-twitter"></i></a></li>
											<li class="googleplus"><a target="_blank" href="https://plus.google.com/+PrwatechBigDataHadoopTrainingCoursesBengaluru"><i class="fa fa-google-plus"></i></a></li>
											<li class="linkedin"><a target="_blank" href="https://www.linkedin.com/in/prwatech"><i class="fa fa-linkedin"></i></a></li>
											<li class="xing"><a target="_blank" href="https://www.youtube.com/channel/UCwAaWqnH2MqikDMpb1jBspw"><i class="fa fa-youtube"></i></a></li>
										</ul>
									</div>
								</div>
								<div class="col-md-3">
									<div class="footer-content">
										<h3 class="title">Find Us In Pune</h3>
										<div class="separator-2"></div>
                                                                               <div class="heading-font" itemscope itemtype="http://schema.org/PostalAddress"> 
<span itemprop="name">PRWATECH</span><br> 
<span itemprop="streetAddress">Shiv sai apartment 
Ground Floor 
Pandari nagar 
Kharadi, <br>Land Mark : Radisson Blu
</span><br> 
<span itemprop="addressLocality"> Pune</span>, 
<span itemprop="addressRegion">Maharashtra</span> 
<span itemprop="postalCode">411014</span><br> 
<span itemprop="addressCountry">India</span> 
</div>

										<div class="separator-2"></div>
                                                                               
									</div>
								</div>
							</div>
						</div>
					</div>
                                  
				</div>
				<!-- .footer end -->

				<!-- .subfooter start -->
				<!-- ================ -->
				<div class="subfooter" id="bg-subfooter" >
                                    <div id="footer-bottom">
                                        
                                    </div> 
                                    <div class="bg-fullscreen">
                                        <div class="container">
						<div class="subfooter-inner">
							<div class="row">
								<div class="col-md-12">
									<p class="text-center">Copyright © 2016 Prwatech. All Rights Reserved &nbsp; &nbsp;&nbsp; Powered By Infopark </p>
								</div>
							</div>
						</div>
					</div>
                                    </div>
					
				</div>
				<!-- .subfooter end -->

</footer>
        <script>
            onload_blog();
            function onload_blog(){
                /* Ajax call for Product Display*/
                    $.ajax({
                    type:"post",
                    url:"../server/controller/SelBlogDetails.php",
                    data:{'category_id':""},
                    success: function(data) {
                       var duce = jQuery.parseJSON(data);
                        $("#blog_details_client_in_footer").empty();
                        var count=0;
                        $.each(duce, function (index, article) {
                            
                           var img= "../server/controller/"+article.pic;
                           var btn_class="btn-danger";
                           if(article.status=="Enable"){
                                
                                $("#blog_details_client_in_footer").append($("<div>").addClass("media margin-clear")
                                                .append($("<div>").addClass("media-left")
                                                    .append($("<div>").addClass("overlay-container")
                                                        .append($("<img>").addClass("img-responsive").attr({'src':img}))
                                                        .append($("<a>").addClass("overlay-link small")))
                                                 )
                                                .append($("<div>").addClass("media-body")
                                                        .append($("<h6>").addClass("media-heading heading-font")
                                                            .append($("<a>").append(article.title)).click(function (){
                                                                 window.location="../blog/blogpostdetail.php?id="+article.blog_details_id;
                                                            }).css({'cursor':'pointer'}))
                                                            .append($("<p>").addClass("small margin-clear")
                                                                 .append($("<i>").addClass("fa fa-calendar pr-10"))
                                                                 .append(article.date)
                                                            )
                                                )
                                                .append($("<hr>"))
                                                );
                           }
                         count++;
                         if(count==3){
                             return false;
                         }
                        });
                    }
                });
                }
            
            
</script>

<!--Add the following script at the bottom of the web page (before </body></html>)-->
<script type="text/javascript" async="async" defer="defer" data-cfasync="false" src="https://mylivechat.com/chatinline.aspx?hccid=19313265"></script>