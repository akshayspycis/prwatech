<?php include '../includes/session.php'; ?>
<!DOCTYPE html>
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
	<!--<![endif]-->
<head>
		<meta charset="utf-8">
                <title>Hadoop Admin Online Course | BigData & HADOOP</title>
                <meta name="description" content="Hadoop Online Course offers comprehensive course on an array of Hadoop technologies for administrators, data analysts and developers." />
                <meta property="og:type" content="article" />
                <meta property="og:title" content="Big Data Hadoop Online Course" />
                <meta property="og:url" content="http://prwatech.in/big-data-hadoop-online-course/" />
                <meta property="og:site_name" content="BigData &amp; HADOOP" />
                <meta name="google-site-verification" content="fSzBVSEUMu0l5lnD0qBVGv5F_16zaI6xiUZFMm-iMqQ" />  
                <?php include '../includes/csslinks.php'; ?>
        </head>

	<!-- body classes:  -->
	<!-- "boxed": boxed layout mode e.g. <body class="boxed"> -->
	<!-- "pattern-1 ... pattern-9": background patterns for boxed layout mode e.g. <body class="boxed pattern-1"> -->
	<!-- "transparent-header": makes the header transparent and pulls the banner to top -->
	<body class="no-trans front-page transparent-header ">
            <!-- scrollToTop -->
		<!-- ================ -->
		<div class="scrollToTop circle"><i class="icon-up-open-big"></i></div>
		
		<!-- page wrapper start -->
		<!-- ================ -->
		<div class="page-wrapper">
		
			<!-- header-container start -->
			<?php include '../includes/header.php' ;  ?>
			<!-- header-container end -->
		
			<!-- banner start -->
			<!-- ================ -->
			<div class="banner dark-translucent-bg" style="background-image:url('../images/page-course-banner-2.jpg'); background-position: 50% 27%;">
				<!-- breadcrumb start -->
				<!-- ================ -->
				<div class="breadcrumb-container object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
					<div class="container">
						<ol class="breadcrumb">
							<li><i class="fa fa-home pr-10"></i><a class="link-dark" href="index.html">Home</a></li>
							<li class="active">All Courses</li>
							<li class="active">Hadoop Admin Online Course</li>
						</ol>
					</div>
				</div>
				<!-- breadcrumb end -->
				<div class="container">
					
				</div>
			</div>
			<!-- banner end -->
			
			<div id="page-start"></div>

			<!-- section start -->
			<!-- ================ -->
			<section class="light-gray-bg pv-30 clearfix object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100" id="cdrow1">
				<div class="container">
					<div class="row">
						<div class="col-md-8">
							 <h1 class="text-center heading-font" style="text-transform:none;"> <strong>Hadoop Admin  Online </strong> Course</h1>
                                                  
							<div class="separator"></div>
                                                        <p class="large text-justify heading-font">Hadoop Online Course offers comprehensive course on an array of Hadoop technologies for administrators, data analysts and developers. The Hadoop Online Course is designed in such a format that meets your availability, convenience and flexibility needs, these online courses will lead you in the direction to become a certified Hadoop professional.</p>
						</div>
						<div class="col-md-4">
                                                    <img src="../images/por-2.png"/>
                                                </div>
						
					</div>
                                     <p>&nbsp;</p>
                                    <div class="separator"></div>
                                    <h2 class="text-center heading-font object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100" style="text-transform:none;">Contact Us <strong>+91 8147111254 </strong></h2>
                                     <div class="separator"></div>
                                      <p>&nbsp;</p>
                            
				</div>
                               <div class="container">
                                <div class="row" id="batch_details">
                                    <div class="col-md-3">
                                        <div class="col-md-3">
                                            <div class="row">
                                                <p id ="date-time">21 <br><span id="month">NOV</span></p>
                                            </div>
                                       </div>
                                        <div class="col-md-9" id="upcomming-batch">
                                            <div class="row">
                                                <div class="col-md-12"><p id="dt-content"><b> Mon-Wed (3 Days) <br><small>&nbsp; <br> Rs. 14000/- <a href="#" class="" data-toggle="modal" data-target="#demoPopUp">Enroll Now</a></small></b></p></div>
                                                
                                            </div>
                                       </div>
                                    </div>
                                  <div class="col-md-3">
                                        <div class="col-md-3">
                                            <div class="row">
                                                <p id ="date-time">24 <br><span id="month">NOV</span></p>
                                            </div>
                                       </div>
                                        <div class="col-md-9" id="upcomming-batch">
                                            <div class="row">
                                                <div class="col-md-12"><p id="dt-content"><b> Mon-Wed (3 Days) <br><small>&nbsp; <br> Rs. 14000/- <a href="#" class="" data-toggle="modal" data-target="#demoPopUp">Enroll Now</a></small></b></p></div>
                                                
                                            </div>
                                       </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="col-md-3">
                                            <div class="row">
                                                <p id ="date-time">27 <br><span id="month">NOV</span></p>
                                            </div>
                                       </div>
                                        <div class="col-md-9" id="upcomming-batch">
                                            <div class="row">
                                                <div class="col-md-12"><p id="dt-content"><b> Mon-Wed (3 Days) <br><small>&nbsp; <br> Rs. 14000/- <a href="#" class="" data-toggle="modal" data-target="#demoPopUp">Enroll Now</a></small></b></p></div>
                                                
                                            </div>
                                       </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="col-md-3">
                                            <div class="row">
                                                <p id ="date-time">30 <br><span id="month">NOV</span></p>
                                            </div>
                                       </div>
                                        <div class="col-md-9" id="upcomming-batch">
                                            <div class="row">
                                                <div class="col-md-12"><p id="dt-content"><b> Mon-Wed (3 Days) <br><small>&nbsp; <br> Rs. 14000/- <a href="#" class="" data-toggle="modal" data-target="#demoPopUp">Enroll Now</a></small></b></p></div>
                                                
                                            </div>
                                       </div>
                                    </div>
                                </div>
                            </div>
			</section>
                         <section id="coursed-row2" class="object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
                            <div class="container">
                                
                                 <h2 class="text-center heading-font object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100" style="text-transform:none;"> <strong>Course</strong> Details</h2>
                                 <div id="vseparator" class="object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
                                     <div class="vhr"></div>
                                </div>
                                <div class="row col-md-9" >
                               <!-- tabs start -->
                                <!-- ================ -->
                                <!-- Nav tabs -->
                                <ul class="nav nav-tabs style-3" id="tab-course" role="tablist">
                                    <li class="active"><a href="#h3tab1" role="tab" data-toggle="tab">Overview</a></li>
                                    <li><a href="#h3tab2" role="tab" data-toggle="tab">Course Curriculum</a></li>
                                    <li><a href="#h3tab3" role="tab" data-toggle="tab">FAQ's</a></li>
                                    <li><a href="#h3tab4" role="tab" data-toggle="tab">Download Course Outline</a></li>
                                    <li><a href="#h3tab5" role="tab" data-toggle="tab">Reviews</a></li>
                                   
                                </ul>
                                <!-- Tab panes -->
                                <div class="tab-content">
                                    <div class="tab-pane in active " id="h3tab1">
                                         <div class="row">
                                            <div class="col-md-12 object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
                                                
                                                <p id="cpara">
                                                   Hadoop Online Course offers comprehensive course on an array of Hadoop technologies for administrators, data analysts and developers. The Hadoop Online Course is designed in such a format that meets your availability, convenience and flexibility needs, these online courses will lead you in the direction to become a certified Hadoop professional.
                                                </p>
                                                <p id="cpara">
                                                   The online Hadoop training course by Prwatech imparts knowledge about the fundamental principles of Hadoop and gets the participants acquainted with the benefits of Big Data. The Hadoop online Course provides in-depth knowledge about Hadoop MapReduce, Hadoop Distributed File System (HDFS), Hadoop Cluster and implementation of a Hadoop Project.
                                                </p>
                                                <p id="cpara">
                                                   At the completion of this course, the participants become proficient in performing data analytics with Pig & Hive, writing complex MapReduce Programs and in processing Big Data sets efficiently.
                                                </p>
                                              
                                                <h2 class="heading-font">Who should enroll for Hadoop Online Course:</h2>
                                                <ul class="list-icons" id="clist">
                                                    <li><i class="icon-check pr-10"></i> Data Integration Architects</li>
                                                    <li><i class="icon-check pr-10"></i> Data Architects</li>
                                                   <li><i class="icon-check pr-10"></i>  Data Analysts</li>
                                                    <li><i class="icon-check pr-10"></i> Data Scientists</li>
                                                    <li><i class="icon-check pr-10"></i> Decision Makers</li>
                                                    <li><i class="icon-check pr-10"></i> Hadoop Developers and Administrators</li>
                                                    
                                                </ul>
                                                <h2 class="heading-font">What is this course about?</h2>
                                                <p>The Hadoop online course by PrwaTech is designed to make sure that you are fully equipped to take up any assignments in Hadoop. This training not only prepares you with necessary Hadoop skills, but also gives you the required work experience in Big Data Hadoop by the use of real-life industry project implementation.</p>
                                                <h2 class="heading-font">Key Features:</h2>
                                                <ul class="list-icons" id="clist">
                                                    <li><i class="icon-check pr-10"></i>36 hours of Training by experts</li>
                                                    <li><i class="icon-check pr-10"></i> 24 hours of first-rate E-Learning</li>
                                                    <li><i class="icon-check pr-10"></i> More than 60 hours of industry projects</li>
                                                    <li><i class="icon-check pr-10"></i> Hands-on execution with CloudLab</li>
                                                    <li><i class="icon-check pr-10"></i> Availability of on demand support</li>
                                                    <li><i class="icon-check pr-10"></i> Experience certificate in Hadoop 2.7</li>
                                                        
                                                </ul>
                                                <h2 class="heading-font">Hadoop online course objectives:</h2>
                                                <ul class="list-icons" id="clist">
                                                    <li><i class="icon-check pr-10"></i> Mastering the concept of Hadoop framework and deployment in cluster environment.</li>
                                                    <li><i class="icon-check pr-10"></i> Become skilled at writing complex programs in MapReduce.</li>
                                                    <li><i class="icon-check pr-10"></i> Execute Data Analytics using Pig & Hive.</li>
                                                    <li><i class="icon-check pr-10"></i> Acquire comprehensive understanding of Hadoop Ecosystem like Apache Oozie workflow scheduler, Flume etc.</li>
                                                    <li><i class="icon-check pr-10"></i> Mastering concepts of Hadoop: Zookeeper, Hbase and Sqoop.</li>
                                                    <li><i class="icon-check pr-10"></i> Hands-on experience in configuring different set-ups of Hadoop cluster.</li>
                                                    <li><i class="icon-check pr-10"></i> Hands-on experience in industry based projects using Hadoop.</li>
                                                    
                                                        
                                                </ul>
                                                
                                                <div class="separator"></div>
                                                
                                            </div>
                                                
                                        </div>
                                            
                                    </div>
                                    <div class="tab-pane" id="h3tab2">
                                          <div class="row">
                                            <div class="col-md-12 ">
                                                  <div class="panel-group collapse-style-2" id="accordion-2">
								<div class="panel panel-default">
									<div class="panel-heading">
										<h4 class="panel-title">
											<a data-toggle="collapse" data-parent="#accordion-2" href="#collapseOne-2">
												<i class="fa fa-arrow-right pr-10"></i>Module 1
											</a>
										</h4>
									</div>
                                                                    <div id="collapseOne-2" class="panel-collapse collapse in">
                                                                        <div class="panel-body">
                                                                            <h5 class="heading-font">Hadoop Cluster Administration</h5>
                                                                            <p id="cpara">In this module of Hadoop Admin Training, the learners get a clear understanding of Apache Hadoop and Big Data. Apart from this, the learners also get to know about the procedure of using Hadoop for solving Big Data issues, the loading techniques of Hadoop Data, Map Reduce Introduction and the Role played by Hadoop Cluster Administration.</p>
                                                                            <p id="cpara">The topics covered are Big Data and Hadoop Architecture Introduction, the Roles and the Responsibilities of Hadoop Cluster Administration, Map Reduce Structure and Data Loading onto HDFS.</p>
                                                                        </div>
                                                                    </div>
								</div>
								<div class="panel panel-default">
									<div class="panel-heading">
										<h4 class="panel-title">
											<a data-toggle="collapse" data-parent="#accordion-2" href="#collapseTwo-2" class="collapsed">
												<i class="fa fa-arrow-right pr-10"></i>Module 2
											</a>
										</h4>
									</div>
									<div id="collapseTwo-2" class="panel-collapse collapse">
										<div class="panel-body">
                                                                                    <h5 class="heading-font">Cluster Set Up and Hadoop Architecture</h5>
                                                                                    <p id="cpara">The module helps in getting a clear understanding of the roles played by different Hadoop servers like DataNode and NameNode. Learners also get to know about the set up procedure of Hadoop 1.0 Cluster and also the configuration. You also get to learn the different steps of setting up Hadoop clients by making use of Hadoop 1.0. Apart from this, the module covers significant Hadoop configuration parameters and files.</p>
                                                                                    <p id="cpara"><b>Topics </b>Hadoop Initial Configuration and Installation, Using Hadoop in Multi-Node Hadoop Cluster, Hadoop Server Usage and Roles, Anatomy of Read and Write, and Data Processing.</p>
                                                                                        
                                                                                </div>
									</div>
								</div>
                                                      <div class="panel panel-default">
                                                          <div class="panel-heading">
                                                              <h4 class="panel-title">
                                                                  <a data-toggle="collapse" data-parent="#accordion-2" href="#collapseThree-2" class="collapsed">
                                                                      <i class="fa fa-arrow-right pr-10"></i>Module 3 
                                                                  </a>
                                                              </h4>
                                                          </div>
                                                          <div id="collapseThree-2" class="panel-collapse collapse">
                                                              <div class="panel-body">
                                                                  <h5 class="heading-font">Hadoop Cluster- Managing and Planning</h5>
                                                                  <p id="cpara">This module in Hadoop Admin Training deal with the clear understanding of the key features of managing and planning the Hadoop cluster along with the monitoring and the troubleshooting of Hadoop cluster. The module also discusses auditing and analysis logs. Learners also get to realize executing and scheduling map reduce activities and varied schedulers.</p>
                                                                  <p id="cpara">The topics covered are Planning Hadoop Cluster, Scheduler Types in Hadoop, Cluster Size, Software and Hardware Considerations, Scheduling and managing jobs. Scheduler Configuration and Running Map Reduce Activities.</p>
                                                                      
                                                              </div>
                                                          </div>
                                                      </div>
                                                      <div class="panel panel-default">
                                                          <div class="panel-heading">
                                                              <h4 class="panel-title">
                                                                  <a data-toggle="collapse" data-parent="#accordion-2" href="#collapseFour-2" class="collapsed">
                                                                      <i class="fa fa-arrow-right pr-10"></i>Module 4
                                                                  </a>
                                                              </h4>
                                                          </div>
                                                          <div id="collapseFour-2" class="panel-collapse collapse">
                                                              <div class="panel-body">
                                                                  <h5 class="heading-font">Maintenance, Backup and Recovery</h5>
                                                                  <p id="cpara">The module offers a clear understanding of regular activities involved in cluster administration like removing and adding data nodes, configuring recovery and backup in Hadoop, NameNode recovery, diagnosing the failures of Node and Hadoop upgradation.</p>
                                                                  <p id="cpara"><b>Topics </b> Hadoop backup set up, upgradation of Hadoop Cluster, Cluster Maintenance, Using DISTCP for Copying Data Through Clusters, Recovery and Diagnostics.</p>
                                                                      
                                                              </div>
                                                          </div>
                                                      </div>
                                                      <div class="panel panel-default">
                                                          <div class="panel-heading">
                                                              <h4 class="panel-title">
                                                                  <a data-toggle="collapse" data-parent="#accordion-2" href="#collapseFive-2" class="collapsed">
                                                                      <i class="fa fa-arrow-right pr-10"></i>Module 5
                                                                  </a>
                                                              </h4>
                                                          </div>
                                                          <div id="collapseFive-2" class="panel-collapse collapse">
                                                              <div class="panel-body">
                                                                  <h5 class="heading-font">High Availability and Hadoop 2.0</h5>
                                                                  <p id="cpara">This module in Hadoop Admin Training is aimed towards offering a clear understanding of the implementation of secondary NameNode check pointing and set up. It also discusses the new features of Hadoop 2.0, YARN structure and MRv2.</p>
                                                                  <p id="cpara"><b>Topics </b> Hadoop 2.0, Configuring Secondary NameNode, MRv2 and the Set Up of Hadoop 2.0 Cluster.</p>
                                                             </div>
                                                          </div>
                                                      </div>
                                                      <div class="panel panel-default">
                                                          <div class="panel-heading">
                                                              <h4 class="panel-title">
                                                                  <a data-toggle="collapse" data-parent="#accordion-2" href="#collapseSix-2" class="collapsed">
                                                                      <i class="fa fa-arrow-right pr-10"></i>Module 6
                                                                  </a>
                                                              </h4>
                                                          </div>
                                                          <div id="collapseSix-2" class="panel-collapse collapse">
                                                              <div class="panel-body">
                                                                  <h5 class="heading-font">Advanced Topics: HDFS Federation, Security and QJM</h5>
                                                                  <p id="cpara">This module helps in strengthening the key points of the Hadoop security, the set up and the log management of HDFS Federation, management of security using Kerberos and getting a clear understanding of the HDFS high availability by the use of Quorum Journal Manager.</p>
                                                                  <p id="cpara"><b>Topics </b> The Basics of the Hadoop platform security, configuring Kerberos, Platform Security, Service Monitoring, Log and Service Management, HDFS Federation Configuration and Alerts and Auditing.</p>
                                                                      
                                                              </div>
                                                          </div>
                                                      </div>
                                                       <div class="panel panel-default">
                                                          <div class="panel-heading">
                                                              <h4 class="panel-title">
                                                                  <a data-toggle="collapse" data-parent="#accordion-2" href="#collapseSeven-2" class="collapsed">
                                                                      <i class="fa fa-arrow-right pr-10"></i>Module 7
                                                                  </a>
                                                              </h4>
                                                          </div>
                                                          <div id="collapseSeven-2" class="panel-collapse collapse">
                                                              <div class="panel-body">
                                                                  <h5 class="heading-font">Hcatalog/Hive, HBase an Oozie Administration</h5>
                                                                  <p id="cpara">This module helps the learners in setting up the Apache Oozie workflow scheduler for different Hadoop activities, using HBase with various Hadoop elements, Hcatalog/Hive administration, making the effective use of HBase for loading data and reading and writing to and from HBase.</p>
                                                                  <p id="cpara"><b>Topics </b> HBase Architecture, Administration of Oozie, Hive and Hcatalog, HBase Set Up, Hive and HBase Integration and Optimization of HBase Performance.</p>
                                                                      
                                                              </div>
                                                          </div>
                                                      </div>
                                                       <div class="panel panel-default">
                                                          <div class="panel-heading">
                                                              <h4 class="panel-title">
                                                                  <a data-toggle="collapse" data-parent="#accordion-2" href="#collapseEight-2" class="collapsed">
                                                                      <i class="fa fa-arrow-right pr-10"></i>Module 8
                                                                  </a>
                                                              </h4>
                                                          </div>
                                                          <div id="collapseEight-2" class="panel-collapse collapse">
                                                              <div class="panel-body">
                                                                  <h5 class="heading-font">Hadoop Implementation Assignment</h5>
                                                                  <p id="cpara">The module provides in-depth knowledge of the right procedure of working with different Hadoop ecosystem elements for solving Big Data issues. Learners also get to learn how to design, deploy and plan Hadoop Cluster by making use of real world use scenarios.</p>
                                                                  <p id="cpara">Understanding Cluster Issues, Planning, Designing and Creating Hadoop Cluster, Configuring and Setting Up Hadoop Ecosystem Elements.</p>
                                                              </div>
                                                          </div>
                                                      </div>
                                                    </div>
                                                
                                                </div>

                                        </div>
                                       
                                    </div>
                                          <div class="tab-pane" id="h3tab3">
                                        <div class="row">
                                            <div class="col-md-12 ">
                                                   	<div class="panel-group collapse-style-3" id="accordion-3">
								<div class="panel panel-default">
									<div class="panel-heading">
										<h4 class="panel-title">
											<a data-toggle="collapse" data-parent="#accordion-3" href="#collapseOne-3">
												Why Should I Learn Hadoop From Prwatech?
											</a>
										</h4>
									</div>
									<div id="collapseOne-3" class="panel-collapse collapse in">
										<div class="panel-body">
                                                                                    <p id="cpara" class="text-justify">
                                                                                        Prwatech is the pioneer of Hadoop training in India. As you know today the demand for Hadoop professionals far exceeds the supply. So it pays to be with the market leader like Prwatech when it comes to learning Hadoop in order to command top salaries. As part of the training you will learn about the various components of Hadoop like MapReduce, HDFS, HBase, Hive, Pig, Sqoop, Flume, Oozie among others. You will get an in-depth understanding of the entire Hadoop framework for processing huge volumes of data in real world scenarios.
                                                                                    </p>
                                                                                    <p id="cpara" class="text-justify">
                                                                                       The Prwatech training is the most comprehensive course, designed by industry experts keeping in mind the job scenario and corporate requirements. We also provide lifetime access to videos, course materials, 24/7 Support, and free course material upgrade. Hence it is a one-time investment. 
                                                                                    </p>
										</div>
									</div>
								</div>
								<div class="panel panel-default">
									<div class="panel-heading">
										<h4 class="panel-title">
											<a data-toggle="collapse" data-parent="#accordion-3" href="#collapseTwo-3" class="collapsed">
												What Are The Various Modes Of Training That Prwatech Offers?
											</a>
										</h4>
									</div>
									<div id="collapseTwo-3" class="panel-collapse collapse">
										<div class="panel-body">
                                                                                    <p id="cpara" class="text-justify">
                                                                                        Prwatech basically offers the self-paced training and online instructor-led training. Apart from that we also provide corporate training for enterprises. All our trainers come with over 5 years of industry experience in relevant technologies and also they are subject matter experts working as consultants. You can check about the quality of our trainers in the sample videos provided.
                                                                                    </p>
                                                                                    
										</div>
									</div>
								</div>
								<div class="panel panel-default">
									<div class="panel-heading">
										<h4 class="panel-title">
											<a data-toggle="collapse" data-parent="#accordion-3" href="#collapseThree-3" class="collapsed">
												Can I Request For A Support Session If I Find Difficulty In Grasping Topics?
											</a>
										</h4>
									</div>
									<div id="collapseThree-3" class="panel-collapse collapse">
										<div class="panel-body">
                                                                                    <p id="cpara" class="text-justify">
                                                                                          If you have any queries you can contact our 24/7 dedicated support to raise a ticket. We provide you email support and solution to your queries. If the query is not resolved by email we can arrange for a one-on-one session with our trainers. The best part is that you can contact Prwatech even after completion of training to get support and assistance. There is also no limit on the number of queries you can raise when it comes to doubt clearance and query resolution.
                                                                                    </p>
                                                                                    
										</div>
									</div>
								</div>
                                                            <div class="panel panel-default">
									<div class="panel-heading">
										<h4 class="panel-title">
											<a data-toggle="collapse" data-parent="#accordion-3" href="#collapseFour-3" class="collapsed">
												If I Am Not From A Programming Background But Have A Basic Knowledge Of Programming Can I Still Learn Hadoop?
											</a>
										</h4>
									</div>
									<div id="collapseFour-3" class="panel-collapse collapse">
										<div class="panel-body">
                                                                                    <p id="cpara" class="text-justify">
                                                                                         Yes, you can learn Hadoop without being from a software background. We provide complimentary courses in Java and Linux so that you can brush up on your programming skills. This will help you in learning Hadoop technologies better and faster.
                                                                                    </p>
                                                                                    
										</div>
									</div>
								</div>
                                                            <div class="panel panel-default">
									<div class="panel-heading">
										<h4 class="panel-title">
											<a data-toggle="collapse" data-parent="#accordion-3" href="#collapseFive-3" class="collapsed">
												What Kind Of Projects Will I Be Working On As Part Of The Training?
											</a>
										</h4>
									</div>
									<div id="collapseFive-3" class="panel-collapse collapse">
										<div class="panel-body">
                                                                                    <p id="cpara" class="text-justify">
                                                                                        We provide you with the opportunity to work on real world projects wherein you can apply your knowledge and skills that you acquired through our training. We have multiple projects that thoroughly test your skills and knowledge of various Hadoop components making you perfectly industry-ready. These projects could be in exciting and challenging fields like banking, insurance, retail, social networking, high technology and so on. The Prwatech projects are equivalent to six months of relevant experience in the corporate world.
                                                                                    </p>
                                                                                    
										</div>
									</div>
								</div>
                                                            <div class="panel panel-default">
									<div class="panel-heading">
										<h4 class="panel-title">
											<a data-toggle="collapse" data-parent="#accordion-3" href="#collapseSix-3" class="collapsed">
												Do You Provide Placement Assistance?
											</a>
										</h4>
									</div>
									<div id="collapseSix-3" class="panel-collapse collapse">
										<div class="panel-body">
                                                                                    <p id="cpara" class="text-justify">
                                                                                        Yes, Prwatech does provide you with placement assistance. We have tie-ups with 80+ organizations including Ericsson, Cisco, Cognizant, TCS, among others that are looking for Hadoop professionals and we would be happy to assist you with the process of preparing yourself for the interview and the job.
                                                                                    </p>
                                                                                    
										</div>
									</div>
								</div>
							</div>
                                                
                                                </div>

                                        </div>
                                       
                                    </div>
                                    <div class="tab-pane" id="h3tab4">
                                        <div class="row object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
                                            <div class="col-md-4">
                                                <p id="cpara" class="text-default"><a href="">Click Here to Download</a></p>
                                                
                                            </div>
                                            <div class="col-md-8">
                                                 <h4 class="title text-default" class="heading-font">Bigdata and Hadoop Services</h4>
                                                <ul class="list-icons">
                                                    <li><i class="icon-check pr-10"></i> PowerPoint Presentation covering all classes</li>
                                                    <li><i class="icon-check pr-10"></i> Recorded Videos Sessions On Bigdata and Hadoop with LMS Access.(lifetime support)</li>
                                                    <li><i class="icon-check pr-10"></i> Quiz , Assignment & POC.</li>
                                                    <li><i class="icon-check pr-10"></i> On Demand Online Support .</li>
                                                    <li><i class="icon-check pr-10"></i> Discussion Forum.</li>
                                                    <li><i class="icon-check pr-10"></i> Material
                                                        <ul class="list-icons" style="margin-left:50px;">
                                                            <li>a. Sample Question papers of Cloudera Certification.</li>
                                                            <li>b. Technical Notes & Study Material.</li>
                                                        </ul>
                                                    
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        
                                    </div>
                                    <div class="tab-pane" id="h3tab5">
                                        <div class="row object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
                                            <div class="col-md-6 ">
                                                <div class="pv-20 ph-20 feature-box-2 boxed shadow object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
                                                    <div class="col-md-3 testimonial-image"><img src="../images/testimonial-2.jpg" /></div> 
                                                    <div class="body">
                                                        <h4 class="title">Anupam Khamparia 
                                                            <br>
                                                            <span style="font-size:11px;">Consultant, Cognizant Technology Solutions, Bangalore</span>
                                                        </h4>
                                                        <p class ="text-justify">“Excellent course and instructor. I learnt a lot in a short period. Good value for money. Instructor took us through Advanced Hadoop Development in depth.”</p>
                                                       
                                                    </div>
                                                </div>
                                            </div>
                                          <div class="col-md-6 ">
                                                <div class="pv-20 ph-20 feature-box-2 boxed shadow object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
                                                    <div class="col-md-3 testimonial-image"><img src="../images/testimonial-1.png" /></div> 
                                                    <div class="body">
                                                        <h4 class="title">Anukanksha Garg
                                                          <br>
                                                            <span style="font-size:11px;">B.Tech. CSE</span>
                                                        </h4>
                                                        <p class ="text-justify">It was a nice learning experience with prwatech. The classes were well scheduled and managed.
Verma has good understanding of the topics taught and catered to our problems and doubts very patiently. The best thing about him was that he handled the situations accordingly, when needed a friend, he became one and also a teacher to always guide us.</p>
                                                       
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6 ">
                                                <div class="pv-20 ph-20 feature-box-2 boxed shadow object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
                                                    <div class="col-md-3 testimonial-image"><img src="../images/testimonial-4.PNG" /></div> 
                                                    <div class="body">
                                                        <h4 class="title">Varun Shivashanmugum
                                                        
                                                        <br>
                                                            <span style="font-size:11px;">Associate Consultant, ITC Infotech Ltd</span>
                                                        </h4>
                                                        
                                                        <p class ="text-justify">“Faculty is good,Verma takes keen interest and personnel care in improvising skills in students. And most importantly,Verma will be available and clears doubts at any time apart from class hours. And he always keeps boosting and trying to increase confidence in his students which adds extra attribute to him and organization as well. and organization as well.</p>
                                                       
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6 ">
                                                <div class="pv-20 ph-20 feature-box-2 boxed shadow object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
                                                    <div class="col-md-3 testimonial-image"><img src="../images/testimonial-5.jpg" /></div> 
                                                    <div class="body">
                                                        <h4 class="title">Mayank Srivastava <br>
                                                        <span style="font-size:11px;">Hadoop Developer, L&T, Bangalore</span>
                                                        </h4>
                                                        <p class ="text-justify">“Really good course content and labs, patient enthusiastic instructor. Good instructor, with in depth skills…Very relevant practicals allowed me to put theory into practice.”</p>
                                                       
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                                 <div class="col-md-3" style="position:relative;top:-10px;">
                                     <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center" >
                                         <div class="overlay-container">
                                             <img src="../images/icon/curse-d1.png" alt="">
                                         </div>
                                         <div class="body">
                                             <h3 class="heading-font">Rs. 14,000 + Tax </h3>
                                             <span class="small-font">per 2 weeks</span>
                                             <div class="separator"></div>
                                             <p class="heading-font"></p>
                                             <table class="table table-striped ">
                                                 <tbody>
                                                     <tr><th><center>35 Hours</center></th></tr>
                                                 <tr><th><center>15 Seats</center></th></tr>
                                                 <tr><th><center>Course Badge</center></th></tr>
                                                 <tr><th><center>Course Certificate</center></th></tr>
                                                 <tr><th><center> <a  onclick="openNav()" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Enroll Now<i class="fa fa-arrow-right pl-10"></i></a></center></th></tr>
                                                 </tbody>
                                             </table>
                                            <?php include '../includes/demo-popup.php';?>
                                                
                                         </div>
                                     </div>
                                      <h3 class="heading-font text-center" style="background:#4c4c4c;color:white;padding-top:10px;padding-bottom:10px">Suggested Courses</h3> 
                                      <div class="owl-carousel content-slider">
                                          <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
                                              <div class="overlay-container">
                                                  <img src="../images/por-1.png" alt="">
                                                  <div class="overlay-to-top">
                                                      <p class="small margin-clear">&nbsp;</p>
                                                  </div>
                                              </div>
                                              <div class="body">
                                                  <h3 class="heading-font">Big Data Hadoop Training</h3>
                                                  <div class="separator"></div>
                                                  <p class="heading-font">Advanced Program for Top-notch Developers </p>
                                                  <a href="big-data-hadoop-training.php"  target="_blank" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read More<i class="fa fa-arrow-right pl-10"></i></a>												
                                              </div>
                                          </div>
                                          <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
                                              <div class="overlay-container">
                                                  <img src="../images/por-2.png" alt="">
                                                  <div class="overlay-to-top">
                                                      <p class="small margin-clear">&nbsp;</p>
                                                  </div>
                                              </div>
                                              <div class="body">
                                                  <h3 class="heading-font">Hadoop Admin Training</h3>
                                                  <div class="separator"></div>
                                                  <p class="heading-font">Advanced Program for Top-notch Developers </p>
                                                  <a href="hadoop-admin.php" target="_blank" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read More<i class="fa fa-arrow-right pl-10"></i></a>												
                                              </div>
                                          </div>
                                          <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
                                              <div class="overlay-container">
                                                  <img src="../images/por-3.png" alt="">
                                                  <div class="overlay-to-top">
                                                      <p class="small margin-clear">&nbsp;</p>
                                                  </div>
                                              </div>
                                              <div class="body">
                                                  <h3 class="heading-font">Apache Spark Scala Training</h3>
                                                  <div class="separator"></div>
                                                  <p class="heading-font">Apache Spark Scala Combo Training </p>
                                                  <a href="apache-spark-scala-training.php" target="_blank" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read More<i class="fa fa-arrow-right pl-10"></i></a>												
                                              </div>
                                          </div>
                                          <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
                                              <div class="overlay-container">
                                                  <img src="../images/por-4.png" alt="">
                                                  <div class="overlay-to-top">
                                                      <p class="small margin-clear">&nbsp;</p>
                                                  </div>
                                              </div>
                                              <div class="body">
                                                  <h3 class="heading-font">Data Science Training</h3>
                                                  <div class="separator"></div>
                                                  <p class="heading-font">Find out the truth about what Data Science is. </p>
                                                  <a href="data-science-course-training-bangalore.php" target="_blank" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read More<i class="fa fa-arrow-right pl-10"></i></a>												
                                              </div>
                                          </div>
                                          <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
                                              <div class="overlay-container">
                                                  <img src="../images/por-5.png" alt="">
                                                  <div class="overlay-to-top">
                                                      <p class="small margin-clear">&nbsp;</p>
                                                  </div>
                                              </div>
                                              <div class="body">
                                                  <h3 class="heading-font">R Programming Training</h3>
                                                  <div class="separator"></div>
                                                  <p class="heading-font">Business Analytics With R Training</p>
                                                  <a href="r-programing.php" target="_blank" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read More<i class="fa fa-arrow-right pl-10"></i></a>												
                                              </div>
                                          </div>
                                          <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
                                              <div class="overlay-container">
                                                  <img src="../images/por-6.png" alt="">
                                                  <div class="overlay-to-top">
                                                      <p class="small margin-clear">&nbsp;</p>
                                                  </div>
                                              </div>
                                              <div class="body">
                                                  <h3 class="heading-font"> Tableau Training & Certification</h3>
                                                  <div class="separator"></div>
                                                  <p class="heading-font">Speed on concepts of data visualization</p>
                                                  <a href="tableau.php" target="_blank" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read More<i class="fa fa-arrow-right pl-10"></i></a>												
                                              </div>
                                          </div>
                                          <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
                                              <div class="overlay-container">
                                                  <img src="../images/por-7.png" alt="">
                                                  <div class="overlay-to-top">
                                                      <p class="small margin-clear">&nbsp;</p>
                                                  </div>
                                              </div>
                                              <div class="body">
                                                  <h3 class="heading-font">Python Programming</h3>
                                                  <div class="separator"></div>
                                                  <p class="heading-font">Advanced Concepts for Top-notch Developers  </p>
                                                  <a href="python.php" target="_blank" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read More<i class="fa fa-arrow-right pl-10"></i></a>												
                                              </div>
                                          </div>
                                      </div> 
                                      <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center" >
                                         <div class="body">
                                             <div itemscope itemtype="http://schema.org/Product"style="text-align: left">
                                                            <span itemprop="name">Hadoop Admin Online Course</span><br>
                                                            <div itemprop="aggregateRating" itemscope itemtype="http://schema.org/AggregateRating">
                                                                Average Rating: <span itemprop="ratingValue">4.76</span><br>
                                                                Votes: <span itemprop="ratingCount">876</span><br>
                                                                Reviews: <span itemprop="reviewCount">876</span>
                                                            </div>
                                            </div>
                                         </div>
                                     </div>
                                 </div> 
                            </div> 
                           
                           
                            <p>&nbsp;</p>
                            
                        </section>
                     
                        
			<!-- section end -->
                    
			<div class="clearfix"></div>
			
			<!-- ================ -->
			
			<!-- section end -->
                        <section class="pv-30 dark-bg padding-bottom-clear clearfix object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
					<br>
					<div class="container">
						<div class="row">
							<div class="col-md-3 ">
								<div class="ph-20 feature-box text-center object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
									<span class="icon default-bg circle"><i class="fa fa-institution"></i></span>
									<h3>Live classes</h3>
									<div class="separator clearfix"></div>
                                                                        <p class="heading-font ">Live online and interactive classes conducted by instructor</p>
									
								</div>
							</div>
							<div class="col-md-3 ">
								<div class="ph-20 feature-box text-center object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
									<span class="icon default-bg circle"><i class="fa fa-user"></i></span>
									<h3>Expert instructions</h3>
									<div class="separator clearfix"></div>
									<p class="heading-font">Learn from our Experts and get Real-Time Guidance</p>
									
								</div>
							</div>
							<div class="col-md-3 ">
								<div class="ph-20 feature-box text-center object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
									<span class="icon default-bg circle"><i class="icon-signal"></i></span>
									<h3>24 X 7 Support</h3>
									<div class="separator clearfix"></div>
									<p class="heading-font">Personalized Guidance from our 24X7 Support Team</p>
									
								</div>
							</div>
							<div class="col-md-3 ">
								<div class="ph-20 feature-box text-center object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
									<span class="icon default-bg circle"><i class="icon-calendar"></i></span>
									<h3>Flexible schedule</h3>
									<div class="separator clearfix"></div>
									<p class="heading-font">Reschedule your Batch/Class at Your Convenience</p>
									
								</div>
							</div>
						</div>
					</div>
					<br>
				</section>
			
			<!-- section end -->

			<!-- section -->
			<!-- ================ -->
			 <section class="pv-30   padding-bottom-clear dark-translucent-bg parallax padding-bottom-clear" style="background: url('../images/bg/451.jpg') 50% -9px; box-shadow: rgba(0, 0, 0, 0.247059) 0px 2px 7px inset;">
				
				<div class="space-bottom">
					
					<div class="owl-carousel content-slider">
						<div class="container">
							<div class="row">
								<div class="col-md-8 col-md-offset-2">
									<div class="testimonial text-center">
										<div class="testimonial-image">
                                                                                    <img src="../images/testimonial-1.png" alt="" title="" class="img-circle">
										</div>
										
										<div class="testimonial-body">
                                                                                    <p>&nbsp;</p>
												<p id="cpara">It was a nice learning experience with prwatech. The classes were well scheduled and managed.
Verma has good understanding of the topics taught and catered to our problems and doubts very patiently. The best thing about him was that he handled the situations accordingly, when needed a friend, he became one and also a teacher to always guide us.</p>
											
											<div class="testimonial-info-1">Anukanksha Garg</div>
											<div class="testimonial-info-2">B.TECH- CS</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="container">
							<div class="row">
								<div class="col-md-8 col-md-offset-2">
									<div class="testimonial text-center">
										<div class="testimonial-image">
											<img src="../images/testimonial-2.jpg" alt="" title="" class="img-circle">
										</div>
										
										<div class="testimonial-body">
											 <p>&nbsp;</p>
												<p id="cpara">Excellent course and instructor. I learnt a lot in a short period. Good value for money. Instructor took us through Advanced Hadoop Development in depth.</p>
											
											<div class="testimonial-info-1">Anupam Khamparia</div>
											<div class="testimonial-info-2">Consultant, Cognizant Technology Solutions, Bangalore</div>
										</div>
									</div>
								</div>
							</div>
						</div>
                                            <div class="container">
							<div class="row">
								<div class="col-md-8 col-md-offset-2">
									<div class="testimonial text-center">
										<div class="testimonial-image">
											<img src="../images/testimonial-3.jpg" alt="" title="" class="img-circle">
										</div>
										
										<div class="testimonial-body">
											<p>&nbsp;</p>
												<p id="cpara">Excellent course and instructor. I learnt a lot in a short period. Good value for money. Instructor took us through Advanced Hadoop Development in depth.</p>
											<div class="testimonial-info-1">Kumar Waibhav</div>
                                                                                        <div class="testimonial-info-2">Software Engineer, Cognizant Technology Solutions,<br> Philadelphia, Pennsylvania, USA</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					
				</div>
			</section>
			<!-- section end -->

			<!-- section start -->
			<!-- ================ -->
                        
			                   
			<!-- section end -->
			<!-- footer start (Add "dark" class to #footer in order to enable dark footer) -->
			<!-- ================ -->
			<?php include '../includes/footer.php'; ?>
			<!-- footer end -->
			
		</div>
		<!-- page-wrapper end -->

		<?php include '../includes/jslinks.php';?>
                <!-- Color Switcher End -->
                    <?php include '../includes/quick-query.php';?>
                     <?php include '../includes/UserSignup.php';?>
	</body>

        <script>
            $(document).ready(function(){
                onlad(2);
            });  
        </script>

</html>

