<?php include 'includes/session.php'; ?>
<?php
$request  = str_replace("/prwatech/", "", $_SERVER['REQUEST_URI']);
if (strpos($request, '/') !== false) {
	$request = str_replace("/","", $request);
} 
if ($request== 'index.php') {
	?>
<script>
window.location="/prwatech/";
</script>

<?php
} 
?>


<!DOCTYPE html>
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
	<!--<![endif]-->
<head>
		<meta charset="utf-8">
                <title>Big Data Hadoop Training in Bangalore and Pune</title>
                <meta name="description" content="PRWATECH Institute offers big data Hadoop Training Bangalore. Learn Big Data Hadoop Training &Hadoop Certification Courses with concepts of Apache Hadoop." />
                <meta property="og:type" content="article" />
                <meta property="og:title" content="Home" />
                <meta property="og:url" content="http://prwatech.in/" />
                <meta property="og:site_name" content="BigData &amp; HADOOP" />
                <meta name="twitter:card" content="summary" />
                <meta name="google-site-verification" content="fSzBVSEUMu0l5lnD0qBVGv5F_16zaI6xiUZFMm-iMqQ" />  
                <!-- Mobile Meta -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0">

		<!-- Favicon -->
                <link rel="shortcut icon" href="images/fevicon.JPG">

		<!-- Web Fonts -->
		<link href='http://fonts.googleapis.com/css?family=Roboto:400,300,300italic,400italic,500,500italic,700,700italic' rel='stylesheet' type='text/css'>
		<link href='http://fonts.googleapis.com/css?family=Raleway:700,400,300' rel='stylesheet' type='text/css'>
		<link href='http://fonts.googleapis.com/css?family=Pacifico' rel='stylesheet' type='text/css'>
		<link href='http://fonts.googleapis.com/css?family=PT+Serif' rel='stylesheet' type='text/css'>

		<!-- Bootstrap core CSS -->
		<link href="bootstrap/css/bootstrap.css" rel="stylesheet">

		<!-- Font Awesome CSS -->
		<link href="fonts/font-awesome/css/font-awesome.css" rel="stylesheet">

		<!-- Fontello CSS -->
		<link href="fonts/fontello/css/fontello.css" rel="stylesheet">

		<!-- Plugins -->
		<link href="plugins/magnific-popup/magnific-popup.css" rel="stylesheet">
		<link href="plugins/rs-plugin/css/settings.css" rel="stylesheet">
		<link href="css/animations.css" rel="stylesheet">
		<link href="plugins/owl-carousel/owl.carousel.css" rel="stylesheet">
		<link href="plugins/owl-carousel/owl.transitions.css" rel="stylesheet">
		<link href="plugins/hover/hover-min.css" rel="stylesheet">		
		<!-- the project core CSS file -->
		<link href="css/style.css" rel="stylesheet" >
                <!-- Style Switcher Styles (Remove these two lines) -->
		<!-- Custom css --> 
		<link href="css/custom.css" rel="stylesheet"> 
                <script type="text/javascript" src="plugins/jquery.min.js"></script>
                
</head>

	<!-- body classes:  -->
	<!-- "boxed": boxed layout mode e.g. <body class="boxed"> -->
	<!-- "pattern-1 ... pattern-9": background patterns for boxed layout mode e.g. <body class="boxed pattern-1"> -->
	<!-- "transparent-header": makes the header transparent and pulls the banner to top -->
	<body class="no-trans front-page transparent-header ">
            <!-- scrollToTop -->
		<!-- ================ -->
		<div class="scrollToTop circle"><i class="icon-up-open-big"></i></div>
		
		<!-- page wrapper start -->
		<!-- ================ -->
		<div class="page-wrapper">
		
			<!-- header-container start -->
			    
<div class="header-container">
    <div class="header-top" id="bg-blue">
        <div class="bg-fullscreen">
            <div class="container">
                <div class="row">
                    <div class="col-xs-3 col-sm-6 col-md-8">
                        <!-- header-top-first start -->
                        <!-- ================ -->
                        <div class="header-top-first clearfix">
                            <ul class="list-inline hidden-sm hidden-xs contact-detail">
                                <li><i class="fa fa-phone pr-5 pl-10"></i> +91 8147111254 </li>
                                <li><i class="fa fa-envelope-o pr-5 pl-10"></i> info@prwatech.in</li>
                                <li><i class="fa fa-desktop pr-5 pl-10"></i><a href="blog/index.php">Blog</a></li>
                            </ul>
                        </div>
                        <!-- header-top-first end -->
                    </div>
                    <div class="col-xs-9 col-sm-6 col-md-4">
                
                        <!-- header-top-second start -->
                        <!-- ================ -->
                        <div id="header-top-second"  class="clearfix">
                
                            <!-- header top dropdowns start -->
                            <!-- ================ -->
            <?php
            if(isset($_SESSION['user_id'])){
            if($_SESSION['user_type']==='normal'){
            ?>
                            <div class="header-top-dropdown text-right">
                                <div class="btn-group ">
                                    <p style="position:relative;top:8px;right:5px;font-family:verdana;"><?php echo ""." "."<b>".$_SESSION['user_name']."</b>"; ?><p>
                                </div>  
                                <div class="btn-group">
                                    <a href="profile-page.php" class="btn  btn-login btn-sm"><b>Profile</b></a>
                                </div>  
                                <div class="btn-group">
                                    <a href="logout.php" class="btn  btn-login btn-sm"><b>Sign Out</b></a>
                                </div>  
                            </div>
                
            <?php
                
            }else{
            header("Location:iiadmin/home.php");
            }
            }else{
            ?>
                            <div class="header-top-dropdown text-right">
                                <div class="btn-group">
                                    <a  class="btn btn-signup btn-sm"  data-toggle="modal" data-target="#mySignUp"><i class="fa fa-user pr-10"></i> Sign Up</a>
                                </div>
                                <div class="btn-group dropdown">
                                    <button type="button" class="btn  btn-login btn-sm" data-toggle="modal" data-target="#mySignIn"><i class="fa fa-lock pr-10"></i> Login</button>
                
                                </div>
                            </div>
                
            <?php
            }
            ?>
                            <!--  header top dropdowns end -->
                        </div>
                        <!-- header-top-second end -->
                    </div>
                </div>
            </div>
        </div>
                
    </div>
    <header class="header  fixed   clearfix" style="box-shadow: 0 4px 10px rgba(0,0,0, 0.50);">
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <!-- header-left start -->
                    <!-- ================ -->
                    <div class="header-left clearfix">
                
                        <!-- logo -->
                        <div id="logo" class="logo">
                            <a href="index.php"><img id="logo_img" src="images/logo_light_blue.png" alt="Prwatech"></a>
                        </div>
                        <div class="site-slogan" style="font-family:century gothic;">
                            &nbsp;&nbsp;Share Ideas, Start Something Good.
                        </div>
                
                    </div>
                    <!-- header-left end -->
                
                </div>
                <div class="col-md-9">
                
                    <!-- header-right start -->
                    <!-- ================ -->
                    <div class="header-right clearfix">
                
                        <!-- main-navigation start -->
                        <!-- classes: -->
                        <!-- "onclick": Makes the dropdowns open on click, this the default bootstrap behavior e.g. class="main-navigation onclick" -->
                        <!-- "animated": Enables animations on dropdowns opening e.g. class="main-navigation animated" -->
                        <!-- "with-dropdown-buttons": Mandatory class that adds extra space, to the main navigation, for the search and cart dropdowns -->
                        <!-- ================ -->
                        <div class="main-navigation  animated with-dropdown-buttons">
                
                            <!-- navbar start -->
                            <!-- ================ -->
                            <nav class="navbar navbar-default" role="navigation">
                                <div class="container-fluid">
                
                                    <!-- Toggle get grouped for better mobile display -->
                                    <div class="navbar-header">
                                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse-1">
                                            <span class="sr-only">Toggle navigation</span>
                                            <span class="icon-bar"></span>
                                            <span class="icon-bar"></span>
                                            <span class="icon-bar"></span>
                                        </button>
                
                                    </div>
                
                                    <!-- Collect the nav links, forms, and other content for toggling -->
                                    <div class="collapse navbar-collapse" id="navbar-collapse-1">
                                        <!-- main-menu -->
                                        <ul class="nav navbar-nav " style="font-family:century gothic">
                                            <li>
                                                <a  href="index.php">Home</a>
                                            </li>
                                            <li>
                                                <a  href="about/">About Us</a>
                                            </li>
                
                                            <!-- mega-menu start -->
                
                                            <!-- mega-menu end -->
                                            <li class="dropdown ">
                                                <a class="dropdown-toggle" data-toggle="dropdown" href="#">Courses</a>
                                                <ul class="dropdown-menu">
                                                    <li class="dropdown ">
                                                        <a  class="dropdown-toggle" data-toggle="dropdown" href="big-data-hadoop-training/index.php">Big Data Hadoop Training </a>
                                                        <ul class="dropdown-menu">
                                                            <li ><a href="big-data-hadoop-training/">Bigdata Hadoop Training </a></li>
                                                            <li ><a href="big-data-hadoop-training-in-pune/">Bigdata Hadoop Training in Pune</a></li>
                                                            <li ><a href="big-data-hadoop-training/">Bigdata Hadoop Online Training </a></li>
                                                            <li ><a href="big-data-hadoop-training/">Bigdata Hadoop Online self Training</a></li>
                                                        </ul>
                                                    </li>
                                                    <li class="dropdown ">
                                                        <a  class="dropdown-toggle" data-toggle="dropdown" href="hadoop-admin/index.php">Hadoop Admin</a>
                                                        <ul class="dropdown-menu">
                                                            <li ><a href="hadoop-admin/">Class Room Training</a></li>
                                                            <li ><a href="hadoop-admin-online-course/">Hadoop Admin Online Course</a></li>
                                                            <li ><a href="hadoop-admin/">Hadoop Admin Online Self Training</a></li>
                
                                                        </ul>
                                                    </li>
                                                    <li ><a href="apache-spark-scala-training-2/">Apache Spark Scala Training</a></li>
                                                    <li ><a href="data-science-course-training-bangalore/">Data Science Course</a></li>
                                                    <li ><a href="r-programing/">R Programming</a></li>
                                                    <li ><a href="tableau/">Tableau</a></li>
                                                    <li ><a href="python-online-course/">Python</a></li>
                                                    <li ><a href="sastraining/">SAS Training</a></li>
                                                </ul>
                                            </li>
                                            <!-- mega-menu start -->													
                
                                            <!-- mega-menu end -->
                                            <li class="dropdown ">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Services</a>
                                                <ul class="dropdown-menu">
                                                    <li><a  href="services/corporate-training">Corporate Training</a></li>
                                                    <li><a  href="services/online-softwares">Hadoop Softwares</a></li>
                                                    <li><a  href="services/workshop-on-bigdata-hadoop">Workshop on Bigdata & Hadoop</a></li>
                                                    <li ><a href="#">LMS</a></li>
                                                    <li ><a href="services/placement-assistance">Placement Assistance</a></li>
                                                </ul>
                                            </li>
                                            <li ><a href="careers/">Career</a></li>
                
                                            <li ><a href="gallery/">Gallery</a></li>
                                            <li ><a href="contact/">Contact</a></li>
                
                                        </ul>
                                        <!-- main-menu end -->
                
                                        <!-- header dropdown buttons -->
                                        <div class="header-dropdown-buttons hidden-xs ">
                                            <div class="btn-group dropdown">
                                                <button type="button" class="btn dropdown-toggle" data-toggle="dropdown"><i class="icon-search"></i></button>
                                                <ul class="dropdown-menu dropdown-menu-right dropdown-animation" id="search-form">
                                                    <li>
                                                        <form role="search" class="search-box margin-clear">
                                                            <div class="form-group has-feedback">
                                                                <input type="text" class="form-control" placeholder="Search" style="background:rgba(0,0,0,0.7);color:white">
                                                                <i class="icon-search form-control-feedback"></i>
                                                            </div>
                                                        </form>
                                                    </li>
                                                </ul>
                                            </div>
                
                                        </div>
                                        <!-- header dropdown buttons end-->
                
                                    </div>
                
                                </div>
                            </nav>
                            <!-- navbar end -->
                
                        </div>
                        <!-- main-navigation end -->	
                    </div>
                    <!-- header-right end -->
                
                </div>
            </div>
        </div>
                
    </header>
</div>
                        <?php
if ($request == "") {
    ?>
                        <div class="banner clearfix">

				<!-- slideshow start -->
				<!-- ================ -->
				<div class="slideshow">
					<!-- slider revolution start -->
					<!-- ================ -->
					<div class="slider-banner-container">
						<div class="slider-banner-fullscreen">
							<ul class="slides">
                                                                <!-- slide 2 start -->
								<!-- ================ -->
								<li data-transition="slidehorizontal" data-slotamount="1"  data-masterspeed="1000" data-fstransition="fade" data-fsmasterspeed="1000" data-saveperformance="off" data-title="">
								
								<!-- main image -->
                                                                <img src="videos/header-img.jpg" alt="slidebg1" data-bgposition="center center"  data-bgrepeat="no-repeat" data-bgfit="cover">
								
								<!-- Transparent Background -->
								<div class="tp-caption dark-translucent-bg"
									data-x="center"
									data-y="bottom"
									data-speed="600"
									data-easing="easeOutQuad"
									data-start="0">
								</div>

								<!-- Video Background -->
								<div class="tp-caption tp-fade fadeout fullscreenvideo"
									data-x="0"
									data-y="0"
									data-speed="1000"
									data-start="1100"
									data-easing="Power4.easeOut"
									data-elementdelay="0.01"
									data-endelementdelay="0.1"
									data-endspeed="1500"
									data-endeasing="Power4.easeIn"
									data-autoplay="true"
									data-autoplayonlyfirsttime="false"
									data-nextslideatend="true"
						 			data-volume="mute" 
						 			data-forceCover="1" 
						 			data-aspectratio="16:9" 
						 			data-forcerewind="on" 
						 			style="z-index: 2;">
                                                                    <video class="" preload="none" width="100%" height="100%" poster='videos/header-img.jpg'> 
                                                                        <source src='videos/Renningen.webm' type='video/mp4'/>
										<source src='videos/' type='video/webm'/>
									</video>
								</div>

								<!-- LAYER NR. 1 -->
								<div class="tp-caption sfb fadeout large_white text-center"
									data-x="center"
									data-y="200"
									data-speed="500"
									data-start="1000"
									data-easing="easeOutQuad"><span class="heading-font">Big Data Fundamentals</span>
								</div>	

								<!-- LAYER NR. 2 -->
								<div class="tp-caption sfb fadeout large_white tp-resizeme"
									data-x="center"
									data-y="250"
									data-speed="500"
									data-start="1300"
									data-easing="easeOutQuad"><div class="separator light"></div>
								</div>	

								<!-- LAYER NR. 3 -->
								<div class="tp-caption sfb fadeout medium_white text-center"
									data-x="center"
									data-y="290"
									data-speed="500"
									data-start="1300"
									data-easing="easeOutQuad"
                                                                        data-endspeed="600"><span style = "margin-left:auto;margin-right:auto;font-family:century gothic;text-align:justify">Become Big Data & Hadoop Developer</span>
								</div>

								<!-- LAYER NR. 4 -->
								<div class="tp-caption fade fadeout"
									data-x="center"
									data-y="bottom"
									data-voffset="-100"
									data-speed="500"
									data-easing="easeOutQuad"
									data-start="2000"
									data-endspeed="200">
									<a href="#dishes" class="btn btn-lg moving smooth-scroll"><i class="icon-down-open-big"></i><i class="icon-down-open-big"></i><i class="icon-down-open-big"></i></a>
								</div>

								</li>
								<!-- slide 2 end -->
								<!-- slide 1 start -->
								<!-- ================ -->
								<li data-transition="random-static" data-slotamount="7" data-masterspeed="500" data-saveperformance="on" data-title="">
								
								<!-- main image -->
                                                                <img src="images/sliderbg3.jpg" alt="slidebg1" data-bgposition="center top"  data-bgrepeat="no-repeat" data-bgfit="cover">
								
								<!-- Transparent Background -->
								<div class="tp-caption dark-translucent-bg"
									data-x="center"
									data-y="bottom"
									data-speed="500"
									data-easing="easeOutQuad"
									data-start="0">
								</div>

								<!-- LAYER NR. 1 -->
								<div class="tp-caption sfr stl xlarge_white"
									data-x="center"
									data-y="70"
									data-speed="200"
									data-easing="easeOutQuad"
									data-start="1000"
									data-end="2500"
									data-splitin="chars"
									data-elementdelay="0.1"
									data-endelementdelay="0.1"
									data-splitout="chars">Get Certified.
								</div>

								<!-- LAYER NR. 2 -->
								<div class="tp-caption sfl str xlarge_white"
									data-x="center"
									data-y="70"
									data-speed="200"
									data-easing="easeOutQuad"
									data-start="2500"
									data-end="4000"
									data-splitin="chars"
									data-elementdelay="0.1"
									data-endelementdelay="0.1"
									data-splitout="chars">Get Ahead.
								</div>

								<!-- LAYER NR. 3 -->
								<div class="tp-caption sfr stt xlarge_white"
									data-x="center"
									data-y="70"
									data-speed="200"
									data-easing="easeOutQuad"
									data-start="4000"
									data-end="6000"
									data-splitin="chars"
									data-elementdelay="0.1"
									data-endelementdelay="0.1"
									data-splitout="chars"
									data-endspeed="400">Get Success.
								</div>					

								<!-- LAYER NR. 4 -->
                                                                
								

								<!-- LAYER NR. 5 -->
								<div class="tp-caption sfr fadeout"
									data-x="center"
									data-y="210"
									data-hoffset="-232"
									data-speed="500"
									data-easing="easeOutQuad"
									data-start="1000"
									data-end="5500"><span class="icon large circle"><i class="circle icon-lightbulb"></i></span>
								</div>

								<!-- LAYER NR. 6 -->
								<div class="tp-caption sfl fadeout"
									data-x="center"
									data-y="210"
									data-speed="500"
									data-easing="easeOutQuad"
									data-start="2500"
									data-end="5500"><span class="icon large circle"><i class="circle icon-arrows-ccw"></i></span>
								</div>

								<!-- LAYER NR. 7 -->
								<div class="tp-caption sfr fadeout"
									data-x="center"
									data-y="210"
									data-hoffset="232"
									data-speed="500"
									data-easing="easeOutQuad"
									data-start="4000"
									data-end="5500"><span class="icon large circle"><i class="circle icon-chart-line"></i></span>
								</div>

								<!-- LAYER NR. 8 -->
									

								<!-- LAYER NR. 9 -->
								<div class="tp-caption sft fadeout medium_white text-center"
									data-x="center"
									data-y="210"
									data-speed="500"
									data-easing="easeOutQuad"
									data-start="5800"
									data-end="10000"
                                                                        data-endspeed="600"><span class="heading-font">Achieve your career goal with industry recognized learning paths.</span>
								</div>

								<!-- LAYER NR. 10 -->
								<div class="tp-caption fade fadeout"
									data-x="center"
									data-y="bottom"
									data-voffset="100"
									data-speed="500"
									data-easing="easeOutQuad"
									data-start="2000"
									data-end="10000"
									data-endspeed="200">
									<a href="#page-start" class="btn btn-lg moving smooth-scroll"><i class="icon-down-open-big"></i><i class="icon-down-open-big"></i><i class="icon-down-open-big"></i></a>
								</div>
								</li>
								<!-- slide 1 end -->

								<!-- slide 2 start -->
								<!-- ================ -->
								<li data-transition="random-static" data-slotamount="7" data-masterspeed="500" data-saveperformance="on" data-title="">
								
								<!-- main image -->
                                                                <img src="images/sliderbg8.jpg" alt="slidebg2" data-bgposition="center top"  data-bgrepeat="no-repeat" data-bgfit="cover">

								<!-- Transparent Background -->
								<div class="tp-caption dark-translucent-bg"
									data-x="center"
									data-y="bottom"
									data-speed="500"
									data-easing="easeOutQuad"
									data-start="0">
								</div>

								<!-- LAYER NR. 1 -->
								<div class="tp-caption sfb fadeout large_white"
									data-x="left"
									data-y="70"
									data-speed="500"
									data-start="1000"
									data-easing="easeOutQuad"
									data-end="10000"><span class="heading-font">The Prwatech</span> <br> 
								</div>	

								<!-- LAYER NR. 2 -->
								<div class="tp-caption sfb fadeout text-left medium_white"
									data-x="left"
									data-y="200" 
									data-speed="500"
									data-start="1300"
									data-easing="easeOutQuad"
                                                                        data-endspeed="600"><span class="icon default-bg circle small hidden-xs"><i class="fa fa-laptop"></i></span> <span class="heading-font">Learn from the experts</span>
								</div>

								<!-- LAYER NR. 3 -->
								<div class="tp-caption sfb fadeout text-left medium_white"
									data-x="left"
									data-y="260" 
									data-speed="500"
									data-start="1600"
									data-easing="easeOutQuad"
                                                                        data-endspeed="600"><span class="icon default-bg circle small hidden-xs"><i class="icon-check"></i></span>  <span class="heading-font">Guaranteed career growth </span>
								</div>

								<!-- LAYER NR. 4 -->
								<div class="tp-caption sfb fadeout text-left medium_white"
									data-x="left"
									data-y="320" 
									data-speed="500"
									data-start="1900"
									data-easing="easeOutQuad"
                                                                        data-endspeed="600"><span class="icon default-bg circle small hidden-xs"><i class="icon-gift"></i></span>  <span class="heading-font">Accredited curriculum</span>
								</div>

								<!-- LAYER NR. 5 -->
								<div class="tp-caption sfb fadeout text-left medium_white"
									data-x="left"
									data-y="380" 
									data-speed="500"
									data-start="2200"
									data-easing="easeOutQuad"
                                                                        data-endspeed="600"><span class="icon default-bg circle small hidden-xs"><i class="icon-hourglass"></i></span>  <span class="heading-font">In-depth  curriculum</span>
								</div>

								<!-- LAYER NR. 6 -->
<!--								<div class="tp-caption sfb fadeout small_white"
									data-x="left"
									data-y="450"
									data-speed="500"
									data-start="2500"
									data-easing="easeOutQuad"
									data-endspeed="600"><a href="#" class="btn btn-default btn-lg btn-animated">Purchase <i class="fa fa-cart-arrow-down"></i></a>
								</div>-->
								</li>
								<!-- slide 2 end -->
							</ul>
							<div class="tp-bannertimer"></div>
						</div>
					</div>
					<!-- slider revolution end -->

				</div>
				<!-- slideshow end -->
                        </div>
			<div id="page-start"></div>
			<section class="light-gray-bg pv-30 clearfix" id="row1">
				<div class="container">
					<div class="row">
						<div class="col-md-8">
                                                    <h1 class="text-center heading-font" style="text-transform:none;"><strong>Big Data Hadoop Training</strong> Courses In Bangalore & Pune </h1>
                                                    <div class="separator"></div>
                                                         <p class="cpara2">Welcome to Prwatech. The on-demand technology learning platform you count on to stay relevant, with tools that measure your skills and solve your problems—faster.</p>
                                                        <p class="cpara2">Learn to lead the future communication technology. Data management occupies first place among all other existing technologies. Equip to face the challenges of by developing the capabilities and skill sets to match the present industry requirements. A comprehensive Big Data Internship leads to explore the amazing world of data management from the core.</p>
						</div>
						<div class="col-md-4">
                                                    <a class="btn" data-toggle="modal" data-target="#demoVideoPopUp"><img src="images/demo-1.png"/></a>
                                                    
                                               </div>
						
					</div>
                                    <p>&nbsp;</p>
                                 </div>
                        </section>
			<div class="clearfix"></div>
<section class="pv-30" id="row2">
                            <div class="container">
                                <div class="row">
                                    <div class="col-md-8 col-md-offset-2">
                                        <h3 class="text-center heading-font" style="font-size:28px;text-transform:none">Masters <strong>Program</strong></h3>
                                        <div class="separator"></div>
                                        <p class="large text-center heading-font">Highly specialized courses to drive your success.Your awesome career in Data Science and Data Engineering
                                            starts here.</p>
                                        <br>
                                    </div>
                                </div>
                            </div>
                            
                                <div class="container">
                                    <div class="row">
                                        <div class="col-sm-3">
                                            <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
                                                <div class="overlay-container">
                                                    <img src="images/por-1.png" alt="">
                                                    <div class="overlay-to-top">
                                                        <p class="small margin-clear">&nbsp;</p>
                                                    </div>
                                                </div>
                                                <div class="body">
                                                    <h3 class="heading-font">Big Data Hadoop Training</h3>
                                                    <div class="separator"></div>
                                                    <p class="heading-font">Advanced Program for Top-notch Developers </p>
                                                    <a href="big-data-hadoop-training/"  target="_blank" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read More<i class="fa fa-arrow-right pl-10"></i></a>												
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
                                                <div class="overlay-container">
                                                    <img src="images/por-2.png" alt="">
                                                    <div class="overlay-to-top">
                                                        <p class="small margin-clear">&nbsp;</p>
                                                    </div>
                                                </div>
                                                <div class="body">
                                                    <h3 class="heading-font">Hadoop Admin Training</h3>
                                                    <div class="separator"></div>
                                                    <p class="heading-font">Advanced Program for Top-notch Developers </p>
                                                    <a href="hadoop-admin/" target="_blank" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read More<i class="fa fa-arrow-right pl-10"></i></a>												
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
                                                <div class="overlay-container">
                                                    <img src="images/por-3.png" alt="">
                                                    <div class="overlay-to-top">
                                                        <p class="small margin-clear">&nbsp;</p>
                                                    </div>
                                                </div>
                                                <div class="body">
                                                    <h3 class="heading-font">Apache Spark Scala Training</h3>
                                                    <div class="separator"></div>
                                                    <p class="heading-font">Apache Spark Scala Combo Training </p>
                                                    <a href="apache-spark-scala-training-2/" target="_blank" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read More<i class="fa fa-arrow-right pl-10"></i></a>												
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
                                                <div class="overlay-container">
                                                    <img src="images/por-4.png" alt="">
                                                    <div class="overlay-to-top">
                                                        <p class="small margin-clear">&nbsp;</p>
                                                    </div>
                                                </div>
                                                <div class="body">
                                                    <h3 class="heading-font">Data Science Training</h3>
                                                    <div class="separator"></div>
                                                    <p class="heading-font">Find out the truth about what Data Science is. </p>
                                                    <a href="data-science-course-training-bangalore/" target="_blank" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read More<i class="fa fa-arrow-right pl-10"></i></a>												
                                                </div>
                                            </div>
                                        </div>
                                       
                                    </div>
                                       <p>&nbsp;</p>
                                </div>
                                <div class="container">
                                    <div class="row">
                                        <div class="col-sm-3">
                                            <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
                                                <div class="overlay-container">
                                                    <img src="images/por-5.png" alt="">
                                                    <div class="overlay-to-top">
                                                        <p class="small margin-clear">&nbsp;</p>
                                                    </div>
                                                </div>
                                                <div class="body">
                                                    <h3 class="heading-font">R Programming Training</h3>
                                                    <div class="separator"></div>
                                                    <p class="heading-font">Business Analytics With R Training</p>
                                                    <a href="r-programing/" target="_blank" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read More<i class="fa fa-arrow-right pl-10"></i></a>												
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
                                                <div class="overlay-container">
                                                    <img src="images/por-6.png" alt="">
                                                    <div class="overlay-to-top">
                                                        <p class="small margin-clear">&nbsp;</p>
                                                    </div>
                                                </div>
                                                <div class="body">
                                                    <h3 class="heading-font"> Tableau Training & Certification</h3>
                                                    <div class="separator"></div>
                                                    <p class="heading-font">Speed on concepts of data visualization</p>
                                                    <a href="tableau/" target="_blank" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read More<i class="fa fa-arrow-right pl-10"></i></a>												
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
                                                <div class="overlay-container">
                                                    <img src="images/por-7.png" alt="">
                                                    <div class="overlay-to-top">
                                                        <p class="small margin-clear">&nbsp;</p>
                                                    </div>
                                                </div>
                                                <div class="body">
                                                    <h3 class="heading-font">Python Programming</h3>
                                                    <div class="separator"></div>
                                                    <p class="heading-font">Advanced Concepts for Top-notch Developers  </p>
                                                    <a href="python-online-course/" target="_blank" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read More<i class="fa fa-arrow-right pl-10"></i></a>												
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
                                                <div class="overlay-container">
                                                    <img src="images/por-7.png" alt="">
                                                    <div class="overlay-to-top">
                                                        <p class="small margin-clear">&nbsp;</p>
                                                    </div>
                                                </div>
                                                <div class="body">
                                                    <h3 class="heading-font">Big Data Hadoop Training in Pune</h3>
                                                    <div class="separator"></div>
                                                    <p class="heading-font"> Advanced Program for Top-notch Developers </p>
                                                    <a href="big-data-hadoop-training-in-pune/" target="_blank" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read More<i class="fa fa-arrow-right pl-10"></i></a>												
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                       
                                
                                       <p>&nbsp;</p>
                                </div>
                                 
                                
                             
                           
			</section>
                        <div class="section pv-40  dark-translucent-bg background-img-7">
                            <div class="container">
                                
                                <h3 class="text-center heading-font object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100" style="text-transform:none;">Our  <strong>Offerings</strong>.</h3>
                                    
                                <p>&nbsp;</p>
                                <div class="col-md-3" >
                                    <div  style="" class="service-box ph-20 feature-box text-center object-non-visible animated object-visible fadeInDownSmall" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
                                        <span class="icon default-bg circle"><i class="fa fa-umbrella"></i></span>
                                        <h3>Corporate Training</h3>
                                        <div class="separator clearfix"></div>
                                        <p class="text-justify">IT professionals, or anyone who is looking towards building a career in Big Data and Hadoop are ideal participants for the Big Data and Hadoop training. </p>
                                        <a style="color:#FFC107;text-decoration:none;" href="services/corporate-training.php">Read More<i class="pl-5 fa fa-angle-double-right"></i></a>
                                    </div>
                                </div>
                                <div class="col-md-3" >
                                    <div  style="" class="service-box ph-20 feature-box text-center object-non-visible animated object-visible fadeInDownSmall" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
                                        <span class="icon default-bg circle"><i class="fa fa-area-chart"></i></span>
                                        <h3>IT Training</h3>
                                        <div class="separator clearfix"></div>
                                        <p class="text-justify">The Big Data and Hadoop internship is an opportunity to access the knowledge on YARN,the data operating system where you are going to learn Script, SQL, JAVA,..</p>
                                        <a style="color:#FFC107;text-decoration:none;" href="big-data-hadoop-training/">Read More<i class="pl-5 fa fa-angle-double-right"></i></a>
                                    </div>
                                </div>
                                <div class="col-md-3" >
                                    <div  style="" class="service-box ph-20 feature-box text-center object-non-visible animated object-visible fadeInDownSmall" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
                                        <span class="icon default-bg circle"><i class="fa fa-dashcube"></i></span>
                                        <h3>Online Training</h3>
                                        <div class="separator clearfix"></div>
                                        <p class="text-justify">Live online and interactive classes conducted with certified  instructors from the convenience of your home or office! <br/> <br/> </p>
                                        <a style="color:#FFC107;text-decoration:none;" href="services/corporate-training.php">Read More<i class="pl-5 fa fa-angle-double-right"></i></a>
                                    </div>
                                </div>
                                <div class="col-md-3" >
                                    <div  style="" class="service-box ph-20 feature-box text-center object-non-visible animated object-visible fadeInDownSmall" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
                                        <span class="icon default-bg circle"><i class="fa fa-hospital-o"></i></span>
                                        <h3>College Workshop</h3>
                                        <div class="separator clearfix"></div>
                                        <p class="text-justify">In PRWATECH we make sure that real time experience is being given to students and professional , through workshop in advance fields like BIG Data & Hadoop..</p>
                                        <a style="color:#FFC107;text-decoration:none;" href="services/workshop-on-bigdata-hadoop.php">Read More<i class="pl-5 fa fa-angle-double-right"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>	
                        <section class="pv-30  padding-bottom-clear dark-translucent-bg parallax" id="row-testimonials"style="background:url('images/bg/451.jpg'); box-shadow:inset 0 2px 7px rgba(0,0,0, 0.25);">
                            <div class="space-bottom">
                                <div class="owl-carousel content-slider">
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-md-8 col-md-offset-2">
                                                <div class="testimonial text-center">
                                                    <div class="testimonial-image">
                                                        <img src="images/testimonial-1.png" alt="" title="" class="img-circle">
                                                    </div>
                                                        
                                                    <div class="testimonial-body">
                                                        <blockquote>
                                                            <p class="cpara3">It was a nice learning experience with prwatech. The classes were well scheduled and managed.
                                                                Verma has good understanding of the topics taught and catered to our problems and doubts very patiently. The best thing about him was that he handled the situations accordingly, when needed a friend, he became one and also a teacher to always guide us.</p>
                                                        </blockquote>
                                                        <div class="cpara3">Anukanksha Garg</div>
                                                        <div class="testimonial-info-2 cpara3">B.TECH- CS</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-md-8 col-md-offset-2">
                                                <div class="testimonial text-center">
                                                    <div class="testimonial-image">
                                                        <img src="images/testimonial-2.jpg" alt="" title="" class="img-circle">
                                                    </div>
                                                        
                                                    <div class="testimonial-body">
                                                        <blockquote>
                                                            <p class="cpara3">“Excellent course and instructor. I learnt a lot in a short period. Good value for money. Instructor took us through Advanced Hadoop Development in depth.”</p>
                                                        </blockquote>
                                                        <div class="testimonial-info-1 cpara3">Anupam Khamparia</div>
                                                        <div class="testimonial-info-2 cpara3">Consultant, Cognizant Technology Solutions, Bangalore</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-md-8 col-md-offset-2">
                                                <div class="testimonial text-center">
                                                    <div class="testimonial-image">
                                                        <img src="images/testimonial-3.jpg" alt="" title="" class="img-circle">
                                                    </div>
                                                        
                                                    <div class="testimonial-body">
                                                        <blockquote>
                                                            <p class="cpara3">“Excellent course and instructor. I learnt a lot in a short period. Good value for money. Instructor took us through Advanced Hadoop Development in depth.”</p>
                                                        </blockquote>
                                                        <div class="testimonial-info-1 cpara3">Kumar Waibhav</div>
                                                        <div class="testimonial-info-2 cpara3">Software Engineer, Cognizant Technology Solutions,<br> Philadelphia, Pennsylvania, USA</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                    
                            </div>
                        </section> 
                        	<section class="light-gray-bg pv-30 clearfix" id="row1">
				
                                <div class="container">
					<div class="row">
						<div class="col-md-8">
                                                    <h2 class="text-center heading-font" style="text-transform:none;"><strong> Prwatech </strong>provide paid Internship<br>  On <br> <b>Bigdata & Hadoop</b></h2>
                                                    <div class="separator"></div>
                                                         <p class="cpara2">Learn to lead the future communication technology. Data management occupies first place among all other existing technologies. Equip to face the challenges of by developing the capabilities and skill sets to match the present industry requirements. A comprehensive Big Data Internship leads to explore the amazing world of data management from the core.</p>
                                                         <p class="cpara2">The PRWATECH data platform extensively provides integrated solutions to your existing business operations that is reliable and compatible with your existing database management system. Understand the modern database management system through <span id="highlighter">Hadoop Online Training Bangalore </span>.</p>
                                                         <p class="cpara2">Knowing the power of <span id="highlighter">Hadoop Certification Courses </span>makes you handle efficiently all the operations from beginning to end, enabling to handle all the levels of projects smoothly.</p>
                                                         <p class="cpara2">The <span id="highlighter"><span id="highlighter">Big Data</span> Hadoop Training Bangalore </span>internship is an opportunity to access the knowledge on YARN, the data operating system where you are going to learn Script, SQL, JAVA, NoSQL, Stream, in memory, and become an expert in HDFS (Hadoop distributed file system). The Big Data Hadoop training in Bangalore, introduces multiple processes and languages that works successfully with Hadoop cluster.</p>
                                                        <p class="cpara2">Big data training institutes in Bangalore such as PRWATECH assists aspirants and professionals to know more about the different platforms used for Hadoop operations. YARN divides the purpose of handling the data into four major categories.</p>
                                                        <ul class="list-icons" id="clist">
                                                            <li><i class="icon-check pr-10"></i>  It acts as a global resource manager</li>
                                                            <li><i class="icon-check pr-10"></i>   It acts as an application Master</li>
                                                            <li><i class="icon-check pr-10"></i>  Functions as node Manager</li>
                                                            <li><i class="icon-check pr-10"></i>  Work per application container <br> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; with the Node manager.</li>
                                                                
                                                        </ul>
                                                        <p class="cpara2">The Best Hadoop training in Bangalore is provided by the PRWATECH, one of the pioneers in the field of <span id="highlighter">Big Data Courses In Bangalore</span>. Training professionals helping them to hone their skills and develop requisite expertise in the field of Hadoop and big data management.</p>
                                                        <p class="cpara2">The <span id="highlighter">Big Data Training In Bangalore</span> and <span id="highlighter">Big Data Hadoop Training Institutes In Bangalore</span> covers all the programs such as Apache, Apache Cassandra and other relevant subjects, making aspirants capable enough to explore the world of Hadoop from the core to finish. Explore and expedite your career life with the help of PRWATECH.</p>
						</div>
						<div class="col-md-4">
                                                    <a class="btn" data-toggle="modal" data-target="#demoVideoPopUp"><img src="images/demo-3.png"/></a>
                                                   
                                                <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center" >
                                                <div class="body">
                                                    <div itemscope itemtype="http://schema.org/Product"style="text-align: left">
                                                        <span itemprop="name">Big Data Hadoop Training</span><br>
                                                        <div itemprop="aggregateRating" itemscope itemtype="http://schema.org/AggregateRating">
                                                            Average Rating: <span itemprop="ratingValue">4.6</span><br>
                                                            Votes: <span itemprop="ratingCount">894</span><br>
                                                            Reviews: <span itemprop="reviewCount">894</span>
                                                        </div>

                                                    </div>
                                                </div>
                                                </div>
                                               </div>
						
					</div>
                                    <p>&nbsp;</p>
                                    
                                 
				</div>
                              
                          
				<div class="container">
					<div class="row">
						<div class="col-md-12">
                                                    <h3 class="text-center heading-font" style="text-decoration:none" ><b>Corporate Training Conducted & Our Clients</b></h3>
							<div class="separator"></div>
                                                        <div class="row">
                                                          <div class="owl-carousel carousel-autoplay">
								<div class="overlay-container">
									<img src="images/client/flipkart.png" alt="">
								</div>
								<div class="overlay-container">
									<img src="images/client/ibm.png" alt="">
								</div>
								<div class="overlay-container">
									<img src="images/client/capgimini.png" alt="">
								</div>
								<div class="overlay-container">
									<img src="images/client/syntel.png" alt="">
								</div>
								<div class="overlay-container">
									<img src="images/client/synchron.png" alt="">
								</div>
								<div class="overlay-container">
									<img src="images/client/sungaurd.png" alt="">
								</div>
								<div class="overlay-container">
									<img src="images/client/hcl.png" alt="">
								</div>
								<div class="overlay-container">
									<img src="images/client/ciber.png" alt="">
								</div>
								<div class="overlay-container">
									<img src="images/client/amdocs.png" alt="">
								</div>
								<div class="overlay-container">
									<img src="images/client/yodlee.png" alt="">
								</div>
								
							</div>
                                                          
                                                      
							</div>
                                                       
						</div>
					</div>
				</div>
		
			</section>
                        <section class="pv-30 dark-bg padding-bottom-clear clearfix" >
					<br>
					<div class="container">
						<div class="row">
							<div class="col-md-3 ">
								<div class="ph-20 feature-box text-center object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
									<span class="icon default-bg circle"><i class="fa fa-institution"></i></span>
									<h3>Live classes</h3>
									<div class="separator clearfix"></div>
                                                                        <p class="heading-font ">Live online and interactive classes conducted by instructor</p>
									
								</div>
							</div>
							<div class="col-md-3 ">
								<div class="ph-20 feature-box text-center object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
									<span class="icon default-bg circle"><i class="fa fa-user"></i></span>
									<h3>Expert instructions</h3>
									<div class="separator clearfix"></div>
									<p class="heading-font">Learn from our Experts and get Real-Time Guidance</p>
									
								</div>
							</div>
							<div class="col-md-3 ">
								<div class="ph-20 feature-box text-center object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
									<span class="icon default-bg circle"><i class="icon-signal"></i></span>
									<h3>24 X 7 Support</h3>
									<div class="separator clearfix"></div>
									<p class="heading-font">Personalized Guidance from our 24X7 Support Team</p>
									
								</div>
							</div>
							<div class="col-md-3 ">
								<div class="ph-20 feature-box text-center object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
									<span class="icon default-bg circle"><i class="icon-calendar"></i></span>
									<h3>Flexible schedule</h3>
									<div class="separator clearfix"></div>
									<p class="heading-font">Reschedule your Batch/Class at Your Convenience</p>
									
								</div>
							</div>
						</div>
					</div>
					<br>
				</section>
<?php
} else{
    ?>
                        <div class="banner dark-translucent-bg" style="background-image:url('images/page-course-banner-1.jpg'); background-position: 50% 27%;">
				<!-- breadcrumb start -->
				<!-- ================ -->
				<div class="breadcrumb-container object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
					<div class="container">
						<ol class="breadcrumb">
                                                    <li><i class="fa fa-home pr-10"></i><a class="link-dark" href="prwatech.in">Home</a></li>
							
							<li class="active">Blog</li>
						</ol>
					</div>
				</div>
				<!-- breadcrumb end -->
				
			</div>
			<div id="page-start"></div>
                        <section class="pv-30 clearfix object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100" style="background:#f5f5f5;" >
				<div class="container">
                                     <div class="row">
                                         <div class="col-md-9" id="blog_details">
                                            <!-- page-title start -->
							<!-- ================ -->
							<h1 class="page-title">Blog Post</h1>
							<!-- page-title end -->

							<!-- blogpost start -->
							<!-- ================ -->
							<article class="blogpost full">
								<header>
									<div class="post-info">
										<span class="post-date">
											<i class="icon-calendar"></i>
											<span class="day">12</span>
											<span class="month">May 2015</span>
										</span>
										<span class="submitted"><i class="icon-user-1"></i> by <a href="#">John Doe</a></span>
										<span class="comments"><i class="icon-chat"></i> <a href="#">22 comments</a></span>
									</div>
								</header>
								<div class="blogpost-content">
                                                                   <div class="overlay-container">
											<img src="images/page-course-banner-1.jpg" alt="">
										</div>
                                                                    <p>&nbsp;</p>
									<p>Mauris dolor sapien, <a href="#">malesuada at interdum ut</a>, hendrerit eget lorem. Nunc interdum mi neque, et  sollicitudin purus fermentum ut. Suspendisse faucibus nibh odio, a vehicula eros pharetra in. Maecenas  ullamcorper commodo rutrum. In iaculis lectus vel augue eleifend dignissim. Aenean viverra semper sollicitudin.</p>
									<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corporis ullam nemo itaque excepturi suscipit unde repudiandae nesciunt ad voluptates minima recusandae illum exercitationem, neque, ut totam ratione. Consequuntur consequatur ad nesciunt nulla voluptate voluptates qui natus labore facilis dolore odit vero ea sint inventore tenetur et eligendi nobis fugit veniam quod possimus, quasi, voluptatem. Cupiditate?</p>
									<ol>
										<li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quasi, laboriosam tempore, veniam repudiandae dolor aperiam, iste porro amet odio eius earum tempora? Ex nobis suscipit, nam in eius, deserunt nihil.</li>
										<li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corporis odit est quae amet iure quia reiciendis maxime eos blanditiis tenetur voluptates, ab, obcaecati eaque accusamus dolorem a beatae mollitia quod?</li>
										<li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quam aperiam vel cum quisquam enim reprehenderit sunt cupiditate ullam, id quidem perspiciatis dolore molestiae iure odio. Dolore fuga voluptate, deleniti placeat.</li>
										<li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illo, eaque. Totam nisi ducimus dolor ab obcaecati temporibus asperiores, ad dignissimos dolorum unde fuga quae voluptates beatae quasi voluptatum culpa dolore?</li>
									</ol>
									<blockquote>
										<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.</p>
									</blockquote>
									<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ducimus incidunt, beatae rem omnis distinctio, dolor, praesentium impedit quisquam nobis pariatur nulla expedita aliquid repellendus laudantium. A illum sint corrupti eligendi quae ab, facilis eos quas! Velit earum facere ex maxime.</p>
								</div>
								<footer class="clearfix">
									<div class="tags pull-left"><i class="icon-tags"></i> <a href="#">tag 1</a>, <a href="#">tag 2</a>, <a href="#">long tag 3</a></div>
									<div class="link pull-right">
										<ul class="social-links circle small colored clearfix margin-clear text-right animated-effect-1">
											<li class="twitter"><a target="_blank" href="http://www.twitter.com/"><i class="fa fa-twitter"></i></a></li>
											<li class="googleplus"><a target="_blank" href="http://plus.google.com/"><i class="fa fa-google-plus"></i></a></li>
											<li class="facebook"><a target="_blank" href="http://www.facebook.com/"><i class="fa fa-facebook"></i></a></li>
										</ul>
									</div>
                                                                        
								</footer>
							</article>
							<!-- blogpost end -->

							<!-- comments start -->
							<!-- ================ -->
							<div id="comments" class="comments">
								<h2 class="title">There are 3 comments</h2>

								<!-- comment start -->
								<div class="comment clearfix">
									
									<div class="comment-content">
                                                                            <div class="comment-avatar">
										<img class="img-circle" src="images/avatar.jpg" alt="avatar">
                                                                            </div>
										<div class="comment-body clearfix">
											<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo </p>
										</div>
									</div>
									
									<!-- comment start -->
								
									<!-- comment end -->

								</div>
								<!-- comment end -->

								<!-- comment start -->

								<!-- comment end -->

							</div>
							<!-- comments end -->

							<!-- comments form start -->
							<!-- ================ -->
							<div class="comments-form">
                                                            
									<div class="">
<!--										<h4 class="panel-title">-->
											<a data-toggle="collapse" data-parent="#accordion-2" href="#collapseTwo-2" class="collapsed" aria-expanded="false">
												<i class="fa fa-comment pr-10"></i>Add your comment
											</a>
										<!--</h4>-->
									</div>
									<div id="collapseTwo-2" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                                                                
								<form role="form" id="comment-form">
									<div class="form-group has-feedback">
										<label for="name4">Name</label>
										<input type="text" class="form-control" id="name4" placeholder="" name="name4" required>
										<i class="fa fa-user form-control-feedback"></i>
									</div>
									<div class="form-group has-feedback">
										<label for="subject4">Subject</label>
										<input type="text" class="form-control" id="subject4" placeholder="" name="subject4" required>
										<i class="fa fa-pencil form-control-feedback"></i>
									</div>
									<div class="form-group has-feedback">
										<label for="message4">Message</label>
										<textarea class="form-control" rows="8" id="message4" placeholder="" name="message4" required></textarea>
										<i class="fa fa-envelope-o form-control-feedback"></i>
									</div>
									<input type="submit" value="Submit" class="btn btn-default">
								</form>
									</div>
								

							</div>
                                         </div>
                                     <div class="col-md-3" style="position:relative;top:80px;">
                                     <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center" >
                                         <h3 class="heading-font text-center" style="background:#4c4c4c;color:white;padding-top:10px;padding-bottom:10px">Categories</h3> 
                                         <div class="body">
                                            <table class="table table-striped ">
                                                <tbody id="category_tbody">

                                                 <tr><th><a href="">Data Science</a></th></tr>
                                                 
                                                 </tbody>
                                             </table>
                                          
                                             
                                         </div>
                                     </div>
                                      <h3 class="heading-font text-center" style="background:#4c4c4c;color:white;padding-top:10px;padding-bottom:10px">Suggested Courses</h3> 
                                      <div class="owl-carousel content-slider">
                                          <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
                                              <div class="overlay-container">
                                                  <img src="images/por-1.png" alt="">
                                                  <div class="overlay-to-top">
                                                      <p class="small margin-clear">&nbsp;</p>
                                                  </div>
                                              </div>
                                              <div class="body">
                                                  <h3 class="heading-font">Big Data R-Programming Training</h3>
                                                  <div class="separator"></div>
                                                  <p class="heading-font">Advanced Program for Top-notch Developers </p>
                                                  <a href="big-data-hadoop-training.php"  target="_blank" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read More<i class="fa fa-arrow-right pl-10"></i></a>												
                                              </div>
                                          </div>
                                          <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
                                              <div class="overlay-container">
                                                  <img src="images/por-2.png" alt="">
                                                  <div class="overlay-to-top">
                                                      <p class="small margin-clear">&nbsp;</p>
                                                  </div>
                                              </div>
                                              <div class="body">
                                                  <h3 class="heading-font">R-Programming Admin Training</h3>
                                                  <div class="separator"></div>
                                                  <p class="heading-font">Advanced Program for Top-notch Developers </p>
                                                  <a href="hadoop-admin.php" target="_blank" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read More<i class="fa fa-arrow-right pl-10"></i></a>												
                                              </div>
                                          </div>
                                          <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
                                              <div class="overlay-container">
                                                  <img src="images/por-3.png" alt="">
                                                  <div class="overlay-to-top">
                                                      <p class="small margin-clear">&nbsp;</p>
                                                  </div>
                                              </div>
                                              <div class="body">
                                                  <h3 class="heading-font">Apache Spark Scala Training</h3>
                                                  <div class="separator"></div>
                                                  <p class="heading-font">Apache Spark Scala Combo Training </p>
                                                  <a href="apache-spark-scala-training.php" target="_blank" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read More<i class="fa fa-arrow-right pl-10"></i></a>												
                                              </div>
                                          </div>
                                          <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
                                              <div class="overlay-container">
                                                  <img src="images/por-4.png" alt="">
                                                  <div class="overlay-to-top">
                                                      <p class="small margin-clear">&nbsp;</p>
                                                  </div>
                                              </div>
                                              <div class="body">
                                                  <h3 class="heading-font">Data Science Training</h3>
                                                  <div class="separator"></div>
                                                  <p class="heading-font">Find out the truth about what Data Science is. </p>
                                                  <a href="data-science-course-training-bangalore.php" target="_blank" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read More<i class="fa fa-arrow-right pl-10"></i></a>												
                                              </div>
                                          </div>
                                          <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
                                              <div class="overlay-container">
                                                  <img src="images/por-5.png" alt="">
                                                  <div class="overlay-to-top">
                                                      <p class="small margin-clear">&nbsp;</p>
                                                  </div>
                                              </div>
                                              <div class="body">
                                                  <h3 class="heading-font">R Programming Training</h3>
                                                  <div class="separator"></div>
                                                  <p class="heading-font">Business Analytics With R Training</p>
                                                  <a href="r-programing.php" target="_blank" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read More<i class="fa fa-arrow-right pl-10"></i></a>												
                                              </div>
                                          </div>
                                          <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
                                              <div class="overlay-container">
                                                  <img src="images/por-6.png" alt="">
                                                  <div class="overlay-to-top">
                                                      <p class="small margin-clear">&nbsp;</p>
                                                  </div>
                                              </div>
                                              <div class="body">
                                                  <h3 class="heading-font"> Tableau Training & Certification</h3>
                                                  <div class="separator"></div>
                                                  <p class="heading-font">Speed on concepts of data visualization</p>
                                                  <a href="tableau.php" target="_blank" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read More<i class="fa fa-arrow-right pl-10"></i></a>												
                                              </div>
                                          </div>
                                          <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
                                              <div class="overlay-container">
                                                  <img src="images/por-7.png" alt="">
                                                  <div class="overlay-to-top">
                                                      <p class="small margin-clear">&nbsp;</p>
                                                  </div>
                                              </div>
                                              <div class="body">
                                                  <h3 class="heading-font">Python Programming</h3>
                                                  <div class="separator"></div>
                                                  <p class="heading-font">Advanced Concepts for Top-notch Developers  </p>
                                                  <a href="python.php" target="_blank" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read More<i class="fa fa-arrow-right pl-10"></i></a>												
                                              </div>
                                          </div>
                                      </div> 
                                 </div> 
                                     </div>
					
                                    
				</div>
                             
                           
                         
			</section>
			<div class="clearfix"></div>
			
                        
<?php
}
?>
                        <!--comman-->
			
                        
			<footer id="footer" class="clearfix ">
<!--                            <div class="footer-upper-top">
                                
                            </div>-->
                            <div class="footer-upper">
                                
                            </div>
<!--                            <div id="footer-bottom" >
                                
                            </div>-->
				<!-- .footer start -->
				<!-- ================ -->
				<div class="footer" style="background:url('images/bg/3a.png');background-position:bottom center; box-shadow:inset 0 6px 10px rgba(0,0,0, 0.25);">
					<div class="container">
						<div class="footer-inner">
							<div class="row">
								<div class="col-md-3">
									<div class="footer-content">
                                                                            <div class="logo-footer"><img id="logo-footer" src="images/logo_light_blue.png" alt=""/></div>
                                                                            <p class="heading-font"style="text-align:justify">Training lays the foundation for an engineer.
                                                                                It provides a strong platform to build ones perception and implementation by mastering a wide range of skills .
                                                                                One of India’s leading and largest training provider for Big Data and Hadoop Corporate training programs is the prestigious PrwaTech. <a href="#">Learn More<i class="fa fa-long-arrow-right pl-5"></i></a></p>
										<div class="separator-2"></div>
										<nav>
										 <a href="terms-and-conditions.php">Terms, Conditions & Privacy</a>	
										</nav>
									</div>
								</div>
								<div class="col-md-3">
									<div class="footer-content">
										<h3 class="title">Latest From Blog</h3>
										<div class="separator-2"></div>
                                                                                <div id="blog_details_client_in_footer">
<!--										<div class="media margin-clear">
											<div class="media-left">
												<div class="overlay-container">
                                                                                                    <img class="media-object" src="../images/por-1.png" alt="blog-thumb">
													<a href="blog-post.html" class="overlay-link small"><i class="fa fa-link"></i></a>
												</div>
											</div>
											<div class="media-body">
												<h6 class="media-heading heading-font"><a href="blog-post.html">Learn about the Fundamentals of Hadoop Development</a></h6>
												<p class="small margin-clear"><i class="fa fa-calendar pr-10"></i>Mar 23, 2015</p>
											</div>
											<hr>
										</div>
										<div class="media margin-clear">
											<div class="media-left">
												<div class="overlay-container">
                                                                                                    <img class="media-object" src="../images/por-1.png" alt="blog-thumb">
													<a href="blog-post.html" class="overlay-link small"><i class="fa fa-link"></i></a>
												</div>
											</div>
											<div class="media-body">
												<h6 class="media-heading heading-font"><a href="blog-post.html">Learn about the Fundamentals of Hadoop Development</a></h6>
												<p class="small margin-clear"><i class="fa fa-calendar pr-10"></i>Mar 23, 2015</p>
											</div>
											<hr>
										</div>
										<div class="media margin-clear">
											<div class="media-left">
												<div class="overlay-container">
                                                                                                    <img class="media-object" src="../images/por-1.png" alt="blog-thumb">
													<a href="blog-post.html" class="overlay-link small"><i class="fa fa-link"></i></a>
												</div>
											</div>
											<div class="media-body">
												<h6 class="media-heading heading-font"><a href="blog-post.html">Learn about the Fundamentals of Hadoop Development</a></h6>
												<p class="small margin-clear"><i class="fa fa-calendar pr-10"></i>Mar 23, 2015</p>
											</div>
											<hr>
										</div>-->
										</div>
										<div class="text-right space-top">
                                                                                    <a href="blog/" class="link-dark"><i class="fa fa-plus-circle pl-5 pr-5"></i>More</a>	
										</div>
									</div>
								</div>
								<div class="col-md-3">
									<div class="footer-content">
										<h3 class="title">Find Us In Bangalore</h3>
										<div class="separator-2"></div>
                                                                                <div class="heading-font" itemscope itemtype="http://schema.org/PostalAddress"> 
<span itemprop="name">PRWATECH</span><br> 
<span itemprop="streetAddress">No.14, 29th Main , 2nd Cross, V.P road BTM-1st Stage, Behind AXA building, Land Mark : Vijaya Bank ATM</span><br> 
<span itemprop="addressLocality">Bangalore</span>, 
<span itemprop="addressRegion"> Karnataka </span> 
<span itemprop="postalCode">560 068</span><br> 
<span itemprop="addressCountry">India</span> 
</div>
									
										<div class="separator-2"></div>
											<ul class="social-links circle animated-effect-1">
											<li class="facebook"><a target="_blank" href="https://www.facebook.com/JustHadoop"><i class="fa fa-facebook"></i></a></li>
											<li class="twitter"><a target="_blank" href=" https://twitter.com/prwatech"><i class="fa fa-twitter"></i></a></li>
											<li class="googleplus"><a target="_blank" href="https://plus.google.com/+PrwatechBigDataHadoopTrainingCoursesBengaluru"><i class="fa fa-google-plus"></i></a></li>
											<li class="linkedin"><a target="_blank" href="https://www.linkedin.com/in/prwatech"><i class="fa fa-linkedin"></i></a></li>
											<li class="xing"><a target="_blank" href="https://www.youtube.com/channel/UCwAaWqnH2MqikDMpb1jBspw"><i class="fa fa-youtube"></i></a></li>
										</ul>
									</div>
								</div>
								<div class="col-md-3">
									<div class="footer-content">
										<h3 class="title">Find Us In Pune</h3>
										<div class="separator-2"></div>
                                                                               <div class="heading-font" itemscope itemtype="http://schema.org/PostalAddress"> 
<span itemprop="name">PRWATECH</span><br> 
<span itemprop="streetAddress">2nd Floor, Punyai Bldg., Yashwant Nagar, Behind Ekbote Furniture Mall kharadi Bypass Road,kharadi</span><br> 
<span itemprop="addressLocality"> Pune</span>, 
<span itemprop="addressRegion">Maharashtra</span> 
<span itemprop="postalCode">411014</span><br> 
<span itemprop="addressCountry">India</span> 
</div>
										<div class="separator-2"></div>
                                                                               
									</div>
								</div>
							</div>
						</div>
					</div>
                                  
				</div>
				<!-- .footer end -->

				<!-- .subfooter start -->
				<!-- ================ -->
				<div class="subfooter" id="bg-subfooter" >
                                    <div id="footer-bottom">
                                        
                                    </div> 
                                    <div class="bg-fullscreen">
                                        <div class="container">
						<div class="subfooter-inner">
							<div class="row">
								<div class="col-md-12">
									<p class="text-center">Copyright © 2016 Prwatech. All Rights Reserved</p>
								</div>
							</div>
						</div>
					</div>
                                    </div>
					
				</div>
				<!-- .subfooter end -->

</footer>
                        
                        
                        <!--comman-->
        <script>
            onload_blog();
            function onload_blog(){
                /* Ajax call for Product Display*/
                    $.ajax({
                    type:"post",
                    url:"server/controller/SelBlogDetails.php",
                    data:{'category_id':""},
                    success: function(data) {
                       var duce = jQuery.parseJSON(data);
                        $("#blog_details_client_in_footer").empty();
                        var count=0;
                        $.each(duce, function (index, article) {
                            
                           var img= "server/controller/"+article.pic;
                           var btn_class="btn-danger";
                           if(article.status=="Enable"){
                                
                                $("#blog_details_client_in_footer").append($("<div>").addClass("media margin-clear")
                                                .append($("<div>").addClass("media-left")
                                                    .append($("<div>").addClass("overlay-container")
                                                        .append($("<img>").addClass("img-responsive").attr({'src':img}))
                                                        .append($("<a>").addClass("overlay-link small")))
                                                 )
                                                .append($("<div>").addClass("media-body")
                                                        .append($("<h6>").addClass("media-heading heading-font")
                                                            .append($("<a>").append(article.title)).click(function (){
                                                                 window.location="blog/blogpostdetail.php?id="+article.blog_details_id;
                                                            }).css({'cursor':'pointer'}))
                                                            .append($("<p>").addClass("small margin-clear")
                                                                 .append($("<i>").addClass("fa fa-calendar pr-10"))
                                                                 .append(article.date)
                                                            )
                                                )
                                                .append($("<hr>"))
                                                );
                           }
                         count++;
                         if(count==3){
                             return false;
                         }
                        });
                    }
                });
                }
            
            
</script>
 <?php
if ($request != "") {
    ?>
<script>
    categroy_details_link={};
             $(document).ready(function(){
                var cta_id="";
                    $.ajax({
                    type:"post",
                    url:"server/controller/SelBlogCategoryDetails.php",
                      success: function(data) {
                        $("#category_tbody").empty();
                        var duce = jQuery.parseJSON(data);
                        $.each(duce, function (index, article) {
                        if(cta_id==""){
                            cta_id=article.blog_category_details_id;
                        }
                        categroy_details_link[article.link]=article.blog_category_details_id;
                            $("#category_tbody").append($('<tr/>')
                                    .append($("<th>").append($("<a>").append(article.category+" ("+article.counter+")").css({'cursor':'pointer'})).click(function (){
                                         window.location="category/"+article.link+"/"
                                    }))
                            );   
                        })
                                onladBlog(decodeURI('<?php echo $request; ?>'));
                    }
                })
               
                   
            });
            function urlParam(name){
    var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
    if (results==null){
       return null;
    }
    else{
       return results[1] || 0;
    }
}
  function onladBlog(blog_details_id){
      alert(blog_details_id)
                /* Ajax call for Product Display*/
                    $.ajax({
                    type:"post",
                    url:"../server/controller/SelClientBlogDetails.php",
                    data:{'blog_details_id':blog_details_id},
                    success: function(data) {
                        blog={}
                        temp_date={}
                        var count=0;
                       var duce = jQuery.parseJSON(data);
                      $("#blog_details").empty();
                        $.each(duce, function (index, article) {
                            blog[article.blog_details_id]={};
                            blog[article.blog_details_id]["user_id"]=article.user_id;
                            blog[article.blog_details_id]["title"]=article.title;
                            blog[article.blog_details_id]["short_description"]=article.short_description;
                            blog[article.blog_details_id]["long_description"]=article.long_description;
                            blog[article.blog_details_id]["date"]=article.date;
                            blog[article.blog_details_id]["category_id"]=article.category_id;
                            blog[article.blog_details_id]["status"]=article.status;
                            blog[article.blog_details_id]["pic"]=article.pic;
                            blog[article.blog_details_id]["fb"]=article.fb;
                            blog[article.blog_details_id]["tw"]=article.tw;
                           var img= "../server/controller/"+article.pic;
                           var btn_class="btn-danger";
                           if(article.status=="Enable"){
                                $("#blog_details").append($("<div>").addClass("timeline-item")
                                                .append($("<article>").addClass("blogpost shadow light-gray-bg bordered")
                                        .append($("<header>")
                                                        .append($("<h2>").addClass("img-responsive")
                                                            .append($("<a>").append(article.title)))
                                                        .append($("<div>").addClass("post-info")
                                                            .append($("<span>").addClass("post-date")
                                                                 .append($("<i>").addClass("icon-calendar"))
                                                                 .append($("<span>").addClass("month").append(article.date))
                                                             )
                                                            .append($("<span>").addClass("comments")
                                                                 .append($("<i>").addClass("icon-user-1"))
                                                                 .append($("<a>").append("Admin"))
                                                             )
                                                            .append($("<span>").addClass("comments")
                                                                 .append($("<i>").addClass("icon-chat"))
                                                                 .append($("<a>").append("comment"))
                                                             )
                                                        ))
                                                    .append($("<div>").addClass("overlay-container")
                                                        .append($("<img>").addClass("img-responsive").attr({'src':img}).css({'width':'100%','height':'250px'}))
                                                        )
                                                    .append($("<p>&nbsp;</p>"))
                                                    .append($("<div>").addClass("blogpost-content").append(decodeURIComponent(article.long_description)))
                                                    .append($("<footer>").addClass("clearfix")
                                                        .append($("<div>").addClass("link pull-left")
                                                            .append($("<i>").addClass("icon-tags"))
                                                            .append($("<a>").append("&nbsp; tag 1").css({'cursor':'pointer'}))
                                                         )
                                                        .append($("<div>").addClass("link pull-right")
                                                            .append($("<ul>").addClass("social-links circle small colored clearfix margin-clear text-right animated-effect-1")
                                                                .append($("<li>").addClass("twitter")
                                                                    .append($("<a>").attr({'target':'_blank','href':article.tw}).css({'cursor':'pointer'})
                                                                       .append($("<i>").addClass("fa fa-twitter"))
                                                                    )
                                                                )
                                                                .append($("<li>").addClass("facebook")
                                                                    .append($("<a>").attr({'target':'_blank','href':article.fb}).css({'cursor':'pointer'})
                                                                       .append($("<i>").addClass("fa fa-facebook"))
                                                                    )
                                                                )
                                                           )
                                                            
                                                        ))
                                                ));
                                        var id=""
                                        var commen_di=$("<div>");
                                        var asb=$("<b>")
                                        $.ajax({
                                            type:"post",
                                            url:"../server/controller/SelBlogRplyDetails.php",
                                            data:{'blog_details_id':blog_details_id},
                                            success: function(data){
                                                $("#comment_model").empty();
                                                var duce = jQuery.parseJSON(data);
                                                asb.empty()
                                                asb.append("There are "+Object.keys(duce).length+" comments")
                                                $.each(duce, function (index, article) {
                                                    var btn_class="btn-danger";
                                                   if(article.status=="Enable"){
                                                           commen_di.append($("<div>").addClass("comment-content")
                                                                                .append($("<div>").addClass("comment-avatar").css({'width': '23px'})
                                                                                    .append($("<img>").addClass("img-circle").attr({'src':'../images/avatar.jpg'}))
                                                                                )
                                                                                .append($("<div>").addClass("comment-body clearfix")
                                                                                    .append($("<p>").append(article.user_name))
                                                                                    .append($("<p>").append(article.comment))
                                                                                    .append($("<p>").addClass("small margin-clear").css({'color':'#827979'}).append($("<i>").addClass("fa fa-pencil")).append("&nbsp;"+article.date))
                                                                                ))
                                                   }
                                                });
                                               },
                                               error:function (e){
                                               alert(e.responseText)
                                               }
                                            });
                                        $("#blog_details").append($("<div>").addClass("comment clearfix").attr({'id':'comments'})
                                                .append($("<h4>").addClass("title").append(asb))
                                                .append(commen_di)
                                                .append($("<div>").addClass("comments-form")
                                                    .append($("<div>").append($("<a>").attr({'data-toggle':'collapse','data-parent':'#accordion-2' ,'href':'#collapseTwo-comment' ,'class':'collapsed' ,'aria-expanded':'false'})
                                                        .append($("<i>").addClass("fa fa-comment pr-10")).append("Add your comment").click(function (){
                                                            id="";
                                                            <?php
                                                            if(isset($_SESSION['user_id']))
                                                                { ?>
                                                             id=<?php echo $_SESSION['user_id']; ?>;
                                                            <?php    }?>
                                                            if(id==""){
                                                             $("#mySignIn").modal("toggle")   ;
                                                             setTimeout(function (){
                                                                 $("#collapseTwo-comment").collapse('hide');
                                                             },500);
                                                            }
                                                        })))
                                                    .append($("<div>").addClass("panel-collapse collapse").attr({'id':'collapseTwo-comment','aria-expanded':'false'}).css({'height':'0px'})
                                                        .append($("<div>").attr({'id':'comment-form','role':'form'})
                                                            .append($("<div>").addClass("form-group has-feedback")
                                                                .append($("<textarea>").addClass("form-control")
                                                                .attr({'rows':'8','id':'message4','data-gramm':'true','data-txt_gramm_id':'bd4cb204-2cac-1700-1767-d4a0c03c0947','data-gramm_id':'bd4cb204-2cac-1700-1767-d4a0c03c0947','background':'transparent !important'})
                                                                .css({'z-index':'auto','position':'relative','line-height':'20px','font-size':'14px','transition':'none','spellcheck':'false'}))
                                                                .append($("<i>").addClass("fa fa-envelope-o form-control-feedback"))
                                                                .append($('<div class="validate pull-right" style="font-weight:bold;"></span></div>'))
                                                             )
                                                             .append($("<input>").attr({'type':'submit' ,'value':'Submit'}).addClass("btn btn-default").click(function (){
                                                                 if(id==""){
                                                                     $("#mySignIn").modal("toggle")   ;
                                                                     return false;
                                                                 }else{
                                                                         if($(this).parent().find("textarea").val()==""){
                                                                             $(".validate").removeClass("text-danger").addClass("text-danger").fadeIn(100).text(" Please fill valid Comment ").prepend("<span class=\"glyphicon glyphicon-remove text-danger\">").fadeOut(1000);
                                                                         }else{
                                                                             commen_di.append($("<div>").addClass("comment-content")
                                                                                .append($("<div>").addClass("comment-avatar").css({'width': '23px'})
                                                                                    .append($("<img>").addClass("img-circle").attr({'src':'../images/avatar.jpg'}))
                                                                                )
                                                                                .append($("<div>").addClass("comment-body clearfix")
                                                                                    .append($("<p>").append($(this).parent().find("textarea").val()))
                                                                                    .append($("<p>").addClass("small margin-clear").css({'color':'#ce0e0e'}).append($("<i>").addClass("fa fa-pencil")).append("&nbsp;Your Comment visble publicly after admin varification."))
                                                                                ))
                                                                        $.ajax({
                                                                            type:"post",
                                                                            url:"../server/controller/InsBlogRplyDetails.php",
                                                                            data:{'blog_id':article.blog_details_id,'user_id':id,'comment':$(this).parent().find("textarea").val()},
                                                                            success: function(data) {
                                                                                
                                                                            }
                                                                            });
                                                                                $(this).parent().find("textarea").val("");
                                                                                
                                                                         }
                                                                     }
                                                             }))
                                                         )
                                                     )
                                               )
                                      );
                           }
                          
                        });
                    }
                });
                }
        </script>

  <?php
}
    ?>


			<!-- footer end -->
			
		</div>
		<!-- page-wrapper end -->

		<!-- JavaScript files placed at the end of the document so the pages load faster -->
		<!-- ================================================== -->
		<!-- Jquery and Bootstap core js files -->
		<script type="text/javascript" src="plugins/jquery.min.js"></script>
                <script type="text/javascript" src="bootstrap/js/bootstrap.min.js"></script>

		<!-- Modernizr javascript -->
		<script type="text/javascript" src="plugins/modernizr.js"></script>

		<!-- jQuery Revolution Slider  -->
		<script type="text/javascript" src="plugins/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
		<script type="text/javascript" src="plugins/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
                 <!-- Isotope javascript -->
		<script type="text/javascript" src="plugins/isotope/isotope.pkgd.min.js"></script>
		
		<!-- Magnific Popup javascript -->
<!--		<script type="text/javascript" src="plugins/magnific-popup/jquery.magnific-popup.min.js"></script>-->
		
		<!-- Appear javascript -->
		<script type="text/javascript" src="plugins/waypoints/jquery.waypoints.min.js"></script>

		<!-- Count To javascript -->
		<script type="text/javascript" src="plugins/jquery.countTo.js"></script>
		
		<!-- Parallax javascript -->
		<script src="plugins/jquery.parallax-1.1.3.js"></script>

		<!-- Contact form -->
		<script src="plugins/jquery.validate.js"></script>
                    <!-- Background Video -->
		<script src="plugins/vide/jquery.vide.js"></script>

		<!-- Owl carousel javascript -->
		<script type="text/javascript" src="plugins/owl-carousel/owl.carousel.js"></script>
		<!-- SmoothScroll javascript -->
		<script type="text/javascript" src="plugins/jquery.browser.js"></script>
		<script type="text/javascript" src="plugins/SmoothScroll.js"></script>
                <!-- Initialization of Plugins -->
		<script type="text/javascript" src="js/template.js"></script>

		<!-- Custom Scripts -->
		<script type="text/javascript" src="js/custom.js"></script>
		<!-- Color Switcher (Remove these lines) -->
		<script type="text/javascript" src="style-switcher/style-switcher.js"></script>
                
               
		<script type="text/javascript">
          course_batch_details={}
          
            function onlad(course_id){
                /* Ajax call for Product Display*/
                    $.ajax({
                    type:"post",
                    url:"server/controller/SelCourseBatchDetails.php",
                    data:{'course_id':course_id},
                    success: function(data) {
                      var duce = jQuery.parseJSON(data);
                        $("#batch_details").empty();
                        $.each(duce, function (index, article) {
                            course_batch_details[article.course_batch_details_id]={};
                            course_batch_details[article.course_batch_details_id]["course_id"]=article.course_id;
                            course_batch_details[article.course_batch_details_id]["date"]=article.date;
                            course_batch_details[article.course_batch_details_id]["day_type"]=article.day_type;
                            course_batch_details[article.course_batch_details_id]["day"]=article.day;
                            course_batch_details[article.course_batch_details_id]["time"]=article.time;
                            course_batch_details[article.course_batch_details_id]["fees"]=article.fees;
                            course_batch_details[article.course_batch_details_id]["discount"]=article.discount;
                            var btn_class="btn-danger";
                           if(article.status=="Enable"){
                               btn_class="btn-success";
                           }
                           var d = new Date(article.date);
                           var n = d.getDate();
                           var m = d.getMonth();
                           var month_name=["","Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"]
                          $("#batch_details").append($("<div>").addClass("col-md-3")
                                  .append($("<div>").addClass("col-md-3")
                                        .append($("<div>").addClass("row")
                                            .append($("<p>").attr({'id':'date-time'}).append(n)
                                                .append($("<br>"))
                                                .append($("<span>").attr({'id':'month'}).append(month_name[m].toUpperCase()))
                                            )
                                        )
                                   )
                                  .append($("<div>").addClass("col-md-9").attr({'id':'upcomming-batch'})
                                    .append($("<div>").addClass("row")
                                        .append($("<div>").addClass("col-md-12")
                                            .append($("<p>").attr({'id':'dt-content'})
                                                .append($("<b>").append(article.day_type+" - "+article.day)
                                                    .append($("<br>"))
                                                    .append($("<small>").append(article.time+" IST" +" (30 Hrs.)")
                                                        .append($("<br>"))
                                                        .append("Rs. "+article.fees+" ")
                                                        .append($("<a>").css({'cursor':'pointer'}).append(" Enroll Now").click(function (){
                                                            showDemoModel(course_id)
                                                        }))
                                                    )
                                                )
                                           )
                                        )
                                    )
                                 ));
                          
                          
                        });
                    }
                });
                }
                function showDemoModel (course_id){
                     openNav();
                    $("#mySidenav").find("form").each(function(){
                            this.reset();
                    });
                    $("#mySidenav").find("#course_id").val(course_id);
                }
                
      </script>
      
      
      
    
		<!-- Color Switcher End -->
                 <div class="modal fade" id="demoVideoPopUp" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header ">
                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                <h4 class="modal-title" id="myModalLabel">Demo Session at Prwatech</h4>
                            </div>
                            <div class="modal-body">
                               <iframe width="570" height="350" src="https://www.youtube.com/embed/xA5TAalZt40" frameborder="0" allowfullscreen></iframe> 
                              
                            </div>
                            <div class="modal-footer">
                                
                            </div>
                        </div>
                    </div>
                </div>
                    <?php include './includes/quick-query_1.php';?>
                    <?php include './includes/UserSignup_1.php';?>
	</body>

       
</html>
