<?php include '../includes/session.php'; ?>
<!DOCTYPE html>
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <title>Apache Spark Training Bangalore & Pune | Online Courses</title>
        <meta name="description" content="Get the best Apache Spark Scala Online Training course with expert trainers, instructors in Marathahalli Pune academy institute and Bangalore BTM.." />
	<meta property="og:type" content="article" />
	<meta property="og:title" content="Apache Spark Scala Training" />
	<meta property="og:description" content="Get the best Apache Spark Scala Online Training course with expert trainers, instructors in Pune academy, BTM, Marathahalli &amp; Bangalore." />
	<meta property="og:url" content="http://prwatech.in/apache-spark-scala-training-2/" />
	<meta property="og:site_name" content="BigData &amp; HADOOP" />
        <meta name="google-site-verification" content="fSzBVSEUMu0l5lnD0qBVGv5F_16zaI6xiUZFMm-iMqQ" /> 
                <?php include '../includes/csslinks.php'; ?>
    </head>
    
    <!-- body classes:  -->
    <!-- "boxed": boxed layout mode e.g. <body class="boxed"> -->
    <!-- "pattern-1 ... pattern-9": background patterns for boxed layout mode e.g. <body class="boxed pattern-1"> -->
    <!-- "transparent-header": makes the header transparent and pulls the banner to top -->
    <body class="no-trans front-page transparent-header ">
        <!-- scrollToTop -->
        <!-- ================ -->
        <div class="scrollToTop circle"><i class="icon-up-open-big"></i></div>
        
        <!-- page wrapper start -->
        <!-- ================ -->
        <div class="page-wrapper">
            
            <!-- header-container start -->
			<?php include '../includes/header.php' ;  ?>
            <!-- header-container end -->
            
            <!-- banner start -->
            <!-- ================ -->
            <div class="banner dark-translucent-bg" style="background-image:url('../images/page-course-banner-3.jpg'); background-position: 50% 27%;">
                <!-- breadcrumb start -->
                <!-- ================ -->
                <div class="breadcrumb-container object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
                    <div class="container">
                        <ol class="breadcrumb">
                            <li><i class="fa fa-home pr-10"></i><a class="link-dark" href="../index">Home</a></li>
                            <li class="active">All Courses</li>
                            <li class="active">Apache Spark Scala Training</li>
                        </ol>
                    </div>
                </div>
                <!-- breadcrumb end -->
                <div class="container">
                    
                </div>
            </div>
            <!-- banner end -->
            
            <div id="page-start"></div>
            
            <!-- section start -->
            <!-- ================ -->
            <section class="light-gray-bg pv-30 clearfix object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100" id="cdrow1">
                <div class="container">
                    <div class="row">
                        <div class="col-md-8">
                            <h1 class="text-center heading-font" style="text-transform:none;"> <strong>Apache Spark Scala</strong> Training In <span id="highlighter">Bangalore & Pune</span></h1>
                            
                            <div class="separator"></div>
                            <p class="large text-justify heading-font">Apache Spark Training will give you expertise to perform large-scale Data Processing using RDD, Spark Streaming, SparkSQL, MLlib, GraphX and Scala with Real Life use-cases on Banking and Telecom domain.</p>
                        </div>
                        <div class="col-md-4">
                            <img src="../images/por-3.png"/>
                        </div>
                        
                    </div>
                    <p>&nbsp;</p>
                    <div class="separator"></div>
                    <h3 class="text-center heading-font object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100" style="text-transform:none;">Contact Us <strong>+91 8147111254 </strong></h3>
                    <div class="separator"></div>
                    <p>&nbsp;</p>
                    <div class="container">
                          <div class="row" id="batch_details">
                            <div class="col-md-3">
                                <div class="col-md-3">
                                    <div class="row">
                                        <p id ="date-time">21 <br><span id="month">NOV</span></p>
                                    </div>
                                </div>
                                <div class="col-md-9" id="upcomming-batch">
                                    <div class="row">
                                        <div class="col-md-12"><p id="dt-content"><b> Mon-Wed (3 Days) <br><small>&nbsp; <br> Rs. 14000/- <a href="#" class="" data-toggle="modal" data-target="#demoPopUp">Enroll Now</a></small></b></p></div>
                                        
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="col-md-3">
                                    <div class="row">
                                        <p id ="date-time">24 <br><span id="month">NOV</span></p>
                                    </div>
                                </div>
                                <div class="col-md-9" id="upcomming-batch">
                                    <div class="row">
                                        <div class="col-md-12"><p id="dt-content"><b> Mon-Wed (3 Days) <br><small>&nbsp; <br> Rs. 14000/- <a href="#" class="" data-toggle="modal" data-target="#demoPopUp">Enroll Now</a></small></b></p></div>
                                        
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="col-md-3">
                                    <div class="row">
                                        <p id ="date-time">27 <br><span id="month">NOV</span></p>
                                    </div>
                                </div>
                                <div class="col-md-9" id="upcomming-batch">
                                    <div class="row">
                                        <div class="col-md-12"><p id="dt-content"><b> Mon-Wed (3 Days) <br><small>&nbsp; <br> Rs. 14000/- <a href="#" class="" data-toggle="modal" data-target="#demoPopUp">Enroll Now</a></small></b></p></div>
                                        
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="col-md-3">
                                    <div class="row">
                                        <p id ="date-time">30 <br><span id="month">NOV</span></p>
                                    </div>
                                </div>
                                <div class="col-md-9" id="upcomming-batch">
                                    <div class="row">
                                        <div class="col-md-12"><p id="dt-content"><b> Mon-Wed (3 Days) <br><small>&nbsp; <br> Rs. 14000/- <a href="#" class="" data-toggle="modal" data-target="#demoPopUp">Enroll Now</a></small></b></p></div>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
            </section>
            <section id="coursed-row2" class="object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
                <div class="container">
                    
                    <h3 class="text-center heading-font object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100" style="text-transform:none;"> <strong>Course</strong> Details</h3>
                    <div id="vseparator" class="object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
                        <div class="vhr"></div>
                    </div>
                    <div class="row col-md-9" >
                        <!-- tabs start -->
                        <!-- ================ -->
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs style-3" id="tab-course" role="tablist">
                            <li class="active"><a href="#h3tab1" role="tab" data-toggle="tab">Overview</a></li>
                            <li><a href="#h3tab2" role="tab" data-toggle="tab">Course Curriculum</a></li>
                            <li><a href="#h3tab3" role="tab" data-toggle="tab">FAQ's</a></li>
                            <li><a href="#h3tab4" role="tab" data-toggle="tab">Download Course Outline</a></li>
                            <li><a href="#h3tab5" role="tab" data-toggle="tab">Reviews</a></li>
                            
                        </ul>
                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div class="tab-pane in active " id="h3tab1">
                                <div class="row">
                                    <div class="col-md-12 object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
                                        <h2 class="heading-font">Introduction of Spark & Scala</h2>
                                        <p id="cpara">
                                            Apache Spark is an in-memory cluster computing, processing engine built for speed and accurate analytics. This engine provides an opportunity to process Big Data which is coupled with low latency and cannot be handled with Map Reduce programs. Spark is 100 times faster and user friendly when compared to Map Reduce and ensures fast speed and also supports Java, Scala and Python APIs.
                                        </p>
                                        
                                        <h2 class="heading-font">About The Apache Spark Scala Training Course:</h2>
                                        <p id="cpara">
                                            If you are looking for course to learn the concepts of Scala, RDD, OOPS, Traits, Spark SQL and MLib enroll yourself at PrwaTech for the <span id="highlighter">Best Apache Spark Scala Training Online. Apache Spark Course In Bangalore </span>will help the participants to learn various concepts of large-scale data processing. This online course is a part of Developer’s learning path.
                                        </p>
                                        <p id="cpara">
                                            <span id="highlighter">Apache Spark Scala Bangalore </span>is quickly gaining momentum not only in the headlines but also in real world adoption. Today, it has grown to the extent that, customers from all the industries are using it to improve their businesses with the HDP.
                                        </p>
                                        <p id="cpara">
                                            Learning Objectives: At the end of Spark Training course at PrwaTech, you will be able to:
                                        </p>
                                        
                                        
                                        
                                        <ul class="list-icons" id="clist">
                                            <li><i class="icon-check pr-10"></i> Learn Storm Architecture and basic distributed concepts.</li>
                                            <li><i class="icon-check pr-10"></i>Learn Big Data features</li>
                                            <li><i class="icon-check pr-10"></i>Understand Legacy architecture of real time systems</li>
                                            <li><i class="icon-check pr-10"></i> Understand Logic Dynamics and Components in Storm</li>
                                            <li><i class="icon-check pr-10"></i> Learn the difference between Hadoop and <span id="highlighter">Apache Spark Course</span></li>
                                            <li><i class="icon-check pr-10"></i> Learn Scala and the programming implementation in Scala</li>
                                            <li><i class="icon-check pr-10"></i> Build Spark Applications using Java, Python and Scala</li>
                                            <li><i class="icon-check pr-10"></i> Implement Spark on cluster</li>
                                            <li><i class="icon-check pr-10"></i> Gain insight into functioning of Scala</li>
                                            <li><i class="icon-check pr-10"></i> Develop Real-life Storm Projects</li>
                                            
                                        </ul>
                                        
                                        <h2 class="heading-font">Pre-Requisites:</h2>
                                        <ul class="list-icons" id="clist">
                                            <li><i class="icon-check pr-10"></i> Basic knowledge of Java basics is required.</li>
                                            <li><i class="icon-check pr-10"></i> Prior knowledge of Hadoop is not mandatory.</li>
                                            
                                            
                                        </ul>
                                        <h2 class="heading-font">Recommended Audience For <span id="highlighter">Apache Spark Training</span>:</h2>
                                        <ul class="list-icons" id="clist">
                                            <li><i class="icon-check pr-10"></i> Anyone aiming to build a career in real-time Data Analytics.</li>
                                            <li><i class="icon-check pr-10"></i> Software Engineers who are eager to learn concepts of Big Data processing.</li>
                                            <li><i class="icon-check pr-10"></i> Software Architects, ETL Developers, Data Scientists and Project Managers.</li>
                                            
                                            
                                        </ul>
                                        <p id ="cpara">For Best <span id="highlighter">Apache Spark Online Training Pune </span>come and visit our Apache Spark training Pune institute, we also have presence in other cities. If you are looking for <span id="highlighter">Apache Spark Training in Bangalore BTM, Prwatech Institute is one step solution for Spark Training Pune & Bangalore</span>. You can visit our Spark Training in <span id="highlighter">Marathahalli </span>institute for <span id="highlighter">Best Apache Spark training in Pune</span>. Visit our Spark training academy Pune to learn from one another as well as get educated through research updates and guest lectures.</p>
                                        <div class="separator"></div>
                                        <h2 class="heading-font">Who should go for <span id="highlighter">Spark Course</span>?</h2>
                                        <ul class="list-icons" id="clist">
                                            <li><i class="icon-check pr-10"></i> Big Data Buffs</li>
                                            <li><i class="icon-check pr-10"></i> Software Engineers, Architects and Developers</li>
                                            <li><i class="icon-check pr-10"></i> Analytics Professionals</li>
                                            <li><i class="icon-check pr-10"></i> Data Scientists</li>
                                            
                                            
                                        </ul>
                                        

                                        
                                    </div>
                                    
                                </div>
                                
                            </div>
                            <div class="tab-pane" id="h3tab2">
                                <div class="row">
                                    <div class="col-md-12 ">
                                        <div class="panel-group collapse-style-2" id="accordion-2">
                                            <div class="panel panel-default">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
                                                        <a data-toggle="collapse" data-parent="#accordion-2" href="#collapseOne-2">
                                                            <i class="fa fa-arrow-right pr-10"></i>Introduction to Spark & Scala
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="collapseOne-2" class="panel-collapse collapse in">
                                                    <div class="panel-body">
                                                        <p id="cpara">In this module, will discuss about Big Data. How Big Data impact in our social life & its important role. How Hadoop is helpful to manage & process Big Data. What is problem with MapReduce and why spark came?</p>
                                                        <h3 class="heading-font">Topics:</h3>
                                                        <ul class="list-icons" id="clist">
                                                            <li><i class="icon-check pr-10"></i> Big Data Problems</li>
                                                            <li><i class="icon-check pr-10"></i> What is Hadoop?</li>
                                                            <li><i class="icon-check pr-10"></i> Introduction to HDFS</li>
                                                            <li><i class="icon-check pr-10"></i> Introduction to MapReduce with Example</li>
                                                            <li><i class="icon-check pr-10"></i> What is problem with Map reduce?</li>
                                                            <li><i class="icon-check pr-10"></i> Why Apache Spark?</li>
                                                            <li><i class="icon-check pr-10"></i> What is Apache Spark?</li>
                                                            <li><i class="icon-check pr-10"></i> MapReduce vs Apache Spark</li>
                                                        </ul>
                                                        
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel panel-default">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
                                                        <a data-toggle="collapse" data-parent="#accordion-2" href="#collapseTwo-2" class="collapsed">
                                                            <i class="fa fa-arrow-right pr-10"></i>Apache Spark Installations & Its basics 
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="collapseTwo-2" class="panel-collapse collapse">
                                                    <div class="panel-body">
                                                        <p id="cpara">In this module, we will learn about RDD creations and RDD operations.</p>
                                                        <h3 class="heading-font">Topics:</h3>
                                                        <ul class="list-icons" id="clist">
                                                            <li><i class="icon-check pr-10"></i> Apache Spark installations in local machine.</li>
                                                            <li><i class="icon-check pr-10"></i> Apache Spark Conf and spark context.</li>
                                                            <li><i class="icon-check pr-10"></i> RDD creations</li>
                                                            <li><i class="icon-check pr-10"></i> Operations on RDD</li>
                                                            <li><i class="icon-check pr-10"></i> RDD transformations functions</li>
                                                            <li><i class="icon-check pr-10"></i> Map & Flat Map difference</li>
                                                            <li><i class="icon-check pr-10"></i> Actions on RDD</li>
                                                            <li><i class="icon-check pr-10"></i> Persistence.</li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel panel-default">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
                                                        <a data-toggle="collapse" data-parent="#accordion-2" href="#collapseThree-2" class="collapsed">
                                                            <i class="fa fa-arrow-right pr-10"></i>Introductions to Scala 
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="collapseThree-2" class="panel-collapse collapse">
                                                    <div class="panel-body">
                                                        <p id="cpara">In this module, we will learn about Scala programing language.</p>
                                                        <h3 class="heading-font">Topics:</h3>
                                                        <ul class="list-icons" id="clist">
                                                            <li><i class="icon-check pr-10"></i> Why Scala ?</li>
                                                            <li><i class="icon-check pr-10"></i> Values.</li>
                                                            <li><i class="icon-check pr-10"></i> Variables.</li>
                                                            <li><i class="icon-check pr-10"></i> Basic oops concepts.</li>
                                                            <li><i class="icon-check pr-10"></i> Scala Flow Control.</li>
                                                            <li><i class="icon-check pr-10"></i> Functions.</li>
                                                            <li><i class="icon-check pr-10"></i> Anonymous Functions.</li>
                                                            <li><i class="icon-check pr-10"></i> Curried functions.</li>
                                                            <li><i class="icon-check pr-10"></i> Classes.</li>
                                                            <li><i class="icon-check pr-10"></i> Constructor.</li>
                                                            <li><i class="icon-check pr-10"></i> Inheritance.</li>
                                                            <li><i class="icon-check pr-10"></i>Abstract Classes.</li>
                                                            <li><i class="icon-check pr-10"></i>Traits.</li>
                                                            <li><i class="icon-check pr-10"></i>Collections.</li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel panel-default">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
                                                        <a data-toggle="collapse" data-parent="#accordion-2" href="#collapseFour-2" class="collapsed">
                                                            <i class="fa fa-arrow-right pr-10"></i>Spark Advance Operations:
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="collapseFour-2" class="panel-collapse collapse">
                                                    <div class="panel-body">
                                                        <p id="cpara">In this module, will learn more operations on apache spark transformation and actions.</p>
                                                        <h3 class="heading-font">Topics:</h3>
                                                        <ul class="list-icons" id="clist">
                                                            <li><i class="icon-check pr-10"></i> Working with RDD key-value</li>
                                                            <li><i class="icon-check pr-10"></i> RDD joins</li>
                                                            <li><i class="icon-check pr-10"></i> Shared Variables</li>
                                                            <li><i class="icon-check pr-10"></i> Broadcast variables</li>
                                                            <li><i class="icon-check pr-10"></i> Accumulators</li>
                                                            
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel panel-default">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
                                                        <a data-toggle="collapse" data-parent="#accordion-2" href="#collapseFive-2" class="collapsed">
                                                            <i class="fa fa-arrow-right pr-10"></i>Apache Spark Architecture
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="collapseFive-2" class="panel-collapse collapse">
                                                    <div class="panel-body">
                                                        <p id="cpara">In this Module we will discuss a Spark Internals execution details</p>
                                                        <h3 class="heading-font">Topics:</h3>
                                                        <ul class="list-icons" id="clist">
                                                            <li><i class="icon-check pr-10"></i> Apache Spark Cluster Details.</li>
                                                            <li><i class="icon-check pr-10"></i> Apache Spark Standalone Mode cluster.</li>
                                                            <li><i class="icon-check pr-10"></i> Running Spark application in Standalone mode cluster.</li>
                                                            <li><i class="icon-check pr-10"></i>Summary of RDD sizes and memory usage.</li>
                                                            <li><i class="icon-check pr-10"></i> Spark web UI.</li>
                                                            <li><i class="icon-check pr-10"></i> Apache Spark internals.</li>
                                                            <li><i class="icon-check pr-10"></i> Apache spark execution flow.</li>
                                                            <li><i class="icon-check pr-10"></i> DAG: Logical graph of RDD operations</li>
                                                            <li><i class="icon-check pr-10"></i> RDD Physical Plan.</li>
                                                            <li><i class="icon-check pr-10"></i>  Tasks.</li>
                                                            <li><i class="icon-check pr-10"></i>  Stages.</li>
                                                            <li><i class="icon-check pr-10"></i> Scheduler.</li>
                                                            <li><i class="icon-check pr-10"></i> Types of RDD transformations</li>
                                                            
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel panel-default">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
                                                        <a data-toggle="collapse" data-parent="#accordion-2" href="#collapseSix-2" class="collapsed">
                                                            <i class="fa fa-arrow-right pr-10"></i>Spark SQL
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="collapseSix-2" class="panel-collapse collapse">
                                                    <div class="panel-body">
                                                        <p id="cpara">In this module, will learn about SQL operations in Spark.</p>
                                                        <h3 class="heading-font">Topics:</h3>
                                                        <ul class="list-icons" id="clist">
                                                            <li><i class="icon-check pr-10"></i> SQL Context.</li>
                                                            <li><i class="icon-check pr-10"></i> Data Frame Creations.</li>
                                                            <li><i class="icon-check pr-10"></i> Creating temporary tables</li>
                                                            <li><i class="icon-check pr-10"></i>Parquet tables.</li>
                                                            <li><i class="icon-check pr-10"></i> Loading and processing csv file.</li>
                                                            <li><i class="icon-check pr-10"></i> Loading and processing json File.</li>
                                                            <li><i class="icon-check pr-10"></i> Writing data to local file system.</li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel panel-default">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
                                                        <a data-toggle="collapse" data-parent="#accordion-2" href="#collapseSeven-2" class="collapsed">
                                                            <i class="fa fa-arrow-right pr-10"></i>Spark Streaming:
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="collapseSeven-2" class="panel-collapse collapse">
                                                    <div class="panel-body">
                                                        <ul class="list-icons" id="clist">
                                                            <li><i class="icon-check pr-10"></i> What is streaming?</li>
                                                            <li><i class="icon-check pr-10"></i> Why streaming?</li>
                                                            <li><i class="icon-check pr-10"></i> Discretized Streams.</li>
                                                            <li><i class="icon-check pr-10"></i>Transformations on DStreams</li>
                                                            <li><i class="icon-check pr-10"></i> Streaming Example</li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel panel-default">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
                                                        <a data-toggle="collapse" data-parent="#accordion-2" href="#collapseEight-2" class="collapsed">
                                                            <i class="fa fa-arrow-right pr-10"></i>Machine learning using Spark
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="collapseEight-2" class="panel-collapse collapse">
                                                    <div class="panel-body">
                                                        <ul class="list-icons" id="clist">
                                                            <li><i class="icon-check pr-10"></i> What is machine learning?</li>
                                                            <li><i class="icon-check pr-10"></i> Machine learning current use cases.</li>
                                                            <li><i class="icon-check pr-10"></i> Type of machine learnings.</li>
                                                            <li><i class="icon-check pr-10"></i>Terminologies.</li>
                                                            <li><i class="icon-check pr-10"></i>Problem with traditional analytics tools</li>
                                                            <li><i class="icon-check pr-10"></i>Supervised learning using Spark with examples.</li>
                                                            <li><i class="icon-check pr-10"></i>Unsupervised learning using Spark with Examples</li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        
                                    </div>
                                    
                                </div>
                                
                            </div>
                            <div class="tab-pane" id="h3tab3">
                                <div class="row">
                                    <div class="col-md-12 ">
                                        <div class="panel-group collapse-style-3" id="accordion-3">
                                            <div class="panel panel-default">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
                                                        <a data-toggle="collapse" data-parent="#accordion-3" href="#collapseOne-3">
                                                           1.	What is the need of learning Apache Spark and Scala Training?
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="collapseOne-3" class="panel-collapse collapse in">
                                                    <div class="panel-body">
                                                        <p id="cpara" class="text-justify">
                                                            Apache Spark is quickly gaining momentum not only in the headlines but also in real world adoption. Today, it has become mandatory for every business needs, customers from all the industries are using it to improve their work with the HDP. Clients are detecting patterns and proving insight which is changing some facets of life and driving organizational change. Apache Spark has grown to become the largest open source community for large data with its own in-memory processing framework.
                                                        </p>
                                                        
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel panel-default">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
                                                        <a data-toggle="collapse" data-parent="#accordion-3" href="#collapseTwo-3" class="collapsed">
                                                           2.	Why should I Join PrwaTech for this training?
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="collapseTwo-3" class="panel-collapse collapse">
                                                    <div class="panel-body">
                                                        <p id="cpara" class="text-justify">
                                                            These are some points why should you choose PrwaTech:
                                                        </p>
                                                            
                                                        <ul class="list-icons" id="clist">
                                                            <li><i class="icon-check pr-10"></i>Get trained by finest qualified professionals </li>
                                                            <li><i class="icon-check pr-10"></i>100% practical training</li>
                                                            <li><i class="icon-check pr-10"></i>Complete course guidance</li>
                                                            <li><i class="icon-check pr-10"></i>Give you a chance to work on real-time projects</li>
                                                            <li><i class="icon-check pr-10"></i>Flexible timings</li>
                                                            <li><i class="icon-check pr-10"></i>Provide Case studies</li>
                                                            <li><i class="icon-check pr-10"></i>Weekdays and weekend batches</li>
                                                            <li><i class="icon-check pr-10"></i>Affordable fees</li>
                                                                                            
                                                        </ul> 
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel panel-default">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
                                                        <a data-toggle="collapse" data-parent="#accordion-3" href="#collapseThree-3" class="collapsed">
                                                           3.	Do you provide placement assistance?  
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="collapseThree-3" class="panel-collapse collapse">
                                                    <div class="panel-body">
                                                        <p id="cpara" class="text-justify">
                                                            PrwaTech will assist you throughout your training. We have 95% of placement record in reputed companies and have organized 2000+ interviews in HCL, Infosys, IBM, Accenture etc. We have trained 2000+ students. Our professionals will provide you with several questions along with solutions that you might face in your interview.         
                                                        </p>
                                                        
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel panel-default">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
                                                        <a data-toggle="collapse" data-parent="#accordion-3" href="#collapseFour-3" class="collapsed">
                                                           4.	What are the different modes of training that PrwaTech offers?
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="collapseFour-3" class="panel-collapse collapse">
                                                    <div class="panel-body">
                                                        <p id="cpara" class="text-justify">
                                                            PrwaTech basically offers two types of trainings: online instructor-led training and self-paced training. Apart from that we provide classroom trainings and corporate training for enterprises. We have the best industry professionals and all our trainers come with over 5 years of industry experience. You can confirm the quality of our professionals in sample videos provided.
                                                        </p>
                                                        
                                                    </div>
                                                </div>
                                            </div>
                                            
                                            <div class="panel panel-default">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
                                                        <a data-toggle="collapse" data-parent="#accordion-3" href="#collapseSix-3" class="collapsed">
                                                            5.	Who will be my training instructor?
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="collapseSix-3" class="panel-collapse collapse">
                                                    <div class="panel-body">
                                                        <p id="cpara" class="text-justify">
                                                            You will be trained by the well qualified professionals who will have more than 8 years of experience in Apache Spark and Scala. Before considering any assignment PrwaTech thoroughly screen the professional. Verify all the documents, educational qualifications and work experience. PrwaTech consider every assignment equal whether it is a big assignment or small assignment. . PrwaTech highly skilled professional team of software engineers and solution providers strive to help individual by proving the accurate technology.
                                                        </p>
                                                        
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        
                                    </div>
                                    
                                </div>
                                
                            </div>
                            <div class="tab-pane" id="h3tab4">
                                <div class="row object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
                                    <div class="col-md-4">
                                        <p id="cpara" class="text-default"><a href="">Click Here to Download</a></p>
                                        
                                    </div>
                                    <div class="col-md-8">
                                        <h4 class="title text-default" class="heading-font">Services</h4>
                                        <ul class="list-icons">
                                            <li><i class="icon-check pr-10"></i> PowerPoint Presentation covering all classes</li>
                                            <li><i class="icon-check pr-10"></i> Recorded Videos Sessions On Bigdata and Hadoop with LMS Access.<br>
                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(lifetime support)</li>
                                            <li><i class="icon-check pr-10"></i> Quiz , Assignment & POC.</li>
                                            <li><i class="icon-check pr-10"></i> On Demand Online Support .</li>
                                            <li><i class="icon-check pr-10"></i> Discussion Forum.</li>
                                            <li><i class="icon-check pr-10"></i> Material
                                                <ul class="list-icons" style="margin-left:50px;">
                                                    <li>a. Sample Question papers of Cloudera Certification.</li>
                                                    <li>b. Technical Notes & Study Material.</li>
                                                </ul>
                                                
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                
                            </div>
                            <div class="tab-pane" id="h3tab5">
                                <div class="row object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
                                    <div class="col-md-6 ">
                                        <div class="pv-20 ph-20 feature-box-2 boxed shadow object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
                                            <div class="col-md-3 testimonial-image"><img src="../images/testimonial-2.jpg" /></div> 
                                            <div class="body">
                                                <h4 class="title">Anupam Khamparia 
                                                    <br>
                                                    <span style="font-size:11px;">Consultant, Cognizant Technology Solutions, Bangalore</span>
                                                </h4>
                                                <p class ="text-justify">“Excellent course and instructor. I learnt a lot in a short period. Good value for money. Instructor took us through Advanced Hadoop Development in depth.”</p>
                                                
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 ">
                                        <div class="pv-20 ph-20 feature-box-2 boxed shadow object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
                                            <div class="col-md-3 testimonial-image"><img src="../images/testimonial-1.png" /></div> 
                                            <div class="body">
                                                <h4 class="title">Anukanksha Garg
                                                    <br>
                                                    <span style="font-size:11px;">B.Tech. CSE</span>
                                                </h4>
                                                <p class ="text-justify">It was a nice learning experience with prwatech. The classes were well scheduled and managed.
                                                    Verma has good understanding of the topics taught and catered to our problems and doubts very patiently. The best thing about him was that he handled the situations accordingly, when needed a friend, he became one and also a teacher to always guide us.</p>
                                                
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 ">
                                        <div class="pv-20 ph-20 feature-box-2 boxed shadow object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
                                            <div class="col-md-3 testimonial-image"><img src="../images/testimonial-4.PNG" /></div> 
                                            <div class="body">
                                                <h4 class="title">Varun Shivashanmugum
                                                    
                                                    <br>
                                                    <span style="font-size:11px;">Associate Consultant, ITC Infotech Ltd</span>
                                                </h4>
                                                
                                                <p class ="text-justify">“Faculty is good,Verma takes keen interest and personnel care in improvising skills in students. And most importantly,Verma will be available and clears doubts at any time apart from class hours. And he always keeps boosting and trying to increase confidence in his students which adds extra attribute to him and organization as well. and organization as well.</p>
                                                
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 ">
                                        <div class="pv-20 ph-20 feature-box-2 boxed shadow object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
                                            <div class="col-md-3 testimonial-image"><img src="../images/testimonial-5.jpg" /></div> 
                                            <div class="body">
                                                <h4 class="title">Mayank Srivastava <br>
                                                    <span style="font-size:11px;">Hadoop Developer, L&T, Bangalore</span>
                                                </h4>
                                                <p class ="text-justify">“Really good course content and labs, patient enthusiastic instructor. Good instructor, with in depth skills…Very relevant practicals allowed me to put theory into practice.”</p>
                                                
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3" style="position:relative;top:-10px;">
                        <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center" >
                            <div class="overlay-container">
                                <img src="../images/icon/curse-d1.png" alt="">
                            </div>
                            <div class="body">
                                <h3 class="heading-font">Rs. 14,000 + Tax </h3>
                                <span class="small-font">per 2 weeks</span>
                                <div class="separator"></div>
                                <p class="heading-font"></p>
                                <table class="table table-striped ">
                                    <tbody>
                                        <tr><th><center>35 Hours</center></th></tr>
                                    <tr><th><center>15 Seats</center></th></tr>
                                    <tr><th><center>Course Badge</center></th></tr>
                                    <tr><th><center>Course Certificate</center></th></tr>
                                    <tr><th><center> <a  onclick="openNav()" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Enroll Now<i class="fa fa-arrow-right pl-10"></i></a></center></th></tr>
                                    </tbody>
                                </table>
                                            <?php include '../includes/demo-popup.php';?>
                                
                            </div>
                        </div>
                        <h3 class="heading-font text-center" style="background:#4c4c4c;color:white;padding-top:10px;padding-bottom:10px">Suggested Courses</h3> 
                        <div class="owl-carousel content-slider">
                            <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
                                <div class="overlay-container">
                                    <img src="../images/por-1.png" alt="">
                                    <div class="overlay-to-top">
                                        <p class="small margin-clear">&nbsp;</p>
                                    </div>
                                </div>
                                <div class="body">
                                    <h3 class="heading-font">Big Data Hadoop Training</h3>
                                    <div class="separator"></div>
                                    <p class="heading-font">Advanced Program for Top-notch Developers </p>
                                    <a href="big-data-hadoop-training.php"  target="_blank" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read More<i class="fa fa-arrow-right pl-10"></i></a>												
                                </div>
                            </div>
                            <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
                                <div class="overlay-container">
                                    <img src="../images/por-2.png" alt="">
                                    <div class="overlay-to-top">
                                        <p class="small margin-clear">&nbsp;</p>
                                    </div>
                                </div>
                                <div class="body">
                                    <h3 class="heading-font">Hadoop Admin Training</h3>
                                    <div class="separator"></div>
                                    <p class="heading-font">Advanced Program for Top-notch Developers </p>
                                    <a href="hadoop-admin.php" target="_blank" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read More<i class="fa fa-arrow-right pl-10"></i></a>												
                                </div>
                            </div>
                            <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
                                <div class="overlay-container">
                                    <img src="../images/por-3.png" alt="">
                                    <div class="overlay-to-top">
                                        <p class="small margin-clear">&nbsp;</p>
                                    </div>
                                </div>
                                <div class="body">
                                    <h3 class="heading-font">Apache Spark Scala Training</h3>
                                    <div class="separator"></div>
                                    <p class="heading-font">Apache Spark Scala Combo Training </p>
                                    <a href="apache-spark-scala-training.php" target="_blank" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read More<i class="fa fa-arrow-right pl-10"></i></a>												
                                </div>
                            </div>
                            <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
                                <div class="overlay-container">
                                    <img src="../images/por-4.png" alt="">
                                    <div class="overlay-to-top">
                                        <p class="small margin-clear">&nbsp;</p>
                                    </div>
                                </div>
                                <div class="body">
                                    <h3 class="heading-font">Data Science Training</h3>
                                    <div class="separator"></div>
                                    <p class="heading-font">Find out the truth about what Data Science is. </p>
                                    <a href="data-science-course-training-bangalore.php" target="_blank" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read More<i class="fa fa-arrow-right pl-10"></i></a>												
                                </div>
                            </div>
                            <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
                                <div class="overlay-container">
                                    <img src="../images/por-5.png" alt="">
                                    <div class="overlay-to-top">
                                        <p class="small margin-clear">&nbsp;</p>
                                    </div>
                                </div>
                                <div class="body">
                                    <h3 class="heading-font">R Programming Training</h3>
                                    <div class="separator"></div>
                                    <p class="heading-font">Business Analytics With R Training</p>
                                    <a href="r-programing.php" target="_blank" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read More<i class="fa fa-arrow-right pl-10"></i></a>												
                                </div>
                            </div>
                            <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
                                <div class="overlay-container">
                                    <img src="../images/por-6.png" alt="">
                                    <div class="overlay-to-top">
                                        <p class="small margin-clear">&nbsp;</p>
                                    </div>
                                </div>
                                <div class="body">
                                    <h3 class="heading-font"> Tableau Training & Certification</h3>
                                    <div class="separator"></div>
                                    <p class="heading-font">Speed on concepts of data visualization</p>
                                    <a href="tableau.php" target="_blank" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read More<i class="fa fa-arrow-right pl-10"></i></a>												
                                </div>
                            </div>
                            <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
                                <div class="overlay-container">
                                    <img src="../images/por-7.png" alt="">
                                    <div class="overlay-to-top">
                                        <p class="small margin-clear">&nbsp;</p>
                                    </div>
                                </div>
                                <div class="body">
                                    <h3 class="heading-font">Python Programming</h3>
                                    <div class="separator"></div>
                                    <p class="heading-font">Advanced Concepts for Top-notch Developers  </p>
                                    <a href="python.php" target="_blank" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read More<i class="fa fa-arrow-right pl-10"></i></a>												
                                </div>
                            </div>
                        </div> 
                        <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center" >
                                        <div class="body" style="text-align: left;">
                                             <div itemscope itemtype="http://schema.org/Product">
                                                <span itemprop="name">Apache Spark Scala</span><br>
                                                <div itemprop="aggregateRating" itemscope itemtype="http://schema.org/AggregateRating">
                                                    Average Rating: <span itemprop="ratingValue">4.8</span><br>
                                                    Votes: <span itemprop="ratingCount">837</span><br>
                                                    Reviews: <span itemprop="reviewCount">837</span>
                                                </div>
                                            </div>

                                         </div>
                                     </div>
                    </div> 
                </div> 
                
                
                <p>&nbsp;</p>
                
            </section>
            <!-- section end -->
            
            <div class="clearfix"></div>
            
            <!-- ================ -->
            
            <!-- section end -->
            <section class="pv-30 dark-bg padding-bottom-clear clearfix object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
                <br>
                <div class="container">
                    <div class="row">
                        <div class="col-md-3 ">
                            <div class="ph-20 feature-box text-center object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
                                <span class="icon default-bg circle"><i class="fa fa-institution"></i></span>
                                <h3>Live classes</h3>
                                <div class="separator clearfix"></div>
                                <p class="heading-font ">Live online and interactive classes conducted by instructor</p>
                                
                            </div>
                        </div>
                        <div class="col-md-3 ">
                            <div class="ph-20 feature-box text-center object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
                                <span class="icon default-bg circle"><i class="fa fa-user"></i></span>
                                <h3>Expert instructions</h3>
                                <div class="separator clearfix"></div>
                                <p class="heading-font">Learn from our Experts and get Real-Time Guidance</p>
                                
                            </div>
                        </div>
                        <div class="col-md-3 ">
                            <div class="ph-20 feature-box text-center object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
                                <span class="icon default-bg circle"><i class="icon-signal"></i></span>
                                <h3>24 X 7 Support</h3>
                                <div class="separator clearfix"></div>
                                <p class="heading-font">Personalized Guidance from our 24X7 Support Team</p>
                                
                            </div>
                        </div>
                        <div class="col-md-3 ">
                            <div class="ph-20 feature-box text-center object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
                                <span class="icon default-bg circle"><i class="icon-calendar"></i></span>
                                <h3>Flexible schedule</h3>
                                <div class="separator clearfix"></div>
                                <p class="heading-font">Reschedule your Batch/Class at Your Convenience</p>
                                
                            </div>
                        </div>
                    </div>
                </div>
                <br>
            </section>
            
            <!-- section end -->
            
            <!-- section -->
            <!-- ================ -->
              <section class="pv-30  padding-bottom-clear dark-translucent-bg parallax" id="row-testimonials"style="background:url('../images/bg/451.jpg'); box-shadow:inset 0 2px 7px rgba(0,0,0, 0.25);">
                
                <div class="space-bottom">
                    
                    <div class="owl-carousel content-slider">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-8 col-md-offset-2">
                                    <div class="testimonial text-center">
                                        <div class="testimonial-image">
                                            <img src="../images/testimonial-1.png" alt="" title="" class="img-circle">
                                        </div>
                                        
                                        <div class="testimonial-body">
                                            <p>&nbsp;</p>
                                            <p id="cpara">It was a nice learning experience with prwatech. The classes were well scheduled and managed.
                                                Verma has good understanding of the topics taught and catered to our problems and doubts very patiently. The best thing about him was that he handled the situations accordingly, when needed a friend, he became one and also a teacher to always guide us.</p>
                                            
                                            <div class="testimonial-info-1">Anukanksha Garg</div>
                                            <div class="testimonial-info-2">B.TECH- CS</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="container">
                            <div class="row">
                                <div class="col-md-8 col-md-offset-2">
                                    <div class="testimonial text-center">
                                        <div class="testimonial-image">
                                            <img src="../images/testimonial-2.jpg" alt="" title="" class="img-circle">
                                        </div>
                                        
                                        <div class="testimonial-body">
                                            <p>&nbsp;</p>
                                            <p id="cpara">Excellent course and instructor. I learnt a lot in a short period. Good value for money. Instructor took us through Advanced Hadoop Development in depth.</p>
                                            
                                            <div class="testimonial-info-1">Anupam Khamparia</div>
                                            <div class="testimonial-info-2">Consultant, Cognizant Technology Solutions, Bangalore</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="container">
                            <div class="row">
                                <div class="col-md-8 col-md-offset-2">
                                    <div class="testimonial text-center">
                                        <div class="testimonial-image">
                                            <img src="../images/testimonial-3.jpg" alt="" title="" class="img-circle">
                                        </div>
                                        
                                        <div class="testimonial-body">
                                            <p>&nbsp;</p>
                                            <p id="cpara">Excellent course and instructor. I learnt a lot in a short period. Good value for money. Instructor took us through Advanced Hadoop Development in depth.</p>
                                            <div class="testimonial-info-1">Kumar Waibhav</div>
                                            <div class="testimonial-info-2">Software Engineer, Cognizant Technology Solutions,<br> Philadelphia, Pennsylvania, USA</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </section>
            <!-- section end -->
            
            <!-- section start -->
            <!-- ================ -->
            
            
            <!-- section end -->
            <!-- footer start (Add "dark" class to #footer in order to enable dark footer) -->
            <!-- ================ -->
			<?php include '../includes/footer.php'; ?>
            <!-- footer end -->
            
        </div>
        <!-- page-wrapper end -->
        
		<?php include '../includes/jslinks.php';?>
        <?php include '../includes/quick-query.php';?> 
         <?php include '../includes/UserSignup.php';?>
        <script type="text/javascript" src="../plugins/magnific-popup/jquery.magnific-popup.min.js"></script>
    </body>
    
<script>
            $(document).ready(function(){
                onlad(3);
            });  
        </script>    
</html>
