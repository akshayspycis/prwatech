<?php include './includes/check_session.php';?>
<html>
  <head>
    <meta charset="UTF-8">
    <title>Forum Topic</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.4 -->
    <?php include 'includes/links.php';?>
   
        <script type="text/javascript">
              cate={}
topic={}         
            
        </script>
        <script type="text/javascript">
          pro={}
          category={}
          var cat_id;
          $(document).ready(function(){
                  $.ajax({
                        type:"post",
                        url:"../server/controller/SelForumCategoryDetails.php",
                        success: function(data) { 
                        var duce = jQuery.parseJSON(data); //here data is passed to js object
                        $.each(duce, function (index, article) {
                            var a=$("<option></option>").append(article.forum_category).attr({'value':article.forum_category_details_id});
                            $("#search_category_id").find('#searchcategory_id').append(a);        
                            a=$("<option></option>").append(article.forum_category).attr({'value':article.forum_category_details_id});
                            $("#insForumTopic").find('#forum_category_details_id').append(a);                    
                        });
                            $("#search_category_id").find('#searchcategory_id').trigger("change");
                        }
                     });
                     $("#search_category_id").find('#searchcategory_id').change(function () {
                        var id = $( this ).val();
                        onlad(id)
                     }); 
                }); 
             
            
        </script>
  </head>
  <!-- ADD THE CLASS sidedar-collapse TO HIDE THE SIDEBAR PRIOR TO LOADING THE SITE -->
  <body class="skin-blue  sidebar-mini">
    <!-- Site wrapper -->
    <div class="wrapper">

   <?php include 'includes/header.php';?>

      <!-- =============================================== -->

      <!-- Left side column. contains the sidebar -->
   <?php include 'includes/sidepanel.php';?>

      <!-- =============================================== -->

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Dashboard
            <small>Admin panel</small>
          </h1>
          <ol class="breadcrumb">
            <li><i class="fa fa-dashboard"></i> Home</li>
            <li class="active">Forum Categories</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
           
          <div class="row">
            <div class="col-xs-12">
                <div class="box">
                <div class="box-header">
                  <h3 class="box-title">Forum Categories</h3>
                  <div class="pull-right">
                      <button type="button" class="btn btn-success" id="addcat">Add Topic</button>
                  </div>
                </div><!-- /.box-header -->
                
                <div class="box-body">
                    <div class="row">
                <div class="col-md-3" id="search_category_id">
        <div class="form-group" id="category">
                    <label>Select Category</label>
                    <select class="form-control select2" id="searchcategory_id" name="searchcategory_id">
                    </select>
                  </div><!-- /.form-group -->    
        </div>
                          
        </div>
                  <div class="table-responsive"> 
                   <table id="data" class="table table-bordered table-hover ">
                    <thead>
                      <tr>
                        <th>S.N.</th>
                        <th>Topic Name</th>
                        <th colspan="2">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                     
                      
                    </tbody>
                   
                  </table>
                  </div>         
                
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section>
      </div><!-- /.content-wrapper -->

     <?php include 'includes/footer.php';?>

     
    </div><!-- ./wrapper -->
    <?php include 'includes/jslinks.php';?>
    <!--Insert Topic Insert Modal Start-->
 <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
         <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Insert Forum Topic </h4>
        </div>
        <form id ="insForumTopic">    
        <div class="modal-body">
        <div class="row">
        <div class="form-group col-md-6">
            <label for="student name">Forum Category</label>
            <select class="form-control select2" id="forum_category_details_id" name="forum_category_details_id">

            </select>
        </div>
        <div class="col-md-6">
            <label for="student name">Topic</label>
        <div class="form-group">
         <input type="text"  name="topic_name" id="topic_name" value="" class="form-control"  placeholder="Enter Topic Name">
        </div>
        <!-- /.form-group -->

        </div><!-- /.col -->
       
        </div><!-- /.box -->


        <div class="modal-footer">
            <div class="validate pull-left" style="font-weight:bold;"></span></div>
        <button type="submit" class="btn btn-primary">Submit</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        
        </div>
        </div>
       </form>
        </div>
        </div>
       </div>
      <!--Insert Topic Insert Modal End-->
       <!--Insert Topic Edit Modal Start-->
<div class="modal fade" id="edit1" role="dialog">
<div class="modal-dialog">

<!-- Modal content-->
<div class="modal-content">

<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h4 class="modal-title">Edit Topic </h4>
</div>
<form id ="editTopic">    
<div class="modal-body">
<div class="row">

<div class="col-md-6">
<div class="form-group">
<input type="hidden"  name="forum_topic_details_id" id="forum_topic_details_id" value="" class="form-control"/>
<input type="text"  name="editcate" id="editcate" value="" class="form-control"  placeholder="Enter Topic Name">
</div>
<!-- /.form-group -->

</div><!-- /.col -->
</div><!-- /.box -->


<div class="modal-footer">
<div class="validate pull-left" style="font-weight:bold;"></span></div>
<button type="submit" id ="update" class="btn btn-primary">Update</button>
<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>

</div>
</div>
</form>
</div>
</div>
</div>
        <!--Insert Topic Edit Modal End-->
         <!--Insert Topic Delete Modal Start-->
 <div class="modal fade" id="delete1" role="dialog">
        <div class="modal-dialog">
                
        <!-- Modal content-->
        <div class="modal-content">
             
         <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Delete Topic </h4>
        </div>
        <form id ="deleteTopic">    
        <div class="modal-body">
        <div class="row">
        <div class="col-md-6">
        <input type="hidden"  name="forum_category_details_id" id="forum_category_details_id" value="" class="form-control"/>
        <input type="hidden"  name="Topic" id="dTopic" value="" class="form-control"/>
        <p>Sure to want to delete "<span style ="color:red" id ="catname"></span>" Topic?</p>
         </div>
        </div>
        <div class="modal-footer">
        <button type="submit" id="delete2" class="btn btn-danger">Delete</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        
        </div>
        </div>
       </form>
        </div>
        </div>
       </div>
          <!--Insert Topic Delete Modal End--> 
  </body>
  <script type="text/javascript" language="javascript">
      $(document).ready(function(){
        $("#addcat").click(function(){
            $("#myModal").modal('show');
            }); 
            $("#myModal").on('shown.bs.modal', function(){
                
                            $('#insForumTopic').off("submit");
                            $('#insForumTopic').submit(function() {
                              var forum_category_details_id=this.forum_category_details_id.value;
                            if(this.topic_name.value == ""){
                                $(".validate").addClass("text-danger").fadeIn(100).text(" Please fill valid Topic").prepend("<span class=\"glyphicon glyphicon-remove text-danger\">").fadeOut(1000);
                                $('#topic_name').focus();
                                $('#insForumTopic').each(function(){
                                   this.reset();
                                    return false;
                                 });
                             return false;  
                           }
                    else {
                        $(".validate").removeClass("text-danger").addClass("text-success").fadeIn(100).text("Ok").prepend("<span class=\"glyphicon glyphicon-ok text-success\">").fadeOut(1000);
                        $.ajax({
                        type:"post",
                        url:"../server/controller/InsForumTopicDetails.php",
                        data:$('#insForumTopic').serialize(),
                        success: function(data){ 
                        onlad(forum_category_details_id);
                            $('#insForumTopic').each(function(){
                                    this.reset();
                                    return false;
                            });
                          return false;
                      } 
               });
                    return false; 
                    }
                   return false;
    });
   
    var modal = this;
            var hash = modal.id;
            window.location.hash = hash;
            window.onhashchange = function() {
                    if (!location.hash){
                            $(modal).modal('hide');
                    }
            }
      });  
$('#myModal').on('hide.bs.modal', function() {
    	location.reload(true);
        
});
         }); 
         
         function onlad(forum_category_details_id){
         topic={}
                     $.ajax({
                    type:"post",
                    url:"../server/controller/SelForumTopicDetails.php",
                    data:{'forum_category_details_id':forum_category_details_id},
                    success: function(data) {
                        var duce = jQuery.parseJSON(data);
                        $("#data tr:has(td)").remove();
                        
                        $.each(duce, function (index, article){
                            topic[article.forum_topic_details_id]=article.topic_name;
                           $("#data").append($('<tr/>')
                                .append($('<td/>').html((index+1)))
                                .append($('<td/>').html(article.topic_name))
                                .append($('<td/>').html("<button type=\"button\" class=\"btn btn-success btn-xs\" data-id="+article.forum_topic_details_id+" data-Topic="+article.topic_name+" id=\"edit\" data-toggle=\"modal\" data-target=\"#edit1\")>&nbsp;&nbsp;Edit&nbsp;&nbsp;</button>"))
                                .append($('<td/>').html("<button type=\"button\" class=\"btn btn-danger btn-xs\"  data-id="+article.forum_topic_details_id+" data-Topic="+article.topic_name+"  id =\"delete\" data-toggle=\"modal\" data-target=\"#delete1\"\")>Delete</button>"))
                            );
                        });
                    }
                });
         }
  $(document).on("click", "#edit", function () {
                       var forum_topic_details_id = $(this).attr('data-id');
                       
                       $("#edit1").on('shown.bs.modal', function(){
                        $("#edit1").find("#forum_topic_details_id").val(forum_topic_details_id);  
                        $("#edit1").find("#editcate").val(topic[forum_topic_details_id]);          
                            $('#editTopic').off("submit");
                            $('#editTopic').submit(function() {
//                            var catfilter = /^[A-Za-z\d\s]+$/;
                            if(this.editcate.value == ""){
                                $(".validate").addClass("text-danger").fadeIn(100).text(" Please fill valid category ").prepend("<span class=\"glyphicon glyphicon-remove text-danger\">").fadeOut(1000);
                                $('#editcate').focus();
                           }

                    else {
                        $(".validate").removeClass("text-danger").addClass("text-success").fadeIn(100).text("Ok").prepend("<span class=\"glyphicon glyphicon-ok text-success\">").fadeOut(1000);
                        $.ajax({
                        type:"post",
                        url:"../server/controller/UpdForumTopicDetails.php",
                        data:$('#editTopic').serialize(),
                        success: function(data){ 
                             location.reload(true);
                                } 
                            });
                    }
                   return false;
    });
   
     var modal = this;
            var hash = modal.id;
            window.location.hash = hash;
            window.onhashchange = function() {
                    if (!location.hash){
                            $(modal).modal('hide');
                    }
            }
      });  
$('#edit1').on('hide.bs.modal', function() {
    	location.reload(true);
        
});
});

</script>

<script type="text/javascript" language="javascript">
        $(document).on("click", "#delete", function () {
                 var forum_topic_details_id = $(this).attr('data-id');
                 var Topic = topic[forum_topic_details_id];
                 $("#delete1").on('shown.bs.modal', function(){
                    $("#delete1").find("#forum_category_details_id").val(forum_topic_details_id);        
                    $("#delete1").find("#catname").text(Topic); 
                    $('#deleteTopic').off("submit");
                    $('#deleteTopic').submit(function() {
                         $.ajax({
                            type:"post",
                            url:"../server/controller/DelForumTopicDetails.php",
                            data:"forum_topic_details_id="+forum_topic_details_id,
                            success: function(data){ 
                            location.reload(true);
                                } 
                  });
                    return false;
    });
    
    });
   
});

</script>
</html>

