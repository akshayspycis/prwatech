<?php include './includes/check_session.php';?>
<html>
  <head>
    <meta charset="UTF-8">
    <title>Prwatech | Batch Details</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.4 -->
    <?php include 'includes/links.php';?>
     
     <script type="text/javascript">
          course_batch_details={}
          $(document).ready(function(){
              onlad($("#search_course_id").find('option:eq(0)').val());
            });  
      </script>
    <style>
        .uploadArea{ min-height:180px; height:auto; border:1px dotted #ccc; padding:10px; cursor:move; margin-bottom:10px; position:relative;}
            h1, h5{ padding:0px; margin:0px; }
            h1.title{ font-family:'Boogaloo', cursive; padding:10px; }
            .uploadArea h1{ color:#ccc; width:100%; z-index:0; text-align:center; vertical-align:middle; position:absolute; top:25px;}
            .dfiles{ clear:both; border:1px solid #ccc; background-color:#E4E4E4; padding:3px;  position:relative; height:25px; margin:3px; z-index:1; width:97%; opacity:0.6; cursor:default;}
    </style>
  </head>
  <!-- ADD THE CLASS sidedar-collapse TO HIDE THE SIDEBAR PRIOR TO LOADING THE SITE -->
  <body class="skin-blue  sidebar-mini sidebar-collapse">
    <!-- Site wrapper -->
    <div class="wrapper">

   <?php include 'includes/header.php';?>

      <!-- =============================================== -->

      <!-- Left side column. contains the sidebar -->
   <?php include 'includes/sidepanel.php';?>

      <!-- =============================================== -->

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Dashboard
            <small>Admin panel</small>
          </h1>
          <ol class="breadcrumb">
            <li><i class="fa fa-dashboard"></i> Home</li>
            <li class="active">Batchs</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
           
          <div class="row">
            <div class="col-xs-12">
           

              <div class="box">
                <div class="box-header">
                  
                  <div class="form-group col-lg-12 col-sm-12">
                      <h3 class="box-title"><b>Batches</b></h3>
                  </div>
                  <div class="col-lg-5 col-sm-5">
                  <div class="row">
                  <div class="col-lg-3 col-sm-4">
                                                        <label for="sel1" >Courses</label>
                                                        </div>
                      <div class="col-lg-8 col-sm-8">
                          <script>
                                   $(document).ready(function(){
                                                                $("#search_course_id").change(function(){
                                                                    onlad($(this).val())
                                                                });
                                                                });
                                                            </script>
                                                        <select class="form-control select2" id="search_course_id" name="search_course_id">
                                                            <option value="1">Big Data Hadoop Training</option>
                                                            <option value="2">Hadoop Admin</option>
                                                            <option value="3">Apache Spark Scala Training</option>
                                                            <option value="4">Data Science Course</option>
                                                            <option value="5">R Programming</option>
                                                            <option value="6">Tableau</option>
                                                            <option value="7">Python</option>
                                                        </select>
                                                     
                                                     </div>
                                                     </div>
                                                     </div>
                  <div class="pull-right">
                      <button type="button" class="btn btn-success" id="addBatch">Add Batch</button>
                  </div>
                </div><!-- /.box-header -->
                
                <div class="box-body">
                  <div class="table-responsive"> 
                   <table id="data" class="table table-bordered table-hover ">
                    <thead>
                       <tr>
                        <th width="5%">S.N.</th>
                        <th>Course name</th>
                        <th>Date</th>
                        <th>Day Type</th>
                        <th>Day</th>
                        <th>Time</th>
                        <th>Fees</th>
                        <th>Discount</th>
                        <th colspan="2" style="text-align:center">Status</th>
                      </tr>
                    </thead>
                    <tbody>
                        
                    </tbody>
                    <tfoot>
                      
                    </tfoot>
                  </table>
                  </div>         
                
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section>
      </div><!-- /.content-wrapper -->

     <?php // include 'includes/footer.php';?>

     
    </div><!-- ./wrapper -->
    <?php include 'includes/jslinks.php';?>
    <!--Insert Courses Insert Modal Start-->
<div class="modal fade" id="insBatch" role="dialog">
            <div class="modal-dialog">
                
                <!-- Modal content-->
                <div class="modal-content">
                    
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Insert Faq's </h4>
                    </div>
                    <form id ="insertfaq" enctype="multipart/form-data">    
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="box">
                                        <div class="box-body">
                                            <div class="row">
                                                <div class='col-md-12'>
                                                    <div class="form-group col-lg-6 col-sm-6">
                                                        <label for="sel1" >Courses</label>
                                                        <select class="form-control select2" id="course_id" name="course_id">
                                                            <option value="">Please Select Courses</option>
                                                            <option value="1">Big Data Hadoop Training</option>
                                                            <option value="2">Hadoop Admin</option>
                                                            <option value="3">Apache Spark Scala Training</option>
                                                            <option value="4">Data Science Course</option>
                                                            <option value="5">R Programming</option>
                                                            <option value="6">Tableau</option>
                                                            <option value="7">Python</option>
                                                        </select>
                                                     </div>
                                                    <div class="form-group col-lg-6 col-sm-6">
                                                        <label for="question">Date</label>
                                                        <input type="date" class="form-control"  id="date" name="date" placeholder="Question here">
                                                    </div>
                                                    <div class="form-group col-lg-6 col-sm-6">
                                                        <label for="question">Day Type</label>
                                                        <select class="form-control select2" id="day_type" name="day_type">
                                                            <option value="">Select</option>
                                                            <option value="Week Day">Week Day</option>
                                                            <option value="Week End">Week End</option>
                                                        </select>

                                                    </div>
                                                    <div class="form-group col-lg-6 col-sm-6">
                                                        <label for="question">Day</label>
                                                        <select class="form-control select2" id="day" name="day">
                                                            <script>
                                                                var weekday=["Mon","Tue","Web","Thur","Fri"];
                                                                var weekend=["Sat","Sun"];
                                                                $("#day_type").change(function(){
                                                                    $("#day").empty();
                                                                    $("#day").append($("<option>").append("Select").attr({'value':""}));
                                                                    if($(this).val()!=""){ 
                                                                        var a;
                                                                        if($(this).val()=="Week Day"){
                                                                            a=weekday;
                                                                        }else{
                                                                            a=weekend;
                                                                        }
                                                                        
                                                                        for(var i=0;i<a.length ;i++)
                                                                            $("#day").append($("<option>").append(a[i]).attr({'value':a[i]}));
                                                                    }
                                                                    
                                                                });
                                                            </script>
                                                            
                                                        </select>
                                                    </div>
                                                    <div class="form-group col-lg-6 col-sm-6">
                                                        <label for="question">Time</label>
                                                        <input type="time" class="form-control"  id="time" name="time" >
                                                    </div>
                                                    <div class="form-group col-lg-6 col-sm-6">
                                                        <label for="question">Fees</label>
                                                        <input type="number" class="form-control"  id="fees" name="fees" value="0">
                                                    </div>
                                                    <div class="form-group col-lg-6 col-sm-6">
                                                        <label for="question">Discount <small>In Percentage</small></label>
                                                        <input type="number" class="form-control"  id="discount" name="discount" value="0">
                                                    </div>
                                                    <div class="form-group col-lg-6 col-sm-6">
                                                        <label for="question">Status</label>
                                                        <select class="form-control select2" id="status" name="status">
                                                            <option value="Enable">Enable</option>
                                                            <option value="Disable">Disable</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div><!-- /.row -->
                                        </div><!-- /.box-body -->
                                        
                                        
                                    </div><!-- /.box -->
                                </div><!-- /.col -->
                                
                                
                            </div><!-- /.box -->
                            
                            
                            <div class="modal-footer">
                                <div class="validate pull-left" style="font-weight:bold;"></span></div>
                                <button type="submit" class="btn btn-primary">Submit</button>
                                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>     

      <div class="modal fade" id="delete1" role="dialog">
        <div class="modal-dialog">
                
        <!-- Modal content-->
        <div class="modal-content">
             
         <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Delete Feedbacks </h4>
        </div>
        <form id ="deleteFeedback">    
        <div class="modal-body">
        <div class="row">
        <div class="col-md-6">
        <input type="hidden"  name="sid" id="sid" value="" class="form-control"/>
        <p id ="msg">Sure to want to delete ?</p>
         </div>
        </div>
        <div class="modal-footer">
        <button type="submit" id="delete2" class="btn btn-danger">Ok</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        
        </div>
        </div>
       </form>
        </div>
        </div>
       </div> 
  </body>

    <script type="text/javascript" language="javascript">
     $(document).ready(function(){
        $("#addBatch").click(function(){
            $("#insBatch").modal('show'); 
        }); 
            $("#insBatch").on('shown.bs.modal', function(){
                var courseid;
                            $('#insertfaq').off("submit");
                            $('#insertfaq').submit(function() {
                           if(this.course_id.value === ""){
                                $(".validate").addClass("text-danger").fadeIn(100).text(" Please Select Courses ").prepend("<span class=\"glyphicon glyphicon-remove text-danger\">").fadeOut(1000);
                             return false;  
                           }else if(this.date.value === ""){
                                 $(".validate").addClass("text-danger").fadeIn(100).text(" Please Select Date ").prepend("<span class=\"glyphicon glyphicon-remove text-danger\">").fadeOut(1000);
                                 this.date.focus();
                                 return false;  
                           }else if(this.day_type.value === ""){
                                $(".validate").addClass("text-danger").fadeIn(100).text(" Please Select Day Type  ").prepend("<span class=\"glyphicon glyphicon-remove text-danger\">").fadeOut(1000);
                                return false;  
                           }else if(this.day.value === ""){
                                $(".validate").addClass("text-danger").fadeIn(100).text(" Please Select Day ").prepend("<span class=\"glyphicon glyphicon-remove text-danger\">").fadeOut(1000);
                             return false;  
                           }else if(this.time.value === ""){
                              $(".validate").addClass("text-danger").fadeIn(100).text(" Please fill valid Time ").prepend("<span class=\"glyphicon glyphicon-remove text-danger\">").fadeOut(1000);
                             return false;  
                           }else if(this.fees.value === "" && this.fees.value != "0"){
                              $(".validate").addClass("text-danger").fadeIn(100).text(" Please fill valid Fees ").prepend("<span class=\"glyphicon glyphicon-remove text-danger\">").fadeOut(1000);
                                this.fees.focus();
                             return false;  
                        }else {
                            courseid=this.course_id.value ;
                            $(".validate").removeClass("text-danger").addClass("text-success").fadeIn(100).text("Ok").prepend("<span class=\"glyphicon glyphicon-ok text-success\">").fadeOut(1000);
                            $.ajax({
                            type:"post",
                            url:"../server/controller/InsCourseBatchDetails.php",
                            data:$('#insertfaq').serialize(),
                            success: function(data){ 
                                $("#search_course_id").val(courseid);
                                $("#search_course_id").val(courseid).trigger("change");;
                            $('#insertfaq').each(function(){
                            this.reset();
                             
                            return false;
                            });
                              return false;
                      } 
               });
                    return false; 
                    }
                   return false;
    }); });});
    </script>    

<script type="text/javascript" language="javascript">
      /* Code for Product Delete start */  
        $(document).on("click", "#delete", function () {
                 var course_batch_details_id = $(this).data('course_batch_details_id');
               
                    $("#delete1").on('shown.bs.modal', function(){
                    $("#delete1").find("#course_batch_details_id").val(course_batch_details_id);        
                        $('#deleteFeedback').submit(function() {
                         $.ajax({
                            type:"post",
                            url:"../server/controller/deleteBatchs.php",
                            data:"course_batch_details_id="+course_batch_details_id,
                            success: function(data){ 
                               alert(data)
                        $('#deleteFeedback').each(function(){
                                this.reset();
                                onlad();
               $('#delete1').modal('hide');
                        return false;
                     });
             } 
                  });
                    return false;
    });
    
    });
   
});
  /* Code for Product Delete end */  
     
//...........................................................................................................

function onlad(course_id){
                /* Ajax call for Product Display*/
                    $.ajax({
                    type:"post",
                    url:"../server/controller/SelCourseBatchDetails.php",
                    data:{'course_id':course_id},
                    success: function(data) {
                        
                      var duce = jQuery.parseJSON(data);
                        $("#data tr:has(td)").remove();
                        $.each(duce, function (index, article) {
                            course_batch_details[article.course_batch_details_id]={};
                            course_batch_details[article.course_batch_details_id]["course_id"]=article.course_id;
                            course_batch_details[article.course_batch_details_id]["date"]=article.date;
                            course_batch_details[article.course_batch_details_id]["day_type"]=article.day_type;
                            course_batch_details[article.course_batch_details_id]["day"]=article.day;
                            course_batch_details[article.course_batch_details_id]["time"]=article.time;
                            course_batch_details[article.course_batch_details_id]["fees"]=article.fees;
                            course_batch_details[article.course_batch_details_id]["discount"]=article.discount;
                            var btn_class="btn-danger";
                           if(article.status=="Enable"){
                               btn_class="btn-success";
                           }
                            
                          $("#data").append($('<tr/>')
                                .append($('<td/>').html((index+1)))
                                .append($('<td/>').html(article.course_id))
                                .append($('<td/>').html(article.date))
                                .append($('<td/>').html(article.day_type))
                                .append($('<td/>').html(article.day))
                                .append($('<td/>').html(article.time))
                                .append($('<td/>').html(article.fees))
                                .append($('<td/>').html(article.discount))
                                .append($('<td/>').append($("<button></button>").addClass("btn "+btn_class+" btn-xs").attr({"id":"status"}).append(article.status).click(function(){
                                     updatestatus($(this),article.course_batch_details_id);
                                     
                                })))
                             );
                        });
                    }
                });
                }
                 function updatestatus(obj,id){
                      var value;
                      if(obj.text().trim()=="Enable"){
                                value="Disable";
                                obj.empty();
                                obj.append("Disable");
                                obj.removeClass("btn-success");
                                obj.addClass("btn-danger");
                      }else{
                                value="Enable";
                                obj.empty();
                                obj.append("Enable");
                                obj.removeClass("btn-danger");
                                obj.addClass("btn-success");
                      }           
                  $.ajax({
                    type:"post",
                    url:"../server/controller/UpdCourseBatchDetailsStatus.php",
                    data:{'course_batch_details_id':id,'status':value},
                    success: function(data) {
                    }
                  });
              }
</script>


</html>


