  <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar user panel -->
          <div class="user-panel">
            <div class="pull-left image">
              <img src="style/images/user.png" class="img-circle" alt="User Image" />
            </div>
            <div class="pull-left info">
                <p><?php echo "Welcome"." ".$_SESSION['user_name']; ?></p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
          </div>

          <ul class="sidebar-menu">
            <li class="header">Main  Navigation</li>
            <li>
                <a href="home.php">
                <i class="fa fa-dashboard"></i>
                <span>Dashboard</span>
              </a>
            </li>
             <li>
                <a href="user_detials.php">
                <i class="fa fa-users"></i>
                <span>User Details</span>
              </a>
             
            </li> 
             <li>
                <a href="queries.php">
                <i class="fa fa-calendar"></i>
                <span>Course Queries</span>
              </a>
             
            </li> 
             <li>
                <a href="queries.php">
                <i class="fa fa-files-o"></i>
                <span>Queries</span>
              </a>
             
            </li> 
<!--            <li class="treeview">
              <a href="newsupdate.php">
                <i class="fa fa-dashboard"></i> <span>News & Updates</span>
              </a>
            </li>-->
             <li class="treeview">
                <a href="gallerycate.php">
                    <i class="fa fa-list-alt"></i>
                    <span>Gallery Categories</span>
                </a>
            </li>
            <li class="treeview">
              <a href="gallery.php">
                <i class="fa fa-picture-o"></i>
                <span>Gallery</span>
              </a>
            </li>
           
           
            <li>
                <a href="feedback.php">
                <i class="fa fa-user"></i>
                <span>Student Feedback </span>
              </a>
            </li> 
           
            <li>
                <a href="blog_category.php">
                <i class="fa fa-list"></i>
                <span>Blog Category </span>
              </a>
            </li> 
            <li>
                <a href="blog_details.php">
                <i class="fa fa-desktop "></i>
                <span>Blog Details</span>
              </a>
            </li> 
<!--            <li>
                <a href="cfaq.php">
                <i class="fa fa-user"></i>
                <span>Faq's Category</span>
              </a>
           </li> -->
<!--            <li><a href="faq.php">
                <i class="fa fa-user"></i>
                <span>Faq's</span>
              </a></li>-->
            <li>&nbsp;</li>
            <li>&nbsp;</li>
            <li>&nbsp;</li>
            <li>&nbsp;</li>
            <li>&nbsp;</li>
            <li>&nbsp;</li>
            <li>&nbsp;</li>
            <li>&nbsp;</li>
            <li>&nbsp;</li>
            <li>&nbsp;</li>
            <li>&nbsp;</li>
            <li>&nbsp;</li>
            <li>&nbsp;</li>
            <li>&nbsp;</li>
            <li>&nbsp;</li>
            <li>&nbsp;</li>
            <li>&nbsp;</li>
            <li>&nbsp;</li>
         </ul>
        </section>
        <!-- /.sidebar -->
      </aside>