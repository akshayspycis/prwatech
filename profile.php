<?php include 'includes/session.php'; ?>
<!DOCTYPE html>
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
	<!--<![endif]-->
        <head>
            <meta charset="utf-8">
            <title>PrwaTech | Profile </title>
                <meta name="title" content="Python  Training  Classes Pune &amp; Bangalore | Big Data Hadoop Online Training | Hadoop Certification Course | Prwatech" />
                <meta name="keywords" content="Python training, online Python training, Python training classes, Learn Python course online" />
                <meta name="description" content="Our Python training helps you learn Python programming with tutorials on integrating Big Data to Pig, Hive etc. You earn a Python certification on successful completion of the course" />
                <meta name="author" content="prwatech">
                <meta name="google-site-verification" content="fSzBVSEUMu0l5lnD0qBVGv5F_16zaI6xiUZFMm-iMqQ" />  
                <?php include 'includes/csslinks.php'; ?>
        </head>

	<!-- body classes:  -->
	<!-- "boxed": boxed layout mode e.g. <body class="boxed"> -->
	<!-- "pattern-1 ... pattern-9": background patterns for boxed layout mode e.g. <body class="boxed pattern-1"> -->
	<!-- "transparent-header": makes the header transparent and pulls the banner to top -->
	<body class="no-trans front-page transparent-header ">
            <!-- scrollToTop -->
		<!-- ================ -->
		<div class="scrollToTop circle"><i class="icon-up-open-big"></i></div>
		
		<!-- page wrapper start -->
		<!-- ================ -->
		<div class="page-wrapper">
		
			<!-- header-container start -->
			<?php include './includes/header.php' ;  ?>
			<!-- header-container end -->
		
			<!-- banner start -->
			<!-- ================ -->
			<div class="banner dark-translucent-bg" style="background-image:url('images/bg/451.png'); background-position: 50% 27%;">
				<!-- breadcrumb start -->
				<!-- ================ -->
				<div class="breadcrumb-container object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
					<div class="container">
						<ol class="breadcrumb">
							<li><i class="fa fa-home pr-10"></i>Home</li>
							<li class="active">User</li>
							<li class="active">Profile</li>
						</ol>
					</div>
				</div>
				<!-- breadcrumb end -->
				<div class="container">
					
				</div>
			</div>
			<!-- banner end -->
			
			<div id="page-start"></div>

			<!-- section start -->
			<!-- ================ -->
			
                          <section id="coursed-row2" class="object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
                            <div class="container">
                                
                                 <h2 class="text-center heading-font object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100" style="text-transform:none;"> <strong>User</strong> Details</h2>
                                 <div id="vseparator" class="object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
                                     <div class="vhr"></div>
                                </div>
                                <div class="row col-md-12" >
                               <!-- tabs start -->
                                <!-- ================ -->
                                <!-- Nav tabs -->
                                <ul class="nav nav-tabs style-3" id="tab-course" role="tablist">
                                    <li class="active"><a href="#h3tab1" role="tab" data-toggle="tab">Profile</a></li>
                                    <li><a href="#h3tab2" role="tab" data-toggle="tab">My Orders</a></li>
                                    
                                   
                                </ul>
                                <!-- Tab panes -->
                                <div class="tab-content">
                                    <div class="tab-pane in active " id="h3tab1">
                                        <div class="row">
                                            <div class="col-md-12 object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
                                                   
                                            </div>
                                            
                                        </div>
                                            
                                    </div>
                                    <div class="tab-pane" id="h3tab2">
                                          <div class="row">
                                            <div class="col-md-12 ">
                                                 
                                                
                                                </div>

                                        </div>
                                       
                                    </div>
                                          <div class="tab-pane" id="h3tab3">
                                        <div class="row">
                                            <div class="col-md-12 ">
                                                   	<div class="panel-group collapse-style-3" id="accordion-3">
								<div class="panel panel-default">
									<div class="panel-heading">
										<h4 class="panel-title">
											<a data-toggle="collapse" data-parent="#accordion-3" href="#collapseOne-3">
												Why Should I Learn Hadoop From Prwatech?
											</a>
										</h4>
									</div>
									<div id="collapseOne-3" class="panel-collapse collapse in">
										<div class="panel-body">
                                                                                    <p id="cpara" class="text-justify">
                                                                                        Prwatech is the pioneer of Hadoop training in India. As you know today the demand for Hadoop professionals far exceeds the supply. So it pays to be with the market leader like Prwatech when it comes to learning Hadoop in order to command top salaries. As part of the training you will learn about the various components of Hadoop like MapReduce, HDFS, HBase, Hive, Pig, Sqoop, Flume, Oozie among others. You will get an in-depth understanding of the entire Hadoop framework for processing huge volumes of data in real world scenarios.
                                                                                    </p>
                                                                                    <p id="cpara" class="text-justify">
                                                                                       The Prwatech training is the most comprehensive course, designed by industry experts keeping in mind the job scenario and corporate requirements. We also provide lifetime access to videos, course materials, 24/7 Support, and free course material upgrade. Hence it is a one-time investment. 
                                                                                    </p>
										</div>
									</div>
								</div>
								<div class="panel panel-default">
									<div class="panel-heading">
										<h4 class="panel-title">
											<a data-toggle="collapse" data-parent="#accordion-3" href="#collapseTwo-3" class="collapsed">
												What Are The Various Modes Of Training That Prwatech Offers?
											</a>
										</h4>
									</div>
									<div id="collapseTwo-3" class="panel-collapse collapse">
										<div class="panel-body">
                                                                                    <p id="cpara" class="text-justify">
                                                                                        Prwatech basically offers the self-paced training and online instructor-led training. Apart from that we also provide corporate training for enterprises. All our trainers come with over 5 years of industry experience in relevant technologies and also they are subject matter experts working as consultants. You can check about the quality of our trainers in the sample videos provided.
                                                                                    </p>
                                                                                    
										</div>
									</div>
								</div>
								<div class="panel panel-default">
									<div class="panel-heading">
										<h4 class="panel-title">
											<a data-toggle="collapse" data-parent="#accordion-3" href="#collapseThree-3" class="collapsed">
												Can I Request For A Support Session If I Find Difficulty In Grasping Topics?
											</a>
										</h4>
									</div>
									<div id="collapseThree-3" class="panel-collapse collapse">
										<div class="panel-body">
                                                                                    <p id="cpara" class="text-justify">
                                                                                          If you have any queries you can contact our 24/7 dedicated support to raise a ticket. We provide you email support and solution to your queries. If the query is not resolved by email we can arrange for a one-on-one session with our trainers. The best part is that you can contact Prwatech even after completion of training to get support and assistance. There is also no limit on the number of queries you can raise when it comes to doubt clearance and query resolution.
                                                                                    </p>
                                                                                    
										</div>
									</div>
								</div>
                                                            <div class="panel panel-default">
									<div class="panel-heading">
										<h4 class="panel-title">
											<a data-toggle="collapse" data-parent="#accordion-3" href="#collapseFour-3" class="collapsed">
												If I Am Not From A Programming Background But Have A Basic Knowledge Of Programming Can I Still Learn Hadoop?
											</a>
										</h4>
									</div>
									<div id="collapseFour-3" class="panel-collapse collapse">
										<div class="panel-body">
                                                                                    <p id="cpara" class="text-justify">
                                                                                         Yes, you can learn Hadoop without being from a software background. We provide complimentary courses in Java and Linux so that you can brush up on your programming skills. This will help you in learning Hadoop technologies better and faster.
                                                                                    </p>
                                                                                    
										</div>
									</div>
								</div>
                                                            <div class="panel panel-default">
									<div class="panel-heading">
										<h4 class="panel-title">
											<a data-toggle="collapse" data-parent="#accordion-3" href="#collapseFive-3" class="collapsed">
												What Kind Of Projects Will I Be Working On As Part Of The Training?
											</a>
										</h4>
									</div>
									<div id="collapseFive-3" class="panel-collapse collapse">
										<div class="panel-body">
                                                                                    <p id="cpara" class="text-justify">
                                                                                        We provide you with the opportunity to work on real world projects wherein you can apply your knowledge and skills that you acquired through our training. We have multiple projects that thoroughly test your skills and knowledge of various Hadoop components making you perfectly industry-ready. These projects could be in exciting and challenging fields like banking, insurance, retail, social networking, high technology and so on. The Prwatech projects are equivalent to six months of relevant experience in the corporate world.
                                                                                    </p>
                                                                                    
										</div>
									</div>
								</div>
                                                            <div class="panel panel-default">
									<div class="panel-heading">
										<h4 class="panel-title">
											<a data-toggle="collapse" data-parent="#accordion-3" href="#collapseSix-3" class="collapsed">
												Do You Provide Placement Assistance?
											</a>
										</h4>
									</div>
									<div id="collapseSix-3" class="panel-collapse collapse">
										<div class="panel-body">
                                                                                    <p id="cpara" class="text-justify">
                                                                                        Yes, Prwatech does provide you with placement assistance. We have tie-ups with 80+ organizations including Ericsson, Cisco, Cognizant, TCS, among others that are looking for Hadoop professionals and we would be happy to assist you with the process of preparing yourself for the interview and the job.
                                                                                    </p>
                                                                                    
										</div>
									</div>
								</div>
							</div>
                                                
                                                </div>

                                        </div>
                                       
                                    </div>
                                    <div class="tab-pane" id="h3tab4">
                                        <div class="row object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
                                            <div class="col-md-4">
                                                <p id="cpara" class="text-default"><a href="">Click Here to Download</a></p>
                                                
                                            </div>
                                            <div class="col-md-8">
                                                 <h4 class="title text-default" class="heading-font">Bigdata and Hadoop Services</h4>
                                                <ul class="list-icons">
                                                    <li><i class="icon-check pr-10"></i> PowerPoint Presentation covering all classes</li>
                                                    <li><i class="icon-check pr-10"></i> Recorded Videos Sessions On Bigdata and Hadoop with LMS Access.(lifetime support)</li>
                                                    <li><i class="icon-check pr-10"></i> Quiz , Assignment & POC.</li>
                                                    <li><i class="icon-check pr-10"></i> On Demand Online Support .</li>
                                                    <li><i class="icon-check pr-10"></i> Discussion Forum.</li>
                                                    <li><i class="icon-check pr-10"></i> Material
                                                        <ul class="list-icons" style="margin-left:50px;">
                                                            <li>a. Sample Question papers of Cloudera Certification.</li>
                                                            <li>b. Technical Notes & Study Material.</li>
                                                        </ul>
                                                    
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        
                                    </div>
                                    <div class="tab-pane" id="h3tab5">
                                        <div class="row object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
                                            <div class="col-md-6 ">
                                                <div class="pv-20 ph-20 feature-box-2 boxed shadow object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
                                                    <div class="col-md-3 testimonial-image"><img src="images/testimonial-2.jpg" /></div> 
                                                    <div class="body">
                                                        <h4 class="title">Anupam Khamparia 
                                                            <br>
                                                            <span style="font-size:11px;">Consultant, Cognizant Technology Solutions, Bangalore</span>
                                                        </h4>
                                                        <p class ="text-justify">“Excellent course and instructor. I learnt a lot in a short period. Good value for money. Instructor took us through Advanced Hadoop Development in depth.”</p>
                                                       
                                                    </div>
                                                </div>
                                            </div>
                                          <div class="col-md-6 ">
                                                <div class="pv-20 ph-20 feature-box-2 boxed shadow object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
                                                    <div class="col-md-3 testimonial-image"><img src="images/testimonial-1.png" /></div> 
                                                    <div class="body">
                                                        <h4 class="title">Anukanksha Garg
                                                          <br>
                                                            <span style="font-size:11px;">B.Tech. CSE</span>
                                                        </h4>
                                                        <p class ="text-justify">It was a nice learning experience with prwatech. The classes were well scheduled and managed.
Verma has good understanding of the topics taught and catered to our problems and doubts very patiently. The best thing about him was that he handled the situations accordingly, when needed a friend, he became one and also a teacher to always guide us.</p>
                                                       
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6 ">
                                                <div class="pv-20 ph-20 feature-box-2 boxed shadow object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
                                                    <div class="col-md-3 testimonial-image"><img src="images/testimonial-4.PNG" /></div> 
                                                    <div class="body">
                                                        <h4 class="title">Varun Shivashanmugum
                                                        
                                                        <br>
                                                            <span style="font-size:11px;">Associate Consultant, ITC Infotech Ltd</span>
                                                        </h4>
                                                        
                                                        <p class ="text-justify">“Faculty is good,Verma takes keen interest and personnel care in improvising skills in students. And most importantly,Verma will be available and clears doubts at any time apart from class hours. And he always keeps boosting and trying to increase confidence in his students which adds extra attribute to him and organization as well. and organization as well.</p>
                                                       
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6 ">
                                                <div class="pv-20 ph-20 feature-box-2 boxed shadow object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
                                                    <div class="col-md-3 testimonial-image"><img src="images/testimonial-5.jpg" /></div> 
                                                    <div class="body">
                                                        <h4 class="title">Mayank Srivastava <br>
                                                        <span style="font-size:11px;">Hadoop Developer, L&T, Bangalore</span>
                                                        </h4>
                                                        <p class ="text-justify">“Really good course content and labs, patient enthusiastic instructor. Good instructor, with in depth skills…Very relevant practicals allowed me to put theory into practice.”</p>
                                                       
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                                
                            </div> 
                           
                           
                            <p>&nbsp;</p>
                            
                        </section>
                        <!-- section end -->
                       <div class="clearfix"></div>
			
			<!-- ================ -->
			
			<!-- section end -->
                        <section class="pv-30 dark-bg padding-bottom-clear clearfix object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
					<br>
					<div class="container">
						<div class="row">
							<div class="col-md-3 ">
								<div class="ph-20 feature-box text-center object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
									<span class="icon default-bg circle"><i class="fa fa-institution"></i></span>
									<h3>Live classes</h3>
									<div class="separator clearfix"></div>
                                                                        <p class="heading-font ">Live online and interactive classes conducted by instructor</p>
									
								</div>
							</div>
							<div class="col-md-3 ">
								<div class="ph-20 feature-box text-center object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
									<span class="icon default-bg circle"><i class="fa fa-user"></i></span>
									<h3>Expert instructions</h3>
									<div class="separator clearfix"></div>
									<p class="heading-font">Learn from our Experts and get Real-Time Guidance</p>
									
								</div>
							</div>
							<div class="col-md-3 ">
								<div class="ph-20 feature-box text-center object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
									<span class="icon default-bg circle"><i class="icon-signal"></i></span>
									<h3>24 X 7 Support</h3>
									<div class="separator clearfix"></div>
									<p class="heading-font">Personalized Guidance from our 24X7 Support Team</p>
									
								</div>
							</div>
							<div class="col-md-3 ">
								<div class="ph-20 feature-box text-center object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
									<span class="icon default-bg circle"><i class="icon-calendar"></i></span>
									<h3>Flexible schedule</h3>
									<div class="separator clearfix"></div>
									<p class="heading-font">Reschedule your Batch/Class at Your Convenience</p>
									
								</div>
							</div>
						</div>
					</div>
					<br>
				</section>
			
			<!-- section end -->

			<!-- section -->
			<!-- ================ -->
			<section class="pv-30 light-gray-bg padding-bottom-clear" >
				
				<div class="space-bottom">
					
					<div class="owl-carousel content-slider">
						<div class="container">
							<div class="row">
								<div class="col-md-8 col-md-offset-2">
									<div class="testimonial text-center">
										<div class="testimonial-image">
                                                                                    <img src="images/testimonial-1.png" alt="" title="" class="img-circle">
										</div>
										
										<div class="testimonial-body">
                                                                                    <p>&nbsp;</p>
												<p id="cpara">It was a nice learning experience with prwatech. The classes were well scheduled and managed.
Verma has good understanding of the topics taught and catered to our problems and doubts very patiently. The best thing about him was that he handled the situations accordingly, when needed a friend, he became one and also a teacher to always guide us.</p>
											
											<div class="testimonial-info-1">Anukanksha Garg</div>
											<div class="testimonial-info-2">B.TECH- CS</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="container">
							<div class="row">
								<div class="col-md-8 col-md-offset-2">
									<div class="testimonial text-center">
										<div class="testimonial-image">
											<img src="images/testimonial-2.jpg" alt="" title="" class="img-circle">
										</div>
										
										<div class="testimonial-body">
											 <p>&nbsp;</p>
												<p id="cpara">Excellent course and instructor. I learnt a lot in a short period. Good value for money. Instructor took us through Advanced Hadoop Development in depth.</p>
											
											<div class="testimonial-info-1">Anupam Khamparia</div>
											<div class="testimonial-info-2">Consultant, Cognizant Technology Solutions, Bangalore</div>
										</div>
									</div>
								</div>
							</div>
						</div>
                                            <div class="container">
							<div class="row">
								<div class="col-md-8 col-md-offset-2">
									<div class="testimonial text-center">
										<div class="testimonial-image">
											<img src="images/testimonial-3.jpg" alt="" title="" class="img-circle">
										</div>
										
										<div class="testimonial-body">
											<p>&nbsp;</p>
												<p id="cpara">Excellent course and instructor. I learnt a lot in a short period. Good value for money. Instructor took us through Advanced Hadoop Development in depth.</p>
											<div class="testimonial-info-1">Kumar Waibhav</div>
                                                                                        <div class="testimonial-info-2">Software Engineer, Cognizant Technology Solutions,<br> Philadelphia, Pennsylvania, USA</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					
				</div>
			</section>
			<!-- section end -->

			<!-- section start -->
			<!-- ================ -->
                        
			                   
			<!-- section end -->
			<!-- footer start (Add "dark" class to #footer in order to enable dark footer) -->
			<!-- ================ -->
			<?php include './includes/footer.php'; ?>
			<!-- footer end -->
			
		</div>
		<!-- page-wrapper end -->

		<?php include './includes/jslinks.php';?>
		<!-- Color Switcher End -->
                    <?php include './includes/quick-query.php';?>
                 <?php include './includes/UserSignup.php';?>
	</body>

        <script>
            $(document).ready(function(){
                onlad(7);
            });  
        </script>

</html>

