 <?php include 'includes/session.php'; ?>
<?php
$request  = str_replace("/category/", "", $_SERVER['REQUEST_URI']);
    if (strpos($request, '/') !== false) {
            $request = str_replace("/","", $request);
    } 
?>
<html lang="en">
	<!--<![endif]-->
<head>
    <base href="/">
		<meta charset="utf-8">
                <title>PrwaTech | Blog </title>
		<meta name="title" content="Big Data Hadoop Online Training | Hadoop Certification Course | Prwatech" />
                <meta name="keywords" content="hadoop training, online hadoop training, hadoop training classes, hadoop course online, big data training, big data course, big data online course, hadoop tutorial, HDFS training, Yarn training, MapReduce training, Pig training, Hive training, HBase training" />
                <meta name="description" content="Our Big Data Hadoop Certification Training helps you master HDFS, Yarn, MapReduce, Pig, Hive, HBase with use cases on Retail, Social Media, Aviation, Finance, Tourism domain" />
                <meta name="description" content="PrwaTech provides big data Hadoop Training Classes for beginners and developers with versatile carrier options. Know more about courses visit our website.">
                <meta name="author" content="prwatech">
                <meta name="google-site-verification" content="fSzBVSEUMu0l5lnD0qBVGv5F_16zaI6xiUZFMm-iMqQ" />  
                <meta name="viewport" content="width=device-width, initial-scale=1.0">

		<!-- Favicon -->
                <link rel="shortcut icon" href="images/fevicon.JPG">

		<!-- Web Fonts -->
		<link href='http://fonts.googleapis.com/css?family=Roboto:400,300,300italic,400italic,500,500italic,700,700italic' rel='stylesheet' type='text/css'>
		<link href='http://fonts.googleapis.com/css?family=Raleway:700,400,300' rel='stylesheet' type='text/css'>
		<link href='http://fonts.googleapis.com/css?family=Pacifico' rel='stylesheet' type='text/css'>
		<link href='http://fonts.googleapis.com/css?family=PT+Serif' rel='stylesheet' type='text/css'>

		<!-- Bootstrap core CSS -->
		<link href="bootstrap/css/bootstrap.css" rel="stylesheet">

		<!-- Font Awesome CSS -->
		<link href="fonts/font-awesome/css/font-awesome.css" rel="stylesheet">

		<!-- Fontello CSS -->
		<link href="fonts/fontello/css/fontello.css" rel="stylesheet">

		<!-- Plugins -->
		<link href="plugins/magnific-popup/magnific-popup.css" rel="stylesheet">
		<link href="plugins/rs-plugin/css/settings.css" rel="stylesheet">
		<link href="css/animations.css" rel="stylesheet">
		<link href="plugins/owl-carousel/owl.carousel.css" rel="stylesheet">
		<link href="plugins/owl-carousel/owl.transitions.css" rel="stylesheet">
		<link href="plugins/hover/hover-min.css" rel="stylesheet">		
		<!-- the project core CSS file -->
		<link href="css/style.css" rel="stylesheet" >
                <!-- Style Switcher Styles (Remove these two lines) -->
		<!-- Custom css --> 
		<link href="css/custom.css" rel="stylesheet"> 
                <script type="text/javascript" src="plugins/jquery.min.js"></script>
        </head>

	<!-- body classes:  -->
	<!-- "boxed": boxed layout mode e.g. <body class="boxed"> -->
	<!-- "pattern-1 ... pattern-9": background patterns for boxed layout mode e.g. <body class="boxed pattern-1"> -->
	<!-- "transparent-header": makes the header transparent and pulls the banner to top -->
	<body class="no-trans front-page transparent-header ">
            <!-- scrollToTop -->
		<!-- ================ -->
		<div class="scrollToTop circle"><i class="icon-up-open-big"></i></div>
		
		<!-- page wrapper start -->
		<!-- ================ -->
		<div class="page-wrapper">
		
			<!-- header-container start -->
			<div class="header-container">
                
    <!-- ================ -->
    <div class="header-top" id="bg-blue">
        <div class="bg-fullscreen">
            <div class="container">
                <div class="row">
                    <div class="col-xs-3 col-sm-6 col-md-8">
                        <!-- header-top-first start -->
                        <!-- ================ -->
                        <div class="header-top-first clearfix">
                            <ul class="list-inline hidden-sm hidden-xs contact-detail">
                                <li><i class="fa fa-phone pr-5 pl-10"></i> +91 8147111254 </li>
                                <li><i class="fa fa-envelope-o pr-5 pl-10"></i> info@prwatech.in</li>
                                <li><i class="fa fa-desktop pr-5 pl-10"></i><a href="blog/index.php">Blog</a></li>
                            </ul>
                        </div>
                        <!-- header-top-first end -->
                    </div>
                    <div class="col-xs-9 col-sm-6 col-md-4">
                
                        <!-- header-top-second start -->
                        <!-- ================ -->
                        <div id="header-top-second"  class="clearfix">
                
                            <!-- header top dropdowns start -->
                            <!-- ================ -->
            <?php
            if(isset($_SESSION['user_id'])){
            if($_SESSION['user_type']==='normal'){
            ?>
                            <div class="header-top-dropdown text-right">
                                <div class="btn-group ">
                                    <p style="position:relative;top:8px;right:5px;font-family:verdana;"><?php echo ""." "."<b>".$_SESSION['user_name']."</b>"; ?><p>
                                </div>  
                                <div class="btn-group">
                                    <a href="profile-page.php" class="btn  btn-login btn-sm"><b>Profile</b></a>
                                </div>  
                                <div class="btn-group">
                                    <a href="logout.php" class="btn  btn-login btn-sm"><b>Sign Out</b></a>
                                </div>  
                            </div>
                
            <?php
                
            }else{
            header("Location:iiadmin/home.php");
            }
            }else{
            ?>
                            <div class="header-top-dropdown text-right">
                                <div class="btn-group">
                                    <a  class="btn btn-signup btn-sm"  data-toggle="modal" data-target="#mySignUp"><i class="fa fa-user pr-10"></i> Sign Up</a>
                                </div>
                                <div class="btn-group dropdown">
                                    <button type="button" class="btn  btn-login btn-sm" data-toggle="modal" data-target="#mySignIn"><i class="fa fa-lock pr-10"></i> Login</button>
                
                                </div>
                            </div>
                
            <?php
            }
            ?>
                            <!--  header top dropdowns end -->
                        </div>
                        <!-- header-top-second end -->
                    </div>
                </div>
            </div>
        </div>
                
    </div>
                
    <!-- ================ --> 
    <header class="header  fixed   clearfix" style="box-shadow: 0 4px 10px rgba(0,0,0, 0.50);">
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <!-- header-left start -->
                    <!-- ================ -->
                    <div class="header-left clearfix">
                
                        <!-- logo -->
                        <div id="logo" class="logo">
                            <a href="index.php"><img id="logo_img" src="images/logo_light_blue.png" alt="Prwatech"></a>
                        </div>
                        <div class="site-slogan" style="font-family:century gothic;">
                            &nbsp;&nbsp;Share Ideas, Start Something Good.
                        </div>
                
                    </div>
                    <!-- header-left end -->
                
                </div>
                <div class="col-md-9">
                
                    <!-- header-right start -->
                    <!-- ================ -->
                    <div class="header-right clearfix">
                
                        <!-- main-navigation start -->
                        <!-- classes: -->
                        <!-- "onclick": Makes the dropdowns open on click, this the default bootstrap behavior e.g. class="main-navigation onclick" -->
                        <!-- "animated": Enables animations on dropdowns opening e.g. class="main-navigation animated" -->
                        <!-- "with-dropdown-buttons": Mandatory class that adds extra space, to the main navigation, for the search and cart dropdowns -->
                        <!-- ================ -->
                        <div class="main-navigation  animated with-dropdown-buttons">
                
                            <!-- navbar start -->
                            <!-- ================ -->
                            <nav class="navbar navbar-default" role="navigation">
                                <div class="container-fluid">
                
                                    <!-- Toggle get grouped for better mobile display -->
                                    <div class="navbar-header">
                                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse-1">
                                            <span class="sr-only">Toggle navigation</span>
                                            <span class="icon-bar"></span>
                                            <span class="icon-bar"></span>
                                            <span class="icon-bar"></span>
                                        </button>
                
                                    </div>
                
                                    <!-- Collect the nav links, forms, and other content for toggling -->
                                    <div class="collapse navbar-collapse" id="navbar-collapse-1">
                                        <!-- main-menu -->
                                        <ul class="nav navbar-nav " style="font-family:century gothic">
                                            <li>
                                                <a  href="index.php">Home</a>
                                            </li>
                                            <li>
                                                <a  href="about/">About Us</a>
                                            </li>
                
                                            <!-- mega-menu start -->
                
                                            <!-- mega-menu end -->
                                            <li class="dropdown ">
                                                <a class="dropdown-toggle" data-toggle="dropdown" href="#">Courses</a>
                                                <ul class="dropdown-menu">
                                                    <li class="dropdown ">
                                                        <a  class="dropdown-toggle" data-toggle="dropdown" href="big-data-hadoop-training/index.php">Big Data Hadoop Training </a>
                                                        <ul class="dropdown-menu">
                                                            <li ><a href="big-data-hadoop-training/">Bigdata Hadoop Training </a></li>
                                                            <li ><a href="big-data-hadoop-training-in-pune/">Bigdata Hadoop Training in Pune</a></li>
                                                            <li ><a href="big-data-hadoop-training/">Bigdata Hadoop Online Training </a></li>
                                                            <li ><a href="big-data-hadoop-training/">Bigdata Hadoop Online self Training</a></li>
                                                        </ul>
                                                    </li>
                                                    <li class="dropdown ">
                                                        <a  class="dropdown-toggle" data-toggle="dropdown" href="hadoop-admin/index.php">Hadoop Admin</a>
                                                        <ul class="dropdown-menu">
                                                            <li ><a href="hadoop-admin/">Class Room Training</a></li>
                                                            <li ><a href="hadoop-admin-online-course/">Hadoop Admin Online Course</a></li>
                                                            <li ><a href="hadoop-admin/">Hadoop Admin Online Self Training</a></li>
                
                                                        </ul>
                                                    </li>
                                                    <li ><a href="apache-spark-scala-training-2/">Apache Spark Scala Training</a></li>
                                                    <li ><a href="data-science-course-training-bangalore/">Data Science Course</a></li>
                                                    <li ><a href="r-programing/">R Programming</a></li>
                                                    <li ><a href="tableau/">Tableau</a></li>
                                                    <li ><a href="python-online-course/">Python</a></li>
                                                    <li ><a href="sastraining/">SAS Training</a></li>
                                                </ul>
                                            </li>
                                            <!-- mega-menu start -->													
                
                                            <!-- mega-menu end -->
                                            <li class="dropdown ">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Services</a>
                                                <ul class="dropdown-menu">
                                                    <li><a  href="services/corporate-training">Corporate Training</a></li>
                                                    <li><a  href="services/online-softwares">Hadoop Softwares</a></li>
                                                    <li><a  href="services/workshop-on-bigdata-hadoop">Workshop on Bigdata & Hadoop</a></li>
                                                    <li ><a href="#">LMS</a></li>
                                                    <li ><a href="services/placement-assistance">Placement Assistance</a></li>
                                                </ul>
                                            </li>
                                            <li ><a href="careers/">Career</a></li>
                
                                            <li ><a href="gallery/">Gallery</a></li>
                                            <li ><a href="contact/">Contact</a></li>
                
                                        </ul>
                                        <!-- main-menu end -->
                
                                        <!-- header dropdown buttons -->
                                        <div class="header-dropdown-buttons hidden-xs ">
                                            <div class="btn-group dropdown">
                                                <button type="button" class="btn dropdown-toggle" data-toggle="dropdown"><i class="icon-search"></i></button>
                                                <ul class="dropdown-menu dropdown-menu-right dropdown-animation" id="search-form">
                                                    <li>
                                                        <form role="search" class="search-box margin-clear">
                                                            <div class="form-group has-feedback">
                                                                <input type="text" class="form-control" placeholder="Search" style="background:rgba(0,0,0,0.7);color:white">
                                                                <i class="icon-search form-control-feedback"></i>
                                                            </div>
                                                        </form>
                                                    </li>
                                                </ul>
                                            </div>
                
                                        </div>
                                        <!-- header dropdown buttons end-->
                
                                    </div>
                
                                </div>
                            </nav>
                            <!-- navbar end -->
                
                        </div>
                        <!-- main-navigation end -->	
                    </div>
                    <!-- header-right end -->
                
                </div>
            </div>
        </div>
                
    </header>
    <!-- header end -->
</div>
			<!-- header-container end -->
		
			<!-- banner start -->
			<!-- ================ -->
			<div class="banner dark-translucent-bg" style="background-image:url('images/page-course-banner-1.jpg'); background-position: 50% 27%;">
				<!-- breadcrumb start -->
				<!-- ================ -->
				<div class="breadcrumb-container object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
					<div class="container">
						<ol class="breadcrumb">
							<li><i class="fa fa-home pr-10"></i><a class="link-dark" href="index.html">Home</a></li>
							
							<li class="active">Blog</li>
						</ol>
					</div>
				</div>
				<!-- breadcrumb end -->
				
			</div>
			<!-- banner end -->
			
			<div id="page-start"></div>

			<!-- section start -->
			<!-- ================ -->
                        <section class="pv-30 clearfix object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100" style="background:#f5f5f5;" >
				<div class="container">
                                     <div class="row">
                                         <div class="col-md-9" >
                                             <div class="timeline clearfix" id="blog_details_client">
								
							</div>
                                         </div>
                                     <div class="col-md-3" style="position:relative;top:80px;">
                                     <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center" >
                                         <h3 class="heading-font text-center" style="background:#4c4c4c;color:white;padding-top:10px;padding-bottom:10px">Categories</h3> 
                                         <div class="body">
                                            <table class="table table-striped ">
                                                 <tbody id="category_tbody">
                                                     <tr><th><a href="">Big Data</a></th></tr>
                                                     <tr><th><a href="">Big data and Hadoop Dump Questions</a></th></tr>
                                                     <tr><th><a href="">Big Data Hadoop Training</a></th></tr>
                                                 <tr><th><a href="">Data Science</a></th></tr>
                                                 
                                                 </tbody>
                                             </table>
                                          
                                             
                                         </div>
                                     </div>
                                      <h3 class="heading-font text-center" style="background:#4c4c4c;color:white;padding-top:10px;padding-bottom:10px">Suggested Courses</h3> 
                                      <div class="owl-carousel content-slider">
                                          <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
                                              <div class="overlay-container">
                                                  <img src="images/por-1.png" alt="">
                                                  <div class="overlay-to-top">
                                                      <p class="small margin-clear">&nbsp;</p>
                                                  </div>
                                              </div>
                                              <div class="body">
                                                  <h3 class="heading-font">Big Data R-Programming Training</h3>
                                                  <div class="separator"></div>
                                                  <p class="heading-font">Advanced Program for Top-notch Developers </p>
                                                  <a href="big-data-hadoop-training.php"  target="_blank" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read More<i class="fa fa-arrow-right pl-10"></i></a>												
                                              </div>
                                          </div>
                                          <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
                                              <div class="overlay-container">
                                                  <img src="images/por-2.png" alt="">
                                                  <div class="overlay-to-top">
                                                      <p class="small margin-clear">&nbsp;</p>
                                                  </div>
                                              </div>
                                              <div class="body">
                                                  <h3 class="heading-font">R-Programming Admin Training</h3>
                                                  <div class="separator"></div>
                                                  <p class="heading-font">Advanced Program for Top-notch Developers </p>
                                                  <a href="hadoop-admin.php" target="_blank" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read More<i class="fa fa-arrow-right pl-10"></i></a>												
                                              </div>
                                          </div>
                                          <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
                                              <div class="overlay-container">
                                                  <img src="images/por-3.png" alt="">
                                                  <div class="overlay-to-top">
                                                      <p class="small margin-clear">&nbsp;</p>
                                                  </div>
                                              </div>
                                              <div class="body">
                                                  <h3 class="heading-font">Apache Spark Scala Training</h3>
                                                  <div class="separator"></div>
                                                  <p class="heading-font">Apache Spark Scala Combo Training </p>
                                                  <a href="apache-spark-scala-training.php" target="_blank" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read More<i class="fa fa-arrow-right pl-10"></i></a>												
                                              </div>
                                          </div>
                                          <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
                                              <div class="overlay-container">
                                                  <img src="images/por-4.png" alt="">
                                                  <div class="overlay-to-top">
                                                      <p class="small margin-clear">&nbsp;</p>
                                                  </div>
                                              </div>
                                              <div class="body">
                                                  <h3 class="heading-font">Data Science Training</h3>
                                                  <div class="separator"></div>
                                                  <p class="heading-font">Find out the truth about what Data Science is. </p>
                                                  <a href="data-science-course-training-bangalore.php" target="_blank" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read More<i class="fa fa-arrow-right pl-10"></i></a>												
                                              </div>
                                          </div>
                                          <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
                                              <div class="overlay-container">
                                                  <img src="images/por-5.png" alt="">
                                                  <div class="overlay-to-top">
                                                      <p class="small margin-clear">&nbsp;</p>
                                                  </div>
                                              </div>
                                              <div class="body">
                                                  <h3 class="heading-font">R Programming Training</h3>
                                                  <div class="separator"></div>
                                                  <p class="heading-font">Business Analytics With R Training</p>
                                                  <a href="r-programing.php" target="_blank" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read More<i class="fa fa-arrow-right pl-10"></i></a>												
                                              </div>
                                          </div>
                                          <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
                                              <div class="overlay-container">
                                                  <img src="images/por-6.png" alt="">
                                                  <div class="overlay-to-top">
                                                      <p class="small margin-clear">&nbsp;</p>
                                                  </div>
                                              </div>
                                              <div class="body">
                                                  <h3 class="heading-font"> Tableau Training & Certification</h3>
                                                  <div class="separator"></div>
                                                  <p class="heading-font">Speed on concepts of data visualization</p>
                                                  <a href="tableau.php" target="_blank" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read More<i class="fa fa-arrow-right pl-10"></i></a>												
                                              </div>
                                          </div>
                                          <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
                                              <div class="overlay-container">
                                                  <img src="images/por-7.png" alt="">
                                                  <div class="overlay-to-top">
                                                      <p class="small margin-clear">&nbsp;</p>
                                                  </div>
                                              </div>
                                              <div class="body">
                                                  <h3 class="heading-font">Python Programming</h3>
                                                  <div class="separator"></div>
                                                  <p class="heading-font">Advanced Concepts for Top-notch Developers  </p>
                                                  <a href="python.php" target="_blank" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read More<i class="fa fa-arrow-right pl-10"></i></a>												
                                              </div>
                                          </div>
                                      </div> 
                                 </div> 
                                     </div>
					
                                    
				</div>
                             
                           
                         
			</section>
                    
                       
                        
			<!-- section end -->
                    
			<div class="clearfix"></div>
			
			<!-- ================ -->
		
			<!-- ================ -->
						<footer id="footer" class="clearfix ">
<!--                            <div class="footer-upper-top">
                                
                            </div>-->
                            <div class="footer-upper">
                                
                            </div>
<!--                            <div id="footer-bottom" >
                                
                            </div>-->
				<!-- .footer start -->
				<!-- ================ -->
				<div class="footer" style="background:url('images/bg/3a.png');background-position:bottom center; box-shadow:inset 0 6px 10px rgba(0,0,0, 0.25);">
					<div class="container">
						<div class="footer-inner">
							<div class="row">
								<div class="col-md-3">
									<div class="footer-content">
                                                                            <div class="logo-footer"><img id="logo-footer" src="images/logo_light_blue.png" alt=""/></div>
                                                                            <p class="heading-font"style="text-align:justify">Training lays the foundation for an engineer.
                                                                                It provides a strong platform to build ones perception and implementation by mastering a wide range of skills .
                                                                                One of India’s leading and largest training provider for Big Data and Hadoop Corporate training programs is the prestigious PrwaTech. <a href="#">Learn More<i class="fa fa-long-arrow-right pl-5"></i></a></p>
										<div class="separator-2"></div>
										<nav>
										 <a href="terms-and-conditions.php">Terms, Conditions & Privacy</a>	
										</nav>
									</div>
								</div>
								<div class="col-md-3">
									<div class="footer-content">
										<h3 class="title">Latest From Blog</h3>
										<div class="separator-2"></div>
                                                                                <div id="blog_details_client_in_footer">
<!--										<div class="media margin-clear">
											<div class="media-left">
												<div class="overlay-container">
                                                                                                    <img class="media-object" src="../images/por-1.png" alt="blog-thumb">
													<a href="blog-post.html" class="overlay-link small"><i class="fa fa-link"></i></a>
												</div>
											</div>
											<div class="media-body">
												<h6 class="media-heading heading-font"><a href="blog-post.html">Learn about the Fundamentals of Hadoop Development</a></h6>
												<p class="small margin-clear"><i class="fa fa-calendar pr-10"></i>Mar 23, 2015</p>
											</div>
											<hr>
										</div>
										<div class="media margin-clear">
											<div class="media-left">
												<div class="overlay-container">
                                                                                                    <img class="media-object" src="../images/por-1.png" alt="blog-thumb">
													<a href="blog-post.html" class="overlay-link small"><i class="fa fa-link"></i></a>
												</div>
											</div>
											<div class="media-body">
												<h6 class="media-heading heading-font"><a href="blog-post.html">Learn about the Fundamentals of Hadoop Development</a></h6>
												<p class="small margin-clear"><i class="fa fa-calendar pr-10"></i>Mar 23, 2015</p>
											</div>
											<hr>
										</div>
										<div class="media margin-clear">
											<div class="media-left">
												<div class="overlay-container">
                                                                                                    <img class="media-object" src="../images/por-1.png" alt="blog-thumb">
													<a href="blog-post.html" class="overlay-link small"><i class="fa fa-link"></i></a>
												</div>
											</div>
											<div class="media-body">
												<h6 class="media-heading heading-font"><a href="blog-post.html">Learn about the Fundamentals of Hadoop Development</a></h6>
												<p class="small margin-clear"><i class="fa fa-calendar pr-10"></i>Mar 23, 2015</p>
											</div>
											<hr>
										</div>-->
										</div>
										<div class="text-right space-top">
                                                                                    <a href="blog/" class="link-dark"><i class="fa fa-plus-circle pl-5 pr-5"></i>More</a>	
										</div>
									</div>
								</div>
								<div class="col-md-3">
									<div class="footer-content">
										<h3 class="title">Find Us In Bangalore</h3>
										<div class="separator-2"></div>
                                                                                <div class="heading-font" itemscope itemtype="http://schema.org/PostalAddress"> 
<span itemprop="name">PRWATECH</span><br> 
<span itemprop="streetAddress">No.14, 29th Main , 2nd Cross, V.P road BTM-1st Stage, Behind AXA building, Land Mark : Vijaya Bank ATM</span><br> 
<span itemprop="addressLocality">Bangalore</span>, 
<span itemprop="addressRegion"> Karnataka </span> 
<span itemprop="postalCode">560 068</span><br> 
<span itemprop="addressCountry">India</span> 
</div>
									
										<div class="separator-2"></div>
											<ul class="social-links circle animated-effect-1">
											<li class="facebook"><a target="_blank" href="https://www.facebook.com/JustHadoop"><i class="fa fa-facebook"></i></a></li>
											<li class="twitter"><a target="_blank" href=" https://twitter.com/prwatech"><i class="fa fa-twitter"></i></a></li>
											<li class="googleplus"><a target="_blank" href="https://plus.google.com/+PrwatechBigDataHadoopTrainingCoursesBengaluru"><i class="fa fa-google-plus"></i></a></li>
											<li class="linkedin"><a target="_blank" href="https://www.linkedin.com/in/prwatech"><i class="fa fa-linkedin"></i></a></li>
											<li class="xing"><a target="_blank" href="https://www.youtube.com/channel/UCwAaWqnH2MqikDMpb1jBspw"><i class="fa fa-youtube"></i></a></li>
										</ul>
									</div>
								</div>
								<div class="col-md-3">
									<div class="footer-content">
										<h3 class="title">Find Us In Pune</h3>
										<div class="separator-2"></div>
                                                                               <div class="heading-font" itemscope itemtype="http://schema.org/PostalAddress"> 
<span itemprop="name">PRWATECH</span><br> 
<span itemprop="streetAddress">2nd Floor, Punyai Bldg., Yashwant Nagar, Behind Ekbote Furniture Mall kharadi Bypass Road,kharadi</span><br> 
<span itemprop="addressLocality"> Pune</span>, 
<span itemprop="addressRegion">Maharashtra</span> 
<span itemprop="postalCode">411014</span><br> 
<span itemprop="addressCountry">India</span> 
</div>
										<div class="separator-2"></div>
                                                                               
									</div>
								</div>
							</div>
						</div>
					</div>
                                  
				</div>
				<!-- .footer end -->

				<!-- .subfooter start -->
				<!-- ================ -->
				<div class="subfooter" id="bg-subfooter" >
                                    <div id="footer-bottom">
                                        
                                    </div> 
                                    <div class="bg-fullscreen">
                                        <div class="container">
						<div class="subfooter-inner">
							<div class="row">
								<div class="col-md-12">
									<p class="text-center">Copyright © 2016 Prwatech. All Rights Reserved</p>
								</div>
							</div>
						</div>
					</div>
                                    </div>
					
				</div>
				<!-- .subfooter end -->

</footer>
        <script>
            onload_blog();
            function onload_blog(){
                /* Ajax call for Product Display*/
                    $.ajax({
                    type:"post",
                    url:"server/controller/SelBlogDetails.php",
                    data:{'category_id':""},
                    success: function(data) {
                       var duce = jQuery.parseJSON(data);
                        $("#blog_details_client_in_footer").empty();
                        var count=0;
                        $.each(duce, function (index, article) {
                            
                           var img= "server/controller/"+article.pic;
                           var btn_class="btn-danger";
                           if(article.status=="Enable"){
                                
                                $("#blog_details_client_in_footer").append($("<div>").addClass("media margin-clear")
                                                .append($("<div>").addClass("media-left")
                                                    .append($("<div>").addClass("overlay-container")
                                                        .append($("<img>").addClass("img-responsive").attr({'src':img}))
                                                        .append($("<a>").addClass("overlay-link small")))
                                                 )
                                                .append($("<div>").addClass("media-body")
                                                        .append($("<h6>").addClass("media-heading heading-font")
                                                            .append($("<a>").append(article.title)).click(function (){
                                                                 window.location="blog/blogpostdetail.php?id="+article.blog_details_id;
                                                            }).css({'cursor':'pointer'}))
                                                            .append($("<p>").addClass("small margin-clear")
                                                                 .append($("<i>").addClass("fa fa-calendar pr-10"))
                                                                 .append(article.date)
                                                            )
                                                )
                                                .append($("<hr>"))
                                                );
                           }
                         count++;
                         if(count==3){
                             return false;
                         }
                        });
                    }
                });
                }
            
            
</script>



  


			<!-- footer end -->
			
		</div>
		<!-- page-wrapper end -->

		<!-- JavaScript files placed at the end of the document so the pages load faster -->
		<!-- ================================================== -->
		<!-- Jquery and Bootstap core js files -->
		<script type="text/javascript" src="plugins/jquery.min.js"></script>
                <script type="text/javascript" src="bootstrap/js/bootstrap.min.js"></script>

		<!-- Modernizr javascript -->
		<script type="text/javascript" src="plugins/modernizr.js"></script>

		<!-- jQuery Revolution Slider  -->
		<script type="text/javascript" src="plugins/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
		<script type="text/javascript" src="plugins/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
                 <!-- Isotope javascript -->
		<script type="text/javascript" src="plugins/isotope/isotope.pkgd.min.js"></script>
		
		<!-- Magnific Popup javascript -->
<!--		<script type="text/javascript" src="plugins/magnific-popup/jquery.magnific-popup.min.js"></script>-->
		
		<!-- Appear javascript -->
		<script type="text/javascript" src="plugins/waypoints/jquery.waypoints.min.js"></script>

		<!-- Count To javascript -->
		<script type="text/javascript" src="plugins/jquery.countTo.js"></script>
		
		<!-- Parallax javascript -->
		<script src="plugins/jquery.parallax-1.1.3.js"></script>

		<!-- Contact form -->
		<script src="plugins/jquery.validate.js"></script>
                    <!-- Background Video -->
		<script src="plugins/vide/jquery.vide.js"></script>

		<!-- Owl carousel javascript -->
		<script type="text/javascript" src="plugins/owl-carousel/owl.carousel.js"></script>
		<!-- SmoothScroll javascript -->
		<script type="text/javascript" src="plugins/jquery.browser.js"></script>
		<script type="text/javascript" src="plugins/SmoothScroll.js"></script>
                <!-- Initialization of Plugins -->
		<script type="text/javascript" src="js/template.js"></script>

		<!-- Custom Scripts -->
		<script type="text/javascript" src="js/custom.js"></script>
		<!-- Color Switcher (Remove these lines) -->
		<script type="text/javascript" src="style-switcher/style-switcher.js"></script>

			<!-- footer end -->
			
		</div>
		<!-- page-wrapper end -->


                <div class="modal fade" id="demoVideoPopUp" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header ">
                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                <h4 class="modal-title" id="myModalLabel">Demo Session at Prwatech</h4>
                            </div>
                            <div class="modal-body">
                               <iframe width="570" height="350" src="https://www.youtube.com/embed/xA5TAalZt40" frameborder="0" allowfullscreen></iframe> 
                              
                            </div>
                            <div class="modal-footer">
                                
                            </div>
                        </div>
                    </div>
                </div>
		<!-- Color Switcher End -->
                   <?php include './includes/quick-query_1.php';?>
                    <?php include './includes/UserSignup_1.php';?>
                
	</body>

        <script>
            blog={}
            temp_date={}
            $(document).ready(function(){
                var cta_id="";
                    $.ajax({
                    type:"post",
                    url:"server/controller/SelBlogCategoryDetails.php",
                      success: function(data) {
                        $("#category_tbody").empty();
                        var duce = jQuery.parseJSON(data);
                        $.each(duce, function (index, article) {
                        if(cta_id==""){
                            cta_id=article.blog_category_details_id;
                        }
                            $("#category_tbody").append($('<tr/>')
                                    .append($("<th>").append($("<a>").append(article.category+" ("+article.counter+")").css({'cursor':'pointer'})).click(function (){
                                        onlad(article.blog_category_details_id)
                                    }))
                            );   
                        })
                        
                    }
                })
                     onlad('<?php echo $request;?>');
            
            });
            function urlParam(name){
    var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
    if (results==null){
       return null;
    }
    else{
       return results[1] || 0;
    }
}
            function onlad(link){
                /* Ajax call for Product Display*/
                    $.ajax({
                    type:"post",
                    url:"server/controller/SelBlogDetailsClient.php",
                    data:{'link':link},
                    success: function(data) {
                        blog={}
                        temp_date={}
                        var count=0;
//                        alert(data)
                       var duce = jQuery.parseJSON(data);
                        $("#blog_details_client").empty();
                        $.each(duce, function (index, article) {
                            blog[article.blog_details_id]={};
                            blog[article.blog_details_id]["user_id"]=article.user_id;
                            blog[article.blog_details_id]["title"]=article.title;
                            blog[article.blog_details_id]["short_description"]=article.short_description;
                            blog[article.blog_details_id]["long_description"]=article.long_description;
                            blog[article.blog_details_id]["date"]=article.date;
                            blog[article.blog_details_id]["category_id"]=article.category_id;
                            blog[article.blog_details_id]["status"]=article.status;
                            blog[article.blog_details_id]["pic"]=article.pic;
                            blog[article.blog_details_id]["link"]=article.link;
                            
                           var img= "server/controller/"+article.pic;
                           var btn_class="btn-danger";
                           if(article.status=="Enable"){
                               var cl=""
                                if(count%2==1){
                                    cl=" pull-right"
                                }
                                if(temp_date[article.date]==null){
                                    temp_date[article.date]={}
                                    $("#blog_details_client").append($("<div>").addClass("timeline-date-label clearfix").append(article.date));
                                    cl="";
                                }
                                
                                $("#blog_details_client").append($("<div>").addClass("timeline-item"+cl)
                                                .append($("<article>").addClass("blogpost shadow light-gray-bg bordered")
                                                    .append($("<div>").addClass("overlay-container")
                                                        .append($("<img>").addClass("img-responsive").attr({'src':img}))
                                                        .append($("<a>").addClass("overlay-link")))
                                                    .append($("<header>")
                                                        .append($("<h2>").addClass("img-responsive")
                                                            .append($("<a>").append(article.title)))
                                                        .append($("<div>").addClass("post-info")
                                                            .append($("<span>").addClass("post-date")
                                                                 .append($("<i>").addClass("icon-calendar"))
                                                                 .append($("<span>").addClass("month").append(article.date))
                                                             )
                                                            .append($("<span>").addClass("comments")
                                                                 .append($("<i>").addClass("icon-user-1"))
                                                                 .append($("<a>").append("Admin"))
                                                             )
                                                            .append($("<span>").addClass("comments")
                                                                 .append($("<i>").addClass("icon-chat"))
                                                                 .append($("<a>").append("comment"))
                                                             )
                                                        ))
                                                    .append($("<div>").addClass("blogpost-content").append(article.short_description))
                                                    .append($("<footer>").addClass("clearfix")
                                                        .append($("<div>").addClass("link pull-right")
                                                            .append($("<i>").addClass("icon-link"))
                                                            .append($("<a>").append("Read More").css({'cursor':'pointer'}).click(function (){
                                                                 window.location="blogpostdetail.php?id="+article.blog_details_id;
                                                            }))
                                                        ))
                                                ));
                            count++;        
                           }
                          
                        });
                    }
                });
                }
                function updatestatus(obj,id){
                      var value;
                      if(obj.text().trim()=="Enable"){
                                value="Disable";
                                obj.empty();
                                obj.append("Disable");
                                obj.removeClass("btn-success");
                                obj.addClass("btn-danger");
                      }else{
                                value="Enable";
                                obj.empty();
                                obj.append("Enable");
                                obj.removeClass("btn-danger");
                                obj.addClass("btn-success");
                      }           
                      
                  $.ajax({
                    type:"post",
                    url:"server/controller/UpdBlogStatus.php",
                    data:{'blog_details_id':id,'status':value},
                    success: function(data) {
                        
                    }
                  });
              }
            
</script>

</html>



