<?php include '../includes/session.php'; ?>
<!DOCTYPE html>
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
	<!--<![endif]-->
<head>
		<meta charset="utf-8">
                <title>PrwaTech | SAS Training   </title>
                <meta name="title" content="Big Data Hadoop Training Institute In Pune | Hadoop Certification Course | Prwatech" />
                <meta name="keywords" content="hadoop training, online hadoop training, hadoop training classes, hadoop course online, big data training, big data course, big data online course, hadoop tutorial, HDFS training, Yarn training, MapReduce training, Pig training, Hive training, HBase training" />
                <meta name="description" content="Bigdata and hadoop training, best classes in Pune areas centers Hinjewadi, Magarpatta, MG Road, Kharadi, pimple saudagar for beginners and developers." />
                <meta name="description" content="Bigdata and hadoop training, best classes in Pune areas centers Hinjewadi, Magarpatta, MG Road, Kharadi, pimple saudagar for beginners and developers.">
                <meta name="author" content="prwatech">
                <meta name="google-site-verification" content="fSzBVSEUMu0l5lnD0qBVGv5F_16zaI6xiUZFMm-iMqQ" />    
                <?php include '../includes/csslinks.php'; ?>
        </head>

	<!-- body classes:  -->
	<!-- "boxed": boxed layout mode e.g. <body class="boxed"> -->
	<!-- "pattern-1 ... pattern-9": background patterns for boxed layout mode e.g. <body class="boxed pattern-1"> -->
	<!-- "transparent-header": makes the header transparent and pulls the banner to top -->
	<body class="no-trans front-page transparent-header ">
            <!-- scrollToTop -->
		<!-- ================ -->
		<div class="scrollToTop circle"><i class="icon-up-open-big"></i></div>
		
		<!-- page wrapper start -->
		<!-- ================ -->
		<div class="page-wrapper">
		
			<!-- header-container start -->
			<?php include '../includes/header.php' ;  ?>
			<!-- header-container end -->
		
			<!-- banner start -->
			<!-- ================ -->
			<div class="banner dark-translucent-bg" style="background-image:url('../images/page-about-banner-4.png'); background-position: 50% 27%;">
				<!-- breadcrumb start -->
				<!-- ================ -->
				<div class="breadcrumb-container object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
					<div class="container">
						<ol class="breadcrumb">
							<li><i class="fa fa-home pr-10"></i><a class="link-dark" href="index.html">Home</a></li>
							<li class="active">All Courses</li>
							<li class="active">SAS Training</li>
						</ol>
					</div>
				</div>
				<!-- breadcrumb end -->
				
			</div>
			<!-- banner end -->
			
			<div id="page-start"></div>

			<!-- section start -->
			<!-- ================ -->
			 <section class="light-gray-bg pv-30 clearfix"  style="background:#f8f8f8 url('../images/bg/bigbg2.PNG') no-repeat;background-position:right center;box-shadow:inset 0 2px 7px rgba(0,0,0, 0.25);">
				<div class="container">
					<div class="row">
                                            <div class="col-md-8">
                                                <h2 class="text-center heading-font" style="text-transform:none;"><strong> SAS </strong> Training </h2>
                                                <div class="separator"></div>
                                                <h5 class="heading-font">Big data analytics and business intelligence</h5>
                                                <p class="cpara2">One among many skills that is overwhelming the technical industry is Big Data Analytics also knows as business intelligence. Big data analytics / business intelligence courses play a crucial role in establishing the foundations for every enthusiastic leaner who aspires to climb up the career ladder as a Data Analyst or a Business Intelligence expert.</p>
                                                
                                            </div>
						<div class="col-md-4">
                                                    <p>&nbsp;</p>
                                                    <div class="overlay-container overlay-visible" style="box-shadow:0 6px 10px rgba(0,0,0, 0.25);">
                                                        <img src="../images/bg/sas.png" />
                                                        <a href="../images/bg/sas.png" class="popup-img overlay-link" ><i class="icon-plus-1"></i></a>
                                                    </div>
                                                  
                                                    
                                               </div>
						
					</div>
                                    <p>&nbsp;</p>
                                   
                        <div class="separator"></div>
                        <h2 class="text-center heading-font object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100" style="text-transform:none;">Contact Us <strong>+91 8147111254 </strong></h2>
                        <div class="separator"></div>
                        <p>&nbsp;</p>
                                 </div>
                        </section>
                       
                        <section id="coursed-row2" class="object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
                            <div class="container">
                                
                                 <h2 class="text-center heading-font object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100" style="text-transform:none;"> <strong>Course</strong> Details</h2>
                                 <div id="vseparator" class="object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
                                     <div class="vhr"></div>
                                </div>
                                <div class="row col-md-9" >
                               <!-- tabs start -->
                                <!-- ================ -->
                                <!-- Nav tabs -->
                                <ul class="nav nav-tabs style-3" id="tab-course" role="tablist">
                                    <li class="active"><a href="#h3tab1" role="tab" data-toggle="tab">Overview</a></li>
                                    <li><a href="#h3tab2" role="tab" data-toggle="tab">Course Curriculum</a></li>
                                    <li><a href="#h3tab3" role="tab" data-toggle="tab">FAQ's</a></li>
                                    <li><a href="#h3tab4" role="tab" data-toggle="tab">Download Course Outline</a></li>
                                    <li><a href="#h3tab5" role="tab" data-toggle="tab">Reviews</a></li>
                                   
                                </ul>
                                <!-- Tab panes -->
                                <div class="tab-content">
                                    <div class="tab-pane in active " id="h3tab1">
                                        <div class="row">
                                            <div class="col-md-12 object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
                                              
                                                <h5 class="heading-font">About SAS Programming</h5>
                                                <p id="cpara">SAS is a leader in data analytics. Through innovative analytical approaches it caters to business intelligence and data management. SAS is used for business intelligence, Predictive Analysis, data management, Prescriptive and Descriptive Analysis etc.,</p>
                                                
                                                <h5 class="heading-font">What special in SAS:</h5>
                                                <p id="cpara">SAS takes an extensive programming approach to data transformation and analysis. It gives much finer control over data manipulation. It has large number of components customized for specific.</p>
    
                                                <h5 class="heading-font">Main Objective/benefits of SAS programming:</h5>
                                                 <ul class="list-icons" id="clist">
                                                    <li><i class="icon-check pr-10"></i> Data extraction and its Management</li>
                                                    <li><i class="icon-check pr-10"></i> Data transformation, updating and modification</li>
                                                    <li><i class="icon-check pr-10"></i> Statistical Analysis </li>
                                                    <li><i class="icon-check pr-10"></i> Formation of Report with accurate graphics</li>
                                                    <li><i class="icon-check pr-10"></i> Business Planning with quality Improvement</li>
                                                    <li><i class="icon-check pr-10"></i> Operations Research </li>
                                                    <li><i class="icon-check pr-10"></i> Project Management</li>
                                                    <li><i class="icon-check pr-10"></i> Application and industry specific tool development.</li>
                                                </ul>
                                                <h5 class="heading-font">Who should opt for AngularJS course?</h5>
                                                <ul class="list-icons" id="clist">
                                                    <li><i class="icon-check pr-10"></i> Graduates whose aim is to build a career in web development</li>
                                                    <li><i class="icon-check pr-10"></i> Junior and senior web developers</li>
                                                    <li><i class="icon-check pr-10"></i> Technical architects</li>
                                                    <li><i class="icon-check pr-10"></i> Quality professionals</li>
                                                    <li><i class="icon-check pr-10"></i> Testing professionals</li>
                                                    <li><i class="icon-check pr-10"></i> SAS developers.</li>
                                                    
                                                </ul>
                                                    
                                                <h5 class="heading-font">Some important factors why you should choose PrwaTech are as follows:</h5>
                                                <ul class="list-icons" id="clist">
                                                    <li><i class="icon-check pr-10"></i> Finest qualified professionals </li>
                                                    <li><i class="icon-check pr-10"></i> More than 5 years of industry experience.</li>
                                                    <li><i class="icon-check pr-10"></i> 100% practical training</li>
                                                    <li><i class="icon-check pr-10"></i> Flexible timings</li>
                                                    <li><i class="icon-check pr-10"></i> Back-up classes available</li>
                                                    <li><i class="icon-check pr-10"></i> Provide Case studies</li>
                                                    <li><i class="icon-check pr-10"></i> Complete course guidance and support you till you reach your goal.</li>
                                                        
                                                </ul>
                                                <h5 class="heading-font">Placement Cell :</h5>
                                                <ul class="list-icons" id="clist">
                                                    <li><i class="icon-check pr-10"></i> 100% job assistance.</li>
                                                    <li><i class="icon-check pr-10"></i> Team will help you to update your resume</li>
                                                    <li><i class="icon-check pr-10"></i> Mock tests</li>
                                                    <li><i class="icon-check pr-10"></i> Mock Interviews.</li>
                                                    <li><i class="icon-check pr-10"></i> Important questions with answers which you will face in Interviews</li>
                                                        
                                                </ul>
                                                
                                                <div class="separator"></div>
                                                <h5 class="heading-font">Batch Format:</h5>
                                                 <ul class="list-icons" id="clist">
                                                    <li><i class="icon-check pr-10"></i>Regular Batch- Morning, Day Time and Evening</li>
                                                    <li><i class="icon-check pr-10"></i>Weekend Batch- Saturday and Sunday Classes</li>
                                                    <li><i class="icon-check pr-10"></i> Fast Track Batch</li>
                                                    <li><i class="icon-check pr-10"></i> Corporate training</li>
                                                    <li><i class="icon-check pr-10"></i> Online SAS classes</li>
                                                        
                                                </ul>        
                                                <P id="cpara"><b>A single batch will not have more than 8 students.</b> </P>
                                                <h5 class="heading-font">Our SAS training centers:</h5>
                                                 <ul class="list-icons" id="clist">
                                                    <li><i class="icon-check pr-10"></i>Bangalore</li>
                                                 </ul> 
                                            </div>
                                                
                                        </div>
                                            
                                    </div>
                                    <div class="tab-pane" id="h3tab2">
                                        <div class="row">
                                            <div class="col-md-12 ">
                                                  <div class="panel-group collapse-style-2" id="accordion-2">
								<div class="panel panel-default">
									<div class="panel-heading">
										<h4 class="panel-title">
											<a data-toggle="collapse" data-parent="#accordion-2" href="#collapseOne-2">
												<i class="fa fa-arrow-right pr-10"></i>Module 1
											</a>
										</h4>
									</div>
                                                                    <div id="collapseOne-2" class="panel-collapse collapse in">
                                                                        <div class="panel-body">
                                                                            <h5 class="heading-font">SAS programming:</h5>
                                                                            <ul class="list-icons" id="clist">
                                                                                <li><i class="icon-check pr-10"></i>Arrays</li>
                                                                                <li><i class="icon-check pr-10"></i>Data sets</li>
                                                                                <li><i class="icon-check pr-10"></i>Looping</li>
                                                                                <li><i class="icon-check pr-10"></i>Decision making</li>
                                                                                <li><i class="icon-check pr-10"></i>Functions and input methods</li>
                                                                                <li><i class="icon-check pr-10"></i>Macros, Dates & Times.</li>
                                                                            </ul> 
                                                                        </div>
                                                                    </div>
								</div>
								<div class="panel panel-default">
									<div class="panel-heading">
										<h4 class="panel-title">
											<a data-toggle="collapse" data-parent="#accordion-2" href="#collapseTwo-2" class="collapsed">
												<i class="fa fa-arrow-right pr-10"></i>Module 2:
											</a>
										</h4>
									</div>
									<div id="collapseTwo-2" class="panel-collapse collapse">
										<div class="panel-body">
                                                                                    <h5 class="heading-font">Data Set Operations: </h5>
                                                                                    <ul class="list-icons" id="clist">
                                                                                        <li><i class="icon-check pr-10"></i>Read and write data.</li>
                                                                                        <li><i class="icon-check pr-10"></i>Concatenate datasets</li>
                                                                                        <li><i class="icon-check pr-10"></i>Merging, subsetting, formatting and sorting data sets</li>
                                                                                        
                                                                                    </ul>
										</div>
									</div>
								</div>
                                                      <div class="panel panel-default">
                                                          <div class="panel-heading">
                                                              <h4 class="panel-title">
                                                                  <a data-toggle="collapse" data-parent="#accordion-2" href="#collapseThree-2" class="collapsed">
                                                                      <i class="fa fa-arrow-right pr-10"></i>Module 3:
                                                                  </a>
                                                              </h4>
                                                          </div>
                                                          <div id="collapseThree-2" class="panel-collapse collapse">
                                                              <div class="panel-body">
                                                                <h5 class="heading-font">SAS – SQL:</h5>
                                                                                    <ul class="list-icons" id="clist">
                                                                                        <li><i class="icon-check pr-10"></i>Create operation</li>
                                                                                        <li><i class="icon-check pr-10"></i>Read</li>
                                                                                        <li><i class="icon-check pr-10"></i>Update and delete, etc.,</li>
                                                                                        
                                                                                    </ul>
                                                                  
                                                              </div>
                                                          </div>
                                                      </div>
                                                      <div class="panel panel-default">
                                                          <div class="panel-heading">
                                                              <h4 class="panel-title">
                                                                  <a data-toggle="collapse" data-parent="#accordion-2" href="#collapseFour-2" class="collapsed">
                                                                      <i class="fa fa-arrow-right pr-10"></i>Module 4:
                                                                  </a>
                                                              </h4>
                                                          </div>
                                                          <div id="collapseFour-2" class="panel-collapse collapse">
                                                              <div class="panel-body">
                                                                    <h5 class="heading-font">Report generation: </h5>
                                                                                    <ul class="list-icons" id="clist">
                                                                                        <li><i class="icon-check pr-10"></i>HTML file</li>
                                                                                        <li><i class="icon-check pr-10"></i>PDF report</li>
                                                                                       
                                                                                        
                                                                                    </ul>
                                                              </div>
                                                          </div>
                                                      </div>
                                                      <div class="panel panel-default">
                                                          <div class="panel-heading">
                                                              <h4 class="panel-title">
                                                                  <a data-toggle="collapse" data-parent="#accordion-2" href="#collapseFive-2" class="collapsed">
                                                                      <i class="fa fa-arrow-right pr-10"></i>Module 5:
                                                                  </a>
                                                              </h4>
                                                          </div>
                                                          <div id="collapseFive-2" class="panel-collapse collapse">
                                                              <div class="panel-body">
                                                                <h5 class="heading-font">Data Representation:  </h5>
                                                                                    <ul class="list-icons" id="clist">
                                                                                        <li><i class="icon-check pr-10"></i>Histogram</li>
                                                                                        <li><i class="icon-check pr-10"></i>Bar and pie – charts</li>
                                                                                        <li><i class="icon-check pr-10"></i>Scatter and box plots.</li>
                                                                                        
                                                                                    </ul>
                                                              </div>
                                                          </div>
                                                      </div>
                                                      <div class="panel panel-default">
                                                          <div class="panel-heading">
                                                              <h4 class="panel-title">
                                                                  <a data-toggle="collapse" data-parent="#accordion-2" href="#collapseSix-2" class="collapsed">
                                                                      <i class="fa fa-arrow-right pr-10"></i>Module 6:
                                                                  </a>
                                                              </h4>
                                                          </div>
                                                          <div id="collapseSix-2" class="panel-collapse collapse">
                                                              <div class="panel-body">
                                                                <h5 class="heading-font">Statistical analysis: </h5>
                                                                                    <ul class="list-icons" id="clist">
                                                                                        <li><i class="icon-check pr-10"></i>3 and 6 sigma analysis</li>
                                                                                        <li><i class="icon-check pr-10"></i>Mean, SD’s, FD’s</li>
                                                                                        <li><i class="icon-check pr-10"></i>Tabulations, correlation</li>
                                                                                        <li><i class="icon-check pr-10"></i>Regression</li>
                                                                                        <li><i class="icon-check pr-10"></i>Bland – Altman analysis</li>
                                                                                        <li><i class="icon-check pr-10"></i>Fisher’s exact, Ch- - square</li>
                                                                                        <li><i class="icon-check pr-10"></i>Measure analysis, hypothesis testing and Anova, etc.,</li>
                                                                                        
                                                                                    </ul> 
                                                              </div>
                                                          </div>
                                                      </div>
                                                       <div class="panel panel-default">
                                                          <div class="panel-heading">
                                                              <h4 class="panel-title">
                                                                  <a data-toggle="collapse" data-parent="#accordion-2" href="#collapseSeven-2" class="collapsed">
                                                                      <i class="fa fa-arrow-right pr-10"></i>Module 7:
                                                                  </a>
                                                              </h4>
                                                          </div>
                                                          <div id="collapseSeven-2" class="panel-collapse collapse">
                                                              <div class="panel-body">
                                                                <h5 class="heading-font">Real time Project discussion.</h5>
                                                                 
                                                              </div>
                                                          </div>
                                                      </div>
                                    
                                                       
							</div>
                                                
                                                </div>

                                        </div>
                                       
                                    </div>
                                          <div class="tab-pane" id="h3tab3">
                                        <div class="row">
                                            <div class="col-md-12 ">
                                                   	<div class="panel-group collapse-style-3" id="accordion-3">
								<div class="panel panel-default">
									<div class="panel-heading">
										<h4 class="panel-title">
											<a data-toggle="collapse" data-parent="#accordion-3" href="#collapseOne-3">
												1.	Who are the training instructors?
											</a>
										</h4>
									</div>
									<div id="collapseOne-3" class="panel-collapse collapse in">
										<div class="panel-body">
                                                                                    <p id="cpara" class="text-justify">
                                                                                        India’s leading provider for SAS training in Bangalore is the prestigious PrwaTech. We have       the best industries professionals who are working at leading MNCs and have more than 5-8 years of relevant experience in SAS. Along with the professional experience they are trained by PrwaTech so that the candidates can start with a great learning experience.
                                                                                    </p>
										</div>
									</div>
								</div>
								<div class="panel panel-default">
									<div class="panel-heading">
										<h4 class="panel-title">
											<a data-toggle="collapse" data-parent="#accordion-3" href="#collapseTwo-3" class="collapsed">
												2.	Various modes of training that PrwaTech offers?
											</a>
										</h4>
									</div>
									<div id="collapseTwo-3" class="panel-collapse collapse">
										<div class="panel-body">
                                                                                    <p id="cpara" class="text-justify">
                                                                                        PrwaTech basically offers two types of trainings: online instructor-led training and self-paced training. Apart from that we provide classroom trainings and corporate training for enterprises. We have the best industry professionals and all our trainers come with over 5 years of industry experience. You can confirm the quality of our professionals in sample videos provided.
                                                                                    </p>
                                                                                    
										</div>
									</div>
								</div>
								<div class="panel panel-default">
									<div class="panel-heading">
										<h4 class="panel-title">
											<a data-toggle="collapse" data-parent="#accordion-3" href="#collapseThree-3" class="collapsed">
                                                                                            3.	As a part of SAS training what are the types of projects that I complete during this training?
											</a>
										</h4>
									</div>
									<div id="collapseThree-3" class="panel-collapse collapse">
										<div class="panel-body">
                                                                                    <p id="cpara" class="text-justify">
                                                                                          As a part of this training you will work on real world based projects. You can use your skills and knowledge on various SAS related projects. Our professionals will assist you throughout your training and completion of the projects. The projects could be in banking, finance, social networking, insurance etc.  
                                                                                    </p>
                                                                                    
										</div>
									</div>
								</div>
                                                            <div class="panel panel-default">
									<div class="panel-heading">
										<h4 class="panel-title">
											<a data-toggle="collapse" data-parent="#accordion-3" href="#collapseFour-3" class="collapsed">
												4.	Can I request for SAS online training?
											</a>
										</h4>
									</div>
									<div id="collapseFour-3" class="panel-collapse collapse">
										<div class="panel-body">
                                                                                    <p id="cpara" class="text-justify">
                                                                                        Yes you can apply for SAS online training. PrwaTech is India’s leading online training provider. It provides a strong platform to the engineers by mastering a wide range of skills. Our highly skilled online trainer’s works hard to understand the problem and provide the most appropriate solution in any circumstances.
                                                                                    </p>
                                                                                    
										</div>
									</div>
								</div>
                                                            <div class="panel panel-default">
									<div class="panel-heading">
										<h4 class="panel-title">
											<a data-toggle="collapse" data-parent="#accordion-3" href="#collapseFive-3" class="collapsed">
												5.	What is the procedure of Placement? Do PrwaTech provide placement assistance?
											</a>
										</h4>
									</div>
									<div id="collapseFive-3" class="panel-collapse collapse">
										<div class="panel-body">
                                                                                    <p id="cpara" class="text-justify">
                                                                                       PrwaTech will assist you throughout your training. We have 95% of placement record in reputed companies and have organized 2000+ interviews in HCL, Infosys, IBM, Accenture etc. We have trained 2000+ students. Our professionals will provide you with several questions along with solutions that you might face in your interview.
                                                                                    </p>
                                                                                    
										</div>
									</div>
								</div>
                                                            <div class="panel panel-default">
									<div class="panel-heading">
										<h4 class="panel-title">
											<a data-toggle="collapse" data-parent="#accordion-3" href="#collapseSix-3" class="collapsed">
												6.	Can I request for support session if I have missed some of my classes or have some difficulty with the course? 
											</a>
										</h4>
									</div>
									<div id="collapseSix-3" class="panel-collapse collapse">
										<div class="panel-body">
                                                                                    <p id="cpara" class="text-justify">
                                                                                        Do not worry in case you lose your lectures. If for any reason you lose your classes: You have two options then: 1. you can view our recorded sessions that are available in your LMS. 2. You can directly go to direct faculty and ask extra time or you can attend any other live batch of  your missed section.
                                                                                    </p>
                                                                                    
										</div>
									</div>
								</div>
							</div>
                                                
                                                </div>

                                        </div>
                                       
                                    </div>
                                    <div class="tab-pane" id="h3tab4">
                                        <div class="row object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
                                            <div class="col-md-4">
                                                <p id="cpara" class="text-default"><a href="">Click Here to Download</a></p>
                                                
                                            </div>
                                            <div class="col-md-8">
                                                 <h4 class="title text-default" class="heading-font">Bigdata and Hadoop Services</h4>
                                                <ul class="list-icons">
                                                    <li><i class="icon-check pr-10"></i> PowerPoint Presentation covering all classes</li>
                                                    <li><i class="icon-check pr-10"></i> Recorded Videos Sessions On Bigdata and Hadoop with LMS Access.(lifetime support)</li>
                                                    <li><i class="icon-check pr-10"></i> Quiz , Assignment & POC.</li>
                                                    <li><i class="icon-check pr-10"></i> On Demand Online Support .</li>
                                                    <li><i class="icon-check pr-10"></i> Discussion Forum.</li>
                                                    <li><i class="icon-check pr-10"></i> Material
                                                        <ul class="list-icons" style="margin-left:50px;">
                                                            <li>a. Sample Question papers of Cloudera Certification.</li>
                                                            <li>b. Technical Notes & Study Material.</li>
                                                        </ul>
                                                    
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        
                                    </div>
                                    <div class="tab-pane" id="h3tab5">
                                        <div class="row object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
                                            <div class="col-md-6 ">
                                                <div class="pv-20 ph-20 feature-box-2 boxed shadow object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
                                                    <div class="col-md-3 testimonial-image"><img src="../images/testimonial-2.jpg" /></div> 
                                                    <div class="body">
                                                        <h4 class="title">Anupam Khamparia 
                                                            <br>
                                                            <span style="font-size:11px;">Consultant, Cognizant Technology Solutions, Bangalore</span>
                                                        </h4>
                                                        <p class ="text-justify">“Excellent course and instructor. I learnt a lot in a short period. Good value for money. Instructor took us through Advanced Hadoop Development in depth.”</p>
                                                       
                                                    </div>
                                                </div>
                                            </div>
                                          <div class="col-md-6 ">
                                                <div class="pv-20 ph-20 feature-box-2 boxed shadow object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
                                                    <div class="col-md-3 testimonial-image"><img src="../images/testimonial-1.png" /></div> 
                                                    <div class="body">
                                                        <h4 class="title">Anukanksha Garg
                                                          <br>
                                                            <span style="font-size:11px;">B.Tech. CSE</span>
                                                        </h4>
                                                        <p class ="text-justify">It was a nice learning experience with prwatech. The classes were well scheduled and managed.
Verma has good understanding of the topics taught and catered to our problems and doubts very patiently. The best thing about him was that he handled the situations accordingly, when needed a friend, he became one and also a teacher to always guide us.</p>
                                                       
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6 ">
                                                <div class="pv-20 ph-20 feature-box-2 boxed shadow object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
                                                    <div class="col-md-3 testimonial-image"><img src="../images/testimonial-4.PNG" /></div> 
                                                    <div class="body">
                                                        <h4 class="title">Varun Shivashanmugum
                                                        
                                                        <br>
                                                            <span style="font-size:11px;">Associate Consultant, ITC Infotech Ltd</span>
                                                        </h4>
                                                        
                                                        <p class ="text-justify">“Faculty is good,Verma takes keen interest and personnel care in improvising skills in students. And most importantly,Verma will be available and clears doubts at any time apart from class hours. And he always keeps boosting and trying to increase confidence in his students which adds extra attribute to him and organization as well. and organization as well.</p>
                                                       
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6 ">
                                                <div class="pv-20 ph-20 feature-box-2 boxed shadow object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
                                                    <div class="col-md-3 testimonial-image"><img src="../images/testimonial-5.jpg" /></div> 
                                                    <div class="body">
                                                        <h4 class="title">Mayank Srivastava <br>
                                                        <span style="font-size:11px;">Hadoop Developer, L&T, Bangalore</span>
                                                        </h4>
                                                        <p class ="text-justify">“Really good course content and labs, patient enthusiastic instructor. Good instructor, with in depth skills…Very relevant practicals allowed me to put theory into practice.”</p>
                                                       
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                                 <div class="col-md-3" style="position:relative;top:-10px;">
                                     <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center" >
                                         <div class="overlay-container">
                                             <img src="../images/icon/curse-d1.png" alt="">
                                         </div>
                                         <div class="body">
                                           <h3 class="heading-font">Rs. 14,000 + Tax </h3>
                                <span class="small-font">per 2 weeks</span>
                                <div class="separator"></div>
                                <p class="heading-font"></p>
                                <table class="table table-striped ">
                                    <tbody>
                                        <tr><th><center>35 Hours</center></th></tr>
                                    <tr><th><center>15 Seats</center></th></tr>
                                    <tr><th><center>Course Badge</center></th></tr>
                                    <tr><th><center>Course Certificate</center></th></tr>
                                    <tr><th><center> <a  onclick="openNav()" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Enroll Now<i class="fa fa-arrow-right pl-10"></i></a></center></th></tr>
                                    </tbody>
                                </table>
                                            <?php include '../includes/demo-popup.php';?>
                                
                                         </div>
                                     </div>
                                      <h3 class="heading-font text-center" style="background:#4c4c4c;color:white;padding-top:10px;padding-bottom:10px">Suggested Courses</h3> 
                                      <div class="owl-carousel content-slider">
                                          <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
                                              <div class="overlay-container">
                                                  <img src="../images/por-1.png" alt="">
                                                  <div class="overlay-to-top">
                                                      <p class="small margin-clear">&nbsp;</p>
                                                  </div>
                                              </div>
                                              <div class="body">
                                                  <h3 class="heading-font">Big Data Hadoop Training</h3>
                                                  <div class="separator"></div>
                                                  <p class="heading-font">Advanced Program for Top-notch Developers </p>
                                                  <a href="big-data-hadoop-training.php"  target="_blank" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read More<i class="fa fa-arrow-right pl-10"></i></a>												
                                              </div>
                                          </div>
                                          <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
                                              <div class="overlay-container">
                                                  <img src="../images/por-2.png" alt="">
                                                  <div class="overlay-to-top">
                                                      <p class="small margin-clear">&nbsp;</p>
                                                  </div>
                                              </div>
                                              <div class="body">
                                                  <h3 class="heading-font">Hadoop Admin Training</h3>
                                                  <div class="separator"></div>
                                                  <p class="heading-font">Advanced Program for Top-notch Developers </p>
                                                  <a href="hadoop-admin.php" target="_blank" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read More<i class="fa fa-arrow-right pl-10"></i></a>												
                                              </div>
                                          </div>
                                          <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
                                              <div class="overlay-container">
                                                  <img src="../images/por-3.png" alt="">
                                                  <div class="overlay-to-top">
                                                      <p class="small margin-clear">&nbsp;</p>
                                                  </div>
                                              </div>
                                              <div class="body">
                                                  <h3 class="heading-font">Apache Spark Scala Training</h3>
                                                  <div class="separator"></div>
                                                  <p class="heading-font">Apache Spark Scala Combo Training </p>
                                                  <a href="apache-spark-scala-training.php" target="_blank" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read More<i class="fa fa-arrow-right pl-10"></i></a>												
                                              </div>
                                          </div>
                                          <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
                                              <div class="overlay-container">
                                                  <img src="../images/por-4.png" alt="">
                                                  <div class="overlay-to-top">
                                                      <p class="small margin-clear">&nbsp;</p>
                                                  </div>
                                              </div>
                                              <div class="body">
                                                  <h3 class="heading-font">Data Science Training</h3>
                                                  <div class="separator"></div>
                                                  <p class="heading-font">Find out the truth about what Data Science is. </p>
                                                  <a href="data-science-course-training-bangalore.php" target="_blank" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read More<i class="fa fa-arrow-right pl-10"></i></a>												
                                              </div>
                                          </div>
                                          <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
                                              <div class="overlay-container">
                                                  <img src="../images/por-5.png" alt="">
                                                  <div class="overlay-to-top">
                                                      <p class="small margin-clear">&nbsp;</p>
                                                  </div>
                                              </div>
                                              <div class="body">
                                                  <h3 class="heading-font">R Programming Training</h3>
                                                  <div class="separator"></div>
                                                  <p class="heading-font">Business Analytics With R Training</p>
                                                  <a href="r-programing.php" target="_blank" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read More<i class="fa fa-arrow-right pl-10"></i></a>												
                                              </div>
                                          </div>
                                          <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
                                              <div class="overlay-container">
                                                  <img src="../images/por-6.png" alt="">
                                                  <div class="overlay-to-top">
                                                      <p class="small margin-clear">&nbsp;</p>
                                                  </div>
                                              </div>
                                              <div class="body">
                                                  <h3 class="heading-font"> Tableau Training & Certification</h3>
                                                  <div class="separator"></div>
                                                  <p class="heading-font">Speed on concepts of data visualization</p>
                                                  <a href="tableau.php" target="_blank" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read More<i class="fa fa-arrow-right pl-10"></i></a>												
                                              </div>
                                          </div>
                                          <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
                                              <div class="overlay-container">
                                                  <img src="../images/por-7.png" alt="">
                                                  <div class="overlay-to-top">
                                                      <p class="small margin-clear">&nbsp;</p>
                                                  </div>
                                              </div>
                                              <div class="body">
                                                  <h3 class="heading-font">Python Programming</h3>
                                                  <div class="separator"></div>
                                                  <p class="heading-font">Advanced Concepts for Top-notch Developers  </p>
                                                  <a href="python.php" target="_blank" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read More<i class="fa fa-arrow-right pl-10"></i></a>												
                                              </div>
                                          </div>
                                      </div> 
                                 </div> 
                            </div> 
                            <p>&nbsp;</p>
                        </section>
                        
			<!-- section end -->
                    
			<div class="clearfix"></div>
			
			<!-- ================ -->
			
			<!-- section end -->
                        <section class="pv-30 dark-bg padding-bottom-clear clearfix object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
					<br>
					<div class="container">
						<div class="row">
							<div class="col-md-3 ">
								<div class="ph-20 feature-box text-center object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
									<span class="icon default-bg circle"><i class="fa fa-institution"></i></span>
									<h3>Live classes</h3>
									<div class="separator clearfix"></div>
                                                                        <p class="heading-font ">Live online and interactive classes conducted by instructor</p>
									
								</div>
							</div>
							<div class="col-md-3 ">
								<div class="ph-20 feature-box text-center object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
									<span class="icon default-bg circle"><i class="fa fa-user"></i></span>
									<h3>Expert instructions</h3>
									<div class="separator clearfix"></div>
									<p class="heading-font">Learn from our Experts and get Real-Time Guidance</p>
									
								</div>
							</div>
							<div class="col-md-3 ">
								<div class="ph-20 feature-box text-center object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
									<span class="icon default-bg circle"><i class="icon-signal"></i></span>
									<h3>24 X 7 Support</h3>
									<div class="separator clearfix"></div>
									<p class="heading-font">Personalized Guidance from our 24X7 Support Team</p>
									
								</div>
							</div>
							<div class="col-md-3 ">
								<div class="ph-20 feature-box text-center object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
									<span class="icon default-bg circle"><i class="icon-calendar"></i></span>
									<h3>Flexible schedule</h3>
									<div class="separator clearfix"></div>
									<p class="heading-font">Reschedule your Batch/Class at Your Convenience</p>
									
								</div>
							</div>
						</div>
					</div>
					<br>
				</section>
			
			<!-- section end -->

			<!-- section -->
			<!-- ================ -->
                        <section class="pv-30   padding-bottom-clear dark-translucent-bg parallax padding-bottom-clear" style="background: url('../images/bg/451.png') 50% -9px; box-shadow: rgba(0, 0, 0, 0.247059) 0px 2px 7px inset;">
				
				<div class="space-bottom">
					
					<div class="owl-carousel content-slider">
						<div class="container">
							<div class="row">
								<div class="col-md-8 col-md-offset-2">
									<div class="testimonial text-center">
										<div class="testimonial-image">
                                                                                    <img src="../images/testimonial-1.png" alt="" title="" class="img-circle">
										</div>
										
										<div class="testimonial-body">
                                                                                    <p>&nbsp;</p>
												<p id="cpara">It was a nice learning experience with prwatech. The classes were well scheduled and managed.
Verma has good understanding of the topics taught and catered to our problems and doubts very patiently. The best thing about him was that he handled the situations accordingly, when needed a friend, he became one and also a teacher to always guide us.</p>
											
											<div class="testimonial-info-1">Anukanksha Garg</div>
											<div class="testimonial-info-2">B.TECH- CS</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="container">
							<div class="row">
								<div class="col-md-8 col-md-offset-2">
									<div class="testimonial text-center">
										<div class="testimonial-image">
											<img src="../images/testimonial-2.jpg" alt="" title="" class="img-circle">
										</div>
										
										<div class="testimonial-body">
											 <p>&nbsp;</p>
												<p id="cpara">Excellent course and instructor. I learnt a lot in a short period. Good value for money. Instructor took us through Advanced Hadoop Development in depth.</p>
											
											<div class="testimonial-info-1">Anupam Khamparia</div>
											<div class="testimonial-info-2">Consultant, Cognizant Technology Solutions, Bangalore</div>
										</div>
									</div>
								</div>
							</div>
						</div>
                                            <div class="container">
							<div class="row">
								<div class="col-md-8 col-md-offset-2">
									<div class="testimonial text-center">
										<div class="testimonial-image">
											<img src="../images/testimonial-3.jpg" alt="" title="" class="img-circle">
										</div>
										
										<div class="testimonial-body">
											<p>&nbsp;</p>
												<p id="cpara">Excellent course and instructor. I learnt a lot in a short period. Good value for money. Instructor took us through Advanced Hadoop Development in depth.</p>
											<div class="testimonial-info-1">Kumar Waibhav</div>
                                                                                        <div class="testimonial-info-2">Software Engineer, Cognizant Technology Solutions,<br> Philadelphia, Pennsylvania, USA</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					
				</div>
			</section>
			<!-- section end -->

			<!-- section start -->
			<!-- ================ -->
                        
			                   
			<!-- section end -->
			<!-- footer start (Add "dark" class to #footer in order to enable dark footer) -->
			<!-- ================ -->
			<?php include './../includes/footer.php'; ?>
			<!-- footer end -->
			
		</div>
		<!-- page-wrapper end -->

		<?php include './../includes/jslinks.php';?>
                <div class="modal fade" id="demoVideoPopUp" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header ">
                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                <h4 class="modal-title" id="myModalLabel">Demo Session at Prwatech</h4>
                            </div>
                            <div class="modal-body">
                               <iframe width="570" height="350" src="https://www.youtube.com/embed/xA5TAalZt40" frameborder="0" allowfullscreen></iframe> 
                              
                            </div>
                            <div class="modal-footer">
                                
                            </div>
                        </div>
                    </div>
                </div>
		<!-- Color Switcher End -->
              <?php include '../includes/quick-query.php';?> 
              <?php include '../includes/UserSignup.php';?>
                  
	</body>
        <script>
            $(document).ready(function(){
                onlad(9);
            });  
        </script>
        <script type="text/javascript" src="../plugins/magnific-popup/jquery.magnific-popup.min.js"></script>

</html>
