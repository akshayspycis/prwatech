<?php
    class CourseBatchDetails {

        private $course_batch_details_id;
        private $course_id;
        private $date;
        private $day_type;
        private $day;
        private $time;
        private $fees;
        private $status;
        private $discout;
        
        function getCourse_batch_details_id() {
            return $this->course_batch_details_id;
        }

        function getCourse_id() {
            return $this->course_id;
        }

        function getDate() {
            return $this->date;
        }

        function getDay_type() {
            return $this->day_type;
        }

        function getDay() {
            return $this->day;
        }

        function getTime() {
            return $this->time;
        }

        function getFees() {
            return $this->fees;
        }

        function getStatus() {
            return $this->status;
        }

        function getDiscout() {
            return $this->discout;
        }

        function setCourse_batch_details_id($course_batch_details_id) {
            $this->course_batch_details_id = $course_batch_details_id;
        }

        function setCourse_id($course_id) {
            $this->course_id = $course_id;
        }

        function setDate($date) {
            $this->date = $date;
        }

        function setDay_type($day_type) {
            $this->day_type = $day_type;
        }

        function setDay($day) {
            $this->day = $day;
        }

        function setTime($time) {
            $this->time = $time;
        }

        function setFees($fees) {
            $this->fees = $fees;
        }

        function setStatus($status) {
            $this->status = $status;
        }

        function setDiscout($discout) {
            $this->discout = $discout;
        }


    }


