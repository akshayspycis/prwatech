<?php
    class ForumCategoryDetails{
        
        private $forum_category_details_id;
        private $forum_category;
        private $category_url;
        
        
        public function getForum_category_details_id() {
            return $this->forum_category_details_id;
        }

        public function getForum_category() {
            return $this->forum_category;
        }

        public function getCategory_url() {
            return $this->category_url;
        }

        public function setForum_category_details_id($forum_category_details_id) {
            $this->forum_category_details_id = $forum_category_details_id;
        }

        public function setForum_category($forum_category) {
            $this->forum_category = $forum_category;
        }

        public function setCategory_url($category_url) {
            $this->category_url = $category_url;
        }

        }



