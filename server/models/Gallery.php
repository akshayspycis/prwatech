<?php
    class Gallery{
        
        private $gallery_id;
        private $ipath;
        private $icat;
        private $idate;
        private $itime;
        
        function getGallery_id() {
            return $this->gallery_id;
        }

        function getIpath() {
            return $this->ipath;
        }

        function getIcat() {
            return $this->icat;
        }

        function getIdate() {
            return $this->idate;
        }

        function getItime() {
            return $this->itime;
        }

        function setGallery_id($gallery_id) {
            $this->gallery_id = $gallery_id;
        }

        function setIpath($ipath) {
            $this->ipath = $ipath;
        }

        function setIcat($icat) {
            $this->icat = $icat;
        }

        function setIdate($idate) {
            $this->idate = $idate;
        }

        function setItime($itime) {
            $this->itime = $itime;
        }


    }


