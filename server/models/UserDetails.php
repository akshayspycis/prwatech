<?php
    class UserDetails {

        private $user_id;
        private $user_name;
        private $dob;
        private $email;
        private $contact_no;
        private $date;
        
        function getUser_id() {
            return $this->user_id;
        }

        function getUser_name() {
            return $this->user_name;
        }

        function getDob() {
            return $this->dob;
        }

        function getEmail() {
            return $this->email;
        }

        function getContact_no() {
            return $this->contact_no;
        }

        function getDate() {
            return $this->date;
        }

        function setUser_id($user_id) {
            $this->user_id = $user_id;
        }

        function setUser_name($user_name) {
            $this->user_name = $user_name;
        }

        function setDob($dob) {
            $this->dob = $dob;
        }

        function setEmail($email) {
            $this->email = $email;
        }

        function setContact_no($contact_no) {
            $this->contact_no = $contact_no;
        }

        function setDate($date) {
            $this->date = $date;
        }


    }


