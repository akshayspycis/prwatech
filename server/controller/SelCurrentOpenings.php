<?php
     include_once '../models/CurrentOpenings.php';
    include_once '../managers/CurrentOpeningsMgr.php';
    $obj = new CurrentOpeningsMgr();
    $current_openings = $obj->selCurrentOpenings();
    $str = array();    
    while($row = $current_openings->fetch()){
            $arr = array(
            'current_openings_id' => $row['current_openings_id'], 
            'title' => $row['title'], 
            'date' => $row['date'], 
            'discription' => $row['discription'], 
            'status' => $row['status'], 
       );
        array_push($str, $arr); 
    }
    echo json_encode($str);
?>