<?php
    include_once '../models/ForumPostDetails.php'; 
    include_once '../managers/ForumPostDetailsMgr.php'; 
    $date = new DateTime("now", new DateTimeZone("Asia/Kolkata"));
    $forum_post_details = new ForumPostDetails();
    $forum_post_details->setForum_category_details_id($_POST["forum_category_details_id"]);
    $forum_post_details->setForum_topic_details_id($_POST["forum_topic_details_id"]);
    $forum_post_details->setPost($_POST["post"]);
    $forum_post_details->setUser_id($_POST["user_id"]);
    $forum_post_details->setUser_name($_POST["user_name"]);
    $forum_post_details->setStatus("Enable");
    $forum_post_details->setDate($date->format('D, d M Y'));   
    $forum_post_detailsMgr = new ForumPostDetailsMgr();
    if ($forum_post_detailsMgr->insForumPostDetails($forum_post_details)) {
        echo 'Query inserted Successfully.';
    } else {
        echo 'Error';
    }
?>