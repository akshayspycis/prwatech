<?php
    include_once '../managers/BlogDetailsMgr.php';
    
    $blog_details_id = $_POST["blog_details_id"];
    $blog_details_mgr = new BlogDetailsMgr();
    if ($blog_details_mgr->delBlogDetails($blog_details_id)) {
        echo 'Blog Deleted Successfully.';
    } else {
        echo 'Error';
    }    
?>