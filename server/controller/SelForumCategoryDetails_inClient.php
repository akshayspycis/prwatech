<?php
     include_once '../models/ForumCategoryDetails.php';
    include_once '../managers/ForumCategoryDetailsMgr.php';
    $obj = new ForumCategoryDetailsMgr();
    $forum_forum_category_details = $obj->selForumCategoryDetailsInClient();
    $str = array();    
    while($row = $forum_forum_category_details->fetch()){
            $arr = array(
            'forum_category_details_id' => $row['forum_category_details_id'], 
            'forum_category' => $row['forum_category'], 
            'topic' => $row['topic'], 
            'post' => $row['post'], 
            'category_url' => $row['category_url'], 
            'latest' => $row['latest'], 
            //'counter' => $row['counter'], 
       );
        array_push($str, $arr); 
    }
    echo json_encode($str);
?>