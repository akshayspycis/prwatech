<?php
     include_once '../models/ForumPostDetails.php';
    include_once '../managers/ForumPostDetailsMgr.php';
    $obj = new ForumPostDetailsMgr();
    $forum_post_details = $obj->selForumPostDetails($_POST["forum_topic_details_id"]);
    $str = array();    
    while($row = $forum_post_details->fetch()){
            $arr = array(
            'forum_post_details_id' => $row['forum_post_details_id'], 
            'date' => $row['date'], 
            'user_id' => $row['user_id'],
            'user_name' => $row['user_name'],
            'post' => $row['post'],
            'status' => $row['status'],
       );
        array_push($str, $arr); 
    }
    echo json_encode($str);
?>