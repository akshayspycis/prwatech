<?php
    include_once '../models/BlogDetails.php'; 
    include_once '../managers/BlogDetailsMgr.php'; 
    $course_batch_details = new BlogDetails();
    $course_batch_details->setUser_id($_POST["user_id"]);
    $course_batch_details->setTitle($_POST["title"]);  
    $course_batch_details->setShort_description($_POST["short_description"]);   
    $course_batch_details->setLong_description($_POST["long_description"]);   
    $course_batch_details->setDate(date("d-m-Y", strtotime($_POST["date"])));   
    $course_batch_details->setCategory_id($_POST["category_id"]);   
    $course_batch_details->setStatus($_POST["status"]);   
    $course_batch_details->setFb($_POST["fb"]);   
    $course_batch_details->setTw($_POST["tw"]);   
    $link = $string = str_replace(' ', '-', urldecode ( strtolower(trim($_POST["title"]))));
    $course_batch_details->setLink($link);
    $upload_dir = "upload/";
    $img = $_POST["nimage"];
    $img = str_replace('data:image/jpeg;base64,', '', $img);
    $img = str_replace(' ', '+', $img);
    $data = base64_decode($img);
    $file = $upload_dir.time().".png";
    $success = file_put_contents($file, $data);
    $course_batch_details->setPic($file);   
    $course_batch_detailsMgr = new BlogDetailsMgr();
    if ($course_batch_detailsMgr->insBlogDetails($course_batch_details)) {
        echo 'Query inserted Successfully.';
    } else {
        echo 'Error';
    }
?>