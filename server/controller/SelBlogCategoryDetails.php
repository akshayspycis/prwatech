<?php
     include_once '../models/BlogCategoryDetails.php';
    include_once '../managers/BlogCategoryDetailsMgr.php';
    $obj = new BlogCategoryDetailsMgr();
    $course_batch_details = $obj->selBlogCategoryDetails();
    $str = array();    
    while($row = $course_batch_details->fetch()){
            $arr = array(
            'blog_category_details_id' => $row['blog_category_details_id'], 
            'category' => $row['category'], 
            'counter' => $row['counter'], 
       );
        array_push($str, $arr); 
    }
    echo json_encode($str);
?>