<?php
    include_once '../managers/UserDetailsMgr.php';
    $obj = new UserDetailsMgr();
    $user_details = $obj->selUserDetails($_POST['user_id']);
    $str = array();    
    while($row = $user_details->fetch()) {
        $arr = array(
            'user_id' => $row['user_id'], 
            'user_name' => $row['user_name'],             
            'dob' => $row['dob'],             
            'email' => $row['email'],             
            'contact_no' => $row['contact_no'],             
            'date' => $row['date'],             
            'pic' => $row['pic'],             
            'user_profile_details_id' => $row['user_profile_details_id'],             
            'user_type' => $row['user_type'],             
            'count' => $row['count'],             
        );
        array_push($str, $arr); 
        
    }
    echo json_encode($str);
?>
