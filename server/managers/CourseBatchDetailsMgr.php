

<?php
    include_once '../dbhelper/DatabaseHelper.php';
    
    class CourseBatchDetailsMgr{    

        //method to insert course_batch_details in database
        public function insCourseBatchDetails(CourseBatchDetails $course_batch_details) {
            $dbh = new DatabaseHelper();
            $sql = "INSERT INTO course_batch_details( "
                    . "course_id, "
                    . "date, "
                    . "day_type, "
                    . "day, "
                    . "time, "
                    . "fees,"
                    . "status,"
                    . "discount) "
                    . "VALUES ('".$course_batch_details->getCourse_id()."',"
                    . "'".$course_batch_details->getDate()."',"
                    . "'".$course_batch_details->getDay_type()."',"
                    . "'".$course_batch_details->getDay()."',"
                    . "'".$course_batch_details->getTime()."',"
                    . "'".$course_batch_details->getFees()."',"
                    . "'".$course_batch_details->getStatus()."',"
                    . "'".$course_batch_details->getDiscout()."')";
            $stmt = $dbh->createConnection()->prepare($sql); 
            $i = $stmt->execute();            
            $dbh->closeConnection();
            if ($i > 0) {                
                return TRUE;
            } else {
                return FALSE;
            }
        }

        //method to delete news in database
        public function delCourseBatchDetails($course_batch_details_id) {
            $dbh = new DatabaseHelper();
             $sql = "delete from course_batch_details where course_batch_details_id = '".$course_batch_details_id."'";
            $stmt = $dbh->createConnection()->prepare($sql);            
            $i = $stmt->execute();            
            $dbh->closeConnection();
            
            if ($i > 0) {
                return TRUE;
            } else {
                return FALSE;
            }
        }
        
        //method to select CourseBatchDetails from database
        public function selCourseBatchDetails($course_id) {
            $dbh = new DatabaseHelper();
            $sql="";
            if($course_id!=""){
                $sql = "select * from course_batch_details where course_id=".$course_id;
            }else{
                $sql = "select * from course_batch_details ";
            }
            $stmt = $dbh->createConnection()->prepare($sql);            
            $stmt->execute();
            
            $dbh->closeConnection();
            return $stmt;
        }
        //        method to update enquiry in database
  public function updateCourseBatchDetails(CourseBatchDetails $course_batch_details) {
            $dbh = new DatabaseHelper();
            $sql ="UPDATE course_batch_details SET " 
                    ."course_id='".$course_batch_details->getCourse_id()."',"
                    ."date='".$course_batch_details->getDate()."',"
                    ."day_type='".$course_batch_details->getDay_type()."',"
                    ."day='".$course_batch_details->getDay()."',"
                    ."time='".$course_batch_details->getTime()."',"
                    ."fees='".$course_batch_details->getFees()."',"
                    ."status='".$course_batch_details->getStatus()."',"
                    ."discount='".$course_batch_details->getDiscout()."'"
                    ."WHERE course_batch_details_id=".$course_batch_details->getCourse_batch_details_id()."";
             $stmt = $dbh->createConnection()->prepare($sql);
            $i = $stmt->execute();
            
            $dbh->closeConnection();
             
            if ($i > 0) {                
                return TRUE;
            } else {
              
                return FALSE;
            }
        }
          public function updCourseBatchDetailstatus(CourseBatchDetails $course_batch_details) {
            $dbh = new DatabaseHelper();
            $sql ="UPDATE course_batch_details SET " 
                    ."status='".$course_batch_details->getStatus()."'"
                   ."WHERE course_batch_details_id=".$course_batch_details->getCourse_batch_details_id()."";
                    
          
            
            $stmt = $dbh->createConnection()->prepare($sql);
            $i = $stmt->execute();
            
            $dbh->closeConnection();
             
            if ($i > 0) {                
                return TRUE;
            } else {
              
                return FALSE;
            }
        } 
          
    }
?>
