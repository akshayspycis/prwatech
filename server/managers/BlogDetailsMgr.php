<?php
    require_once '../dbhelper/DatabaseHelper.php';
    
    class BlogDetailsMgr{    

        //method to insert blog_details in database
        public function insBlogDetails(BlogDetails $blog_details) {
            $dbh = new DatabaseHelper();
            $sql = "INSERT INTO blog_details( "
                    . "user_id, "
                    . "title, "
                    . "short_description, "
                    . "long_description, "
                    . "date, "
                    . "category_id,"
                    . "status,"
                    . "pic,"
                    . "fb,"
                    . "tw,"
                    . "link) "
                    . "VALUES ('".$blog_details->getUser_id()."',"
                    . "'".$blog_details->getTitle()."',"
                    . "'".$blog_details->getShort_description()."',"
                    . "'".$blog_details->getLong_description()."',"
                    . "'".$blog_details->getDate()."',"
                    . "'".$blog_details->getCategory_id()."',"
                    . "'".$blog_details->getStatus()."',"
                    . "'".$blog_details->getPic()."',"
                    . "'".$blog_details->getFb()."',"
                    . "'".$blog_details->getTw()."',"
                    . "'".$blog_details->getLink()."')";
            $stmt = $dbh->createConnection()->prepare($sql); 
            $i = $stmt->execute();            
            $dbh->closeConnection();
            if ($i > 0) {                
                return TRUE;
            } else {
                return FALSE;
            }
        }

        //method to delete news in database
        public function delBlogDetails($blog_details_id) {
            $dbh = new DatabaseHelper();
             $sql = "delete from blog_details where blog_details_id = '".$blog_details_id."'";
            $stmt = $dbh->createConnection()->prepare($sql);            
            $i = $stmt->execute();            
            $dbh->closeConnection();
            
            if ($i > 0) {
                return TRUE;
            } else {
                return FALSE;
            }
        }
        
        //method to select BlogDetails from database
        public function selBlogDetails($category_id) {
            $dbh = new DatabaseHelper();
            $sql="";
            if($category_id!=""){
                $sql = "select * from blog_details where category_id=".$category_id." order by STR_TO_DATE(date, '%d-%m-%Y')";
            }else{
                $sql = "select * from blog_details order by STR_TO_DATE(date, '%d-%m-%Y')";
            }
            $stmt = $dbh->createConnection()->prepare($sql);            
            $stmt->execute();
            
            $dbh->closeConnection();
            return $stmt;
        }
        public function selBlogDetailsLink($link) {
            $dbh = new DatabaseHelper();
            $sql="";
                $sql = "select * from blog_details where category_id=(select blog_category_details_id from blog_category_details where link='".$link."') order by STR_TO_DATE(date, '%d-%m-%Y')";
            $stmt = $dbh->createConnection()->prepare($sql);            
            $stmt->execute();
            
            $dbh->closeConnection();
            return $stmt;
        }
        public function selBlogDetailsClient($link) {
            $dbh = new DatabaseHelper();
            
            $sql = "select * from blog_details where link=".$link."'";
            $stmt = $dbh->createConnection()->prepare($sql);            
            $stmt->execute();
            
            $dbh->closeConnection();
            return $stmt;
        }
        //        method to update enquiry in database
  public function updateBlogDetails(BlogDetails $blog_details) {
            $dbh = new DatabaseHelper();
            $sql ="UPDATE blog_details SET " 
                    ."user_id='".$blog_details->getUser_id()."',"
                    ."title='".$blog_details->getTitle()."',"
                    ."short_description='".$blog_details->getShort_description()."',"
                    ."long_description='".$blog_details->getLong_description()."',"
                    ."date='".$blog_details->getDate()."',"
                    ."category_id='".$blog_details->getCategory_id()."',"
                    ."status='".$blog_details->getStatus()."',"
                    ."pic='".$blog_details->getPic()."',"
                    ."fb='".$blog_details->getFb()."',"
                    ."tw='".$blog_details->getTw()."',"
                    ."link='".$blog_details->getLink()."'"
                    ."WHERE blog_details_id=".$blog_details->getBlog_detail_id()."";
             $stmt = $dbh->createConnection()->prepare($sql);
            $i = $stmt->execute();
            $dbh->closeConnection();
            if ($i > 0) {                
                return TRUE;
            } else {
                return FALSE;
            }
        }
          public function updBlogDetailstatus(BlogDetails $blog_details) {
            $dbh = new DatabaseHelper();
            $sql ="UPDATE blog_details SET " 
                    ."status='".$blog_details->getStatus()."'"
                   ." WHERE blog_details_id=".$blog_details->getBlog_details_id()."";
            $stmt = $dbh->createConnection()->prepare($sql);
            $i = $stmt->execute();
            
            $dbh->closeConnection();
             
            if ($i > 0) {                
                return TRUE;
            } else {
              
                return FALSE;
            }
        } 
          
    }
?>
