

<?php
    include_once '../dbhelper/DatabaseHelper.php';
    
        class LoginDetailsMgr{    

        //method to insert login_details in database
        public function insLoginDetails(LoginDetails $login_details) {
            $dbh = new DatabaseHelper();
            $sql = "INSERT INTO login_details( "
                    . "user_id, "
                    . "password, "
                    . "security_ques, "
                    . "answer, "
                    . "user_type, "
                    . "status) "
                    . "VALUES ((SELECT max(user_id) from user_details),"
                    . "'".$login_details->getPassword()."',"
                    . "'".$login_details->getSecurity_ques()."',"
                    . "'".$login_details->getAnswer()."',"
                    . "'".$login_details->getUser_type()."',"
                    . "'".$login_details->getStatus()."')";
            $stmt = $dbh->createConnection()->prepare($sql); 
            $i = $stmt->execute();            
            $dbh->closeConnection();
            if ($i > 0) {                
                return TRUE;
            } else {
                return FALSE;
            }
        }

        //method to delete news in database
        public function delLoginDetails($login_details_id) {
            $dbh = new DatabaseHelper();
             $sql = "delete from login_details where login_details_id = '".$login_details_id."'";
            $stmt = $dbh->createConnection()->prepare($sql);            
            $i = $stmt->execute();            
            $dbh->closeConnection();
            
            if ($i > 0) {
                return TRUE;
            } else {
                return FALSE;
            }
        }
        
        //method to select LoginDetails from database
        public function selLoginDetails($login_details_id) {
            $dbh = new DatabaseHelper();
            $sql="";
            if($login_details_id!=""){
                $sql = "select * from login_details where course_id=".$login_details_id;
            }else{
                $sql = "select * from login_details ";
            }
            $stmt = $dbh->createConnection()->prepare($sql);            
            $stmt->execute();
            
            $dbh->closeConnection();
            return $stmt;
        }
        //        method to update enquiry in database
       public function updateLoginDetails(LoginDetails $login_details) {
            $dbh = new DatabaseHelper();
            $sql ="UPDATE login_details SET " 
                    ."user_id='".$login_details->getUser_id()."',"
                    ."password='".$login_details->getPassword()."',"
                    ."security_ques='".$login_details->getSecurity_ques()."',"
                    ."answer='".$login_details->getAnswer()."',"
                    ."user_type='".$login_details->getUser_type()."',"
                    ."status='".$login_details->getStatus()."',"
                    ."WHERE user_id =".$login_details->getUser_id()."";
             $stmt = $dbh->createConnection()->prepare($sql);
            $i = $stmt->execute();
            
            $dbh->closeConnection();
             
            if ($i > 0) {                
                return TRUE;
            } else {
              
                return FALSE;
            }
        }
          public function updLoginDetailstatus(LoginDetails $login_details) {
            $dbh = new DatabaseHelper();
            $sql ="UPDATE login_details SET " 
                    ."status='".$login_details->getStatus()."'"
                   ."WHERE user_id=".$login_details->getUser_id()."";
                    
          
            
            $stmt = $dbh->createConnection()->prepare($sql);
            $i = $stmt->execute();
            
            $dbh->closeConnection();
             
            if ($i > 0) {                
                return TRUE;
            } else {
              
                return FALSE;
            }
        } 
          public function updLoginPass(LoginDetails $login_details) {
            $dbh = new DatabaseHelper();
            $sql ="UPDATE login_details SET " 
                    ."password='".$login_details->getPassword()."'"
                   ."WHERE user_id=".$login_details->getUser_id()."";
                    
          
            
            $stmt = $dbh->createConnection()->prepare($sql);
            $i = $stmt->execute();
            
            $dbh->closeConnection();
             
            if ($i > 0) {                
                return TRUE;
            } else {
              
                return FALSE;
            }
        } 
          
    }
?>
