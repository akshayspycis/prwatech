

<?php
    include_once '../dbhelper/DatabaseHelper.php';
    
        class UserDetailsMgr{    

        //method to insert user_details in database
        public function insUserDetails(UserDetails $user_details) {
            $dbh = new DatabaseHelper();
            $sql = "INSERT INTO user_details( "
                    . "user_id, "
                    . "user_name, "
                    . "dob, "
                    . "email, "
                    . "contact_no, "
                    . "date) "
                    . "VALUES ('".$user_details->getUser_id()."',"
                    . "'".$user_details->getUser_name()."',"
                    . "'".$user_details->getDob()."',"
                    . "'".$user_details->getEmail()."',"
                    . "'".$user_details->getContact_no()."',"
                    . "'".$user_details->getDate()."')";
            $stmt = $dbh->createConnection()->prepare($sql); 
            $i = $stmt->execute();            
            $dbh->closeConnection();
            if ($i > 0) {                
                return TRUE;
            } else {
                return FALSE;
            }
        }

        //method to delete news in database
        public function delUserDetails($user_details_id) {
            $dbh = new DatabaseHelper();
             $sql = "delete from user_details where user_details_id = '".$user_details_id."'";
            $stmt = $dbh->createConnection()->prepare($sql);            
            $i = $stmt->execute();            
            $dbh->closeConnection();
            
            if ($i > 0) {
                return TRUE;
            } else {
                return FALSE;
            }
        }
        
        //method to select UserDetails from database
        public function selUserDetails($user_id) {
            $dbh = new DatabaseHelper();
            $sql="";
            if($user_id!=""){
                $sql = "select *,(select user_type from login_details l where l.user_id=ud.user_id limit 1) as user_type,"
                        . "(select pic from user_profile_details up where up.user_id=ud.user_id) as pic,"
                        . "(select user_profile_details_id from user_profile_details up where up.user_id=ud.user_id) "
                        . "as user_profile_details_id,(select count(*) from quick_query qq where qq.email=ud.email or qq.contact=ud.contact_no) as count from user_details ud  where ud.user_id=".$user_id." limit 1";
            }else{
                $sql = "select *,(select user_type from login_details l where l.user_id=ud.user_id limit 1) as user_type,"
                        . "(select pic from user_profile_details up where up.user_id=ud.user_id) as pic,"
                        . "(select user_profile_details_id from user_profile_details up where up.user_id=ud.user_id) "
                        . "as user_profile_details_id,(select count(*) from quick_query qq where qq.email=ud.email or qq.contact=ud.contact_no) as count from user_details ud ";
            }
            $stmt = $dbh->createConnection()->prepare($sql);            
            $stmt->execute();
            $dbh->closeConnection();
            return $stmt;
        }
        //        method to update enquiry in database
  public function updateUserDetails(UserDetails $user_details) {
            $dbh = new DatabaseHelper();
            $sql ="UPDATE user_details SET " 
                    ."user_name='".$user_details->getUser_name()."',"
                    ."email='".$user_details->getEmail()."',"
                    ."contact_no='".$user_details->getContact_no()."'"
                    ."WHERE user_id=".$user_details->getUser_id()."";
             $stmt = $dbh->createConnection()->prepare($sql);
            $i = $stmt->execute();
            $dbh->closeConnection();
            if ($i > 0) {                
                return TRUE;
            } else {
              
                return FALSE;
            }
        }
          public function updUserDetailstatus(UserDetails $user_details) {
            $dbh = new DatabaseHelper();
            $sql ="UPDATE user_details SET " 
                    ."status='".$user_details->getStatus()."'"
                   ."WHERE user_details_id=".$user_details->getCourse_batch_details_id()."";
                    
          
            
            $stmt = $dbh->createConnection()->prepare($sql);
            $i = $stmt->execute();
            
            $dbh->closeConnection();
             
            if ($i > 0) {                
                return TRUE;
            } else {
              
                return FALSE;
            }
        } 
          
    }
?>
