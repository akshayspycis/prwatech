

<?php
    require_once '../dbhelper/DatabaseHelper.php';
    
    class BlogCategoryDetailsMgr{    

        //method to insert blog_category_details in database
        public function insBlogCategoryDetails(BlogCategoryDetails $blog_category_details) {
            $dbh = new DatabaseHelper();
            $sql = "INSERT INTO blog_category_details( "
                    . "category, "
                    . "link) "
                    . "VALUES ('".$blog_category_details->getCategory()."',"
                    . "'".$blog_category_details->getLink()."')";
            $stmt = $dbh->createConnection()->prepare($sql); 
            $i = $stmt->execute();            
            $dbh->closeConnection();
            if ($i > 0) {                
                return TRUE;
            } else {
                return FALSE;
            }
        }

        //method to delete news in database
        public function delBlogCategoryDetails($blog_category_details_id) {
            $dbh = new DatabaseHelper();
             $sql = "delete from blog_category_details where blog_category_details_id = '".$blog_category_details_id."'";
            $stmt = $dbh->createConnection()->prepare($sql);            
            $i = $stmt->execute();            
            $dbh->closeConnection();
            
            if ($i > 0) {
                return TRUE;
            } else {
                return FALSE;
            }
        }
        
        //method to select BlogCategoryDetails from database
        public function selBlogCategoryDetails( ) {
            $dbh = new DatabaseHelper();
            $sql = "select *,(select count(*) from blog_details bd where bd.category_id=b.blog_category_details_id) as counter from blog_category_details b";
            $stmt = $dbh->createConnection()->prepare($sql);            
            $stmt->execute();
            
            $dbh->closeConnection();
            return $stmt;
        }
        //        method to update enquiry in database
  public function updateBlogCategoryDetails(BlogCategoryDetails $blog_category_details) {
            $dbh = new DatabaseHelper();
            $sql ="UPDATE blog_category_details SET " 
                    ."category='".$blog_category_details->getCategory()."',"
                    ."link='".$blog_category_details->get()."'"
                    ."WHERE blog_category_details_id=".$blog_category_details->getBlog_category_details_id()."";
             $stmt = $dbh->createConnection()->prepare($sql);
            $i = $stmt->execute();
            
            $dbh->closeConnection();
             
            if ($i > 0) {                
                return TRUE;
            } else {
              
                return FALSE;
            }
        }
          
    }
?>
