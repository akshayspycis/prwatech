<?php
    include_once '../dbhelper/DatabaseHelper.php';
    
    class CareersMgr{    

        //method to insert careers in database
        public function insCareers(Careers $careers) {
            $dbh = new DatabaseHelper();
            $sql = "INSERT INTO careers(heading, content, city, date, image) VALUES ('".$careers->getHeading()."','".$careers->getContent()."','".$careers->getCity()."','".$careers->getDate()."','".$careers->getImage()."')";
            $stmt = $dbh->createConnection()->prepare($sql);            
            $i = $stmt->execute();            
            $dbh->closeConnection();
            
            if ($i > 0) {                
                return TRUE;
            } else {
                return FALSE;
            }
        }

        //method to delete news in database
        public function delCareers($career_id) {
            $dbh = new DatabaseHelper();
            $sql = "delete from careers where career_id = '".$career_id."'";
            $stmt = $dbh->createConnection()->prepare($sql);            
            $i = $stmt->execute();            
            $dbh->closeConnection();
            
            if ($i > 0) {
                return TRUE;
            } else {
                return FALSE;
            }
        }
        
        //method to select Careers from database
        public function selCareers() {
            $dbh = new DatabaseHelper();
            $sql = "select * from careers";
            $stmt = $dbh->createConnection()->prepare($sql);            
            $stmt->execute();
            
            $dbh->closeConnection();
            return $stmt;
        }
        //        method to update enquiry in database
  public function updateCareers(Careers $careers) {
            $dbh = new DatabaseHelper();
            $sql ="UPDATE careers SET " 
                    ."career_id='".$careers->getCareer_id()."',"
                    ."heading='".$careers->getHeading()."',"
                    ."content='".$careers->getContent()."',"
                    ."city='".$careers->getCity()."',"
                    ."date='".$careers->getDate()."'"
                    ." WHERE career_id=".$careers->getCareer_id()."";
             $stmt = $dbh->createConnection()->prepare($sql);
            $i = $stmt->execute();
            echo $sql;
            $dbh->closeConnection();
             
            if ($i > 0) {                
                return TRUE;
            } else {
              
                return FALSE;
            }
        } 
          public function updCareersImg(Careers $careers) {
            $dbh = new DatabaseHelper();
            $sql = "SELECT careers.image FROM careers WHERE career_id=".$careers->getCareer_id()."";
            $stmt = $dbh->createConnection()->prepare($sql);            
            $stmt->execute();
            $file;
            while($row = $stmt->fetch()) {
                $file=$row['image'];
            }
            if(file_exists($file)&&unlink($file)){
                $sql ="UPDATE careers SET " 
                ."image='".$careers->getImage()."'"
                ."WHERE career_id=".$careers->getCareer_id()."";
                $stmt = $dbh->createConnection()->prepare($sql);
                $i = $stmt->execute();
                $dbh->closeConnection();
                    if ($i > 0) {                
                        return TRUE;
                    } else {
                        return FALSE;
                    }
            }else{
                return FALSE;
            }
        } 
    }
?>

