<?php
    require_once '../dbhelper/DatabaseHelper.php';
    
    class CurrentOpeningsMgr{    

        //method to insert current_openings in database
        public function insCurrentOpenings(CurrentOpenings $current_openings) {
            $dbh = new DatabaseHelper();
            $sql = "INSERT INTO current_openings( "
                    . "title, "
                    . "date, "
                    . "discription, "
                    . "status) "
                    . "VALUES ('".$current_openings->getTitle()."',"
                    . "'".$current_openings->getDate()."',"
                    . "'".$current_openings->getDiscription()."',"
                    . "'".$current_openings->getStatus()."')";
            $stmt = $dbh->createConnection()->prepare($sql); 
            $i = $stmt->execute();            
            $dbh->closeConnection();
            if ($i > 0) {                
                return TRUE;
            } else {
                return FALSE;
            }
        }

        //method to delete news in database
        public function delCurrentOpenings($current_openings_id) {
            $dbh = new DatabaseHelper();
             $sql = "delete from current_openings where current_openings_id = '".$current_openings_id."'";
            $stmt = $dbh->createConnection()->prepare($sql);            
            $i = $stmt->execute();            
            $dbh->closeConnection();
            
            if ($i > 0) {
                return TRUE;
            } else {
                return FALSE;
            }
        }
        
        //method to select CurrentOpenings from database
        public function selCurrentOpenings() {
            $dbh = new DatabaseHelper();
            $sql = "select * from current_openings order by STR_TO_DATE(date, '%d-%m-%Y')";
            $stmt = $dbh->createConnection()->prepare($sql);            
            $stmt->execute();
            $dbh->closeConnection();
            return $stmt;
        }
        
  
          public function updCurrentOpeningstatus(CurrentOpenings $current_openings) {
            $dbh = new DatabaseHelper();
            $sql ="UPDATE current_openings SET " 
                    ."status='".$current_openings->getStatus()."'"
                   ." WHERE current_openings_id=".$current_openings->getCurrent_openings_id()."";
            $stmt = $dbh->createConnection()->prepare($sql);
            $i = $stmt->execute();
            
            $dbh->closeConnection();
             
            if ($i > 0) {                
                return TRUE;
            } else {
              
                return FALSE;
            }
        } 
          
    }
?>
