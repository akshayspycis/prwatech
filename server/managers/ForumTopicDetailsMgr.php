

<?php
    include_once '../dbhelper/DatabaseHelper.php';
    
        class ForumTopicDetailsMgr{    

        //method to insert forum_topic_details in database
        public function insForumTopicDetails(ForumTopicDetails $forum_topic_details) {
            $dbh = new DatabaseHelper();
            $sql = "INSERT INTO forum_topic_details( "
                    . "forum_category_details_id, "
                    . "date, "
                    . "topic_name, "
                    . "topic_url) "
                    . "VALUES ('".$forum_topic_details->getForum_category_details_id()."',"
                    . "'".$forum_topic_details->getDate()."',"
                    . "'".$forum_topic_details->getTopic_name()."',"
                    . "'".$forum_topic_details->getTopic_url()."')";
            $stmt = $dbh->createConnection()->prepare($sql); 
            $i = $stmt->execute();            
            $dbh->closeConnection();
            if ($i > 0) {                
                return TRUE;
            } else {
                return FALSE;
            }
        }

        //method to delete news in database
        public function delForumTopicDetails($forum_topic_details_id) {
            $dbh = new DatabaseHelper();
             $sql = "delete from forum_topic_details where forum_topic_details_id = '".$forum_topic_details_id."'";
            $stmt = $dbh->createConnection()->prepare($sql);            
            $i = $stmt->execute();            
            $dbh->closeConnection();
            
            if ($i > 0) {
                return TRUE;
            } else {
                return FALSE;
            }
        }
        
        //method to select ForumTopicDetails from database
        public function selForumTopicDetails($forum_category_details_id) {
            $dbh = new DatabaseHelper();
            $sql = "select * from forum_topic_details where forum_category_details_id=".$forum_category_details_id;
            $stmt = $dbh->createConnection()->prepare($sql);            
            $stmt->execute();
            $dbh->closeConnection();
            return $stmt;
        }
        public function selForumTopicDetailsInClient( $category_url) {
            $dbh = new DatabaseHelper();
            $sql = " select *,
 (select count(*) from forum_post_details fpd where fpd.forum_topic_details_id=b.forum_topic_details_id) as post,
 (select fpd.date from forum_post_details fpd where fpd.forum_topic_details_id=b.forum_topic_details_id
 order by STR_TO_DATE(date, '%d-%m-%Y') limit 1) as latest
 from forum_topic_details b where b.forum_category_details_id=(select forum_category_details_id from forum_category_details where category_url='".$category_url."')";
            $stmt = $dbh->createConnection()->prepare($sql);            
            $stmt->execute();
            
            $dbh->closeConnection();
            return $stmt;
        }
        
  public function updateForumTopicDetails(ForumTopicDetails $forum_topic_details) {
            $dbh = new DatabaseHelper();
            $sql ="UPDATE forum_topic_details SET " 
                    ."topic_name='".$forum_topic_details->getTopic_name()."',"
                    ."topic_url='".$forum_topic_details->getTopic_url()."'"
                    ." WHERE forum_topic_details_id =".$forum_topic_details->getForum_topic_details_id();
            echo $sql;
             $stmt = $dbh->createConnection()->prepare($sql);
            $i = $stmt->execute();
            $dbh->closeConnection();
            if ($i > 0) {                
                return TRUE;
            } else {
                return FALSE;
            }
        }
        
      public function updForumTopicDetailstatus(ForumTopicDetails $forum_topic_details) {
        $dbh = new DatabaseHelper();
        $sql ="UPDATE forum_topic_details SET " 
                ."status='".$forum_topic_details->getStatus()."'"
               ."WHERE forum_topic_details_id=".$forum_topic_details->getCourse_batch_details_id()."";

        $stmt = $dbh->createConnection()->prepare($sql);
        $i = $stmt->execute();

        $dbh->closeConnection();

        if ($i > 0) {                
            return TRUE;
        } else {

            return FALSE;
        }
    } 
          
    }
?>
