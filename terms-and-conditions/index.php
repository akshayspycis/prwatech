<?php include '../includes/session.php'; ?><!DOCTYPE html>
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
	<!--<![endif]-->
        <head>
            <meta charset="utf-8">
            <title>PrwaTech | Terms & Conditions </title>
            <meta name="title" content="Big Data Hadoop Online Training | Hadoop Certification Course | Prwatech" />
            <meta name="keywords" content="hadoop training, online hadoop training, hadoop training classes, hadoop course online, big data training, big data course, big data online course, hadoop tutorial, HDFS training, Yarn training, MapReduce training, Pig training, Hive training, HBase training" />
            <meta name="description" content="Our Big Data Hadoop Certification Training helps you master HDFS, Yarn, MapReduce, Pig, Hive, HBase with use cases on Retail, Social Media, Aviation, Finance, Tourism domain" />
            <meta name="description" content="PrwaTech provides big data Hadoop Training Classes for beginners and developers with versatile carrier options. Know more about courses visit our website.">
            <meta name="author" content="prwatech">
            <meta name="google-site-verification" content="fSzBVSEUMu0l5lnD0qBVGv5F_16zaI6xiUZFMm-iMqQ" /> 
            <?php include '../includes/csslinks.php'; ?>
           
        </head>

	<!-- body classes:  -->
	<!-- "boxed": boxed layout mode e.g. <body class="boxed"> -->
	<!-- "pattern-1 ... pattern-9": background patterns for boxed layout mode e.g. <body class="boxed pattern-1"> -->
	<!-- "transparent-header": makes the header transparent and pulls the banner to top -->
	<body class="no-trans front-page transparent-header ">
            <!-- scrollToTop -->
		<!-- ================ -->
		<div class="scrollToTop circle"><i class="icon-up-open-big"></i></div>
		
		<!-- page wrapper start -->
		<!-- ================ -->
		<div class="page-wrapper">
		
			<!-- header-container start -->
			<?php include './../includes/header.php' ;  ?>
			<!-- header-container end -->
		
			<!-- banner start -->
			<!-- ================ -->
			<div class="banner dark-translucent-bg" style="background-image:url('../images/page-about-banner-3.jpg'); background-position: 50% 27%;">
				<!-- breadcrumb start -->
				<!-- ================ -->
				<div class="breadcrumb-container object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
					<div class="container">
						<ol class="breadcrumb">
							<li><i class="fa fa-home pr-10"></i><a class="link-dark" href="index.html">Home</a></li>
							<li class="active">Term and Conditions</li>
							
						</ol>
					</div>
				</div>
				<!-- breadcrumb end -->
				<div class="container">
					
				</div>
			</div>
			<!-- banner end -->
			
			<div id="page-start"></div>

			<!-- section start -->
			<!-- ================ -->
			     <section class="light-gray-bg pv-30 clearfix"  style="background:#f8f8f8 url('../images/bg/overlay.png') repeat;box-shadow:inset 0 2px 7px rgba(0,0,0, 0.25);">
				<div class="container">
					<div class="row">
						<div class="col-md-8 col-md-offset-2 text-center">
							<h2 class="text-center heading-font" style="text-transform:none;"><strong>Term and Conditions</strong>  </h2>
							<div class="separator"></div>						
						</div>
					</div>
					 <div class="col-md-12 object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
                                        <h5 class="heading-font">Registration and Identity Protection:</h5>
                                        <p id="cpara">
                                            To gain access to specific company products, you will have to register and obtain an account, username and corresponding password whose confidentiality is maintained from our end and you shall be solely responsible for security of your account. Any unauthorized usage of your account in the form of breach of security must be notified to us immediately. Exiting from your account after end of each course is crucial and must be ensured. We shall not be responsible for any unauthorized activities on or with your account, with or without your knowledge prior to notifying us. And, you shall be liable to losses incurred by us or another party due to unauthorized usage of your Account prior to your notification to us of such unauthorized activities.
                                        </p>
                                        <p id="cpara">
                                           At any instance of time you shall not transfer your account or use other person’s account. In situation where you have authorized or registered another individual ( including a minor), to use your account, you shall be entirely accountable for (i)online conduct of the user (ii) Controlling the user’s access to use the services (iii) and consequences of misus
                                        </p>
                                        
                                        <h5 class="heading-font">User and Submitted Content:</h5>
                                        <p id="cpara">
                                            Any and all material such as ideas, communications, uploads transmitted to us through Company products in the form of “submitted data” shall be identified as non-confidential material and can be reproduced, distributed, communicated to public unless and otherwise used for marketing, delivery, promoting and demonstrating the company’s products which is not only limited to quality control, redistribution or as source to professional development.
                                        </p>
                                         <h5 class="heading-font">Privacy</h5>
                                        <p id="cpara">
                                            Any personal information submitted in connection with your use of the Site is subject to our Privacy Policy,
                                        </p>
                                        <h5 class="heading-font">Refunds</h5>
                                        <p id="cpara">
                                            If any student is not satisfied with the training, then by giving logical reason for the same and before completion of 25 % of Course, he/she can take his/her course fee back( partial refund).aftre completion of 25% of course, fee is not refundable.
                                        </p>
                                        <h5 class="heading-font">Security</h5>
                                        <p id="cpara">
                                           We take steps to protect the Personal Information submitted to us, both during transmission and once we receive it. No method of transmission over the Internet, or method of electronic storage, is 100% secure, however. Therefore, while we strive to use commercially reasonable means to protect your Personal Information, we cannot guarantee its absolute security. If we learn of a security systems breach we may attempt to notify you electronically so that you can take appropriate protective steps. By using this Site or providing personal information to us you agree that we can communicate with you electronically regarding security, privacy, and administrative issues relating to your use of this site. We may post a notice on our Site if a security breach occurs. We may also send an email to you at the email address you have provided to us in these circumstances. Depending on where you live, you may have a legal right to receive notice of a security breach in writing.
                                        </p>
                                        <h5 class="heading-font">Sharing your personal information</h5>
                                        <p id="cpara">
                                            If you have purchased PRWATECH course, we may ask you for testimonial, we will display your testimonial on our website, photos and videos or in our social media channels such as Facebook, YouTube or Flickr. You should be aware that your publicly identifiable information could be used to send you promotional, unsolicited messages. We are not responsible for your personal information which you have chosen to display.
                                        </p>
                                        <p id="cpara">
                                           If you don’t want us to feature your pictures/testimonials on our website or on our social media channels, you can write a mail to support@prwatech.in
                                        </p>
                                         <h5 class="heading-font">Legal Disclaimer</h5>
                                        <p id="cpara">
                                           We reserve the right to disclose your Personal Information as required by law and when we believe that disclosure doing so in the Company’s interest to protect its property or other legal rights or the rights or property of others.
                                        </p>
                                          <h5 class="heading-font">Refund Policy</h5>
                                        <ul class="list-icons" id="clist">
                                            <li><i class="icon-check pr-10"></i>If Client Has any Network/Configuration problem from their side while attending online classes then client is not eligible for refund.</li>
                                            <li><i class="icon-check pr-10"></i>In case of change of venue/lectures being discontinued due to any problem,Client is eligible for partial refund.</li>
                                            <li><i class="icon-check pr-10"></i>Project or Extra lectures added at the time of enrollment are non refundable.</li>
                                            <li><i class="icon-check pr-10"></i>In case if lectures are postponed then client is eligible for partial refund</li>
                                            <li><i class="icon-check pr-10"></i>Full Training material will be given once full fee is paid.</li>
                                            <li><i class="icon-check pr-10"></i>Self-paced Courses/project(classroom) are not eligible for refund.</li>
                                            <li><i class="icon-check pr-10"></i>Illegal , Unauthorized distribution of course material , disobeying privacy policy.in these cases company holds the right to disbar the client and client will not be eligible for refund.</li>
                                            <li><i class="icon-check pr-10"></i>Prwatech Deserves the right to discontinue the services if client found guilty of sharing training material or any associate information about courses with others.</li>
                                            <li><i class="icon-check pr-10"></i> Client will not be served if he/she found distributing the sessions of training.</li>
                                            <li><i class="icon-check pr-10"></i>Refund is matter subject to finance department and the time taken to generate refund would range from 3 to 4 weeks.</li>
                                            <li><i class="icon-check pr-10"></i>For Instructor Led Batches if client does not attend session at desired time then he/she would be eligible for rescheduled lecture which might be either video lecture or instructor lead lecture but client will not be eligible for refund</li>
                                        </ul>
                                     </div>
					
				</div>
				
				
			</section>
                       
                        
			<!-- section end -->
                    
			<div class="clearfix"></div>
			
			<!-- ================ -->
			
			                   
			<!-- section end -->
			<!-- footer start (Add "dark" class to #footer in order to enable dark footer) -->
			<!-- ================ -->
			<?php include './../includes/footer.php'; ?>
			<!-- footer end -->
			
		</div>
		<!-- page-wrapper end -->

                <?php include './../includes/jslinks.php';?>
                <!-- Color Switcher End -->
                <?php include './../includes/quick-query.php';?>
                <?php include './../includes/UserSignup.php';?>
                
                
	</body>


</html>
