<?php include '../includes/session.php'; ?>
<?php
$request  = str_replace("/forums/forum/", "", $_SERVER['REQUEST_URI']);

if (strpos($request, '/') !== false) {
	$request = str_replace("/","", $request);
} 


?>
<!DOCTYPE html>
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
	<!--<![endif]-->
<head>
<base href="/" target="_blank">
		<meta charset="utf-8">
                <title>PrwaTech | Forum Topic Detail </title>
		<meta name="title" content="Big Data Hadoop Online Training | Hadoop Certification Course | Prwatech" />
                <meta name="keywords" content="hadoop training, online hadoop training, hadoop training classes, hadoop course online, big data training, big data course, big data online course, hadoop tutorial, HDFS training, Yarn training, MapReduce training, Pig training, Hive training, HBase training" />
                <meta name="description" content="Our Big Data Hadoop Certification Training helps you master HDFS, Yarn, MapReduce, Pig, Hive, HBase with use cases on Retail, Social Media, Aviation, Finance, Tourism domain" />
                <meta name="description" content="PrwaTech provides big data Hadoop Training Classes for beginners and developers with versatile carrier options. Know more about courses visit our website.">
                <meta name="author" content="prwatech">
                <meta name="google-site-verification" content="fSzBVSEUMu0l5lnD0qBVGv5F_16zaI6xiUZFMm-iMqQ" />  
                <?php include '../includes/csslinks.php'; ?>
                <style>
                    body p{
                        text-align:justify;
                    }
                </style>
        </head>

	<!-- body classes:  -->
	<!-- "boxed": boxed layout mode e.g. <body class="boxed"> -->
	<!-- "pattern-1 ... pattern-9": background patterns for boxed layout mode e.g. <body class="boxed pattern-1"> -->
	<!-- "transparent-header": makes the header transparent and pulls the banner to top -->
	<body class="no-trans front-page transparent-header ">
            <!-- scrollToTop -->
		<!-- ================ -->
		<div class="scrollToTop circle"><i class="icon-up-open-big"></i></div>
		
		<!-- page wrapper start -->
		<!-- ================ -->
		<div class="page-wrapper">
		
			<!-- header-container start -->
			<?php include '../includes/header.php' ;  ?>
			<!-- header-container end -->
		
			<!-- banner start -->
			<!-- ================ -->
			<div class="banner dark-translucent-bg" style="background-image:url('../images/page-course-banner-1.jpg'); background-position: 50% 27%;">
				<!-- breadcrumb start -->
				<!-- ================ -->
				<div class="breadcrumb-container object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
					<div class="container">
						<ol class="breadcrumb">
							<li><i class="fa fa-home pr-10"></i><a class="link-dark" href="index.html">Home</a></li>
							
							<li class="active">Blog</li>
						</ol>
					</div>
				</div>
				<!-- breadcrumb end -->
				
			</div>
			<!-- banner end -->
			
			<div id="page-start"></div>

			<!-- section start -->
			<!-- ================ -->
                        <section class="pv-30 clearfix object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100" style="background:#f5f5f5;" >
				<div class="container">
                                     <div class="row">
                                         <div class="col-md-12" id="blog_details">
                                            <!-- page-title start -->
							<!-- ================ -->
							<h1 class="page-title">Welcome to Forum</h1>
							<!-- page-title end -->
<!--                                                        <div class="separator-2"></div>-->
                                                                        <div class="main">
<!--                                                                            <div class="alert alert-info alert-dismissible" role="alert">
								<button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
								<strong>This !</strong> forum contains 47 topics and 34 replies, and was last updated by  akakum9 7 months, 2 weeks ago.
							</div>-->

                                                                            <div class="separator-2"></div>
                                                                            <!-- page-title end -->

                                                                            <table class="table cart table-hover table-colored">
                                                                                    <thead>
                                                                                            <tr>
                                                                                                    <th>S.No.</th>
                                                                                                    <th>Topics</th>
                                                                                                    <th>Posts</th>
                                                                                                    <th>Freshness </th>
                                                                                            </tr>
                                                                                    </thead>
                                                                                    <tbody id="data">
                                                                                            
                                                                                    </tbody>
                                                                            </table>
                                                                    </div>
							
                                         </div>
                                     
                                     </div>
					
                                    
				</div>
                             
                           
                         
			</section>
                    
                       
                        
			<!-- section end -->
                    
			<div class="clearfix"></div>
			
			<!-- ================ -->
		
			<!-- ================ -->
			<?php include './../includes/footer.php'; ?>
			<!-- footer end -->
			
		</div>
		<!-- page-wrapper end -->

		<?php include './../includes/jslinks.php';?>
                <div class="modal fade" id="demoVideoPopUp" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header ">
                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                <h4 class="modal-title" id="myModalLabel">Demo Session at Prwatech</h4>
                            </div>
                            <div class="modal-body">
                               <iframe width="570" height="350" src="https://www.youtube.com/embed/xA5TAalZt40" frameborder="0" allowfullscreen></iframe> 
                              
                            </div>
                            <div class="modal-footer">
                                
                            </div>
                        </div>
                    </div>
                </div>
		<!-- Color Switcher End -->
                   <?php include './../includes/quick-query.php';?>
                   <?php include './../includes/UserSignup.php';?>
                <script type="text/javascript" src="plugins/magnific-popup/jquery.magnific-popup.min.js"></script>
	</body>

<script>
            $(document).ready(function() {

                        onlad('<?php echo $request;?>');
            });  
            
            function urlParam(name){
    var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
    if (results==null){
       return null;
    }
    else{
       return results[1] || 0;
    }
}
            function onlad(category_url){
                     $.ajax({
                    type:"post",
                    data:{'category_url':category_url},
                    url:"../server/controller/SelForumTopicDetails_inClient.php",
                    success: function(data) {
                        
                        var duce = jQuery.parseJSON(data);
                        $("#data tr:has(td)").remove();
                        $.each(duce, function (index, article) {
                              $("#data").append($('<tr/>').addClass("remove-data")
                                .append($('<td/>').addClass("price").html((index+1)))
                                .append($('<td/>').addClass("product").html($("<a>").append(article.topic_name).css({'cursor':'pointer'}).click(function(){
                                    window.location="topic/"+article.topic_url+"/";
                                 })))
                                .append($('<td/>').addClass("price").addClass("remove-data").html(article.post))
                                .append($('<td/>').addClass("amount").addClass("remove-data").html($("<a>").append(article.latest).css({'cursor':'pointer'})))
                              );
                        });
                    }
                });
         }
        </script>
</html>



