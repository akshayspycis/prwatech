<?php include '../includes/session.php'; ?>
<?php
$request  = str_replace("/forums/topic/", "", $_SERVER['REQUEST_URI']);
if (strpos($request, '/') !== false) {
	$request = str_replace("/","", $request);
} 
?>
<!DOCTYPE html>
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
	<!--<![endif]-->
<head>
		<meta charset="utf-8">
                <title>PrwaTech | Blog Post Detail </title>
		<meta name="title" content="Big Data Hadoop Online Training | Hadoop Certification Course | Prwatech" />
                <meta name="keywords" content="hadoop training, online hadoop training, hadoop training classes, hadoop course online, big data training, big data course, big data online course, hadoop tutorial, HDFS training, Yarn training, MapReduce training, Pig training, Hive training, HBase training" />
                <meta name="description" content="Our Big Data Hadoop Certification Training helps you master HDFS, Yarn, MapReduce, Pig, Hive, HBase with use cases on Retail, Social Media, Aviation, Finance, Tourism domain" />
                <meta name="description" content="PrwaTech provides big data Hadoop Training Classes for beginners and developers with versatile carrier options. Know more about courses visit our website.">
                <meta name="author" content="prwatech">
                <meta name="google-site-verification" content="fSzBVSEUMu0l5lnD0qBVGv5F_16zaI6xiUZFMm-iMqQ" />  
                <?php include '../includes/csslinks.php'; ?>
                <style>
                    body p{
                        text-align:justify;
                    }
                </style>
        </head>

	<!-- body classes:  -->
	<!-- "boxed": boxed layout mode e.g. <body class="boxed"> -->
	<!-- "pattern-1 ... pattern-9": background patterns for boxed layout mode e.g. <body class="boxed pattern-1"> -->
	<!-- "transparent-header": makes the header transparent and pulls the banner to top -->
	<body class="no-trans front-page transparent-header ">
            <!-- scrollToTop -->
		<!-- ================ -->
		<div class="scrollToTop circle"><i class="icon-up-open-big"></i></div>
		
		<!-- page wrapper start -->
		<!-- ================ -->
		<div class="page-wrapper">
		
			<!-- header-container start -->
			<?php include '../includes/header.php' ;  ?>
			<!-- header-container end -->
		
			<!-- banner start -->
			<!-- ================ -->
			<div class="banner dark-translucent-bg" style="background-image:url('../images/page-course-banner-1.jpg'); background-position: 50% 27%;">
				<!-- breadcrumb start -->
				<!-- ================ -->
				<div class="breadcrumb-container object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
					<div class="container">
						<ol class="breadcrumb">
							<li><i class="fa fa-home pr-10"></i><a class="link-dark" href="index.html">Home</a></li>
							
							<li class="active">Blog</li>
						</ol>
					</div>
				</div>
				<!-- breadcrumb end -->
				
			</div>
			<!-- banner end -->
			
			<div id="page-start"></div>

			<!-- section start -->
			<!-- ================ -->
                        <section class="pv-30 clearfix object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100" style="background:#f5f5f5;" >
				<div class="container">
                                     <div class="row">
                                         <div class="col-md-12" id="blog_details">
                                            <!-- page-title start -->
							<!-- ================ -->
							<h1 class="page-title">Welcome to Forum</h1>
							<!-- page-title end -->
                                                        <div class="separator-2"></div>
                                                                        <div class="main">
                                                                            

                                                                            <div class="alert alert-info alert-dismissible" role="alert">
								
								<strong>Hive Questions </strong> 
							</div>
                                                                            <div class="separator-2"></div>
                                                                            <!-- page-title end -->

                                                                            <table class="table cart table-colored">
                                                                                    <thead>
                                                                                            <tr>
                                                                                                <th>Posts</th>
                                                                                                    <th colspan="2">Author</th>
                                                                                                    
                                                                                            </tr>
                                                                                    </thead>
                                                                                    <tbody id="data">
                                                                                            
                                                                                            <tr class="remove-data">
                                                                                                <td class="product">
                                                                                                    
                                                                                                    <p>1.What is Hive?<br />
2.What is Hive Metastore?<br />
3.What kind of datawarehouse application is suitable for Hive?<br />
4.How can the columns of a table in hive be written to a file?<br />
5.CONCAT function in Hive with Example?<br />
6.REPEAT function in Hive with example?<br />
7.TRIM function in Hive with example?<br />
8.REVERSE function in Hive with example?<br />
9.LOWER or LCASE function in Hive with example?<br />
10.UPPER or UCASE function in Hive with example?<br />
11.Double type in Hive &amp;ndash; Important points?<br />
12.Rename a table in Hive &amp;ndash; How to do it?<br />
13.How to change a column data type in Hive?<br />
14.Difference between order by and sort by in hive?<br />
15.RLIKE in Hive?<br />
16.Difference between external table and internal table in HIVE ?</p>
                                                                                                </td>
                                                                                                    <td class="price">
                                                                                                        <b>Akshay Bilani<br><div class="separator-2"></div>
                                                                                                        October 21, 2015 <br>
                                                                                                        at 12:58 pm</b>
                                                                                                    </td>
                                                                                                    <td class="price">#7865</td>
                                                                                            </tr>
                                                                                            
                                                                                            
                                                                                    </tbody>
                                                                            </table>
                                                                    </div>
							
                                         </div>
                                     
                                     </div>
					
                                    
				</div>
                             
                           
                         
			</section>
                    
                       
                        
			<!-- section end -->
                    
			<div class="clearfix"></div>
			
			<!-- ================ -->
		
			<!-- ================ -->
			<?php include './../includes/footer.php'; ?>
			<!-- footer end -->
			
		</div>
		<!-- page-wrapper end -->

		<?php include './../includes/jslinks.php';?>
                <div class="modal fade" id="demoVideoPopUp" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header ">
                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                <h4 class="modal-title" id="myModalLabel">Demo Session at Prwatech</h4>
                            </div>
                            <div class="modal-body">
                               <iframe width="570" height="350" src="https://www.youtube.com/embed/xA5TAalZt40" frameborder="0" allowfullscreen></iframe> 
                              
                            </div>
                            <div class="modal-footer">
                                
                            </div>
                        </div>
                    </div>
                </div>
		<!-- Color Switcher End -->
                   <?php include './../includes/quick-query.php';?>
                   <?php include './../includes/UserSignup.php';?>
                <script type="text/javascript" src="plugins/magnific-popup/jquery.magnific-popup.min.js"></script>
	</body>
<script>
            $(document).ready(function() {

                        //onlad('<?php echo $request;?>');
                        onlad('1');
            });  
            
            function urlParam(name){
    var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
    if (results==null){
       return null;
    }
    else{
       return results[1] || 0;
    }
}
            function onlad(topic_url){
                     $.ajax({
                    type:"post",
                    data:{'topic_url':topic_url},
                    url:"../server/controller/SelForumPostDetails_inClient.php",
                    success: function(data) {
                        var duce = jQuery.parseJSON(data);
                        $("#data tr:has(td)").remove();
                        $.each(duce, function (index, article) {
                              $("#data").append($('<tr/>').addClass("remove-data")
                                .append($('<td/>').addClass("price").html((index+1)))
                                .append($('<td/>').addClass("product").html(article.post))
                                .append($('<td/>').addClass("price").addClass("remove-data").html($("<b>").append(article.user_name).append($('<div class="separator-2"></div>')).append($("<br>")).append(article.date)))
                                .append($('<td/>').addClass("price").addClass("remove-data").html(1000+parseInt(article.forum_post_details_id)))
                              );
                        });
                    }
                });
         }
        </script>

</html>



