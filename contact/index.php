<?php include '../includes/session.php'; ?>
<!DOCTYPE html>
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
	<!--<![endif]-->
        <head>
            <meta charset="utf-8">
            <title>PrwaTech | Contact | Hadoop Developer Training Course Classes Pune &amp; Bangalore </title>
                <meta name="title" content="Big Data Hadoop Online Training | Hadoop Certification Course | Prwatech" />
                <meta name="keywords" content="hadoop training, online hadoop training, hadoop training classes, hadoop course online, big data training, big data course, big data online course, hadoop tutorial, HDFS training, Yarn training, MapReduce training, Pig training, Hive training, HBase training" />
                <meta name="description" content="Our Big Data Hadoop Certification Training helps you master HDFS, Yarn, MapReduce, Pig, Hive, HBase with use cases on Retail, Social Media, Aviation, Finance, Tourism domain" />
                <meta name="description" content="PrwaTech provides big data Hadoop Training Classes for beginners and developers with versatile carrier options. Know more about courses visit our website.">
                <meta name="author" content="prwatech">
                <meta name="google-site-verification" content="fSzBVSEUMu0l5lnD0qBVGv5F_16zaI6xiUZFMm-iMqQ" /> 
                <?php include '../includes/csslinks.php'; ?>
        </head>

	<!-- body classes:  -->
	<!-- "boxed": boxed layout mode e.g. <body class="boxed"> -->
	<!-- "pattern-1 ... pattern-9": background patterns for boxed layout mode e.g. <body class="boxed pattern-1"> -->
	<!-- "transparent-header": makes the header transparent and pulls the banner to top -->
	<body class="no-trans front-page transparent-header ">
            <!-- scrollToTop -->
		<!-- ================ -->
		<div class="scrollToTop circle"><i class="icon-up-open-big"></i></div>
		
		<!-- page wrapper start -->
		<!-- ================ -->
		<div class="page-wrapper">
		
			<!-- header-container start -->
			<?php include '../includes/header.php' ;  ?>
			<!-- header-container end -->
		
			<!-- banner start -->
			<!-- ================ -->
			<div class="banner dark-translucent-bg" style="background-image:url('../images/background-img-3.jpg'); background-position: 50% 30%;">
				<!-- breadcrumb start -->
				<!-- ================ -->
				<div class="breadcrumb-container">
					<div class="container">
						<ol class="breadcrumb">
							<li><i class="fa fa-home pr-10"></i><a class="link-dark" href="index.html">Home</a></li>
							<li class="active">Contact Us</li>
						</ol>
					</div>
				</div>
				<!-- breadcrumb end -->
                                
				<div class="container">
					<div class="row">
                                          
						<div class="col-md-8 text-center col-md-offset-2 pv-20">
							<h1 class="page-title text-center">Contact Us</h1>
							<div class="separator"></div>
							<p class="lead text-center">It would be great to hear from you! Just drop us a line and ask for anything with which you think we could be helpful. We are looking forward to hearing from you!</p>
							<ul class="list-inline mb-20 text-center">
                                                            <li><i class="text-default fa fa-map-marker pr-5"></i>BTM
                                                                 29th Main Road,
                                                                BTM  1st Stage, 
                                                                Bangalore</li>
								<li><a href="tel: 080-416 456 25" class="link-dark"><i class="text-default fa fa-phone pl-10 pr-5"></i>080-416 456 25</a></li>
								<li><a href="info@prwatech.in" class="link-dark"><i class="text-default fa fa-envelope-o pl-10 pr-5"></i>info@theproject.com</a></li>
							</ul>
							<div class="separator"></div>
							
						</div>
					</div>
				</div>
			</div>
			<!-- banner end -->
			
			<div id="page-start"></div>

			<!-- section start -->
			<!-- ================ -->
			<section class="light-gray-bg pv-30 clearfix object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100" id="cdrow1">
				<div class="container">
                                    <div class="container">
                                          <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d15555.390954147912!2d77.61597870000001!3d12.91750600000001!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xad3d2d7d2cd46246!2sPrwatech+-+Python+%2F+Big+Data+Hadoop+Training+Institute+in+Bangalore!5e0!3m2!1sen!2sin!4v1482994750190" width="100%" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>
                                        <div class="row">
                                            <p>&nbsp;</p>
                                            <div class="separator"></div>
                                            <h2 class="text-center heading-font object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100" style="text-transform:none;"> <span style="font-size:36px;font-weight:bold;color:#0078d7">Bangalore Office</span></h2>
                                            <div class="separator"></div>
                                            <p>&nbsp;</p>
                                            <h1 class="text-center heading-font" style="text-transform:none;"></h1>
                                            <div class="col-sm-4">
                                                <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
                                                    
                                                    <div class="body">
                                                        <h3 class="heading-font2"><center><b>BTM</b></center></h3>
                                                        <div class="separator"></div>
                                                        <p class="heading-font"><b>Address</b></p>
                                                        <table class="table table-striped text-left">
                                                            <tbody>
                                                                <tr><td>BTM, No 14, 29th Main Road,  2st Cross,</td></tr>
                                                                <tr><td> BTM Layout, 1st Stage, Behind Axa Building,</td></tr>
                                                                <tr><td>Land Mark : Vijaya Bank ATM,</td></tr>
                                                                <tr><td>Bangalore – 560 068</td></tr>
                                                                <tr><td>Karnataka, INDIA</td></tr>
                                                                <tr><td>Land Line no : <b>080-416 456 25</b></td></tr>
                                                                <tr><td>Talk to Us : <b>18002701019</b></td></tr>
                                                                <tr><td>Mobile no : <b>+91 8147111254</b></td></tr>
                                                                <tr><td>EMail ID : <b>info@prwatech.in</b></td></tr>
                                                            </tbody>
                                                        </table>
                                                       
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-4">
                                                <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
                                                    
                                                    <div class="body">
                                                        <h3 class="heading-font2"><center><b>MALLESHWARAM</b></center></h3>
                                                        <div class="separator"></div>
                                                        <p class="heading-font"><b>Address</b></p>
                                                        <table class="table table-striped text-left">
                                                            <tbody>
                                                                <tr><td>MALLESHWARAM</td></tr>
                                                                <tr><td>Land Line no : <b>080-416 456 25</b></td></tr>
                                                                <tr><td>Talk to Us : <b>18002701019</b></td></tr>
                                                                <tr><td>Mobile no : <b>+91 8147111254</b></td></tr>
                                                                <tr><td>EMail ID : <b>info@prwatech.in</b></td></tr>
                                                            </tbody>
                                                        </table>
                                                       
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-4">
                                                <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
                                                    
                                                    <div class="body">
                                                        <h3 class="heading-font2"><center><b> MARATHALLI</b></center></h3>
                                                        <div class="separator"></div>
                                                        <p class="heading-font"><b>Address</b></p>
                                                        <table class="table table-striped text-left">
                                                            <tbody>
                                                                <tr><td>Marathalli</td></tr>
                                                                <tr><td>Bangalore – 560 068</td></tr>
                                                                <tr><td>Karnataka, INDIA</td></tr>
                                                                <tr><td>Land Line no : <b>080-416 456 25</b></td></tr>
                                                                <tr><td>Talk to Us : <b>18002701019</b></td></tr>
                                                                <tr><td>Mobile no : <b>+91 8147111254</b></td></tr>
                                                                <tr><td>EMail ID : <b>info@prwatech.in</b></td></tr>
                                                            </tbody>
                                                        </table>
                                                       
                                                    </div>
                                                </div>
                                            </div>
                                           
                                            
                                        </div>
                                        <p>&nbsp;</p>
                                        <div class="row">
                                             <div class="col-sm-4">
                                                <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
                                                    
                                                    <div class="body">
                                                        <h3 class="heading-font2"><center><b> KORAMANGALA</b></center></h3>
                                                        <div class="separator"></div>
                                                        <p class="heading-font"><b>Address</b></p>
                                                        <table class="table table-striped text-left">
                                                            <tbody>
                                                                <tr><td>KORAMANGALA</td></tr>
                                                                <tr><td>Bangalore – 560 068</td></tr>
                                                                <tr><td>Karnataka, INDIA</td></tr>
                                                                <tr><td>Land Line no : <b>080-416 456 25</b></td></tr>
                                                                <tr><td>Talk to Us : <b>18002701019</b></td></tr>
                                                                <tr><td>Mobile no : <b>+91 8147111254</b></td></tr>
                                                                <tr><td>EMail ID : <b>info@prwatech.in</b></td></tr>
                                                            </tbody>
                                                        </table>
                                                       
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-4">
                                                <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
                                                    
                                                    <div class="body">
                                                        <h3 class="heading-font2"><center><b> ELECTRONICS CITY</b></center></h3>
                                                        <div class="separator"></div>
                                                        <p class="heading-font"><b>Address</b></p>
                                                        <table class="table table-striped text-left">
                                                            <tbody>
                                                                <tr><td>Electronics City</td></tr>
                                                                <tr><td>Bangalore – 560 068</td></tr>
                                                                <tr><td>Karnataka, INDIA</td></tr>
                                                                <tr><td>Land Line no : <b>080-416 456 25</b></td></tr>
                                                                <tr><td>Talk to Us : <b>18002701019</b></td></tr>
                                                                <tr><td>Mobile no : <b>+91 8147111254</b></td></tr>
                                                                <tr><td>EMail ID : <b>info@prwatech.in</b></td></tr>
                                                            </tbody>
                                                        </table>
                                                       
                                                    </div>
                                                </div>
                                            </div>
                                            
                                        </div>
                                    </div>
				</div>
                            
			</section>
                        <section id="coursed-row2" class="object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
                            
                            <div class="container">
                                 <p>&nbsp;</p>
                                            <div class="separator"></div>
                                            <h2 class="text-center heading-font object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100" style="text-transform:none;"> <span style="font-size:36px;font-weight:bold;color:#0078d7">Pune Office</span></h2>
                                            <div class="separator"></div>
                                            <p>&nbsp;</p>
                                            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d15555.390954147912!2d77.61597870000001!3d12.91750600000001!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xad3d2d7d2cd46246!2sPrwatech+-+Python+%2F+Big+Data+Hadoop+Training+Institute+in+Bangalore!5e0!3m2!1sen!2sin!4v1482994750190" width="100%" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>
                                <div class="row">
                                            
                                            <div class="col-sm-4">
                                                <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
                                                    
                                                    <div class="body">
                                                        <h3 class="heading-font2"><center><b>HINJEWADI</b></center></h3>
                                                        <div class="separator"></div>
                                                        <p class="heading-font"><b>Address</b></p>
                                                        <table class="table table-striped text-left">
                                                            <tbody>
                                                                <tr><td>Hinjewadi, Pune</td></tr>
                                                                 <tr><td>Land Line no : <b>080-416 456 25</b></td></tr>
                                                                <tr><td>Talk to Us : <b>18002701019</b></td></tr>
                                                                <tr><td>Mobile no : <b>+91 8147111254</b></td></tr>
                                                                <tr><td>EMail ID : <b>info@prwatech.in</b></td></tr>
                                                            </tbody>
                                                        </table>
                                                       
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-4">
                                                <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
                                                    
                                                    <div class="body">
                                                        <h3 class="heading-font2"><center><b>MAGARPATTA</b></center></h3>
                                                        <div class="separator"></div>
                                                        <p class="heading-font"><b>Address</b></p>
                                                        <table class="table table-striped text-left">
                                                            <tbody>
                                                                <tr><td>MAGARPATTA, Pune</td></tr>
                                                                 <tr><td>Land Line no : <b>080-416 456 25</b></td></tr>
                                                                <tr><td>Talk to Us : <b>18002701019</b></td></tr>
                                                                <tr><td>Mobile no : <b>+91 8147111254</b></td></tr>
                                                                <tr><td>EMail ID : <b>info@prwatech.in</b></td></tr>
                                                            </tbody>
                                                        </table>
                                                       
                                                    </div>
                                                </div>
                                            </div>
                                           <div class="col-sm-4">
                                                <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
                                                    
                                                    <div class="body">
                                                        <h3 class="heading-font2"><center><b>MG ROAD</b></center></h3>
                                                        <div class="separator"></div>
                                                        <p class="heading-font"><b>Address</b></p>
                                                        <table class="table table-striped text-left">
                                                            <tbody>
                                                                <tr><td>Mg Road, Pune</td></tr>
                                                                 <tr><td>Land Line no : <b>080-416 456 25</b></td></tr>
                                                                <tr><td>Talk to Us : <b>18002701019</b></td></tr>
                                                                <tr><td>Mobile no : <b>+91 8147111254</b></td></tr>
                                                                <tr><td>EMail ID : <b>info@prwatech.in</b></td></tr>
                                                            </tbody>
                                                        </table>
                                                       
                                                    </div>
                                                </div>
                                         
                                            
                                        </div>
                                        <div class="col-sm-4">
                                                <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
                                                    
                                                    <div class="body">
                                                        <h3 class="heading-font2"><center><b>KHARADI</b></center></h3>
                                                        <div class="separator"></div>
                                                        <p class="heading-font"><b>Address</b></p>
                                                        <table class="table table-striped text-left">
                                                            <tbody>
                                                                <tr><td>KHARADI, Pune</td></tr>
                                                                 <tr><td>Land Line no : <b>080-416 456 25</b></td></tr>
                                                                <tr><td>Talk to Us : <b>18002701019</b></td></tr>
                                                                <tr><td>Mobile no : <b>+91 8147111254</b></td></tr>
                                                                <tr><td>EMail ID : <b>info@prwatech.in</b></td></tr>
                                                            </tbody>
                                                        </table>
                                                       
                                                    </div>
                                                </div>
                                            </div>
                            </div>
                            <p>&nbsp;</p>
                            </div>
                        </section>
                        
			<!-- section end -->
                    
			<div class="clearfix"></div>
			
			<!-- ================ -->
			
			
			<!-- section end -->

			<!-- section start -->
			<!-- ================ -->
                        
			                   
			<!-- section end -->
			<!-- footer start (Add "dark" class to #footer in order to enable dark footer) -->
			<!-- ================ -->
			<?php include './../includes/footer.php'; ?>
			<!-- footer end -->
			
		</div>
		<!-- page-wrapper end -->

		<?php include './../includes/jslinks.php';?>
		<!-- Color Switcher End -->
                <?php include './../includes/quick-query.php';?>
                 <?php include './../includes/UserSignup.php';?>
	</body>


</html>
