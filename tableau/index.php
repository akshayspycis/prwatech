<?php include '../includes/session.php'; ?>
<!DOCTYPE html>
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
	<!--<![endif]-->
        <head>
            <meta charset="utf-8">
            <title>PrwaTech | Tableau Training | Tableau Certification | Tableau  Training  Classes Pune &amp; Bangalore </title>
            <meta name="title" content="Data Science Certification Training - R Programming" />
            <meta name="keywords" content="Tableau Training, Tableau Certification, Tableau Desktop 9, Online, Course, Simplilearn" />
            <meta name="description" content="Tableau training course includes tutorials on Tableau dashboards, build visualization & analytics. Also get 4 industry projects to clear the certification" />
            <meta name="author" content="prwatech">
            <meta name="google-site-verification" content="fSzBVSEUMu0l5lnD0qBVGv5F_16zaI6xiUZFMm-iMqQ" />
                <?php include '../includes/csslinks.php'; ?>
        </head>

	<!-- body classes:  -->
	<!-- "boxed": boxed layout mode e.g. <body class="boxed"> -->
	<!-- "pattern-1 ... pattern-9": background patterns for boxed layout mode e.g. <body class="boxed pattern-1"> -->
	<!-- "transparent-header": makes the header transparent and pulls the banner to top -->
	<body class="no-trans front-page transparent-header ">
            <!-- scrollToTop -->
		<!-- ================ -->
		<div class="scrollToTop circle"><i class="icon-up-open-big"></i></div>
		
		<!-- page wrapper start -->
		<!-- ================ -->
		<div class="page-wrapper">
		
			<!-- header-container start -->
			<?php include '../includes/header.php' ; ?>
			<!-- header-container end -->
		
			<!-- banner start -->
			<!-- ================ -->
			<div class="banner dark-translucent-bg" style="background-image:url('../images/page-course-banner-6.jpg'); background-position: 50% 27%;">
				<!-- breadcrumb start -->
				<!-- ================ -->
				<div class="breadcrumb-container object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
					<div class="container">
						<ol class="breadcrumb">
							<li><i class="fa fa-home pr-10"></i><a class="link-dark" href="index.html">Home</a></li>
							<li class="active">All Courses</li>
							<li class="active">Tableau Training</li>
						</ol>
					</div>
				</div>
				<!-- breadcrumb end -->
				<div class="container">
					
				</div>
			</div>
			<!-- banner end -->
			
			<div id="page-start"></div>

			<!-- section start -->
			<!-- ================ -->
			<section class="light-gray-bg pv-30 clearfix object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100" id="cdrow1">
				<div class="container">
					<div class="row">
						<div class="col-md-8">
							 <h2 class="text-center heading-font" style="text-transform:none;"> <strong>Tableau  </strong> Training</h2>
                                                  
							<div class="separator"></div>
                                                        <p class="large text-justify heading-font">Fast-track your career in business intelligence with this Tableau-authorized training course. You'll learn visualization building, analytics, dashboards, and more.</p>
						</div>
						<div class="col-md-4">
                                                    <img src="../images/por-6.png"/>
                                                </div>
						
					</div>
                                                <p></p>
                                                <div class="separator"></div>
                                                <h2 class="text-center heading-font object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100" style="text-transform:none;">Contact Us <strong>+91 8147111254 </strong></h2>
                                                <div class="separator"></div>
                                                <p></p>
                                              
				</div>
                                <div class="container">
                                <div class="row" id="batch_details">
                                    <div class="col-md-3">
                                        <div class="col-md-3">
                                            <div class="row">
                                                <p id ="date-time">21 <br><span id="month">NOV</span></p>
                                            </div>
                                       </div>
                                        <div class="col-md-9" id="upcomming-batch">
                                            <div class="row">
                                                <div class="col-md-12"><p id="dt-content"><b> Mon-Wed (3 Days) <br><small> <br> Rs. 14000/- <a href="#" class="" data-toggle="modal" data-target="#demoPopUp">Enroll Now</a></small></b></p></div>
                                                
                                            </div>
                                       </div>
                                    </div>
                                  <div class="col-md-3">
                                        <div class="col-md-3">
                                            <div class="row">
                                                <p id ="date-time">24 <br><span id="month">NOV</span></p>
                                            </div>
                                       </div>
                                        <div class="col-md-9" id="upcomming-batch">
                                            <div class="row">
                                                <div class="col-md-12"><p id="dt-content"><b> Mon-Wed (3 Days) <br><small> <br> Rs. 14000/- <a href="#" class="" data-toggle="modal" data-target="#demoPopUp">Enroll Now</a></small></b></p></div>
                                                
                                            </div>
                                       </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="col-md-3">
                                            <div class="row">
                                                <p id ="date-time">27 <br><span id="month">NOV</span></p>
                                            </div>
                                       </div>
                                        <div class="col-md-9" id="upcomming-batch">
                                            <div class="row">
                                                <div class="col-md-12"><p id="dt-content"><b> Mon-Wed (3 Days) <br><small> <br> Rs. 14000/- <a href="#" class="" data-toggle="modal" data-target="#demoPopUp">Enroll Now</a></small></b></p></div>
                                                
                                            </div>
                                       </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="col-md-3">
                                            <div class="row">
                                                <p id ="date-time">30 <br><span id="month">NOV</span></p>
                                            </div>
                                       </div>
                                        <div class="col-md-9" id="upcomming-batch">
                                            <div class="row">
                                                <div class="col-md-12"><p id="dt-content"><b> Mon-Wed (3 Days) <br><small> <br> Rs. 14000/- <a href="#" class="" data-toggle="modal" data-target="#demoPopUp">Enroll Now</a></small></b></p></div>
                                                
                                            </div>
                                       </div>
                                    </div>
                                </div>
                            </div>
			</section>
                          <section id="coursed-row2" class="object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
                            <div class="container">
                                
                                 <h2 class="text-center heading-font object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100" style="text-transform:none;"> <strong>Course</strong> Details</h2>
                                 <div id="vseparator" class="object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
                                     <div class="vhr"></div>
                                </div>
                                <div class="row col-md-9" >
                               <!-- tabs start -->
                                <!-- ================ -->
                                <!-- Nav tabs -->
                                <ul class="nav nav-tabs style-3" id="tab-course" role="tablist">
                                    <li class="active"><a href="#h3tab1" role="tab" data-toggle="tab">Overview</a></li>
                                    <li><a href="#h3tab2" role="tab" data-toggle="tab">Course Curriculum</a></li>
                                    <li><a href="#h3tab3" role="tab" data-toggle="tab">FAQ's</a></li>
                                    <li><a href="#h3tab4" role="tab" data-toggle="tab">Download Course Outline</a></li>
                                    <li><a href="#h3tab5" role="tab" data-toggle="tab">Reviews</a></li>
                                   
                                </ul>
                                <!-- Tab panes -->
                                <div class="tab-content">
                                    <div class="tab-pane in active " id="h3tab1">
                                        <div class="row">
                                            <div class="col-md-12 object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
                                                <h5 class="heading-font">What is tableau?</h5>
                                                <p id="cpara">
                                                    Tableau is intelligent business software that provides access to a huge data collection in a compact manner. With visually appealing and interacting dashboards, tableau helps interpreting business data effectively.
                                                </p>
                                                    
                                                <h5 class="heading-font">Why tableau? Need for visual representation of data.</h5>
                                                <p id="cpara">
                                                    Data visualization is simple and yet powerful task of representing information through visual rendering. This allows universal and immediate insight to business growths, downfall, measures etc. Worldwide, Tableau provides the opportunity for all business users from different streams to effectively represent their data with a visual approach.
                                                </p>
                                                <h5 class="heading-font">Future with tableau</h5>
                                                <p id="cpara">
                                                    Analytical representation through visual rendering allows tableau to provide a competitive advantage to the company’s workforce. With tableau, the approach towards operating and communicating on data is improvised. Tableau’s future in mobile analytics gives a vast leap forward in data interpretation and competitive business perspectives. This helps in empowering any organization to make better and fast business decisions.
                                                </p>
                                                    
                                            </div>
                                            
                                        </div>
                                            
                                    </div>
                                    <div class="tab-pane" id="h3tab2">
                                          <div class="row">
                                            <div class="col-md-12 ">
                                                     <div class="panel-group collapse-style-2" id="accordion-2">
								<div class="panel panel-default">
									<div class="panel-heading">
										<h4 class="panel-title">
											<a data-toggle="collapse" data-parent="#accordion-2" href="#collapseOne-2">
												<i class="fa fa-arrow-right pr-10"></i>Module 1 (Basic)
											</a>
										</h4>
									</div>
                                                                    <div id="collapseOne-2" class="panel-collapse collapse ">
                                                                        <div class="panel-body">
                                                                            
                                                                             <h5 class="heading-font">1. Getting Started with Tableau</h5>
                                                                            <ul class="list-icons" id="clist">
                                                                                <li><i class="icon-check pr-10"></i> Power</li>
                                                                                <li><i class="icon-check pr-10"></i> Speed</li>
                                                                                <li><i class="icon-check pr-10"></i> Flexibility</li>
                                                                                <li><i class="icon-check pr-10"></i> Simplicity</li>
                                                                                <li><i class="icon-check pr-10"></i> Beauty</li>
                                                                        
                                                                            </ul>
                                                                            <h5 class="heading-font">2. Tableau Basics</h5>
                                                                            <ul class="list-icons" id="clist">
                                                                                <li><i class="icon-check pr-10"></i> Download, install and open Tableau</li>
                                                                                <li><i class="icon-check pr-10"></i> Connect to sample data and review <br>  the Tableau interface</li>
                                                                                <li><i class="icon-check pr-10"></i> Show Me” Tableau in action</li>
                                                                                <li><i class="icon-check pr-10"></i> Categorically clear views</li>
                                                                        
                                                                            </ul>    
                                                                            <h5 class="heading-font">3. Tableau Objects</h5>
                                                                            <ul class="list-icons" id="clist">
                                                                                <li><i class="icon-check pr-10"></i> Save time with the Tableau toolbar</li>
                                                                                <li><i class="icon-check pr-10"></i> When tables trump graphs</li>
                                                                                <li><i class="icon-check pr-10"></i> Insightful maps</li>
                                                                                <li><i class="icon-check pr-10"></i> View shifting—the underrated histogram  and flexible bins</li>
                                                                        
                                                                            </ul>  
                                                                            <h5 class="heading-font">4. Basic views in Tableau</h5>
                                                                            <ul class="list-icons" id="clist">
                                                                                <li><i class="icon-check pr-10"></i> Tables—an eye for detail</li>
                                                                                <li><i class="icon-check pr-10"></i> Text tables</li>
                                                                                <li><i class="icon-check pr-10"></i> Highlight tables</li>
                                                                                <li><i class="icon-check pr-10"></i> Heat maps</li>
                                                                                <li><i class="icon-check pr-10"></i> Bar Charts—five flavors to meet your  needs</li>
                                                                                <li><i class="icon-check pr-10"></i> Horizontal bars</li>
                                                                                <li><i class="icon-check pr-10"></i> Stacked bars</li>
                                                                                <li><i class="icon-check pr-10"></i> Side-by-side bars</li>
                                                                                <li><i class="icon-check pr-10"></i> Histogram</li>
                                                                                <li><i class="icon-check pr-10"></i> Bullet graphs</li>
                                                                                <li><i class="icon-check pr-10"></i> Line Charts—display what happened  over time</li>
                                                                                <li><i class="icon-check pr-10"></i> Lines (continuous)</li>
                                                                                <li><i class="icon-check pr-10"></i> Lines (discrete)</li>
                                                                                <li><i class="icon-check pr-10"></i> Pie Charts—a common go-to view (use at your own risk!)</li>
                                                                        
                                                                            </ul>
                                                                
                                                                        </div>
                                                                    </div>
								</div>
								<div class="panel panel-default">
									<div class="panel-heading">
										<h4 class="panel-title">
											<a data-toggle="collapse" data-parent="#accordion-2" href="#collapseTwo-2" class="collapsed">
												<i class="fa fa-arrow-right pr-10"></i>Module 2 (Intermediate)
											</a>
										</h4>
									</div>
									<div id="collapseTwo-2" class="panel-collapse collapse">
										<div class="panel-body">
                                                                                        <h5 class="heading-font">1. Advanced views in Tableau</h5>
                                                                                        <ul class="list-icons" id="clist">
                                                                                            <li><i class="icon-check pr-10"></i> Scatter Plots—explore relationships</li>
                                                                                            <li><i class="icon-check pr-10"></i> Circle Plots—go beyond bar charts</li>
                                                                                            <li><i class="icon-check pr-10"></i> Circle views</li>
                                                                                            <li><i class="icon-check pr-10"></i> Side-by-side circles</li>
                                                                                            <li><i class="icon-check pr-10"></i> Maps—great for location data</li>
                                                                                            <li><i class="icon-check pr-10"></i> Symbol maps</li>
                                                                                            <li><i class="icon-check pr-10"></i> Filled maps</li>
                                                                                            <li><i class="icon-check pr-10"></i> Area Charts—track multiple groups  over time</li>
                                                                                            <li><i class="icon-check pr-10"></i> Area charts (continuous)</li>
                                                                                            <li><i class="icon-check pr-10"></i> Area charts (discrete)</li>
                                                                                            <li><i class="icon-check pr-10"></i> Dual Charts—compare  two measures on   two vertical axes</li>
                                                                                            <li><i class="icon-check pr-10"></i> Dual lines</li>
                                                                                            <li><i class="icon-check pr-10"></i> Dual combination</li>
                                                                                            <li><i class="icon-check pr-10"></i> Dual points</li>
                                                                                            <li><i class="icon-check pr-10"></i> Gantt Chart — track activity over time</li>
                                                                                        </ul>
                                                                                        <h5 class="heading-font">2. Customization of views with Tableau</h5>
                                                                                        <ul class="list-icons" id="clist">
                                                                                            <li><i class="icon-check pr-10"></i> Customize views using the Columns,  Rows, Pages and Filters Shelves</li>
                                                                                            <li><i class="icon-check pr-10"></i> Enhance your visual appeal with the  Marks card</li>
                                                                                            <li><i class="icon-check pr-10"></i> 1. Label</li>
                                                                                            <li><i class="icon-check pr-10"></i> 2. Text</li>
                                                                                            <li><i class="icon-check pr-10"></i> 3. Color</li>
                                                                                            <li><i class="icon-check pr-10"></i> 4. Size</li>
                                                                                            <li><i class="icon-check pr-10"></i> 5. Shape</li>
                                                                                            <li><i class="icon-check pr-10"></i> 6. Level of Detail</li>
                                                                                            <li><i class="icon-check pr-10"></i> The Summary Card—handy description    of your data</li>
                                                                                            <li><i class="icon-check pr-10"></i> Headers and Axes</li>
                                                                                            <li><i class="icon-check pr-10"></i> Titles, Captions, Field Labels and  Legends</li>
                                                                                            <li><i class="icon-check pr-10"></i> Format values in your views</li>
                                                                        
                                                                                        </ul>    
                                                                                        <h5 class="heading-font">3. Organize the data in your Views</h5>
                                                                                        <ul class="list-icons" id="clist">
                                                                                            <li><i class="icon-check pr-10"></i> Sort views for quick comparison</li>
                                                                                            <li><i class="icon-check pr-10"></i> Filter views to find the right information</li>
                                                                                            <li><i class="icon-check pr-10"></i> Aggregations for measures—sums,  averages and more</li>
                                                                                            <li><i class="icon-check pr-10"></i> Use percentages to find the right ratios</li>
                                                                                            <li><i class="icon-check pr-10"></i> Spotlight your view to emphasize  important values</li>
                                                                                            <li><i class="icon-check pr-10"></i> Add totals and subtotals</li>
                                                                                            <li><i class="icon-check pr-10"></i> Create a motion chart after connecting   to a new data source</li>
                                                                        
                                                                        
                                                                                        </ul>    
                                                                                
                                                                                </div>
									</div>
								</div>
                                                      <div class="panel panel-default">
                                                          <div class="panel-heading">
                                                              <h4 class="panel-title">
                                                                  <a data-toggle="collapse" data-parent="#accordion-2" href="#collapseThree-2" class="collapsed">
                                                                      <i class="fa fa-arrow-right pr-10"></i>Module 3 (Expert)
                                                                  </a>
                                                              </h4>
                                                          </div>
                                                          <div id="collapseThree-2" class="panel-collapse collapse">
                                                              <div class="panel-body">
                                                                      
                                                                           
                                                                      <h5 class="heading-font">1. Calculations and Models</h5>
                                                                      <ul class="list-icons" id="clist">
                                                                          <li><i class="icon-check pr-10"></i> Quick Table Calculations</li>
                                                                          <li><i class="icon-check pr-10"></i> Custom Table Calculations using data in   your views</li>
                                                                          <li><i class="icon-check pr-10"></i> Reference Lines, Bands and Distributions</li>
                                                                          <li><i class="icon-check pr-10"></i> Model your data with Trend Lines</li>
                                                                          
                                                                      </ul>
                                                                      <h5 class="heading-font">2. Data management in Tableau</h5>
                                                                      <ul class="list-icons" id="clist">
                                                                          <li><i class="icon-check pr-10"></i> Data items: names, types, roles,  properties, attributes and hierarchies</li>
                                                                          <li><i class="icon-check pr-10"></i> View Data to see the data behind your   view</li>
                                                                          <li><i class="icon-check pr-10"></i> Bins to divide numeric data items</li>
                                                                          <li><i class="icon-check pr-10"></i> Group dimensions into categories</li>
                                                                          <li><i class="icon-check pr-10"></i> The power of sets to combine and filter   your views</li>
                                                                      </ul>
                                                                      <h5 class="heading-font">3. Advanced data management in Tableau</h5>
                                                                      <ul class="list-icons" id="clist">
                                                                          <li><i class="icon-check pr-10"></i> Calculated Fields, Functions, and  Parameters</li>
                                                                          <li><i class="icon-check pr-10"></i> Calculated Fields — power to answer  your difficult questions</li>
                                                                          <li><i class="icon-check pr-10"></i> Calculated Field Operators</li>
                                                                          <li><i class="icon-check pr-10"></i> Numeric Functions (Singular)</li>
                                                                          <li><i class="icon-check pr-10"></i> Character Functions (Modify Items)</li>
                                                                          <li><i class="icon-check pr-10"></i> Character Functions (Locate Values in   String)</li>
                                                                          <li><i class="icon-check pr-10"></i> Date Functions</li>
                                                                          <li><i class="icon-check pr-10"></i> Type Conversion Functions</li>
                                                                          <li><i class="icon-check pr-10"></i> Logical Functions (If, Then, Else)</li>
                                                                          <li><i class="icon-check pr-10"></i> Aggregate Functions</li>
                                                                          <li><i class="icon-check pr-10"></i> Table Calculation Functions</li>
                                                                          <li><i class="icon-check pr-10"></i> Parameters add additional control for  your analysis</li>
                                                                          
                                                                      </ul>
                                                                      <h5 class="heading-font">4. Data Blending</h5>
                                                                      <ul class="list-icons" id="clist">
                                                                          <li><i class="icon-check pr-10"></i> Queries to retrieve the data you need</li>
                                                                          <li><i class="icon-check pr-10"></i> Data blending to use data from multiple    sources in one view</li>
                                                                          <li><i class="icon-check pr-10"></i> Extracts to accelerate your data       exploration in Tableau</li>
                                                                          
                                                                          
                                                                      </ul>
                                                                      <h5 class="heading-font">5. Deploy and Publish workbooks</h5>
                                                                      <ul class="list-icons" id="clist">
                                                                          <li><i class="icon-check pr-10"></i> Export images to other applications</li>
                                                                          <li><i class="icon-check pr-10"></i> Export data to other applications or back    into Tableau</li>
                                                                          <li><i class="icon-check pr-10"></i> Print to PDF—export your views to    Adobe Reader format</li>
                                                                          <li><i class="icon-check pr-10"></i> Packaged Workbooks—take Tableau on    the road!</li>
                                                                          <li><i class="icon-check pr-10"></i> Tableau Reader—share packaged  workbooks with your colleagues</li>
                                                                          <li><i class="icon-check pr-10"></i> Tableau Server—powerful insights for   everyone!</li>
                                                                          <li><i class="icon-check pr-10"></i> Tableau Desktop and Tableau Server</li>
                                                                          <li><i class="icon-check pr-10"></i> Tableau Public—share your insights with   the world!</li>
                                                                          
                                                                          
                                                                      </ul>
                                                                      <h5 class="heading-font">6. Customer Specific Topics</h5>
                                                                      <ul class="list-icons" id="clist">
                                                                          <li><i class="icon-check pr-10"></i> Hyperlink Access</li>
                                                                          <li><i class="icon-check pr-10"></i> Regression Analysis</li>
                                                                          <li><i class="icon-check pr-10"></i> Forecasting</li>
                                                                          <li><i class="icon-check pr-10"></i> P/E Ratio</li>
                                                                          <li><i class="icon-check pr-10"></i> Calculations using Statistical Functions</li>
                                                                          <li><i class="icon-check pr-10"></i> Integration with R model</li>
                                                                          <li><i class="icon-check pr-10"></i> Social Media Analytics (Data from     Twitter , Facebook, etc..)</li>
                                                                          <li><i class="icon-check pr-10"></i> Best Practices</li>
                                                                          <li><i class="icon-check pr-10"></i> Performance Tuning / Tipsat Data and   Application Level</li>
                                                                          
                                                                          
                                                                      </ul>
                                                               
                                                              </div>
                                                          </div>
                                                      </div>
                                                      
                                                    </div>
                                                
                                                </div>

                                        </div>
                                       
                                    </div>
                                    <div class="tab-pane" id="h3tab3">
                                        <div class="row">
                                            <div class="col-md-12 ">
                                                <div class="panel-group collapse-style-3" id="accordion-3">
                                                    <div class="panel panel-default">
                                                        <div class="panel-heading">
                                                            <h4 class="panel-title">
                                                                <a data-toggle="collapse" data-parent="#accordion-3" href="#collapseOne-3">
                                                                    1.	Do PrwaTech provide corporate training for Tableau training and certification?
                                                                </a>
                                                            </h4>
                                                        </div>
                                                        <div id="collapseOne-3" class="panel-collapse collapse in">
                                                            <div class="panel-body">
                                                                <p id="cpara" class="text-justify">
                                                                    PrwaTech is India’s leading corporate training provider in various MNCs such as HCL, IBM, Accenture, Wipro, Crisil-Standard and Poors Company. PrwaTech provides corporate training to the candidates. It provides a strong platform to the engineers by mastering a wide range of skills. We at PrwaTech help and support the upcoming business entrepreneurs. We provide the most optimized solution for the business not only on a large scale but also at the small scale. 
                                                                </p>
                                                               
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="panel panel-default">
                                                        <div class="panel-heading">
                                                            <h4 class="panel-title">
                                                                <a data-toggle="collapse" data-parent="#accordion-3" href="#collapseTwo-3" class="collapsed">
                                                                    2.	Do PrwaTech provide online sessions?
                                                                </a>
                                                            </h4>
                                                        </div>
                                                        <div id="collapseTwo-3" class="panel-collapse collapse">
                                                            <div class="panel-body">
                                                                <p id="cpara" class="text-justify">
                                                                    Yes, you can register yourself for PrwaTech online classes. India’s leading provider for Tableau training and certification is the prestigious PrwaTech. Our highly skilled online trainer’s works hard to understand the problem and provide the most appropriate solution in any circumstances. Before considering any assignment PrwaTech thoroughly screen the professional. Verify all the documents, educational qualifications and work experience. PrwaTech consider every assignment equal whether it is a big assignment or small assignment. 
                                                                </p>
                                                                                        
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="panel panel-default">
                                                        <div class="panel-heading">
                                                            <h4 class="panel-title">
                                                                <a data-toggle="collapse" data-parent="#accordion-3" href="#collapseThree-3" class="collapsed">
                                                                   3.	What are the different kinds of project that I will complete during this training?
                                                                </a>
                                                            </h4>
                                                        </div>
                                                        <div id="collapseThree-3" class="panel-collapse collapse">
                                                            <div class="panel-body">
                                                                <p id="cpara" class="text-justify">
                                                                   As a part of Tableau training and certification you will be working on real time based projects. At the end of this training you have to submit a project. Our professional team will assist you with your project. You can think of any new idea and can start with a project of your own or can work on any of the live project provided by our trainers. Our professionals will assist you throughout your training and completion of the projects. The projects could be in banking, finance, social networking, insurance etc.  
                                                                </p>
                                                                                        
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="panel panel-default">
                                                        <div class="panel-heading">
                                                            <h4 class="panel-title">
                                                                <a data-toggle="collapse" data-parent="#accordion-3" href="#collapseFour-3" class="collapsed">
                                                                    4.	What if I have certain doubts after completing this training?
                                                                </a>
                                                            </h4>
                                                        </div>
                                                        <div id="collapseFour-3" class="panel-collapse collapse">
                                                            <div class="panel-body">
                                                                <p id="cpara" class="text-justify">
                                                                    After completing this training still if have any doubts you can either contact any of the faculty member. PrwaTech whole team will support you even if have completed your course. You can ask for live sessions or can ask for online sessions. You can go through the case studies or recorded sessions. Still if you are not comfortable you can ask for direct help from faculty member.
                                                                </p>
                                                                                        
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="panel panel-default">
                                                        <div class="panel-heading">
                                                            <h4 class="panel-title">
                                                                <a data-toggle="collapse" data-parent="#accordion-3" href="#collapseFive-3" class="collapsed">
                                                                    5.	What will be the placement criteria?
                                                                </a>
                                                            </h4>
                                                        </div>
                                                        <div id="collapseFive-3" class="panel-collapse collapse">
                                                            <div class="panel-body">
                                                                <p id="cpara" class="text-justify">
                                                                    PrwaTech has proved to be a paramount organization for its placement criteria. We have 95% of placement record in reputed companies and have organized 2000+ interviews in HCL, Infosys, IBM, Accenture etc. We have trained 2000+ students. Our professionals will provide you with several questions along with solutions that you might face in your interview.
                                                                </p>
                                                                                        
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="panel panel-default">
                                                        <div class="panel-heading">
                                                            <h4 class="panel-title">
                                                                <a data-toggle="collapse" data-parent="#accordion-3" href="#collapseSix-3" class="collapsed">
                                                                    Do You Provide Placement Assistance?
                                                                </a>
                                                            </h4>
                                                        </div>
                                                        <div id="collapseSix-3" class="panel-collapse collapse">
                                                            <div class="panel-body">
                                                                <p id="cpara" class="text-justify">
                                                                    Yes, Prwatech does provide you with placement assistance. We have tie-ups with 80+ organizations including Ericsson, Cisco, Cognizant, TCS, among others that are looking for tableau professionals and we would be happy to assist you with the process of preparing yourself for the interview and the job.
                                                                </p>
                                                                                        
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                            
                                            </div>
                                                    
                                        </div>
                                            
                                    </div>
                                    <div class="tab-pane" id="h3tab4">
                                        <div class="row object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
                                            <div class="col-md-4">
                                                <p id="cpara" class="text-default"><a href="">Click Here to Download</a></p>
                                                
                                            </div>
                                            <div class="col-md-8">
                                                 <h4 class="title text-default" class="heading-font">Bigdata and tableau Services</h4>
                                                <ul class="list-icons">
                                                    <li><i class="icon-check pr-10"></i> PowerPoint Presentation covering all classes</li>
                                                    <li><i class="icon-check pr-10"></i> Recorded Videos Sessions On Bigdata and tableau with LMS Access.(lifetime support)</li>
                                                    <li><i class="icon-check pr-10"></i> Quiz , Assignment & POC.</li>
                                                    <li><i class="icon-check pr-10"></i> On Demand Online Support .</li>
                                                    <li><i class="icon-check pr-10"></i> Discussion Forum.</li>
                                                    <li><i class="icon-check pr-10"></i> Material
                                                        <ul class="list-icons" style="margin-left:50px;">
                                                            <li>a. Sample Question papers of Cloudera Certification.</li>
                                                            <li>b. Technical Notes & Study Material.</li>
                                                        </ul>
                                                    
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        
                                    </div>
                                    <div class="tab-pane" id="h3tab5">
                                        <div class="row object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
                                            <div class="col-md-6 ">
                                                <div class="pv-20 ph-20 feature-box-2 boxed shadow object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
                                                    <div class="col-md-3 testimonial-image"><img src="../images/testimonial-2.jpg" /></div> 
                                                    <div class="body">
                                                        <h4 class="title">Anupam Khamparia 
                                                            <br>
                                                            <span style="font-size:11px;">Consultant, Cognizant Technology Solutions, Bangalore</span>
                                                        </h4>
                                                        <p class ="text-justify">“Excellent course and instructor. I learnt a lot in a short period. Good value for money. Instructor took us through Advanced tableau Development in depth.”</p>
                                                       
                                                    </div>
                                                </div>
                                            </div>
                                          <div class="col-md-6 ">
                                                <div class="pv-20 ph-20 feature-box-2 boxed shadow object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
                                                    <div class="col-md-3 testimonial-image"><img src="../images/testimonial-1.png" /></div> 
                                                    <div class="body">
                                                        <h4 class="title">Anukanksha Garg
                                                          <br>
                                                            <span style="font-size:11px;">B.Tech. CSE</span>
                                                        </h4>
                                                        <p class ="text-justify">It was a nice learning experience with prwatech. The classes were well scheduled and managed.
Verma has good understanding of the topics taught and catered to our problems and doubts very patiently. The best thing about him was that he handled the situations accordingly, when needed a friend, he became one and also a teacher to always guide us.</p>
                                                       
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6 ">
                                                <div class="pv-20 ph-20 feature-box-2 boxed shadow object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
                                                    <div class="col-md-3 testimonial-image"><img src="../images/testimonial-4.PNG" /></div> 
                                                    <div class="body">
                                                        <h4 class="title">Varun Shivashanmugum
                                                        
                                                        <br>
                                                            <span style="font-size:11px;">Associate Consultant, ITC Infotech Ltd</span>
                                                        </h4>
                                                        
                                                        <p class ="text-justify">“Faculty is good,Verma takes keen interest and personnel care in improvising skills in students. And most importantly,Verma will be available and clears doubts at any time apart from class hours. And he always keeps boosting and trying to increase confidence in his students which adds extra attribute to him and organization as well. and organization as well.</p>
                                                       
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6 ">
                                                <div class="pv-20 ph-20 feature-box-2 boxed shadow object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
                                                    <div class="col-md-3 testimonial-image"><img src="../images/testimonial-5.jpg" /></div> 
                                                    <div class="body">
                                                        <h4 class="title">Mayank Srivastava <br>
                                                        <span style="font-size:11px;">tableau Developer, L&T, Bangalore</span>
                                                        </h4>
                                                        <p class ="text-justify">“Really good course content and labs, patient enthusiastic instructor. Good instructor, with in depth skills…Very relevant practicals allowed me to put theory into practice.”</p>
                                                       
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                                 <div class="col-md-3" style="position:relative;top:-10px;">
                                     <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center" >
                                         <div class="overlay-container">
                                             <img src="../images/icon/curse-d1.png" alt="">
                                         </div>
                                         <div class="body">
                                             <h3 class="heading-font">Rs. 14,000 + Tax </h3>
                                             <span class="small-font">per 2 weeks</span>
                                             <div class="separator"></div>
                                             <p class="heading-font"></p>
                                             <table class="table table-striped ">
                                                 <tbody>
                                                     <tr><th><center>35 Hours</center></th></tr>
                                                 <tr><th><center>15 Seats</center></th></tr>
                                                 <tr><th><center>Course Badge</center></th></tr>
                                                 <tr><th><center>Course Certificate</center></th></tr>
                                                 <tr><th><center> <a  onclick="openNav()" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Enroll Now<i class="fa fa-arrow-right pl-10"></i></a></center></th></tr>
                                                 </tbody>
                                             </table>
                                            <?php include '../includes/demo-popup.php';?>
                                           
                                         </div>
                                     </div>
                                      <h3 class="heading-font text-center" style="background:#4c4c4c;color:white;padding-top:10px;padding-bottom:10px">Suggested Courses</h3> 
                                      <div class="owl-carousel content-slider">
                                          <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
                                              <div class="overlay-container">
                                                  <img src="../images/por-1.png" alt="">
                                                  <div class="overlay-to-top">
                                                      <p class="small margin-clear"></p>
                                                  </div>
                                              </div>
                                              <div class="body">
                                                  <h3 class="heading-font">Big Data tableau Training</h3>
                                                  <div class="separator"></div>
                                                  <p class="heading-font">Advanced Program for Top-notch Developers </p>
                                                  <a href="big-data-hadoop-training.php"  target="_blank" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read More<i class="fa fa-arrow-right pl-10"></i></a>												
                                              </div>
                                          </div>
                                          <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
                                              <div class="overlay-container">
                                                  <img src="../images/por-2.png" alt="">
                                                  <div class="overlay-to-top">
                                                      <p class="small margin-clear"></p>
                                                  </div>
                                              </div>
                                              <div class="body">
                                                  <h3 class="heading-font">tableau Admin Training</h3>
                                                  <div class="separator"></div>
                                                  <p class="heading-font">Advanced Program for Top-notch Developers </p>
                                                  <a href="hadoop-admin.php" target="_blank" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read More<i class="fa fa-arrow-right pl-10"></i></a>												
                                              </div>
                                          </div>
                                          <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
                                              <div class="overlay-container">
                                                  <img src="../images/por-3.png" alt="">
                                                  <div class="overlay-to-top">
                                                      <p class="small margin-clear"></p>
                                                  </div>
                                              </div>
                                              <div class="body">
                                                  <h3 class="heading-font">Apache Spark Scala Training</h3>
                                                  <div class="separator"></div>
                                                  <p class="heading-font">Apache Spark Scala Combo Training </p>
                                                  <a href="apache-spark-scala-training.php" target="_blank" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read More<i class="fa fa-arrow-right pl-10"></i></a>												
                                              </div>
                                          </div>
                                          <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
                                              <div class="overlay-container">
                                                  <img src="../images/por-4.png" alt="">
                                                  <div class="overlay-to-top">
                                                      <p class="small margin-clear"></p>
                                                  </div>
                                              </div>
                                              <div class="body">
                                                  <h3 class="heading-font">Data Science Training</h3>
                                                  <div class="separator"></div>
                                                  <p class="heading-font">Find out the truth about what Data Science is. </p>
                                                  <a href="data-science-course-training-bangalore.php" target="_blank" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read More<i class="fa fa-arrow-right pl-10"></i></a>												
                                              </div>
                                          </div>
                                          <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
                                              <div class="overlay-container">
                                                  <img src="../images/por-5.png" alt="">
                                                  <div class="overlay-to-top">
                                                      <p class="small margin-clear"></p>
                                                  </div>
                                              </div>
                                              <div class="body">
                                                  <h3 class="heading-font">R Programming Training</h3>
                                                  <div class="separator"></div>
                                                  <p class="heading-font">Business Analytics With R Training</p>
                                                  <a href="r-programing.php" target="_blank" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read More<i class="fa fa-arrow-right pl-10"></i></a>												
                                              </div>
                                          </div>
                                          <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
                                              <div class="overlay-container">
                                                  <img src="../images/por-6.png" alt="">
                                                  <div class="overlay-to-top">
                                                      <p class="small margin-clear"></p>
                                                  </div>
                                              </div>
                                              <div class="body">
                                                  <h3 class="heading-font"> Tableau Training & Certification</h3>
                                                  <div class="separator"></div>
                                                  <p class="heading-font">Speed on concepts of data visualization</p>
                                                  <a href="tableau.php" target="_blank" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read More<i class="fa fa-arrow-right pl-10"></i></a>												
                                              </div>
                                          </div>
                                          <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
                                              <div class="overlay-container">
                                                  <img src="../images/por-7.png" alt="">
                                                  <div class="overlay-to-top">
                                                      <p class="small margin-clear"></p>
                                                  </div>
                                              </div>
                                              <div class="body">
                                                  <h3 class="heading-font">Python Programming</h3>
                                                  <div class="separator"></div>
                                                  <p class="heading-font">Advanced Concepts for Top-notch Developers  </p>
                                                  <a href="python.php" target="_blank" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read More<i class="fa fa-arrow-right pl-10"></i></a>												
                                              </div>
                                          </div>
                                      </div> 
                                 </div> 
                            </div> 
                           
                           
                            <p></p>
                            
                        </section>
                     
                        
			<!-- section end -->
                    
			<div class="clearfix"></div>
			
			<!-- ================ -->
			
			<!-- section end -->
                        <section class="pv-30 dark-bg padding-bottom-clear clearfix object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
					<br>
					<div class="container">
						<div class="row">
							<div class="col-md-3 ">
								<div class="ph-20 feature-box text-center object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
									<span class="icon default-bg circle"><i class="fa fa-institution"></i></span>
									<h3>Live classes</h3>
									<div class="separator clearfix"></div>
                                                                        <p class="heading-font ">Live online and interactive classes conducted by instructor</p>
									
								</div>
							</div>
							<div class="col-md-3 ">
								<div class="ph-20 feature-box text-center object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
									<span class="icon default-bg circle"><i class="fa fa-user"></i></span>
									<h3>Expert instructions</h3>
									<div class="separator clearfix"></div>
									<p class="heading-font">Learn from our Experts and get Real-Time Guidance</p>
									
								</div>
							</div>
							<div class="col-md-3 ">
								<div class="ph-20 feature-box text-center object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
									<span class="icon default-bg circle"><i class="icon-signal"></i></span>
									<h3>24 X 7 Support</h3>
									<div class="separator clearfix"></div>
									<p class="heading-font">Personalized Guidance from our 24X7 Support Team</p>
									
								</div>
							</div>
							<div class="col-md-3 ">
								<div class="ph-20 feature-box text-center object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
									<span class="icon default-bg circle"><i class="icon-calendar"></i></span>
									<h3>Flexible schedule</h3>
									<div class="separator clearfix"></div>
									<p class="heading-font">Reschedule your Batch/Class at Your Convenience</p>
									
								</div>
							</div>
						</div>
					</div>
					<br>
				</section>
			
			<!-- section end -->

			<!-- section -->
			<!-- ================ -->
			 <section class="pv-30   padding-bottom-clear dark-translucent-bg parallax padding-bottom-clear" style="background: url('../images/bg/451.jpg') 50% -9px; box-shadow: rgba(0, 0, 0, 0.247059) 0px 2px 7px inset;">
				
				<div class="space-bottom">
					
					<div class="owl-carousel content-slider">
						<div class="container">
							<div class="row">
								<div class="col-md-8 col-md-offset-2">
									<div class="testimonial text-center">
										<div class="testimonial-image">
                                                                                    <img src="../images/testimonial-1.png" alt="" title="" class="img-circle">
										</div>
										
										<div class="testimonial-body">
                                                                                    <p></p>
												<p id="cpara">It was a nice learning experience with prwatech. The classes were well scheduled and managed.
Verma has good understanding of the topics taught and catered to our problems and doubts very patiently. The best thing about him was that he handled the situations accordingly, when needed a friend, he became one and also a teacher to always guide us.</p>
											
											<div class="testimonial-info-1">Anukanksha Garg</div>
											<div class="testimonial-info-2">B.TECH- CS</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="container">
							<div class="row">
								<div class="col-md-8 col-md-offset-2">
									<div class="testimonial text-center">
										<div class="testimonial-image">
											<img src="../images/testimonial-2.jpg" alt="" title="" class="img-circle">
										</div>
										
										<div class="testimonial-body">
											 <p></p>
												<p id="cpara">Excellent course and instructor. I learnt a lot in a short period. Good value for money. Instructor took us through Advanced Hadoop Development in depth.</p>
											
											<div class="testimonial-info-1">Anupam Khamparia</div>
											<div class="testimonial-info-2">Consultant, Cognizant Technology Solutions, Bangalore</div>
										</div>
									</div>
								</div>
							</div>
						</div>
                                            <div class="container">
							<div class="row">
								<div class="col-md-8 col-md-offset-2">
									<div class="testimonial text-center">
										<div class="testimonial-image">
											<img src="../images/testimonial-3.jpg" alt="" title="" class="img-circle">
										</div>
										
										<div class="testimonial-body">
											<p></p>
												<p id="cpara">Excellent course and instructor. I learnt a lot in a short period. Good value for money. Instructor took us through Advanced Hadoop Development in depth.</p>
											<div class="testimonial-info-1">Kumar Waibhav</div>
                                                                                        <div class="testimonial-info-2">Software Engineer, Cognizant Technology Solutions,<br> Philadelphia, Pennsylvania, USA</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					
				</div>
			</section>
			<!-- section end -->

			<!-- section start -->
			<!-- ================ -->
                        
			                   
			<!-- section end -->
			<!-- footer start (Add "dark" class to #footer in order to enable dark footer) -->
			<!-- ================ -->
			<?php include './../includes/footer.php'; ?>
			<!-- footer end -->
			
		</div>
		<!-- page-wrapper end -->

		<?php include './../includes/jslinks.php';?>
		<!-- Color Switcher End -->
                    <?php include './../includes/quick-query.php';?>
                 <?php include './../includes/UserSignup.php';?>
	</body>

        <script>
            $(document).ready(function(){
                onlad(6);
            });  
        </script>

</html>

