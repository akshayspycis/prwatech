<?php include '../includes/session.php'; ?>
<!DOCTYPE html>
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <title>PrwaTech | About Us </title>
        <meta name="title" content="Big Data Hadoop Online Training | Hadoop Certification Course | Prwatech" />
        <meta name="keywords" content="hadoop training, online hadoop training, hadoop training classes, hadoop course online, big data training, big data course, big data online course, hadoop tutorial, HDFS training, Yarn training, MapReduce training, Pig training, Hive training, HBase training" />
        <meta name="description" content="Our Big Data Hadoop Certification Training helps you master HDFS, Yarn, MapReduce, Pig, Hive, HBase with use cases on Retail, Social Media, Aviation, Finance, Tourism domain" />
        <meta name="description" content="PrwaTech provides big data Hadoop Training Classes for beginners and developers with versatile carrier options. Know more about courses visit our website.">
        <meta name="author" content="prwatech">
        <meta name="google-site-verification" content="fSzBVSEUMu0l5lnD0qBVGv5F_16zaI6xiUZFMm-iMqQ" />  
            <?php include '../includes/csslinks.php'; ?>
          
    </head>
    
    <!-- body classes:  -->
    <!-- "boxed": boxed layout mode e.g. <body class="boxed"> -->
    <!-- "pattern-1 ... pattern-9": background patterns for boxed layout mode e.g. <body class="boxed pattern-1"> -->
    <!-- "transparent-header": makes the header transparent and pulls the banner to top -->
    <body class="no-trans front-page transparent-header ">
        <!-- scrollToTop -->
        <!-- ================ -->
        <div class="scrollToTop circle"><i class="icon-up-open-big"></i></div>
        
        <!-- page wrapper start -->
        <!-- ================ -->
        <div class="page-wrapper">
            
            <!-- header-container start -->
			<?php include '../includes/header.php' ;  ?>
            <!-- header-container end -->
            
            <!-- banner start -->
            <!-- ================ -->
            <div class="banner dark-translucent-bg" style="background-image:url('../images/page-about-banner-2.png'); background-position: 50% 27%;">
                <!-- breadcrumb start -->
                <!-- ================ -->
                <div class="breadcrumb-container object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
                    <div class="container">
                        <ol class="breadcrumb">
                            <li><i class="fa fa-home pr-10"></i><a class="link-dark" href="index.html">Home</a></li>
                            <li class="active">About Us</li>
                            
                        </ol>
                    </div>
                </div>
                <!-- breadcrumb end -->
                <div class="container">
                    
                </div>
            </div>
            <!-- banner end -->
            
            <div id="page-start"></div>
            
            <!-- section start -->
            <!-- ================ -->
            <section class="light-gray-bg pv-30 clearfix"  style="background:#f8f8f8 url('../images/bg/bigbg4.png') no-repeat;background-position:right center;box-shadow:inset 0 2px 7px rgba(0,0,0, 0.25);">
                <div class="container">
                    <div class="row">
                        <div class="col-md-8">
                            <h2 class="text-center heading-font" style="text-transform:none;">About <strong> Prwatech </strong></h2>
                            <div class="separator"></div>
                            <p class="cpara2">Ever since the ancient times education plays an important role in our life, one cannot separate education from technology.  In ancient period students lived in proximity to the Guru for completing their education. But today, training lays the foundation for an engineer, parents and students have to make constant efforts to search for the best qualified professional training institutions. </p>
                            <p class="cpara2">If you are searching for the best qualified professional training institution, PrwaTech is a one stop place for you. PrwaTech provides corporate training to the candidates. It provides a strong platform to the engineers by mastering a wide range of skills. </p>
                        </div>
                        <div class="col-md-4">
                            <p>&nbsp;</p>
                            <div class="overlay-container overlay-visible" style="box-shadow:0 6px 10px rgba(0,0,0, 0.25);">
                                <img src="../images/training/aboutSmall1.png" />
                                
                                <a href="../images/training/aboutBig1.jpg" class="popup-img overlay-link" ><i class="icon-plus-1"></i></a>
                            </div>
                            
                            
                        </div>
                        
                    </div>
                    <p>&nbsp;</p>
                </div>
            </section>
            <section class="pv-30 clearfix aboutrow2" id="row1" >
                <div class="container">
                    <div class="row">
                        <p>&nbsp;</p>
                        <div class="col-md-4">
                            
                            <div class="overlay-container overlay-visible" style="box-shadow:0 6px 10px rgba(0,0,0, 0.25);">
                                <img src="../images/training/IBM-small1.png"  width="100%"/>
                                
                                <a href="../images/training/IBM-big1.jpg" class="popup-img overlay-link" ><i class="icon-plus-1"></i></a>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <p class="cpara2">PrwaTech is India’s leading corporate training provider in various MNCs such as HCL, IBM, Accenture, Wipro, Crisil-Standard and Poors Company. It takes great pride in providing training for Big data and Hadoop programs. PrwaTech proved to be a paramount organization by conducting more than 50,000 hours of intense training in Big Data and Hadoop. PrwaTech provides training to the best industry professionals working at IBM, HCL, ITC and Accenture.</p>
                            <p class="cpara2">Today for bright future IT skills are important. Whether you want to learn basic Java, ERP, Dot net, Date Warehousing and Business IntelligencePrwaTech will match your learning requirement. Before considering any assignment PrwaTech thoroughly screen the professional. Verify all the documents, educational qualifications and work experience. PrwaTech consider every assignment equal whether it is a big assignment or small assignment. </p>
                        </div>
                    </div>
                    <p>&nbsp;</p>
                </div>
            </section>
            <section class="light-gray-bg pv-30 clearfix"  style="background:#f8f8f8 url('../images/bg/bigbg4.png') no-repeat;background-position:right center;box-shadow:inset 0 2px 7px rgba(0,0,0, 0.25);">
                <div class="container">
                    <div class="row">
                        <p>&nbsp;</p>
                        <div class="col-md-8">
                            <p class="cpara2">Understanding the accurate problem is far more important than actually solving it. PrwaTech highly skilled professional team of software engineers and solution providers strive to help individual by proving the accurate technology. Our highly skilled team works together to understand the problem and provide the best effective business offering in any circumstances. We at PrwaTech help and support the upcoming business entrepreneurs. We provide the most optimized solution for the business not only on a large scale but also at the small scale. </p>
                            <p class="cpara2">PrwaTech builds a training course that is designed primarily for programmers. It has its training centers at silicon cities such as Bangalore. Btm, Marathahalli and Jayanagar training centers provides comfortable access to the best training foundations in overall India. </p>
                        </div>
                        <div class="col-md-4">
                            
                            <div class="overlay-container overlay-visible" style="box-shadow:0 6px 10px rgba(0,0,0, 0.25);">
                                <img src="../images/training/aboutSmall2.png" />
                                
                                <a href="../images/training/aboutBig2.jpg" class="popup-img overlay-link" ><i class="icon-plus-1"></i></a>
                            </div>
                            
                            
                        </div>
                        
                    </div>
                    <p>&nbsp;</p>
                </div>
            </section>
            <section class="pv-30 clearfix aboutrow2" id="row1" >
                <div class="container">
                    <div class="row">
                        <div class="col-md-8 col-md-offset-2">
                            <h2 class="text-center heading-font" style="text-transform:none">At a <strong>Glance</strong></h2>
                            <div class="separator"></div>
                            
                            <br>
                        </div>
                        <p>&nbsp;</p>
                        <div class="col-md-7">
                            
                            <div class="overlay-container overlay-visible" style="box-shadow:0 6px 10px rgba(0,0,0, 0.25);">
                                <img src="../images/training/IMG-20161126-WA0058.jpg"  width="100%"/>
                                
                                <a href="../images/training/IMG-20161126-WA0058.jpg" class="popup-img overlay-link" ><i class="icon-plus-1"></i></a>
                            </div>
                        </div>
                        <div class="col-md-5">
                            <p class="large cpara2">Some important factors why you should choose PRWATECH are as follows:</p>
                            <ul class="list-icons cpara2">
                                <li class="object-non-visible" data-animation-effect="fadeInUpSmall" data-effect-delay="100"><i class="icon-check-1"></i>Get trained by the finest qualified professionals</li>
                                <li class="object-non-visible" data-animation-effect="fadeInUpSmall" data-effect-delay="150"><i class="icon-check-1"></i>100% practical training</li>
                                <li class="object-non-visible" data-animation-effect="fadeInUpSmall" data-effect-delay="200"><i class="icon-check-1"></i>Adjustable timings</li>
                                <li class="object-non-visible" data-animation-effect="fadeInUpSmall" data-effect-delay="250"><i class="icon-check-1"></i>Flexible timings</li>
                                <li class="object-non-visible" data-animation-effect="fadeInUpSmall" data-effect-delay="250"><i class="icon-check-1"></i>Weekend and Weekdays batches</li>
                                <li class="object-non-visible" data-animation-effect="fadeInUpSmall" data-effect-delay="250"><i class="icon-check-1"></i>Wi-fi classrooms</li>
                                <li class="object-non-visible" data-animation-effect="fadeInUpSmall" data-effect-delay="250"><i class="icon-check-1"></i>Affordable fees</li>
                                <li class="object-non-visible" data-animation-effect="fadeInUpSmall" data-effect-delay="250"><i class="icon-check-1"></i>Complete course support </li>
                                <li class="object-non-visible" data-animation-effect="fadeInUpSmall" data-effect-delay="250"><i class="icon-check-1"></i>Guidance till you reach your goal. </li>
                            </ul>
                            
                            
                        </div>
                    </div>
                    <p>&nbsp;</p>
                </div>
            </section>
            <section class="light-gray-bg pv-30 clearfix"  style="background:#f8f8f8 url('../images/bg/overlay.png') repeat;box-shadow:inset 0 2px 7px rgba(0,0,0, 0.25);">
                <div class="container">
                    <div class="row">
                        <div class="col-md-10 col-md-offset-1">
                            <h2 class="text-center heading-font" style="text-transform:none">Our <strong>Clients</strong></h2>
                            <div class="separator"></div>
                            <p class="cpara2">PrwaTech provides training to the best industry professionals working at IBM, HCL, ITC and Accenture and many more.</p>   
                            <br>
                            
                            <p>&nbsp;</p>
                            <div class="col-md-3">
                                <img src="../images/client/ibm.png" style="box-shadow:0 6px 10px rgba(0,0,0, 0.25);"/>
                            </div>
                            <div class="col-md-3">
                                <img src="../images/client/amdocs.png" style="box-shadow:0 6px 10px rgba(0,0,0, 0.25);"/>
                            </div>
                            <div class="col-md-3">
                                <img src="../images/client/capgimini.png" style="box-shadow:0 6px 10px rgba(0,0,0, 0.25);"/>
                            </div>
                            <div class="col-md-3">
                                <img src="../images/client/flipkart.png" style="box-shadow:0 6px 10px rgba(0,0,0, 0.25);"/>
                            </div>
                            <p>&nbsp;</p>
                            <div class="col-md-3">
                                <img src="../images/client/hcl.png" style="box-shadow:0 6px 10px rgba(0,0,0, 0.25);"/>
                            </div>
                            <div class="col-md-3">
                                <img src="../images/client/syntel.png" style="box-shadow:0 6px 10px rgba(0,0,0, 0.25);"/>
                            </div>
                            
                            <div class="col-md-3">
                                <img src="../images/client/ciber.png" style="box-shadow:0 6px 10px rgba(0,0,0, 0.25);"/>
                            </div>
                            <div class="col-md-3">
                                <img src="../images/client/sungaurd.png" style="box-shadow:0 6px 10px rgba(0,0,0, 0.25);"/>
                            </div>
                            <p>&nbsp;</p>
                            
                            <div class="col-md-3">
                                <img src="../images/client/synchron.png" style="box-shadow:0 6px 10px rgba(0,0,0, 0.25);"/>
                            </div>
                            <div class="col-md-3">
                                <img src="../images/client/yodlee.png" style="box-shadow:0 6px 10px rgba(0,0,0, 0.25);"/>
                            </div>
                            
                            <p>&nbsp;</p>
                        </div>
                        <p>&nbsp;</p>
                    </div>
            </section>
            
            <!-- section end -->
            
            <!-- section -->
            <!-- ================ -->
		
            <!-- section end -->
            
            <!-- section start -->
            <!-- ================ -->
            
            
            <!-- section end -->
            <!-- footer start (Add "dark" class to #footer in order to enable dark footer) -->
            <!-- ================ -->
			<?php include '../includes/footer.php'; ?>
            <!-- footer end -->
            
        </div>
        <!-- page-wrapper end -->
        
		<?php include '../includes/jslinks.php';?>
        <!-- Color Switcher End -->
                <?php include '../includes/quick-query.php';?> 
                <?php include '../includes/UserSignup.php';?>
        <script type="text/javascript" src="../plugins/magnific-popup/jquery.magnific-popup.min.js"></script>
    </body>
    
    
</html>


