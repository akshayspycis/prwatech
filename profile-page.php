<?php include 'includes/session.php'; ?>
<!DOCTYPE html>
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
	<!--<![endif]-->
        <head>
            <meta charset="utf-8">
            <title>Welcome to <?php echo $_SESSION['user_name']; ?></title>
            <meta charset="utf-8">
                
                <meta name="description" content="PRWATECH offers big data Hadoop Training Bangalore. Learn Big Data Hadoop Training  &amp; Hadoop Certification Courses with concepts of Apache Hadoop &amp; clusters." />
                <meta property="og:type" content="article" />
                <meta property="og:title" content="Home" />
                <meta property="og:url" content="http://prwatech.in/" />
                <meta property="og:site_name" content="BigData &amp; HADOOP" />
                <meta name="twitter:card" content="summary" />
                <meta name="google-site-verification" content="fSzBVSEUMu0l5lnD0qBVGv5F_16zaI6xiUZFMm-iMqQ" />  
                <!-- Mobile Meta -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0">

		<!-- Favicon -->
                <link rel="shortcut icon" href="images/fevicon.JPG">

		<!-- Web Fonts -->
		<link href='http://fonts.googleapis.com/css?family=Roboto:400,300,300italic,400italic,500,500italic,700,700italic' rel='stylesheet' type='text/css'>
		<link href='http://fonts.googleapis.com/css?family=Raleway:700,400,300' rel='stylesheet' type='text/css'>
		<link href='http://fonts.googleapis.com/css?family=Pacifico' rel='stylesheet' type='text/css'>
		<link href='http://fonts.googleapis.com/css?family=PT+Serif' rel='stylesheet' type='text/css'>

		<!-- Bootstrap core CSS -->
		<link href="bootstrap/css/bootstrap.css" rel="stylesheet">

		<!-- Font Awesome CSS -->
		<link href="fonts/font-awesome/css/font-awesome.css" rel="stylesheet">

		<!-- Fontello CSS -->
		<link href="fonts/fontello/css/fontello.css" rel="stylesheet">

		<!-- Plugins -->
		<link href="plugins/magnific-popup/magnific-popup.css" rel="stylesheet">
		<link href="plugins/rs-plugin/css/settings.css" rel="stylesheet">
		<link href="css/animations.css" rel="stylesheet">
		<link href="plugins/owl-carousel/owl.carousel.css" rel="stylesheet">
		<link href="plugins/owl-carousel/owl.transitions.css" rel="stylesheet">
		<link href="plugins/hover/hover-min.css" rel="stylesheet">		
		<!-- the project core CSS file -->
		<link href="css/style.css" rel="stylesheet" >
                <!-- Style Switcher Styles (Remove these two lines) -->
		<!-- Custom css --> 
		<link href="css/custom.css" rel="stylesheet"> 
                <script type="text/javascript" src="plugins/jquery.min.js"></script>
        </head>

	<!-- body classes:  -->
	<!-- "boxed": boxed layout mode e.g. <body class="boxed"> -->
	<!-- "pattern-1 ... pattern-9": background patterns for boxed layout mode e.g. <body class="boxed pattern-1"> -->
	<!-- "transparent-header": makes the header transparent and pulls the banner to top -->
	<body class="no-trans front-page transparent-header ">
            <!-- scrollToTop -->
		<!-- ================ -->
		<div class="scrollToTop circle"><i class="icon-up-open-big"></i></div>
		
		<!-- page wrapper start -->
		<!-- ================ -->
		<div class="page-wrapper">
		
			<!-- header-container start -->
			    
<div class="header-container">
                
    <!-- ================ -->
    <div class="header-top" id="bg-blue">
        <div class="bg-fullscreen">
            <div class="container">
                <div class="row">
                    <div class="col-xs-3 col-sm-6 col-md-8">
                        <!-- header-top-first start -->
                        <!-- ================ -->
                        <div class="header-top-first clearfix">
                            <ul class="list-inline hidden-sm hidden-xs contact-detail">
                                <li><i class="fa fa-phone pr-5 pl-10"></i> +91 8147111254 </li>
                                <li><i class="fa fa-envelope-o pr-5 pl-10"></i> info@prwatech.in</li>
                                <li><i class="fa fa-desktop pr-5 pl-10"></i><a href="blog/index.php">Blog</a></li>
                            </ul>
                        </div>
                        <!-- header-top-first end -->
                    </div>
                    <div class="col-xs-9 col-sm-6 col-md-4">
                
                        <!-- header-top-second start -->
                        <!-- ================ -->
                        <div id="header-top-second"  class="clearfix">
                
                            <!-- header top dropdowns start -->
                            <!-- ================ -->
            <?php
            if(isset($_SESSION['user_id'])){
            if($_SESSION['user_type']==='normal'){
            ?>
                            <div class="header-top-dropdown text-right">
                                <div class="btn-group ">
                                    <p style="position:relative;top:8px;right:5px;font-family:verdana;"><?php echo ""." "."<b>".$_SESSION['user_name']."</b>"; ?><p>
                                </div>  
                                <div class="btn-group">
                                    <a href="profile-page.php" class="btn  btn-login btn-sm"><b>Profile</b></a>
                                </div>  
                                <div class="btn-group">
                                    <a href="logout.php" class="btn  btn-login btn-sm"><b>Sign Out</b></a>
                                </div>  
                            </div>
                
            <?php
                
            }else{
            header("Location:iiadmin/home.php");
            }
            }else{
            ?>
                            <div class="header-top-dropdown text-right">
                                <div class="btn-group">
                                    <a  class="btn btn-signup btn-sm"  data-toggle="modal" data-target="#mySignUp"><i class="fa fa-user pr-10"></i> Sign Up</a>
                                </div>
                                <div class="btn-group dropdown">
                                    <button type="button" class="btn  btn-login btn-sm" data-toggle="modal" data-target="#mySignIn"><i class="fa fa-lock pr-10"></i> Login</button>
                
                                </div>
                            </div>
                
            <?php
            }
            ?>
                            <!--  header top dropdowns end -->
                        </div>
                        <!-- header-top-second end -->
                    </div>
                </div>
            </div>
        </div>
                
    </div>
                
    <!-- ================ --> 
    <header class="header  fixed   clearfix" style="box-shadow: 0 4px 10px rgba(0,0,0, 0.50);">
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <!-- header-left start -->
                    <!-- ================ -->
                    <div class="header-left clearfix">
                
                        <!-- logo -->
                        <div id="logo" class="logo">
                            <a href="index.php"><img id="logo_img" src="images/logo_light_blue.png" alt="Prwatech"></a>
                        </div>
                        <div class="site-slogan" style="font-family:century gothic;">
                            &nbsp;&nbsp;Share Ideas, Start Something Good.
                        </div>
                
                    </div>
                    <!-- header-left end -->
                
                </div>
                <div class="col-md-9">
                
                    <!-- header-right start -->
                    <!-- ================ -->
                    <div class="header-right clearfix">
                
                        <!-- main-navigation start -->
                        <!-- classes: -->
                        <!-- "onclick": Makes the dropdowns open on click, this the default bootstrap behavior e.g. class="main-navigation onclick" -->
                        <!-- "animated": Enables animations on dropdowns opening e.g. class="main-navigation animated" -->
                        <!-- "with-dropdown-buttons": Mandatory class that adds extra space, to the main navigation, for the search and cart dropdowns -->
                        <!-- ================ -->
                        <div class="main-navigation  animated with-dropdown-buttons">
                
                            <!-- navbar start -->
                            <!-- ================ -->
                            <nav class="navbar navbar-default" role="navigation">
                                <div class="container-fluid">
                
                                    <!-- Toggle get grouped for better mobile display -->
                                    <div class="navbar-header">
                                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse-1">
                                            <span class="sr-only">Toggle navigation</span>
                                            <span class="icon-bar"></span>
                                            <span class="icon-bar"></span>
                                            <span class="icon-bar"></span>
                                        </button>
                
                                    </div>
                
                                    <!-- Collect the nav links, forms, and other content for toggling -->
                                    <div class="collapse navbar-collapse" id="navbar-collapse-1">
                                        <!-- main-menu -->
                                        <ul class="nav navbar-nav " style="font-family:century gothic">
                                            <li>
                                                <a  href="index.php">Home</a>
                                            </li>
                                            <li>
                                                <a  href="about/">About Us</a>
                                            </li>
                
                                            <!-- mega-menu start -->
                
                                            <!-- mega-menu end -->
                                            <li class="dropdown ">
                                                <a class="dropdown-toggle" data-toggle="dropdown" href="#">Courses</a>
                                                <ul class="dropdown-menu">
                                                    <li class="dropdown ">
                                                        <a  class="dropdown-toggle" data-toggle="dropdown" href="big-data-hadoop-training/index.php">Big Data Hadoop Training </a>
                                                        <ul class="dropdown-menu">
                                                            <li ><a href="big-data-hadoop-training/index.php">Bigdata Hadoop Training </a></li>
                                                            <li ><a href="big-data-hadoop-training-in-pune/index.php">Bigdata Hadoop Training in Pune</a></li>
                                                            <li ><a href="big-data-hadoop-training/index.php">Bigdata Hadoop Online Training </a></li>
                                                            <li ><a href="big-data-hadoop-training/index.php">Bigdata Hadoop Online self Training</a></li>
                                                        </ul>
                                                    </li>
                                                    <li class="dropdown ">
                                                        <a  class="dropdown-toggle" data-toggle="dropdown" href="hadoop-admin/index.php">Hadoop Admin</a>
                                                        <ul class="dropdown-menu">
                                                            <li ><a href="hadoop-admin/index.php">Class Room Training</a></li>
                                                            <li ><a href="hadoop-admin-online-course/index.php">Hadoop Admin Online Course</a></li>
                                                            <li ><a href="hadoop-admin/index.php">Hadoop Admin Online Self Training</a></li>
                                                        </ul>
                                                    </li>
                                                    <li ><a href="apache-spark-scala-training-2/index.php">Apache Spark Scala Training</a></li>
                                                    <li ><a href="data-science-course-training-bangalore/index.php">Data Science Course</a></li>
                                                    <li ><a href="r-programing/index.php">R Programming</a></li>
                                                    <li ><a href="tableau/index.php">Tableau</a></li>
                                                    <li ><a href="python-online-course/index.php">Python</a></li>
                                                    <li ><a href="sastraining/index.php">SAS Training</a></li>
                                                </ul>
                                            </li>
                                            <!-- mega-menu start -->													
                
                                            <!-- mega-menu end -->
                                            <li class="dropdown ">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Services</a>
                                                <ul class="dropdown-menu">
                                                    <li><a  href="services/corporate-training.php">Corporate Training</a></li>
                                                    <li><a  href="services/online-softwares.php">Hadoop Softwares</a></li>
                                                    <li><a  href="services/workshop-on-bigdata-hadoop.php">Workshop on Bigdata & Hadoop</a></li>
                                                    <li ><a href="#">LMS</a></li>
                                                    <li ><a href="services/placement-assistance.php">Placement Assistance</a></li>
                                                </ul>
                                            </li>
                                            <li ><a href="careers/index.php">Career</a></li>
                
                                            <li ><a href="gallery/index.php">Gallery</a></li>
                                            <li ><a href="contact/index.php">Contact</a></li>
                
                                        </ul>
                                        <!-- main-menu end -->
                
                                        <!-- header dropdown buttons -->
                                        <div class="header-dropdown-buttons hidden-xs ">
                                            <div class="btn-group dropdown">
                                                <button type="button" class="btn dropdown-toggle" data-toggle="dropdown"><i class="icon-search"></i></button>
                                                <ul class="dropdown-menu dropdown-menu-right dropdown-animation" id="search-form">
                                                    <li>
                                                        <form role="search" class="search-box margin-clear">
                                                            <div class="form-group has-feedback">
                                                                <input type="text" class="form-control" placeholder="Search" style="background:rgba(0,0,0,0.7);color:white">
                                                                <i class="icon-search form-control-feedback"></i>
                                                            </div>
                                                        </form>
                                                    </li>
                                                </ul>
                                            </div>
                
                                        </div>
                                        <!-- header dropdown buttons end-->
                
                                    </div>
                
                                </div>
                            </nav>
                            <!-- navbar end -->
                
                        </div>
                        <!-- main-navigation end -->	
                    </div>
                    <!-- header-right end -->
                
                </div>
            </div>
        </div>
                
    </header>
    <!-- header end -->
</div>
                        
                        
			<!-- header-container end -->
		
			<!-- banner start -->
			<!-- ================ -->
			<div class="banner dark-translucent-bg" style="background-image:url('images/corporate-1-slider-slide-2.jpg'); background-position: 50% 27%;">
				<!-- breadcrumb start -->
				<!-- ================ -->
				<div class="breadcrumb-container object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
					<div class="container">
						<ol class="breadcrumb">
							<li><i class="fa fa-home pr-10"></i><a class="link-dark" href="index.html">Home</a></li>
							<li class="active">Profile</li>
							
						</ol>
					</div>
				</div>
				<!-- breadcrumb end -->
				<div class="container">
					
				</div>
			</div>
			<!-- banner end -->
			
			<div id="page-start"></div>

			<!-- section start -->
			<!-- ================ -->
                        <section class="light-gray-bg pv-30 clearfix"  style="background:#f8f8f8 ;box-shadow:inset 0 2px 7px rgba(0,0,0, 0.25);">
                            <div class="container">
                                
                        </section>
                        
                        <div class="container">
                                 <h2 class="text-center heading-font object-non-visible animated object-visible fadeInDownSmall" data-animation-effect="fadeInDownSmall" data-effect-delay="100" style="text-transform:none;"> <strong><?php echo ""." "."<b>".$_SESSION['user_name']."</b>"; ?></strong> </h2>
                                 <div id="vseparator" class="object-non-visible animated object-visible fadeInDownSmall" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
                                     <div class="vhr"></div>
                                 </div>
                                <div class="row col-md-12">
                                <ul class="nav nav-tabs style-3" id="tab-course" role="tablist">
                                    <li class="active"><a href="#h3tab1" role="tab" data-toggle="tab">Profile Details</a></li>
                                    <li><a href="#h3tab2" role="tab" data-toggle="tab">Order Details</a></li>
                                </ul>
                                <!-- Tab panes -->
                                <div class="tab-content">
                                    <div class="tab-pane in active " id="h3tab1">
                                           <div class="form-block center-block p-30 light-gray-bg border-clear">
                            <h2 class="title">Update Your Profile</h2>
                            <form class="form-horizontal userSignUp" id="updUserRegistration" method="post" role="form">
                                <div class="form-group has-feedback">
                                    <label for="inputName" class="col-sm-3 control-label">Full Name <span class="text-danger small">*</span></label>
                                    <div class="col-sm-8">
                                        <input type="hidden" class="form-control" id="user_id" name="user_id" placeholder="Your Name" >
                                        <input type="text" class="form-control" id="user_name" name="user_name" >
                                        <i class="fa fa-pencil form-control-feedback"></i>
                                    </div>
                                </div>

                                <div class="form-group has-feedback">
                                    <label for="inputEmail" class="col-sm-3 control-label">Email <span class="text-danger small">*</span></label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" id="email" name="email" placeholder="Email" >
                                        <i class="fa fa-envelope form-control-feedback"></i>
                                    </div>
                                </div>
                                <div class="form-group has-feedback">
                                    <label for="inputEmail" class="col-sm-3 control-label">Contact <span class="text-danger small">*</span></label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" id="contact_no" name="contact_no" placeholder="Contact No." >
                                        <i class="fa fa-phone form-control-feedback"></i>
                                    </div>
                                </div>

                                <div class="form-group has-feedback">
                                    <label for="inputPassword" class="col-sm-3 control-label">Old Password <span class="text-danger small">*</span></label>
                                    <div class="col-sm-8">
                                        <input type="password" class="form-control" id="password" name="password" placeholder="Password" >
                                        <i class="fa fa-lock form-control-feedback"></i>
                                    </div>
                                </div>
                                <div class="form-group has-feedback">
                                    <label for="inputPassword" class="col-sm-3 control-label">New Password <span class="text-danger small">*</span></label>
                                    <div class="col-sm-8">
                                        <input type="password" class="form-control" id="newpassword" name="newpassword" placeholder="New Password" >
                                        <i class="fa fa-lock form-control-feedback"></i>
                                    </div>
                                </div>
<!--                                <div class="form-group has-feedback">
                                    <label for="inputPassword" class="col-sm-3 control-label">Dob <span class="text-danger small">*</span></label>
                                    <div class="col-sm-8">
                                        <input type="date" class="form-control" id="dob" name="dob" placeholder="" >
                                        <i class="fa fa-calendar form-control-feedback"></i>
                                    </div>
                                </div>-->

            <!--                    <div class="form-group">
                                    <div class="col-sm-offset-3 col-sm-8">
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" checked name="terms" > Accept our <a href="#">privacy policy</a> and <a href="#">customer agreement</a>
                                            </label>
                                        </div>
                                    </div>
                                </div>-->
                                <div class="form-group">
                                    <div class="col-sm-offset-3 col-sm-8">
                                        <button type="submit" name="submit" id="submit" class="btn btn-group btn-default btn-animated">Sign Up <i class="fa fa-check"></i></button>
                                        <button type="button" class="btn  btn-dark" data-dismiss="modal">Close</button>
                                    </div>
                                </div>
                                <div class="validate" style="color:red;font-family:verdana;font-weight:bold"></div>
                            </form>
                            <div id="errorMsg" style="color:red;font-family:verdana;font-weight:bold"></div>
                            <div id="successMsg" style="color:green;font-family:verdana;font-weight:bold"></div>
                        </div>         
                                    </div>
                                    <div class="tab-pane" id="h3tab2" >
                                        <div class="row" id="vacancies_post">
                                        <div class="col-md-2">
                                            <div  style="box-shadow:0 6px 10px rgba(0,0,0, 0.25);margin-top:15px;">
                                                <img src="images/enquery.png" />
                                            </div>
                                        </div>
                                        <div class="col-md-8">
                                            <h3 class="heading-font"><b class="text-red">Course</b> : Hadoop Developer with 3+ years of experience</h3>
                                            <p><small>Posted on : <b>16 Dec 2016</b> </small>  </p>
                                            <p><small>Subject : <b>Regagess asjdasd asdjh</b> </small>  </p>
                                            <p style="font-family:verdana"><b>Message</b>:3+yrs experience in Bigdata Hadoop Development as well as in Production Support.
                                                Must Have working experience on Java related projects
                                                Must have hands on experience on followings – Map Reduce, Hbase, Hive,
                                                Spark, Scala, Oozie, Sqoop
                                                Good knowledge on Troubleshooting issues with data ingestion, management
                                                & transformation.
                                            </p>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="separator"></div>
                                    </div>
                                    </div>
                                    <div class="tab-pane" id="h3tab3">
                                            
                                    </div>
                                    <div class="tab-pane" id="h3tab4">
                                        
                                    </div>
                                    <div class="tab-pane" id="h3tab5">
                                        
                                    </div>
                                </div>
                            </div>
                                 
                            </div>
			   
			<!-- section end -->

			<!-- section -->
			<!-- ================ -->
		
			<!-- section end -->

			<!-- section start -->
			<!-- ================ -->
                        
			                   
			<!-- section end -->
			<!-- footer start (Add "dark" class to #footer in order to enable dark footer) -->
			<!-- ================ -->
			<footer id="footer" class="clearfix ">
<!--                            <div class="footer-upper-top">
                                
                            </div>-->
                            <div class="footer-upper">
                                
                            </div>
<!--                            <div id="footer-bottom" >
                                
                            </div>-->
				<!-- .footer start -->
				<!-- ================ -->
				<div class="footer" style="background:url('images/bg/3a.png');background-position:bottom center; box-shadow:inset 0 6px 10px rgba(0,0,0, 0.25);">
					<div class="container">
						<div class="footer-inner">
							<div class="row">
								<div class="col-md-3">
									<div class="footer-content">
                                                                            <div class="logo-footer"><img id="logo-footer" src="images/logo_light_blue.png" alt=""/></div>
                                                                            <p class="heading-font"style="text-align:justify">Training lays the foundation for an engineer.
                                                                                It provides a strong platform to build ones perception and implementation by mastering a wide range of skills .
                                                                                One of India’s leading and largest training provider for Big Data and Hadoop Corporate training programs is the prestigious PrwaTech. <a href="#">Learn More<i class="fa fa-long-arrow-right pl-5"></i></a></p>
										<div class="separator-2"></div>
										<nav>
										 <a href="terms-and-conditions.php">Terms, Conditions & Privacy</a>	
										</nav>
									</div>
								</div>
								<div class="col-md-3">
									<div class="footer-content">
										<h2 class="title">Latest From Blog</h2>
										<div class="separator-2"></div>
										<div class="media margin-clear">
											<div class="media-left">
												<div class="overlay-container">
                                                                                                    <img class="media-object" src="images/por-1.png" alt="blog-thumb">
													<a href="blog-post.html" class="overlay-link small"><i class="fa fa-link"></i></a>
												</div>
											</div>
											<div class="media-body">
												<h6 class="media-heading heading-font"><a href="blog-post.html">Learn about the Fundamentals of Hadoop Development</a></h6>
												<p class="small margin-clear"><i class="fa fa-calendar pr-10"></i>Mar 23, 2015</p>
											</div>
											<hr>
										</div>
										<div class="media margin-clear">
											<div class="media-left">
												<div class="overlay-container">
                                                                                                    <img class="media-object" src="images/por-1.png" alt="blog-thumb">
													<a href="blog-post.html" class="overlay-link small"><i class="fa fa-link"></i></a>
												</div>
											</div>
											<div class="media-body">
												<h6 class="media-heading heading-font"><a href="blog-post.html">Learn about the Fundamentals of Hadoop Development</a></h6>
												<p class="small margin-clear"><i class="fa fa-calendar pr-10"></i>Mar 23, 2015</p>
											</div>
											<hr>
										</div>
										<div class="media margin-clear">
											<div class="media-left">
												<div class="overlay-container">
                                                                                                    <img class="media-object" src="images/por-1.png" alt="blog-thumb">
													<a href="blog-post.html" class="overlay-link small"><i class="fa fa-link"></i></a>
												</div>
											</div>
											<div class="media-body">
												<h6 class="media-heading heading-font"><a href="blog-post.html">Learn about the Fundamentals of Hadoop Development</a></h6>
												<p class="small margin-clear"><i class="fa fa-calendar pr-10"></i>Mar 23, 2015</p>
											</div>
											<hr>
										</div>
										
										<div class="text-right space-top">
											<a href="#" class="link-dark"><i class="fa fa-plus-circle pl-5 pr-5"></i>More</a>	
										</div>
									</div>
								</div>
								<div class="col-md-3">
									<div class="footer-content">
										<h2 class="title">Find Us In Bangalore</h2>
										<div class="separator-2"></div>
                                                                                <p class="heading-font"><b>PRWATECH </b><br>
                                                                                    
                                                                                    No.14, 29th Main , 2nd Cross, V.P road <br>
                                                                                    BTM-1st Stage, Behind AXA building,<br>
                                                                                    Land Mark : Vijaya Bank ATM <br>
                                                                                    Bangalore – 560 068<br>
                                                                                    Land Line no : 080-416 456 25<br>
                                                                                    Mobile no :+91 8147111254<br>
                                                                                    Mail ID : info@prwatech.in</p>
									
										<div class="separator-2"></div>
											<ul class="social-links circle animated-effect-1">
											<li class="facebook"><a target="_blank" href="https://www.facebook.com/JustHadoop"><i class="fa fa-facebook"></i></a></li>
											<li class="twitter"><a target="_blank" href=" https://twitter.com/prwatech"><i class="fa fa-twitter"></i></a></li>
											<li class="googleplus"><a target="_blank" href="https://plus.google.com/+PrwatechBigDataHadoopTrainingCoursesBengaluru"><i class="fa fa-google-plus"></i></a></li>
											<li class="linkedin"><a target="_blank" href="https://www.linkedin.com/in/prwatech"><i class="fa fa-linkedin"></i></a></li>
											<li class="xing"><a target="_blank" href="https://www.youtube.com/channel/UCwAaWqnH2MqikDMpb1jBspw"><i class="fa fa-youtube"></i></a></li>
										</ul>
									</div>
								</div>
								<div class="col-md-3">
									<div class="footer-content">
										<h2 class="title">Find Us In Pune</h2>
										<div class="separator-2"></div>
                                                                               <p class="heading-font"><b>PRWATECH </b><br>
                                                                                    
                                                                                   Shiv sai apartment<br> Ground Floor ,  <br>
                                                                                   Pandari nagar  <br>
                                                                                    Kharadi  <br>
                                                                                    Land Mark : Radisson blu<br> Pune : 411014<br>
                                                                                    Land Line no : 080-416 456 25<br>
                                                                                    Mobile no :+91 8147111254<br>
                                                                                    Mail ID : info@prwatech.in</p>
										<div class="separator-2"></div>
                                                                               
									</div>
								</div>
							</div>
						</div>
					</div>
                                  
				</div>
				<!-- .footer end -->

				<!-- .subfooter start -->
				<!-- ================ -->
				<div class="subfooter" id="bg-subfooter" >
                                    <div id="footer-bottom">
                                        
                                    </div> 
                                    <div class="bg-fullscreen">
                                        <div class="container">
						<div class="subfooter-inner">
							<div class="row">
								<div class="col-md-12">
									<p class="text-center">Copyright © 2016 Prwatech. All Rights Reserved</p>
								</div>
							</div>
						</div>
					</div>
                                    </div>
					
				</div>
				<!-- .subfooter end -->

</footer>



  


			<!-- footer end -->
			
		</div>
		<!-- page-wrapper end -->

		<!-- page-wrapper end -->

		<!-- JavaScript files placed at the end of the document so the pages load faster -->
		<!-- ================================================== -->
		<!-- Jquery and Bootstap core js files -->
		<script type="text/javascript" src="plugins/jquery.min.js"></script>
                <script type="text/javascript" src="bootstrap/js/bootstrap.min.js"></script>

		<!-- Modernizr javascript -->
		<script type="text/javascript" src="plugins/modernizr.js"></script>

		<!-- jQuery Revolution Slider  -->
		<script type="text/javascript" src="plugins/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
		<script type="text/javascript" src="plugins/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
                 <!-- Isotope javascript -->
		<script type="text/javascript" src="plugins/isotope/isotope.pkgd.min.js"></script>
		
		<!-- Magnific Popup javascript -->
<!--		<script type="text/javascript" src="plugins/magnific-popup/jquery.magnific-popup.min.js"></script>-->
		
		<!-- Appear javascript -->
		<script type="text/javascript" src="plugins/waypoints/jquery.waypoints.min.js"></script>

		<!-- Count To javascript -->
		<script type="text/javascript" src="plugins/jquery.countTo.js"></script>
		
		<!-- Parallax javascript -->
		<script src="plugins/jquery.parallax-1.1.3.js"></script>

		<!-- Contact form -->
		<script src="plugins/jquery.validate.js"></script>
                    <!-- Background Video -->
		<script src="plugins/vide/jquery.vide.js"></script>

		<!-- Owl carousel javascript -->
		<script type="text/javascript" src="plugins/owl-carousel/owl.carousel.js"></script>
		<!-- SmoothScroll javascript -->
		<script type="text/javascript" src="plugins/jquery.browser.js"></script>
		<script type="text/javascript" src="plugins/SmoothScroll.js"></script>
<!-- Initialization of Plugins -->
		<script type="text/javascript" src="js/template.js"></script>

		<!-- Custom Scripts -->
		<script type="text/javascript" src="js/custom.js"></script>
		<!-- Color Switcher (Remove these lines) -->
		<script type="text/javascript" src="style-switcher/style-switcher.js"></script>
                
               
		<script type="text/javascript">
          course_batch_details={}
          
            function onlad(course_id){
                /* Ajax call for Product Display*/
                    $.ajax({
                    type:"post",
                    url:"server/controller/SelCourseBatchDetails.php",
                    data:{'course_id':course_id},
                    success: function(data) {
                      var duce = jQuery.parseJSON(data);
                        $("#batch_details").empty();
                        $.each(duce, function (index, article) {
                            course_batch_details[article.course_batch_details_id]={};
                            course_batch_details[article.course_batch_details_id]["course_id"]=article.course_id;
                            course_batch_details[article.course_batch_details_id]["date"]=article.date;
                            course_batch_details[article.course_batch_details_id]["day_type"]=article.day_type;
                            course_batch_details[article.course_batch_details_id]["day"]=article.day;
                            course_batch_details[article.course_batch_details_id]["time"]=article.time;
                            course_batch_details[article.course_batch_details_id]["fees"]=article.fees;
                            course_batch_details[article.course_batch_details_id]["discount"]=article.discount;
                            var btn_class="btn-danger";
                           if(article.status=="Enable"){
                               btn_class="btn-success";
                           }
                           var d = new Date(article.date);
                           var n = d.getDate();
                           var m = d.getMonth();
                           var month_name=["","Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"]
                          $("#batch_details").append($("<div>").addClass("col-md-3")
                                  .append($("<div>").addClass("col-md-3")
                                        .append($("<div>").addClass("row")
                                            .append($("<p>").attr({'id':'date-time'}).append(n)
                                                .append($("<br>"))
                                                .append($("<span>").attr({'id':'month'}).append(month_name[m].toUpperCase()))
                                            )
                                        )
                                   )
                                  .append($("<div>").addClass("col-md-9").attr({'id':'upcomming-batch'})
                                    .append($("<div>").addClass("row")
                                        .append($("<div>").addClass("col-md-12")
                                            .append($("<p>").attr({'id':'dt-content'})
                                                .append($("<b>").append(article.day_type+" - "+article.day)
                                                    .append($("<br>"))
                                                    .append($("<small>").append(article.time+" IST" +" (30 Hrs.)")
                                                        .append($("<br>"))
                                                        .append("Rs. "+article.fees+" ")
                                                        .append($("<a>").css({'cursor':'pointer'}).append(" Enroll Now").click(function (){
                                                            showDemoModel(course_id)
                                                        }))
                                                    )
                                                )
                                           )
                                        )
                                    )
                                 ));
                          
                          
                        });
                    }
                });
                }
                function showDemoModel (course_id){
                     openNav();
                    $("#mySidenav").find("form").each(function(){
                            this.reset();
                    });
                    $("#mySidenav").find("#course_id").val(course_id);
                }
                
      </script>
      
    
		<!-- Color Switcher End -->
                 <div class="modal fade" id="demoVideoPopUp" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header ">
                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                <h4 class="modal-title" id="myModalLabel">Demo Session at Prwatech</h4>
                            </div>
                            <div class="modal-body">
                               <iframe width="570" height="350" src="https://www.youtube.com/embed/xA5TAalZt40" frameborder="0" allowfullscreen></iframe> 
                              
                            </div>
                            <div class="modal-footer">
                                
                            </div>
                        </div>
                    </div>
                </div>
                    <?php include 'includes/quick-query_1.php';?>
                    <?php include 'includes/UserSignup_1.php';?>
	</body>
<!--        <script type="text/javascript">
            career={};
           $(document).ready(function(){
                 $.ajax({
                    type:"post",
                    url:"server/controller/SelCareers.php",
                    success: function(data) {
                        career={};
                       var duce = jQuery.parseJSON(data);
                        $('#careers_details').empty();
                        $.each(duce, function (index, article) {
                            career[article.career_id]={};
                            career[article.career_id]["heading"]=article.heading;
                            career[article.career_id]["content"]=article.content;
                            career[article.career_id]["city"]=article.city;
                            career[article.career_id]["date"]=article.date;
                            career[article.career_id]["posted_on"]=article.posted_on;
                            career[article.career_id]["image"]=article.image;
                           var img= "server/controller/"+article.image;
                            $('#careers_details').append($("<div>").addClass("row").attr({'id':'vacancies_post'})
                                            .append($("<div>").addClass("col-md-2")
                                                .append($("<div>").css({'box-shadow':'0 6px 10px rgba(0,0,0, 0.25)','margin-top':'15px'})
                                                        .append($("<img>").attr({'src':img}))
                                                       )
                                                   )
                                            .append($("<div>").addClass("col-md-8")
                                                .append($("<h3>").addClass("heading-font")
                                                        .append(article.heading)
                                                       )
                                                .append($("<p>")
                                                        .append($("<small>").append("Posted on : ").append($("<b>").append(article.date)))
                                                       )
                                                .append($("<p>").css({'font-family':'verdana'})
                                                        .append(article.content)
                                                       )
                                               )
                                            .append($("<div>").addClass("col-md-2")
                                                .append($("<p>"))
                                                .append($("<button>").addClass("btn btn-warning")
                                                    .append("Apply Now")
                                                    .click(function (){
                                                        showApplyModel(article.career_id)
                                                     }))
                                                )
                                            .append($("<div>").addClass("clearfix"))
                                    )
                        });
                    }
                });
          });  
         function showApplyModel (career_id){
                    $("#demoPopUp").modal("show");
                    $("#demoPopUp").find("form").each(function(){
                            this.reset();
                    });
                    $("#demoPopUp").find("#career_id").val(career_id);
                }
        </script>-->

        	<!-- header-container start -->
			<?php include './includes/demo-popup.php' ;  ?>
                <script type="text/javascript" language="javascript">
        /* Ajax call for Edit Customer */
                 $('#updUserRegistration').submit(function() {
                            var emailfilter = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
                                      var genderfilter = /^[a-zA-Z]+$/;
                                     var contactfilter =  /^\d{10}$/;
                                        var passwordfilter = /^\d{10}$/;
                                     if(this.user_name.value == ""){
                                         $(".validate").addClass("text-danger").fadeIn(100).text("Please fill valid name").prepend("<span class=\"glyphicon glyphicon-remove text-danger\">").fadeOut(1000);;
                                          $('#updUserRegistration').find('#user_name').focus();
                                        return false;
                                       }else if(this.email.value == ""){
                                         $(".validate").addClass("text-danger").fadeIn(100).text("Please fill date of Birth").prepend("<span class=\"glyphicon glyphicon-remove text-danger\">").fadeOut(1000);;
                                           $('#updUserRegistration').find('#email').focus();
                                         return false;
                                         
                                     }
                                     else if(this.contact_no.value == "" || !contactfilter.test(this.contact_no.value)){
                                         $(".validate").addClass("text-danger").fadeIn(100).text("Please fill valid contact no.").prepend("<span class=\"glyphicon glyphicon-remove text-danger\">").fadeOut(1000);
                                         $('#updUserRegistration').find('#contact_no').focus();
                                         return false;
                                         
                                     }
                                     else if(this.password.value == "" || this.password.value.match(/^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])([a-zA-Z0-9]{8,})$/)){
                                         $(".validate").addClass("text-danger").fadeIn(100).text("Please fill Old  password").prepend("<span class=\"glyphicon glyphicon-remove text-danger\">").fadeOut(1000);;
                                         $('#updUserRegistration').find('#password').focus();
                                         return false;
                                     }
                                     else if(this.newpassword.value == ""|| this.newpassword.value.match(/^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])([a-zA-Z0-9]{8,})$/)){
                                         $(".validate").addClass("text-danger").fadeIn(100).text("Please fill New password").prepend("<span class=\"glyphicon glyphicon-remove text-danger\">").fadeOut(1000);;
                                         $('#updUserRegistration').find('#newpassword').focus();
                                         return false;
                                     }
                                     else{
                                            $.ajax({
                                            type:"post",
                                            url:"server/controller/updateUser.php",
                                            data:$('#updUserRegistration').serialize(),
                                            success: function(data){ 
                                                if(data.trim()=="Error"){
                                                    alert("There is System error.");
                                                }else if(data.trim()=="1"){
                                                    alert("Porile Details Update Successfully but password does not match.");
                                                }else if(data.trim()=="2"){
                                                    alert("Porile Details Update Successfully");
                                                }
                                                $('#updUserRegistration').each(function(){
                                                this.reset();
                                                location.reload(true);
                                                $('#UserRegistration').modal('hide');
                                                return false;
                                                });
                                            } 
                                            });
                                            return false;
                                     }
                              
                      
                   
    });  
                    

$(document).ready(function() {
              onlad();
            });  
    
function onlad(){
                     $.ajax({
                    type:"post",
                    data:{'user_id':'<?php echo $_SESSION['user_id']; ?>'},
                    url:"server/controller/selectUsers.php",
                    success: function(data) {
                        cus={};
                       var duce = jQuery.parseJSON(data);
                        $("#data tr:has(td)").remove();
                        $.each(duce, function (index, article) {
                                cus[article.user_id]={};
                                cus[article.user_id]["user_name"]=article.user_name;
                                cus[article.user_id]["dob"]=article.dob;
                                cus[article.user_id]["email"]=article.email;
                                cus[article.user_id]["contact_no"]=article.contact_no;
                                cus[article.user_id]["date"]=article.date;
                                cus[article.user_id]["pic"]=article.pic;
                                cus[article.user_id]["user_profile_details_id"]=article.user_profile_details_id;
                                cus[article.user_id]["user_type"]=article.user_type;
                                cus[article.user_id]["count"]=article.count;
//                              cus[article.user_id]["password"]=article.password;
                                var img= "../server/controller/"+article.pic;
                                $("#updUserRegistration").find("#user_id").val(article.user_id);  
                                $("#updUserRegistration").find("#user_name").val(article.user_name); 
                                $("#updUserRegistration").find("#email").val(article.email);  
                                $("#updUserRegistration").find("#contact_no").val(article.contact_no);  
                                show(article.contact_no,article.email);
                        });
                    }
                });
         }
</script>
<script type="text/javascript" language="javascript">
//...........................................................................................................
function show(contact_no,email){ 
    $("#h3tab2").empty();
//              alert("success");
                /* Ajax call for Product Display*/
                    $.ajax({
                    type:"post",
                    data:{'contact_no':contact_no,'email':email},
                    url:"server/controller/SelUserQuery.php",
                    success: function(data) {
                      var duce = jQuery.parseJSON(data);
                        $("#h3tab2").empty();
                        $.each(duce, function (index, article) {
                      $('#h3tab2').append($("<div>").addClass("row").attr({'id':'vacancies_post'})
                                            .append($("<div>").addClass("col-md-2")
                                                .append($("<div>").css({'box-shadow':'0 6px 10px rgba(0,0,0, 0.25)','margin-top':'15px'})
                                                        .append($("<img>").attr({'src':'images/enquery.png'}))
                                                       )
                                                   )
                                            .append($("<div>").addClass("col-md-8")
                                                .append($("<h3>").addClass("heading-font")
                                                        .append("Course :<b>"+article.course+"</b>")
                                                       )
                                                .append($("<p>")
                                                        .append($("<small>").append("Posted on : ").append($("<b>").append(article.date)))
                                                       )
                                                .append($("<p>")
                                                        .append($("<small>").append("Subject : ").append($("<b>").append(article.subject)))
                                                       )
                                                .append($("<p>").css({'font-family':'verdana'})
                                                        .append("Message :<b>"+article.message+"</b>")
                                                       )
                                               )
                                            .append($("<div>").addClass("clearfix"))
                                    )
                     
                        });
                    }
                });
                }
</script>

</html>

