<?php include '../includes/session.php'; ?>
<!DOCTYPE html>
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
	<!--<![endif]-->
        <head>
            <meta charset="utf-8">
            <title>PrwaTech | R-Programming  Classes Pune &amp; Bangalore </title>
            <meta name="title" content="Data Science Certification Training - R Programming" />
            <meta name="keywords" content="Data Scientist Certification, Data Science Certification, Data Scientist Training, Data Science Training" />
            <meta name="description" content="his Data Science training course helps you master the predictive modelling techniques for big data analytics you need for Data Scientist certification.Become an expert in data science with R as well as predictive and descriptive analytics techniques like regression, clustering, association, classification, and hypothesis testing." />
            <meta name="author" content="prwatech">
            <meta name="google-site-verification" content="fSzBVSEUMu0l5lnD0qBVGv5F_16zaI6xiUZFMm-iMqQ" />  
                <?php include '../includes/csslinks.php'; ?>
        </head>

	<!-- body classes:  -->
	<!-- "boxed": boxed layout mode e.g. <body class="boxed"> -->
	<!-- "pattern-1 ... pattern-9": background patterns for boxed layout mode e.g. <body class="boxed pattern-1"> -->
	<!-- "transparent-header": makes the header transparent and pulls the banner to top -->
	<body class="no-trans front-page transparent-header ">
            <!-- scrollToTop -->
		<!-- ================ -->
		<div class="scrollToTop circle"><i class="icon-up-open-big"></i></div>
		
		<!-- page wrapper start -->
		<!-- ================ -->
		<div class="page-wrapper">
		
			<!-- header-container start -->
			<?php include '../includes/header.php' ;  ?>
			<!-- header-container end -->
		
			<!-- banner start -->
			<!-- ================ -->
			<div class="banner dark-translucent-bg" style="background-image:url('../images/page-course-banner-5.jpg'); background-position: 50% 27%;">
				<!-- breadcrumb start -->
				<!-- ================ -->
				<div class="breadcrumb-container object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
					<div class="container">
						<ol class="breadcrumb">
							<li><i class="fa fa-home pr-10"></i><a class="link-dark" href="index.html">Home</a></li>
							<li class="active">All Courses</li>
							<li class="active">R – Programming</li>
						</ol>
					</div>
				</div>
				<!-- breadcrumb end -->
				<div class="container">
					
				</div>
			</div>
			<!-- banner end -->
			
			<div id="page-start"></div>

			<!-- section start -->
			<!-- ================ -->
			<section class="light-gray-bg pv-30 clearfix object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100" id="cdrow1">
				<div class="container">
					<div class="row">
						<div class="col-md-8">
							 <h2 class="text-center heading-font" style="text-transform:none;"> <strong>R – Programming</strong> Training</h2>
                                                  
							<div class="separator"></div>
                                                        <p class="large text-justify heading-font">The Data Science with R training has been designed to prepare you for a job in the analytics space. R is the most used programming language today in data science and analytics space. It is open source in nature and very powerful, hence quickly becoming the language of choice of data scientists around the world. With a vibrant community, number of statistical packages and data visualization tools R is a must have tool for every data scientist. With the immense skill gap for analytics professionals this data science certification is the first step in this field.</p>
                                                        <p class="large text-justify heading-font">This R course has been structured for someone new to the field of analytics. This data science training will make you an expert at understanding the problem, designing the analysis and applying predictive modelling techniques using R to derive business insights from data.</p>
                                                </div>
						<div class="col-md-4">
                                                    <img src="../images/por-5.png"/>
                                                 </div>
                                            <p>&nbsp;</p>
                                            <div class="separator"></div>
                                            <h2 class="text-center heading-font object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100" style="text-transform:none;">Contact Us <strong>+91 8147111254 </strong></h2>
                                            <div class="separator"></div>
                                            <p>&nbsp;</p>
					</div>
                                         
                               
				</div>
                             <div class="container">
                                <div class="row" id="batch_details">
                                    <div class="col-md-3">
                                        <div class="col-md-3">
                                            <div class="row">
                                                <p id ="date-time">21 <br><span id="month">NOV</span></p>
                                            </div>
                                       </div>
                                        <div class="col-md-9" id="upcomming-batch">
                                            <div class="row">
                                                <div class="col-md-12"><p id="dt-content"><b> Mon-Wed (3 Days) <br><small>&nbsp; <br> Rs. 14000/- <a href="#" class="" data-toggle="modal" data-target="#demoPopUp">Enroll Now</a></small></b></p></div>
                                                
                                            </div>
                                       </div>
                                    </div>
                                  <div class="col-md-3">
                                        <div class="col-md-3">
                                            <div class="row">
                                                <p id ="date-time">24 <br><span id="month">NOV</span></p>
                                            </div>
                                       </div>
                                        <div class="col-md-9" id="upcomming-batch">
                                            <div class="row">
                                                <div class="col-md-12"><p id="dt-content"><b> Mon-Wed (3 Days) <br><small>&nbsp; <br> Rs. 14000/- <a href="#" class="" data-toggle="modal" data-target="#demoPopUp">Enroll Now</a></small></b></p></div>
                                                
                                            </div>
                                       </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="col-md-3">
                                            <div class="row">
                                                <p id ="date-time">27 <br><span id="month">NOV</span></p>
                                            </div>
                                       </div>
                                        <div class="col-md-9" id="upcomming-batch">
                                            <div class="row">
                                                <div class="col-md-12"><p id="dt-content"><b> Mon-Wed (3 Days) <br><small>&nbsp; <br> Rs. 14000/- <a href="#" class="" data-toggle="modal" data-target="#demoPopUp">Enroll Now</a></small></b></p></div>
                                                
                                            </div>
                                       </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="col-md-3">
                                            <div class="row">
                                                <p id ="date-time">30 <br><span id="month">NOV</span></p>
                                            </div>
                                       </div>
                                        <div class="col-md-9" id="upcomming-batch">
                                            <div class="row">
                                                <div class="col-md-12"><p id="dt-content"><b> Mon-Wed (3 Days) <br><small>&nbsp; <br> Rs. 14000/- <a href="#" class="" data-toggle="modal" data-target="#demoPopUp">Enroll Now</a></small></b></p></div>
                                                
                                            </div>
                                       </div>
                                    </div>
                                </div>
                            </div>
			</section>
                        
                          <section id="coursed-row2" class="object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
                            <div class="container">
                                
                                 <h2 class="text-center heading-font object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100" style="text-transform:none;"> <strong>Course</strong> Details</h2>
                                 <div id="vseparator" class="object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
                                     <div class="vhr"></div>
                                </div>
                                <div class="row col-md-9" >
                               <!-- tabs start -->
                                <!-- ================ -->
                                <!-- Nav tabs -->
                                <ul class="nav nav-tabs style-3" id="tab-course" role="tablist">
                                    <li class="active"><a href="#h3tab1" role="tab" data-toggle="tab">Overview</a></li>
                                    <li><a href="#h3tab2" role="tab" data-toggle="tab">Course Curriculum</a></li>
                                    <li><a href="#h3tab3" role="tab" data-toggle="tab">FAQ's</a></li>
                                    <li><a href="#h3tab4" role="tab" data-toggle="tab">Download Course Outline</a></li>
                                    <li><a href="#h3tab5" role="tab" data-toggle="tab">Reviews</a></li>
                                   
                                </ul>
                                <!-- Tab panes -->
                                <div class="tab-content">
                                    <div class="tab-pane in active " id="h3tab1">
                                        <div class="row">
                                            <div class="col-md-12 object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
                                                <h5 class="heading-font">About R – Programming</h5>
                                                <p id="cpara">
                                                    It is an analytical tool that helps and assist to analyses statistical information in a highly efficient manner. It makes it easy to analyze the huge data where the traditional fails in providing relevant and reliable result .
                                                </p>
                                                    
                                                <h5 class="heading-font">Learn to become expert business analyst</h5>
                                                <p id="cpara">
                                                    Business enterprises can analyses a data of nearly thirty millions of rows in sixty variables within 10 minutes of time . The speed of analysis is very quick and it produces good reliable scalable results that can be implemented in business development . It will provide the best view and all aspects of the business. A sophisticated an powerful tool is developed and it can be customized to meet industry requirements . It used by statisticians for computing statistics and analysis of information and highly integrated graphics.
                                                </p>
                                                <h5 class="heading-font">The main objectives of the R – Programming</h5>
                                                <ul class="list-icons" id="clist">
                                                    <li><i class="icon-check pr-10"></i> Students learn R – Programming language , data visualization concepts exploration of statistical concepts</li>
                                                    <li><i class="icon-check pr-10"></i>Learn fundamentals concepts of business analytics</li>
                                                    <li><i class="icon-check pr-10"></i>Learn various analytics like descriptive analytics, predictive analytics, prescriptive analytics, and different types of business analytics.</li>
                                                    <li><i class="icon-check pr-10"></i> Supply chain analytics</li>
                                                    <li><i class="icon-check pr-10"></i> Web analytics</li>
                                                    <li><i class="icon-check pr-10"></i> Real time Case Studies</li>
                                                    <li><i class="icon-check pr-10"></i> All in all students are guided through the real time aspects of R Programming efficiently by the experienced and expert professionals who have profound practical knowledge in the field.</li>
                                                </ul>
                                                    
                                                <h5 class="heading-font">What are the benefits of Learning R Programming ?</h5>
                                                <p id="cpara">
                                                    There is a tremendous business opportunity for the data analysts. Even the professionals can get benefits from R-programming to become expert business analysts. Certified data Scientists get very good pay pocket that is hefty.
                                                </p> 
                                                    
                                                <h5 class="heading-font">Who are eligible to take the course ?</h5>
                                                <p id="cpara">
                                                    Anybody who wishes to know and learn about R – Programming with the basic knowledge in coding and programming can join the course . Students and professionals who wish t explore the world of business analytics can join the course.
                                                </p> 
                                                    
                                            </div>
                                            
                                        </div>
                                            
                                    </div>
                                    <div class="tab-pane" id="h3tab2">
                                          <div class="row">
                                            <div class="col-md-12 ">
                                                  <div class="panel-group collapse-style-2" id="accordion-2">
								<div class="panel panel-default">
									<div class="panel-heading">
										<h4 class="panel-title">
											<a data-toggle="collapse" data-parent="#accordion-2" href="#collapseOne-2">
												<i class="fa fa-arrow-right pr-10"></i>R Course curriculum: 7 modules
											</a>
										</h4>
									</div>
                                                                    <div id="collapseOne-2" class="panel-collapse collapse in">
                                                                            <div class="panel-body">
                                                                            
                                                                            <p id="cpara">Modules covered include:</p>
                                                                
                                                                            <ul class="list-icons" id="clist">
                                                                                <li><i class="icon-check pr-10"></i> Module 1 – How R Works</li>
                                                                                <li><i class="icon-check pr-10"></i> Module 2 part 1 – What is R-Packages</li>
                                                                                <li><i class="icon-check pr-10"></i> Module 2 part 2 – What is Sorting</li>
                                                                                <li><i class="icon-check pr-10"></i> Module 2 part 3 – Transpose Function</li>
                                                                                <li><i class="icon-check pr-10"></i> Module 3 part 1 – Functions & Reading Data from External Files</li>
                                                                                <li><i class="icon-check pr-10"></i> Module 3 part 2 – Generating Plots and Pie Charts</li>
                                                                                <li><i class="icon-check pr-10"></i> Module 4 part 1 – Analysis of Variancy (ANOVA)</li>
                                                                                <li><i class="icon-check pr-10"></i> Module 4 part 2 – What is Cluster Analysis</li>
                                                                                <li><i class="icon-check pr-10"></i> Module 5 part 1 – Association Rule Mining Affinity Analysis</li>
                                                                                <li><i class="icon-check pr-10"></i> Module 5 part 2 – Two Variable Relationships</li>
                                                                                <li><i class="icon-check pr-10"></i> Module 6 Part 1 – Database connectivity & Logistic Regression</li>
                                                                                <li><i class="icon-check pr-10"></i> Module 6 Part 2 – ROC Curve in R</li>
                                                                                <li><i class="icon-check pr-10"></i> Module 7 –Integrating R with R-Programming</li>
                                                                            </ul>
                                                                        </div>
                                                                    </div>
								</div>
								
                                                    </div>
                                                
                                                </div>

                                        </div>
                                       
                                    </div>
                                    <div class="tab-pane" id="h3tab3">
                                        <div class="row">
                                            <div class="col-md-12 ">
                                                <div class="panel-group collapse-style-3" id="accordion-3">
                                                    <div class="panel panel-default">
                                                        <div class="panel-heading">
                                                            <h4 class="panel-title">
                                                                <a data-toggle="collapse" data-parent="#accordion-3" href="#collapseOne-3">
                                                                    1.	What are the various modes of training that are offered by PrwaTech?
                                                                </a>
                                                            </h4>
                                                        </div>
                                                        <div id="collapseOne-3" class="panel-collapse collapse in">
                                                            <div class="panel-body">
                                                                <p id="cpara" class="text-justify">
                                                                    PrwaTech offers three types of trainings- Classroom training, online training and corporate training. You can join online instructor-led training and self-paced training. PrwaTech is a leading organization that provides R program training and certification. All the trainers are well qualified with more than 6 years of experience in the same.  
                                                                </p>
                                                               
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="panel panel-default">
                                                        <div class="panel-heading">
                                                            <h4 class="panel-title">
                                                                <a data-toggle="collapse" data-parent="#accordion-3" href="#collapseTwo-3" class="collapsed">
                                                                   2.	What if I miss some of my classes?
                                                                </a>
                                                            </h4>
                                                        </div>
                                                        <div id="collapseTwo-3" class="panel-collapse collapse">
                                                            <div class="panel-body">
                                                                <p id="cpara" class="text-justify">
                                                                    Do not worry in case you miss some of your classes. PrwaTech well trained faculty member will help you at every step. In case you have missed some of your classes you have got two options to cope up with your syllabus. Firstly you can join any other live batch of your missed session or secondly you can go through our recorded sessions that are available in your LMS.
                                                                </p>
                                                                                        
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="panel panel-default">
                                                        <div class="panel-heading">
                                                            <h4 class="panel-title">
                                                                <a data-toggle="collapse" data-parent="#accordion-3" href="#collapseThree-3" class="collapsed">
                                                                    3.	Do you provide placement assistance?
                                                                </a>
                                                            </h4>
                                                        </div>
                                                        <div id="collapseThree-3" class="panel-collapse collapse">
                                                            <div class="panel-body">
                                                                <p id="cpara" class="text-justify">
                                                                    Yes, PrwaTech will help you with your placement. We have 95% of placement record in reputed companies and have organized 2000+ interviews in HCL, Infosys, IBM, Accenture etc. We have trained 2000+ students. Our professionals will provide you with several questions along with solutions that you might face in your interview.
                                                                </p>
                                                                                        
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="panel panel-default">
                                                        <div class="panel-heading">
                                                            <h4 class="panel-title">
                                                                <a data-toggle="collapse" data-parent="#accordion-3" href="#collapseFour-3" class="collapsed">
                                                                    4.	Do PrwaTech provide support session?
                                                                </a>
                                                            </h4>
                                                        </div>
                                                        <div id="collapseFour-3" class="panel-collapse collapse">
                                                            <div class="panel-body">
                                                                <p id="cpara" class="text-justify">
                                                                    PrwaTech proved to be a paramount organization by conducting more than 50,000 hours of intense training in R programming. PrwaTech provides you with recorded sessions and case studies. PrwaTech helps you at every step until you reach your goal. PrwaTech well qualified faculty members will help you until you reach your goal. After completion of this training still if you have any query you can any time call any of the faculty members and ask for the support.
                                                                </p>
                                                                                        
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="panel panel-default">
                                                        <div class="panel-heading">
                                                            <h4 class="panel-title">
                                                                <a data-toggle="collapse" data-parent="#accordion-3" href="#collapseFive-3" class="collapsed">
                                                                    5.	If am not from programming background then also, can I join R programming course?
                                                                </a>
                                                            </h4>
                                                        </div>
                                                        <div id="collapseFive-3" class="panel-collapse collapse">
                                                            <div class="panel-body">
                                                                <p id="cpara" class="text-justify">
                                                                   If you are not from programming background you can still join our R programming course. PrwaTech has many well designed courses like Java, Dotnet, C, C++ and linux. You can join these programs. These programs not only help you in R programming training instead it will definitely help you to boost your career.
                                                                </p>
                                                                                        
                                                            </div>
                                                        </div>
                                                    </div>
                                                   
                                                </div>
                                                            
                                            </div>
                                                    
                                        </div>
                                            
                                    </div>
                                    <div class="tab-pane" id="h3tab4">
                                        <div class="row object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
                                            <div class="col-md-4">
                                                <p id="cpara" class="text-default"><a href="">Click Here to Download</a></p>
                                                
                                            </div>
                                            <div class="col-md-8">
                                                 <h4 class="title text-default" class="heading-font">Bigdata and R-Programming Services</h4>
                                                <ul class="list-icons">
                                                    <li><i class="icon-check pr-10"></i> PowerPoint Presentation covering all classes</li>
                                                    <li><i class="icon-check pr-10"></i> Recorded Videos Sessions On Bigdata and R-Programming with LMS Access.(lifetime support)</li>
                                                    <li><i class="icon-check pr-10"></i> Quiz , Assignment & POC.</li>
                                                    <li><i class="icon-check pr-10"></i> On Demand Online Support .</li>
                                                    <li><i class="icon-check pr-10"></i> Discussion Forum.</li>
                                                    <li><i class="icon-check pr-10"></i> Material
                                                        <ul class="list-icons" style="margin-left:50px;">
                                                            <li>a. Sample Question papers of Cloudera Certification.</li>
                                                            <li>b. Technical Notes & Study Material.</li>
                                                        </ul>
                                                    
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        
                                    </div>
                                    <div class="tab-pane" id="h3tab5">
                                        <div class="row object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
                                            <div class="col-md-6 ">
                                                <div class="pv-20 ph-20 feature-box-2 boxed shadow object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
                                                    <div class="col-md-3 testimonial-image"><img src="../images/testimonial-2.jpg" /></div> 
                                                    <div class="body">
                                                        <h4 class="title">Anupam Khamparia 
                                                            <br>
                                                            <span style="font-size:11px;">Consultant, Cognizant Technology Solutions, Bangalore</span>
                                                        </h4>
                                                        <p class ="text-justify">“Excellent course and instructor. I learnt a lot in a short period. Good value for money. Instructor took us through Advanced R-Programming Development in depth.”</p>
                                                       
                                                    </div>
                                                </div>
                                            </div>
                                          <div class="col-md-6 ">
                                                <div class="pv-20 ph-20 feature-box-2 boxed shadow object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
                                                    <div class="col-md-3 testimonial-image"><img src="../images/testimonial-1.png" /></div> 
                                                    <div class="body">
                                                        <h4 class="title">Anukanksha Garg
                                                          <br>
                                                            <span style="font-size:11px;">B.Tech. CSE</span>
                                                        </h4>
                                                        <p class ="text-justify">It was a nice learning experience with prwatech. The classes were well scheduled and managed.
Verma has good understanding of the topics taught and catered to our problems and doubts very patiently. The best thing about him was that he handled the situations accordingly, when needed a friend, he became one and also a teacher to always guide us.</p>
                                                       
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6 ">
                                                <div class="pv-20 ph-20 feature-box-2 boxed shadow object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
                                                    <div class="col-md-3 testimonial-image"><img src="../images/testimonial-4.PNG" /></div> 
                                                    <div class="body">
                                                        <h4 class="title">Varun Shivashanmugum
                                                        
                                                        <br>
                                                            <span style="font-size:11px;">Associate Consultant, ITC Infotech Ltd</span>
                                                        </h4>
                                                        
                                                        <p class ="text-justify">“Faculty is good,Verma takes keen interest and personnel care in improvising skills in students. And most importantly,Verma will be available and clears doubts at any time apart from class hours. And he always keeps boosting and trying to increase confidence in his students which adds extra attribute to him and organization as well. and organization as well.</p>
                                                       
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6 ">
                                                <div class="pv-20 ph-20 feature-box-2 boxed shadow object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
                                                    <div class="col-md-3 testimonial-image"><img src="../images/testimonial-5.jpg" /></div> 
                                                    <div class="body">
                                                        <h4 class="title">Mayank Srivastava <br>
                                                        <span style="font-size:11px;">R-Programming Developer, L&T, Bangalore</span>
                                                        </h4>
                                                        <p class ="text-justify">“Really good course content and labs, patient enthusiastic instructor. Good instructor, with in depth skills…Very relevant practicals allowed me to put theory into practice.”</p>
                                                       
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                                 <div class="col-md-3" style="position:relative;top:-10px;">
                                     <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center" >
                                         <div class="overlay-container">
                                             <img src="../images/icon/curse-d1.png" alt="">
                                         </div>
                                         <div class="body">
                                             <h3 class="heading-font">Rs. 14,000 + Tax </h3>
                                             <span class="small-font">per 2 weeks</span>
                                             <div class="separator"></div>
                                             <p class="heading-font"></p>
                                             <table class="table table-striped ">
                                                 <tbody>
                                                     <tr><th><center>35 Hours</center></th></tr>
                                                 <tr><th><center>15 Seats</center></th></tr>
                                                 <tr><th><center>Course Badge</center></th></tr>
                                                 <tr><th><center>Course Certificate</center></th></tr>
                                                 <tr><th><center> <a  onclick="openNav()" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Enroll Now<i class="fa fa-arrow-right pl-10"></i></a></center></th></tr>
                                                 </tbody>
                                             </table>
                                            <?php include '../includes/demo-popup.php';?>
                                                
                                         </div>
                                     </div>
                                      <h3 class="heading-font text-center" style="background:#4c4c4c;color:white;padding-top:10px;padding-bottom:10px">Suggested Courses</h3> 
                                      <div class="owl-carousel content-slider">
                                          <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
                                              <div class="overlay-container">
                                                  <img src="../images/por-1.png" alt="">
                                                  <div class="overlay-to-top">
                                                      <p class="small margin-clear">&nbsp;</p>
                                                  </div>
                                              </div>
                                              <div class="body">
                                                  <h3 class="heading-font">Big Data R-Programming Training</h3>
                                                  <div class="separator"></div>
                                                  <p class="heading-font">Advanced Program for Top-notch Developers </p>
                                                  <a href="big-data-hadoop-training.php"  target="_blank" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read More<i class="fa fa-arrow-right pl-10"></i></a>												
                                              </div>
                                          </div>
                                          <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
                                              <div class="overlay-container">
                                                  <img src="../images/por-2.png" alt="">
                                                  <div class="overlay-to-top">
                                                      <p class="small margin-clear">&nbsp;</p>
                                                  </div>
                                              </div>
                                              <div class="body">
                                                  <h3 class="heading-font">R-Programming Admin Training</h3>
                                                  <div class="separator"></div>
                                                  <p class="heading-font">Advanced Program for Top-notch Developers </p>
                                                  <a href="hadoop-admin.php" target="_blank" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read More<i class="fa fa-arrow-right pl-10"></i></a>												
                                              </div>
                                          </div>
                                          <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
                                              <div class="overlay-container">
                                                  <img src="../images/por-3.png" alt="">
                                                  <div class="overlay-to-top">
                                                      <p class="small margin-clear">&nbsp;</p>
                                                  </div>
                                              </div>
                                              <div class="body">
                                                  <h3 class="heading-font">Apache Spark Scala Training</h3>
                                                  <div class="separator"></div>
                                                  <p class="heading-font">Apache Spark Scala Combo Training </p>
                                                  <a href="apache-spark-scala-training.php" target="_blank" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read More<i class="fa fa-arrow-right pl-10"></i></a>												
                                              </div>
                                          </div>
                                          <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
                                              <div class="overlay-container">
                                                  <img src="../images/por-4.png" alt="">
                                                  <div class="overlay-to-top">
                                                      <p class="small margin-clear">&nbsp;</p>
                                                  </div>
                                              </div>
                                              <div class="body">
                                                  <h3 class="heading-font">Data Science Training</h3>
                                                  <div class="separator"></div>
                                                  <p class="heading-font">Find out the truth about what Data Science is. </p>
                                                  <a href="data-science-course-training-bangalore.php" target="_blank" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read More<i class="fa fa-arrow-right pl-10"></i></a>												
                                              </div>
                                          </div>
                                          <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
                                              <div class="overlay-container">
                                                  <img src="../images/por-5.png" alt="">
                                                  <div class="overlay-to-top">
                                                      <p class="small margin-clear">&nbsp;</p>
                                                  </div>
                                              </div>
                                              <div class="body">
                                                  <h3 class="heading-font">R Programming Training</h3>
                                                  <div class="separator"></div>
                                                  <p class="heading-font">Business Analytics With R Training</p>
                                                  <a href="r-programing.php" target="_blank" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read More<i class="fa fa-arrow-right pl-10"></i></a>												
                                              </div>
                                          </div>
                                          <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
                                              <div class="overlay-container">
                                                  <img src="../images/por-6.png" alt="">
                                                  <div class="overlay-to-top">
                                                      <p class="small margin-clear">&nbsp;</p>
                                                  </div>
                                              </div>
                                              <div class="body">
                                                  <h3 class="heading-font"> Tableau Training & Certification</h3>
                                                  <div class="separator"></div>
                                                  <p class="heading-font">Speed on concepts of data visualization</p>
                                                  <a href="tableau.php" target="_blank" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read More<i class="fa fa-arrow-right pl-10"></i></a>												
                                              </div>
                                          </div>
                                          <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
                                              <div class="overlay-container">
                                                  <img src="../images/por-7.png" alt="">
                                                  <div class="overlay-to-top">
                                                      <p class="small margin-clear">&nbsp;</p>
                                                  </div>
                                              </div>
                                              <div class="body">
                                                  <h3 class="heading-font">Python Programming</h3>
                                                  <div class="separator"></div>
                                                  <p class="heading-font">Advanced Concepts for Top-notch Developers  </p>
                                                  <a href="python.php" target="_blank" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read More<i class="fa fa-arrow-right pl-10"></i></a>												
                                              </div>
                                          </div>
                                      </div> 
                                 </div> 
                            </div> 
                           
                           
                            <p>&nbsp;</p>
                            
                        </section>
                 
                        
			<!-- section end -->
                    
			<div class="clearfix"></div>
			
			<!-- ================ -->
			
			<!-- section end -->
                        <section class="pv-30 dark-bg padding-bottom-clear clearfix object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
					<br>
					<div class="container">
						<div class="row">
							<div class="col-md-3 ">
								<div class="ph-20 feature-box text-center object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
									<span class="icon default-bg circle"><i class="fa fa-institution"></i></span>
									<h3>Live classes</h3>
									<div class="separator clearfix"></div>
                                                                        <p class="heading-font ">Live online and interactive classes conducted by instructor</p>
									
								</div>
							</div>
							<div class="col-md-3 ">
								<div class="ph-20 feature-box text-center object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
									<span class="icon default-bg circle"><i class="fa fa-user"></i></span>
									<h3>Expert instructions</h3>
									<div class="separator clearfix"></div>
									<p class="heading-font">Learn from our Experts and get Real-Time Guidance</p>
									
								</div>
							</div>
							<div class="col-md-3 ">
								<div class="ph-20 feature-box text-center object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
									<span class="icon default-bg circle"><i class="icon-signal"></i></span>
									<h3>24 X 7 Support</h3>
									<div class="separator clearfix"></div>
									<p class="heading-font">Personalized Guidance from our 24X7 Support Team</p>
									
								</div>
							</div>
							<div class="col-md-3 ">
								<div class="ph-20 feature-box text-center object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
									<span class="icon default-bg circle"><i class="icon-calendar"></i></span>
									<h3>Flexible schedule</h3>
									<div class="separator clearfix"></div>
									<p class="heading-font">Reschedule your Batch/Class at Your Convenience</p>
									
								</div>
							</div>
						</div>
					</div>
					<br>
				</section>
			
			<!-- section end -->

			<!-- section -->
			<!-- ================ -->
			<section class="pv-30  padding-bottom-clear dark-translucent-bg parallax" id="row-testimonials"style="background:url('../images/bg/451.jpg'); box-shadow:inset 0 2px 7px rgba(0,0,0, 0.25);">
				
				<div class="space-bottom">
					
					<div class="owl-carousel content-slider">
						<div class="container">
							<div class="row">
								<div class="col-md-8 col-md-offset-2">
									<div class="testimonial text-center">
										<div class="testimonial-image">
                                                                                    <img src="../images/testimonial-1.png" alt="" title="" class="img-circle">
										</div>
										
										<div class="testimonial-body">
                                                                                    <p>&nbsp;</p>
												<p id="cpara">It was a nice learning experience with prwatech. The classes were well scheduled and managed.
Verma has good understanding of the topics taught and catered to our problems and doubts very patiently. The best thing about him was that he handled the situations accordingly, when needed a friend, he became one and also a teacher to always guide us.</p>
											
											<div class="testimonial-info-1">Anukanksha Garg</div>
											<div class="testimonial-info-2">B.TECH- CS</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="container">
							<div class="row">
								<div class="col-md-8 col-md-offset-2">
									<div class="testimonial text-center">
										<div class="testimonial-image">
											<img src="../images/testimonial-2.jpg" alt="" title="" class="img-circle">
										</div>
										
										<div class="testimonial-body">
											 <p>&nbsp;</p>
												<p id="cpara">Excellent course and instructor. I learnt a lot in a short period. Good value for money. Instructor took us through Advanced R-Programming Development in depth.</p>
											
											<div class="testimonial-info-1">Anupam Khamparia</div>
											<div class="testimonial-info-2">Consultant, Cognizant Technology Solutions, Bangalore</div>
										</div>
									</div>
								</div>
							</div>
						</div>
                                            <div class="container">
							<div class="row">
								<div class="col-md-8 col-md-offset-2">
									<div class="testimonial text-center">
										<div class="testimonial-image">
											<img src="../images/testimonial-3.jpg" alt="" title="" class="img-circle">
										</div>
										
										<div class="testimonial-body">
											<p>&nbsp;</p>
												<p id="cpara">Excellent course and instructor. I learnt a lot in a short period. Good value for money. Instructor took us through Advanced R-Programming Development in depth.</p>
											<div class="testimonial-info-1">Kumar Waibhav</div>
                                                                                        <div class="testimonial-info-2">Software Engineer, Cognizant Technology Solutions,<br> Philadelphia, Pennsylvania, USA</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					
				</div>
			</section>
			<!-- section end -->

			<!-- section start -->
			<!-- ================ -->
                        
			                   
			<!-- section end -->
			<!-- footer start (Add "dark" class to #footer in order to enable dark footer) -->
			<!-- ================ -->
			<?php include '../includes/footer.php'; ?>
			<!-- footer end -->
		</div>
		<!-- page-wrapper end -->

		<?php include '../includes/jslinks.php';?>
                <!-- Color Switcher End -->
                <?php include '../includes/quick-query.php';?>
                 <?php include '../includes/UserSignup.php';?>
	</body>

        <script>
            $(document).ready(function(){
                onlad(5);
            });  
        </script>

</html>
