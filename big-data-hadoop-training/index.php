<?php include '../includes/session.php'; ?>
<!DOCTYPE html>
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
	<!--<![endif]-->
<head>
		<meta charset="utf-8">
                <title>Hadoop Developer Training Online Course India - Prwatech</title>
                <meta name="description" content="Prwatech provides Big Data Hadoop Developer Training Classes and online certification courses for beginners and developers. To Know more visit Prwatech.." />
                <meta property="og:type" content="article" />
                <meta property="og:title" content="Big Data Hadoop Training " />
                <meta property="og:description" content="Prwatech provides big data Hadoop Training Classes for beginners and developers with versatile carrier options. Know more about courses visit our website." />
                <meta property="og:url" content="http://prwatech.in/big-data-hadoop-training/" />
                <meta property="og:site_name" content="BigData &amp; HADOOP" />
                <meta name="google-site-verification" content="fSzBVSEUMu0l5lnD0qBVGv5F_16zaI6xiUZFMm-iMqQ" />    
                 <?php include '../includes/csslinks.php'; ?>
        </head>

	<!-- body classes:  -->
	<!-- "boxed": boxed layout mode e.g. <body class="boxed"> -->
	<!-- "pattern-1 ... pattern-9": background patterns for boxed layout mode e.g. <body class="boxed pattern-1"> -->
	<!-- "transparent-header": makes the header transparent and pulls the banner to top -->
	<body class="no-trans front-page transparent-header ">
            <!-- scrollToTop -->
		<!-- ================ -->
		<div class="scrollToTop circle"><i class="icon-up-open-big"></i></div>
		
		<!-- page wrapper start -->
		<!-- ================ -->
		<div class="page-wrapper">
		
			<!-- header-container start -->
			<?php include '../includes/header.php' ;  ?>
			<!-- header-container end -->
		
			<!-- banner start -->
			<!-- ================ -->
			<div class="banner dark-translucent-bg" style="background-image:url('../images/page-course-banner-1.jpg'); background-position: 50% 27%;">
				<!-- breadcrumb start -->
				<!-- ================ -->
				<div class="breadcrumb-container object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
					<div class="container">
						<ol class="breadcrumb">
							<li><i class="fa fa-home pr-10"></i><a class="link-dark" href="index.html">Home</a></li>
							<li class="active">All Courses</li>
							<li class="active">Big Data Hadoop Certification Training</li>
						</ol>
					</div>
				</div>
				<!-- breadcrumb end -->
				
			</div>
			<!-- banner end -->
			
			<div id="page-start"></div>

			<!-- section start -->
			<!-- ================ -->
			<section class="light-gray-bg pv-30 clearfix object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100" id="cdrow1">
				<div class="container">
					<div class="row">
						<div class="col-md-8">
							 <h1 class="text-center heading-font" style="text-transform:none;"> <strong>Big Data Hadoop Training </strong></h1>
                                                    <div class="separator"></div>
                                                        <p class="large text-justify heading-font">The Big Data and Hadoop training course from Hadoop Big Data Online Training is designed to enhance your knowledge and skills to become a successful Hadoop developer. In-depth knowledge of core concepts will be covered in the course along with implementation on varied industry use-cases.</p>
						</div>
						<div class="col-md-4">
                                                    <a class="btn" data-toggle="modal" data-target="#demoVideoPopUp"><img src="../images/demo-1.png"/></a>
                                               </div>
						
					</div>
                                    <p>&nbsp;</p>
                                    <div class="separator"></div>
                                    <h3 class="text-center heading-font object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100" style="text-transform:none;">Contact Us <strong>+91 8147111254 </strong></h3>
                                     <div class="separator"></div>
                                      <p>&nbsp;</p>
				</div>
                             
                           
                          <div class="container">
                                  <div class="row" id="batch_details">
                                    <div class="col-md-3">
                                        <div class="col-md-3">
                                            <div class="row">
                                                <p id ="date-time">21 <br><span id="month">NOV</span></p>
                                            </div>
                                       </div>
                                        <div class="col-md-9" id="upcomming-batch">
                                            <div class="row">
                                                <div class="col-md-12"><p id="dt-content"><b> Mon-Wed (3 Days) <br><small>&nbsp; <br> Rs. 14000/- <a href="#" class="" data-toggle="modal" data-target="#demoPopUp">Enroll Now</a></small></b></p></div>
                                                
                                            </div>
                                       </div>
                                    </div>
                                  <div class="col-md-3">
                                        <div class="col-md-3">
                                            <div class="row">
                                                <p id ="date-time">24 <br><span id="month">NOV</span></p>
                                            </div>
                                       </div>
                                        <div class="col-md-9" id="upcomming-batch">
                                            <div class="row">
                                                <div class="col-md-12"><p id="dt-content"><b> Mon-Wed (3 Days) <br><small>&nbsp; <br> Rs. 14000/- <a href="#" class="" data-toggle="modal" data-target="#demoPopUp">Enroll Now</a></small></b></p></div>
                                                
                                            </div>
                                       </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="col-md-3">
                                            <div class="row">
                                                <p id ="date-time">27 <br><span id="month">NOV</span></p>
                                            </div>
                                       </div>
                                        <div class="col-md-9" id="upcomming-batch">
                                            <div class="row">
                                                <div class="col-md-12"><p id="dt-content"><b> Mon-Wed (3 Days) <br><small>&nbsp; <br> Rs. 14000/- <a href="#" class="" data-toggle="modal" data-target="#demoPopUp">Enroll Now</a></small></b></p></div>
                                                
                                            </div>
                                       </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="col-md-3">
                                            <div class="row">
                                                <p id ="date-time">30 <br><span id="month">NOV</span></p>
                                            </div>
                                       </div>
                                        <div class="col-md-9" id="upcomming-batch">
                                            <div class="row">
                                                <div class="col-md-12"><p id="dt-content"><b> Mon-Wed (3 Days) <br><small>&nbsp; <br> Rs. 14000/- <a href="#" class="" data-toggle="modal" data-target="#demoPopUp">Enroll Now</a></small></b></p></div>
                                                
                                            </div>
                                       </div>
                                    </div>
                                </div>
                            </div>
			</section>
                       
                        <section id="coursed-row2" class="object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
                            <div class="container">
                                
                                 <h3 class="text-center heading-font object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100" style="text-transform:none;"> <strong>Course</strong> Details</h3>
                                 <div id="vseparator" class="object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
                                     <div class="vhr"></div>
                                </div>
                                <div class="row col-md-9" >
                               <!-- tabs start -->
                                <!-- ================ -->
                                <!-- Nav tabs -->
                                <ul class="nav nav-tabs style-3" id="tab-course" role="tablist">
                                    <li class="active"><a href="#h3tab1" role="tab" data-toggle="tab">Overview</a></li>
                                    <li><a href="#h3tab2" role="tab" data-toggle="tab">Course Curriculum</a></li>
                                    <li><a href="#h3tab3" role="tab" data-toggle="tab">FAQ's</a></li>
                                    <li><a href="#h3tab4" role="tab" data-toggle="tab">Download Course Outline</a></li>
                                    <li><a href="#h3tab5" role="tab" data-toggle="tab">Reviews</a></li>
                                   
                                </ul>
                                <!-- Tab panes -->
                                <div class="tab-content">
                                    <div class="tab-pane in active " id="h3tab1">
                                        <div class="row">
                                            <div class="col-md-12 object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
                                              
                                                <h2 class="heading-font">What is Big Data?</h2>
                                                <p id="cpara">Big Data is collection of huge or massive amount of data.We live in data age.And it’s not easy to measure the total volume of data or to manage & process this enormous data. The flood of thisBig Data are coming from different resources.
                                                    <br><b>Such as </b>: New York stock exchange, Facebook, Twitter, AirCraft, Wallmart etc.
                                                </p>
                                                <p id="cpara">Today’s world information is getting doubled after every two years (1.8 times).
                                                    And still 80% of data is in unstructured format,which is very difficult to store,process or retrieve. so, we can say all this unstructured data is Big Data.</p>
                                                <h2 class="heading-font">Why Hadoop is called Future of Information Economy ?</h2>
                                                <p id="cpara">Hadoop is a Big Data mechanism, which helps to store and process & analysis unstructured data by using any commodity hardware.Hadoop is an open source software framework written in java,which support distributed application.It was introduced by Dough Cutting & Michael J. Cafarellain in mid of 2006.Yahoo is the first commercial user of Hadoop(2008).
Hadoop works on two different generation Hadoop 1.0 & Hadoop 2.0 which, is based on YARN (yet another resource negotatior) architecture.Hadoop named after Dough cutting’s son’s elephant
.</p>
    
                                                <h2 class="heading-font">Big Data Growth & Future Market.</h2>
                                                <h2 class="heading-font">Commercial growth of BIG DATA and HADOOP</h2>
                                                <div class="col-md-6"><img src="../images/hadoop_companies.png"/></div>
                                                <div class="col-md-6"><img src="../images/Hadoop-Jobs-Indeed.jpg"/></div>
                                                <p id="cpara">World’s Information is getting doubled after every two years.Today’s market agenda to convert Volume to Value .In current time, every company is investing 30% of its investment to maintain Big Data.According to this, the future prediction by 2020 Data Center is going to be 10X times multiple, Storage Device 100X times multiple,which required to stored this enormousBig Data & to manage this it required massive Man power.The opportunity on Big Data & Hadoop will be 1000X times multiple of today’s requirement by 2020.</p>
                                                <p id="cpara"><b>IBM is one of the giant user of Big Data.IBM 10% (Million$ 1036)revenue come from Big Data</b>.</p>
                                                <p id="cpara">Other top five company revenue from Big Data: HP Million$ 664, Teradeta Million$ 435, Dell Million$ 425 ,Oracle Million$ 415, SAP Million$ 368.</p>
                                                <p id = "cpara">Did you know that in the next 3 years more than half of the total data in the world would move to Hadoop? No wonder that at<b>PrwaTech</b> we have estimated a shortage of nearly 1.7 million Big Data professionals in coming 3 years.</p>
                                                <p id = "cpara">Considering the shortage of <span id="highlighter">Hadoop Training Placement and </span>Big Data professionals with the help of this Bigdata Hadoop Training, IT/ ITES professionals can seize lucrative opportunities and enhance their career by gaining desired Big Data Analytics skills. In this Big Data Hadoop Course attendees will get in detail practical skill set on Hadoop, including its latest and core components, like MapReduce, HDFS, Pig & Hive, Jasper, Sqoop, Impala HBase, Zoopkeeper, Flume, Oozie, Spark and Storm. For extensive hands-on practice, in both Hadoop Training Classes and Hadoop Developer Training participants will get full access to the virtual-lab and numerous projects and assignments for <span id="highlighter">Hadoop Certification Courses</span>.</p>
                                                 
                                                <h2 class="heading-font">Learning Objectives: At the end of Hadoop Developer Training course, participants will be able to:</h2>
                                                <ul class="list-icons" id="clist">
                                                    <li><i class="icon-check pr-10"></i> Completely understand Apache Hadoop Framework.</li>
                                                    <li><i class="icon-check pr-10"></i> Learn to work with HDFS.</li>
                                                    <li><i class="icon-check pr-10"></i> Discover how MapReduce works with data and processes it.</li>
                                                    <li><i class="icon-check pr-10"></i> Design and develop big data applications using Hadoop Ecosystem.</li>
                                                    <li><i class="icon-check pr-10"></i> Learn how YARN helps in managing resources into clusters.</li>
                                                    <li><i class="icon-check pr-10"></i> Write as well as execute programs in YARN.</li>
                                                    <li><i class="icon-check pr-10"></i> Implement MapReduce Integration, HBase, Advanced Indexing and Advanced Usage.</li>
                                                    <li><i class="icon-check pr-10"></i> Work on assignments.</li>
                                                </ul>
                                                    
                                                <h2 class="heading-font">Recommended Audience for Bigdata Hadoop Training:</h2>
                                                <ul class="list-icons" id="clist">
                                                    <li><i class="icon-check pr-10"></i> IT Engineers and Software Developers.</li>
                                                    <li><i class="icon-check pr-10"></i> Data Warehouse Developers, Java Architects, Data Analysts and SAAS Professionals.</li>
                                                    <li><i class="icon-check pr-10"></i> Students and Professionals aspiring to learn latest technologies and make a career in Big Data using Hadoop.</li>
                                                        
                                                </ul>
                                                <h2 class="heading-font">Pre–Requisites for <span id="highlighter">Online Hadoop Training In India </span>: </h2>
                                                <ul class="list-icons" id="clist">
                                                    <li><i class="icon-check pr-10"></i> Good analytical skills.</li>
                                                    <li><i class="icon-check pr-10"></i> Some prior experience in Core Java.</li>
                                                    <li><i class="icon-check pr-10"></i> Fundamental knowledge of Unix.</li>
                                                    <li><i class="icon-check pr-10"></i> Basic knowledge of SQL scripting.</li>
                                                    <li><i class="icon-check pr-10"></i> Prior experience in Apache Hadoop is not required.</li>
                                                        
                                                </ul>
                                                <p id = "cpara">Enroll for expert level Big Data Hadoop Course and <span id="highlighter">Online Hadoop Training From India </span>to build a rewarding career as certified Hadoop developer. Our Hadoop Developer Training course material and tutorials are created by highly experienced instructors. Once you have registered with PrwaTech you will have complete access to our Hadoop video tutorials, course materials, PPT’s, case studies, projects and interview question..</p>
                                                <div class="separator"></div>
                                                <h2 class="heading-font">Job Titles for Hadoop Professionals</h2>
                                                <p id = "cpara">Job opportunities for talented software engineers in fields of Hadoop and Big Data are enormous and profitable. Zest to become proficient and well versed in Hadoop environment is all that is required for a fresher. Having technical experience and proficiency in fields described below can help you move up the ladder to great heights in the IT industry.</p>
                                                  
                                                <div class="col-md-12 ">
                                                    <div class="pv-20 ph-20 feature-box-2 boxed shadow object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
                                                        <span class="icon without-bg"><i class="fa fa-database"></i></span>
                                                        <div class="body">
                                                            <h2 class="title" class="heading-font">Hadoop Architect</h2>
                                                            <p id="cpara">A Hadoop Architect is an individual or team of experts who manage penta bytes of data and provide documentation for Hadoop based environments around the globe. An even more crucial role of a Hadoop Architect is to govern administers, managers and manage the best of their efforts as an administrator. Hadoop Architect also needs to govern Hadoop on large cluster. Every HAdoop Architect must have an impeccable experience in Java, MApreduce, Hive, Hbase and Pig..</p>
                                                                                    
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12 ">
                                                    <div class="pv-20 ph-20 feature-box-2 boxed shadow object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
                                                        <span class="icon without-bg"><i class="fa fa-database"></i></span>
                                                        <div class="body">
                                                            <h2 class="title" class="heading-font">Hadoop Developer</h2>
                                                            <p id="cpara">Hadoop developer is one who has a strong hold on programming languages such as Core Java,SQL jQuery and other scripting languages. Hadoop Developer has to be proficient in writing well optimized codes to manage huge amounts of data. Working knowledge of Hadoop related technologies such as Hive, Hbase, Flume facilitates him in building an exponentially successful career in IT industry.</p>
                                                                                    
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12 ">
                                                    <div class="pv-20 ph-20 feature-box-2 boxed shadow object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
                                                        <span class="icon without-bg"><i class="fa fa-database"></i></span>
                                                        <div class="body">
                                                            <h2 class="title" class="heading-font">Hadoop Scientist</h2>
                                                            <p id="cpara">Hadoop Scientist or Data Scientist is a more technical term replacing Business Analyst. They are professionals who generate, evaluate, spread and integrate the humongous knowledge gathered and stored in Hadoop environments. Hadoop Scientists need to have an in-depth knowledge and experience in business and data. Proficiency in programming languages such as R, and tools such as SAS and SPSS is always a plus point.</p>
                                                                                    
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12 ">
                                                    <div class="pv-20 ph-20 feature-box-2 boxed shadow object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
                                                        <span class="icon without-bg"><i class="fa fa-database"></i></span>
                                                        <div class="body">
                                                            <h2 class="title" class="heading-font">Hadoop Administrator</h2>
                                                            <p id="cpara">With colossal sized database systems to be administered, Hadoop Administrator needs to have a profound understanding of designing principals of HAdooop. An extensive knowledge of hardware systems and a strong hold on interpersonal skills is crucial. Having experience in core technologies such as HAdoop MapReduce,Hive,Linux,Java, Database administration helps him always be a forerunner in his field.</p>
                                                                                    
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12 ">
                                                    <div class="pv-20 ph-20 feature-box-2 boxed shadow object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
                                                        <span class="icon without-bg"><i class="fa fa-database"></i></span>
                                                        <div class="body">
                                                            <h2 class="title" class="heading-font">Hadoop Engineer</h2>
                                                            <p id="cpara">Data Engineers/ Hadoop Enginners are those can create the data-processing jobs and build the distributed MapReduce algorithms for data analysts to utilize. Data Engineers with experience in Java, and C++ will have an edge over others.</p>
                                                                                    
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12 ">
                                                    <div class="pv-20 ph-20 feature-box-2 boxed shadow object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
                                                        <span class="icon without-bg"><i class="fa fa-database"></i></span>
                                                        <div class="body">
                                                            <h2 class="title" class="heading-font">Hadoop Analyst</h2>
                                                            <p id="cpara">Big Data Hadoop Analysts need to be well versed in tools such as Impala, Hive, Pig and also a sound understanding of application of business intelligence on a massive scale. Hadoop Analysts need to come up with cost efficient breakthroughs that are faster in jumping between silos and migrating data.</p>
                                                            <p id="cpara">Want to learn the latest trending technology Big Data Hadoop Course? Register yourself for BigData hadoop training classes from the certified <span id="highlighter">Hadoop Training Placement Institute</span>.</p>
                                                            <p id="cpara">Hadoop developer is one who has a strong hold on programming languages such as Core Java,SQL jQuery and other scripting languages. Hadoop Developer has to be proficient in writing well optimized codes to manage huge amounts of data. Working knowledge of Hadoop related technologies such as Hive, Hbase, Flume facilitates him in building an exponentially successful career in IT industry.</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                            </div>
                                                
                                        </div>
                                            
                                    </div>
                                    <div class="tab-pane" id="h3tab2">
                                        <div class="row">
                                            <div class="col-md-12 ">
                                                  <div class="panel-group collapse-style-2" id="accordion-2">
								<div class="panel panel-default">
									<div class="panel-heading">
										<h4 class="panel-title">
											<a data-toggle="collapse" data-parent="#accordion-2" href="#collapseOne-2">
												<i class="fa fa-arrow-right pr-10"></i>Hadoop and Big Data Architecture - An Introduction
											</a>
										</h4>
									</div>
                                                                    <div id="collapseOne-2" class="panel-collapse collapse in">
                                                                        <div class="panel-body">
                                                                            <p id="cpara" class="text-justify">This module discusses the impact of Big Data Hadoop Course in the social life of people and its significant role. It also discusses how Hadoop is useful in processing and managing Big Data.</p>
                                                                            <p id ="cpara" class="text-justify"><b>Topics: </b>What is Big Data, Role Played by Big Data, Map reduce, Hadoop Components, Name Node, Hadoop Architecture, Job Tracker, HDFS.</p>                       
                                                                        </div>
                                                                    </div>
								</div>
								<div class="panel panel-default">
									<div class="panel-heading">
										<h4 class="panel-title">
											<a data-toggle="collapse" data-parent="#accordion-2" href="#collapseTwo-2" class="collapsed">
												<i class="fa fa-arrow-right pr-10"></i>Hadoop Architecture and Multiplenode Cluster
											</a>
										</h4>
									</div>
									<div id="collapseTwo-2" class="panel-collapse collapse">
										<div class="panel-body">
											<p id="cpara" class="text-justify">The module helps in getting a clear understanding of the roles of Multiple Hadoop Server like DataNode and NameNode along with MapReduce data processing.</p>
                                                                            <p id ="cpara" class="text-justify">The topics covered include Hadoop Initial Configuration and Installation, Installing Hadoop Customers, Anatomy of Read and Write, Data Processing and Replication Pipeline.</p>                       
										</div>
									</div>
								</div>
                                                      <div class="panel panel-default">
                                                          <div class="panel-heading">
                                                              <h4 class="panel-title">
                                                                  <a data-toggle="collapse" data-parent="#accordion-2" href="#collapseThree-2" class="collapsed">
                                                                      <i class="fa fa-arrow-right pr-10"></i>Monitoring, Backup, Maintenance and Recovery
                                                                  </a>
                                                              </h4>
                                                          </div>
                                                          <div id="collapseThree-2" class="panel-collapse collapse">
                                                              <div class="panel-body">
                                                                  <p id="cpara" class="text-justify">This module in the Big Data Hadoop Course helps in understanding standard Cluster Administration activities like Removing and Adding Data Nodes, Configuring Backup, NameNode Recovery and Hadoop Upgrade.</p>
                                                                  <p id ="cpara" class="text-justify"><b>Topics: </b>Black list and white list data nodes in cluster, setting Hadoop Backup, Recovery and Diagnostic.</p>                       
                                                              </div>
                                                          </div>
                                                      </div>
                                                      <div class="panel panel-default">
                                                          <div class="panel-heading">
                                                              <h4 class="panel-title">
                                                                  <a data-toggle="collapse" data-parent="#accordion-2" href="#collapseFour-2" class="collapsed">
                                                                      <i class="fa fa-arrow-right pr-10"></i>Configure Rack Awareness and Cluster Maintenance
                                                                  </a>
                                                              </h4>
                                                          </div>
                                                          <div id="collapseFour-2" class="panel-collapse collapse">
                                                              <div class="panel-body">
                                                                  <p id="cpara" class="text-justify">The Basics and the Implementation of Map-Reduce:<br/>
                                                                      This module discusses the Map Reduce structure and the procedure of implementing Map Reduce on data stored in HDFS.
                                                                      The topics covered include Map Reduce ideas, reducer, mapper, driver, record reader and input split.</p>
                                                                 
                                                              </div>
                                                          </div>
                                                      </div>
                                                      <div class="panel panel-default">
                                                          <div class="panel-heading">
                                                              <h4 class="panel-title">
                                                                  <a data-toggle="collapse" data-parent="#accordion-2" href="#collapseFive-2" class="collapsed">
                                                                      <i class="fa fa-arrow-right pr-10"></i>Sqoop (Analysis and Datasets)
                                                                  </a>
                                                              </h4>
                                                          </div>
                                                          <div id="collapseFive-2" class="panel-collapse collapse">
                                                              <div class="panel-body">
                                                                  <p id="cpara" class="text-justify">This module covers a clear understanding of importing and exporting data from RDBMS to HDFS, everything about Sqoop.</p>
                                                                  <p id="cpara" class="text-justify">The importance of using Sqoop, the provision of Hive Metastore, Sqoop features, Sqoop connectors, Sqoop performance benchmarks, everything about Flume and the import of data with the use of Flume.</p>
                                                                 
                                                              </div>
                                                          </div>
                                                      </div>
                                                      <div class="panel panel-default">
                                                          <div class="panel-heading">
                                                              <h4 class="panel-title">
                                                                  <a data-toggle="collapse" data-parent="#accordion-2" href="#collapseSix-2" class="collapsed">
                                                                      <i class="fa fa-arrow-right pr-10"></i>Using Hive for Analysing Twitter Data
                                                                  </a>
                                                              </h4>
                                                          </div>
                                                          <div id="collapseSix-2" class="panel-collapse collapse">
                                                              <div class="panel-body">
                                                                  <p id="cpara" class="text-justify">HQL and Hive with Analytics</p>
                                                                  <p id="cpara" class="text-justify">This module discusses data warehouse package that analyses structure data. It is also discusses about loading data and installing Hive along with the storage of data in various tables.</p>
                                                                  <p id="cpara" class="text-justify">Topics covered are Hive Services, Hive Web Interface, Hive Shell, Differences between DISTRIBUTE By, SORT By and ORDER By, Types of Primitive Data, Different Methods of Operating Hive, Analysis of Log on Hive and Exercises.</p>
                                                                 
                                                              </div>
                                                          </div>
                                                      </div>
                                                       <div class="panel panel-default">
                                                          <div class="panel-heading">
                                                              <h4 class="panel-title">
                                                                  <a data-toggle="collapse" data-parent="#accordion-2" href="#collapseSeven-2" class="collapsed">
                                                                      <i class="fa fa-arrow-right pr-10"></i>HBase, NOSQL Databases and Advance Hive
                                                                  </a>
                                                              </h4>
                                                          </div>
                                                          <div id="collapseSeven-2" class="panel-collapse collapse">
                                                              <div class="panel-body">
                                                                 
                                                                  <p id="cpara" class="text-justify">This module helps in getting a clear understanding of the Advance Hive ideas like UDF. The learners also get in-depth knowledge of HBase, the process of loading data in HBase and the use of query data from HBase.</p>
                                                                  <p id="cpara" class="text-justify">The topics covered are User Defined Functionalities, Data Manipulation Using Hive, Hive Scripting, MapReduce Integration, HBase Architecture, HBase Introduction.</p>
                                                                 
                                                              </div>
                                                          </div>
                                                      </div>
                                                       <div class="panel panel-default">
                                                          <div class="panel-heading">
                                                              <h4 class="panel-title">
                                                                  <a data-toggle="collapse" data-parent="#accordion-2" href="#collapseEight-2" class="collapsed">
                                                                      <i class="fa fa-arrow-right pr-10"></i>ZooKeeper and Advance HBase
                                                                  </a>
                                                              </h4>
                                                          </div>
                                                          <div id="collapseEight-2" class="panel-collapse collapse">
                                                              <div class="panel-body">
                                                                 
                                                                  <p id="cpara" class="text-justify">The module covers different concepts of Advance HBase. Learners also get to know about ZooKeeper and how ZooKeeper helps in cluster monitoring.</p>
                                                                  <p id="cpara" class="text-justify"><b>Topics:</b> Advanced Usage of HBase, Advance Indexing, Schema Design, Data Model of ZooKeeper, Operations of ZooKeeper, ZooKeeper Implementation, Sessions, States and Consistency.</p>
                                                                 
                                                              </div>
                                                          </div>
                                                      </div>
                                                       <div class="panel panel-default">
                                                          <div class="panel-heading">
                                                              <h4 class="panel-title">
                                                                  <a data-toggle="collapse" data-parent="#accordion-2" href="#collapseNine-2" class="collapsed">
                                                                      <i class="fa fa-arrow-right pr-10"></i>MRv2, YARN and Hadoop 2.0
                                                                  </a>
                                                              </h4>
                                                          </div>
                                                          <div id="collapseNine-2" class="panel-collapse collapse">
                                                              <div class="panel-body">
                                                                 
                                                                  <p id="cpara" class="text-justify">This module in the Big Data Hadoop Course helps the learners in understanding Hadoop 2.0 features like MRv2, YARN and HDFS Federation.</p>
                                                                  <p id="cpara" class="text-justify">The topics covered are New Features of Hadoop 2.0, High Availability of NameNode.</p>
                                                                 
                                                              </div>
                                                          </div>
                                                      </div>
                                                       <div class="panel panel-default">
                                                          <div class="panel-heading">
                                                              <h4 class="panel-title">
                                                                  <a data-toggle="collapse" data-parent="#accordion-2" href="#collapseTen-2" class="collapsed">
                                                                      <i class="fa fa-arrow-right pr-10"></i>Hadoop Assignment Environment
                                                                  </a>
                                                              </h4>
                                                          </div>
                                                          <div id="collapseTen-2" class="panel-collapse collapse">
                                                              <div class="panel-body">
                                                                 
                                                                  <p id="cpara" class="text-justify">This module helps the readers in understanding how different Hadoop ecosystem elements work together towards Hadoop implementation for solving Big Data issues.</p>
                                                                 
                                                                 
                                                              </div>
                                                          </div>
                                                      </div>
							</div>
                                                
                                                </div>

                                        </div>
                                       
                                    </div>
                                          <div class="tab-pane" id="h3tab3">
                                        <div class="row">
                                            <div class="col-md-12 ">
                                                   	<div class="panel-group collapse-style-3" id="accordion-3">
								<div class="panel panel-default">
									<div class="panel-heading">
										<h4 class="panel-title">
											<a data-toggle="collapse" data-parent="#accordion-3" href="#collapseOne-3">
												Why Should I Learn Hadoop From Prwatech?
											</a>
										</h4>
									</div>
									<div id="collapseOne-3" class="panel-collapse collapse in">
										<div class="panel-body">
                                                                                    <p id="cpara" class="text-justify">
                                                                                        Prwatech is the pioneer of Hadoop training in India. As you know today the demand for Hadoop professionals far exceeds the supply. So it pays to be with the market leader like Prwatech when it comes to learning Hadoop in order to command top salaries. As part of the training you will learn about the various components of Hadoop like MapReduce, HDFS, HBase, Hive, Pig, Sqoop, Flume, Oozie among others. You will get an in-depth understanding of the entire Hadoop framework for processing huge volumes of data in real world scenarios.
                                                                                    </p>
                                                                                    <p id="cpara" class="text-justify">
                                                                                       The Prwatech training is the most comprehensive course, designed by industry experts keeping in mind the job scenario and corporate requirements. We also provide lifetime access to videos, course materials, 24/7 Support, and free course material upgrade. Hence it is a one-time investment. 
                                                                                    </p>
										</div>
									</div>
								</div>
								<div class="panel panel-default">
									<div class="panel-heading">
										<h4 class="panel-title">
											<a data-toggle="collapse" data-parent="#accordion-3" href="#collapseTwo-3" class="collapsed">
												What Are The Various Modes Of Training That Prwatech Offers?
											</a>
										</h4>
									</div>
									<div id="collapseTwo-3" class="panel-collapse collapse">
										<div class="panel-body">
                                                                                    <p id="cpara" class="text-justify">
                                                                                        Prwatech basically offers the self-paced training and online instructor-led training. Apart from that we also provide corporate training for enterprises. All our trainers come with over 5 years of industry experience in relevant technologies and also they are subject matter experts working as consultants. You can check about the quality of our trainers in the sample videos provided.
                                                                                    </p>
                                                                                    
										</div>
									</div>
								</div>
								<div class="panel panel-default">
									<div class="panel-heading">
										<h4 class="panel-title">
											<a data-toggle="collapse" data-parent="#accordion-3" href="#collapseThree-3" class="collapsed">
												Can I Request For A Support Session If I Find Difficulty In Grasping Topics?
											</a>
										</h4>
									</div>
									<div id="collapseThree-3" class="panel-collapse collapse">
										<div class="panel-body">
                                                                                    <p id="cpara" class="text-justify">
                                                                                          If you have any queries you can contact our 24/7 dedicated support to raise a ticket. We provide you email support and solution to your queries. If the query is not resolved by email we can arrange for a one-on-one session with our trainers. The best part is that you can contact Prwatech even after completion of training to get support and assistance. There is also no limit on the number of queries you can raise when it comes to doubt clearance and query resolution.
                                                                                    </p>
                                                                                    
										</div>
									</div>
								</div>
                                                            <div class="panel panel-default">
									<div class="panel-heading">
										<h4 class="panel-title">
											<a data-toggle="collapse" data-parent="#accordion-3" href="#collapseFour-3" class="collapsed">
												If I Am Not From A Programming Background But Have A Basic Knowledge Of Programming Can I Still Learn Hadoop?
											</a>
										</h4>
									</div>
									<div id="collapseFour-3" class="panel-collapse collapse">
										<div class="panel-body">
                                                                                    <p id="cpara" class="text-justify">
                                                                                         Yes, you can learn Hadoop without being from a software background. We provide complimentary courses in Java and Linux so that you can brush up on your programming skills. This will help you in learning Hadoop technologies better and faster.
                                                                                    </p>
                                                                                    
										</div>
									</div>
								</div>
                                                            <div class="panel panel-default">
									<div class="panel-heading">
										<h4 class="panel-title">
											<a data-toggle="collapse" data-parent="#accordion-3" href="#collapseFive-3" class="collapsed">
												What Kind Of Projects Will I Be Working On As Part Of The Training?
											</a>
										</h4>
									</div>
									<div id="collapseFive-3" class="panel-collapse collapse">
										<div class="panel-body">
                                                                                    <p id="cpara" class="text-justify">
                                                                                        We provide you with the opportunity to work on real world projects wherein you can apply your knowledge and skills that you acquired through our training. We have multiple projects that thoroughly test your skills and knowledge of various Hadoop components making you perfectly industry-ready. These projects could be in exciting and challenging fields like banking, insurance, retail, social networking, high technology and so on. The Prwatech projects are equivalent to six months of relevant experience in the corporate world.
                                                                                    </p>
                                                                                    
										</div>
									</div>
								</div>
                                                            <div class="panel panel-default">
									<div class="panel-heading">
										<h4 class="panel-title">
											<a data-toggle="collapse" data-parent="#accordion-3" href="#collapseSix-3" class="collapsed">
												Do You Provide Placement Assistance?
											</a>
										</h4>
									</div>
									<div id="collapseSix-3" class="panel-collapse collapse">
										<div class="panel-body">
                                                                                    <p id="cpara" class="text-justify">
                                                                                        Yes, Prwatech does provide you with placement assistance. We have tie-ups with 80+ organizations including Ericsson, Cisco, Cognizant, TCS, among others that are looking for Hadoop professionals and we would be happy to assist you with the process of preparing yourself for the interview and the job.
                                                                                    </p>
                                                                                    
										</div>
									</div>
								</div>
							</div>
                                                
                                                </div>

                                        </div>
                                       
                                    </div>
                                    <div class="tab-pane" id="h3tab4">
                                        <div class="row object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
                                            <div class="col-md-4">
                                                <p id="cpara" class="text-default"><a href="">Click Here to Download</a></p>
                                                
                                            </div>
                                            <div class="col-md-8">
                                                 <h4 class="title text-default" class="heading-font">Bigdata and Hadoop Services</h4>
                                                <ul class="list-icons">
                                                    <li><i class="icon-check pr-10"></i> PowerPoint Presentation covering all classes</li>
                                                    <li><i class="icon-check pr-10"></i> Recorded Videos Sessions On Bigdata and Hadoop with LMS Access.(lifetime support)</li>
                                                    <li><i class="icon-check pr-10"></i> Quiz , Assignment & POC.</li>
                                                    <li><i class="icon-check pr-10"></i> On Demand Online Support .</li>
                                                    <li><i class="icon-check pr-10"></i> Discussion Forum.</li>
                                                    <li><i class="icon-check pr-10"></i> Material
                                                        <ul class="list-icons" style="margin-left:50px;">
                                                            <li>a. Sample Question papers of Cloudera Certification.</li>
                                                            <li>b. Technical Notes & Study Material.</li>
                                                        </ul>
                                                    
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        
                                    </div>
                                    <div class="tab-pane" id="h3tab5">
                                        <div class="row object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
                                            <div class="col-md-6 ">
                                                <div class="pv-20 ph-20 feature-box-2 boxed shadow object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
                                                    <div class="col-md-3 testimonial-image"><img src="../images/testimonial-2.jpg" /></div> 
                                                    <div class="body">
                                                        <h4 class="title">Anupam Khamparia 
                                                            <br>
                                                            <span style="font-size:11px;">Consultant, Cognizant Technology Solutions, Bangalore</span>
                                                        </h4>
                                                        <p class ="text-justify">“Excellent course and instructor. I learnt a lot in a short period. Good value for money. Instructor took us through Advanced Hadoop Development in depth.”</p>
                                                       
                                                    </div>
                                                </div>
                                            </div>
                                          <div class="col-md-6 ">
                                                <div class="pv-20 ph-20 feature-box-2 boxed shadow object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
                                                    <div class="col-md-3 testimonial-image"><img src="../images/testimonial-1.png" /></div> 
                                                    <div class="body">
                                                        <h4 class="title">Anukanksha Garg
                                                          <br>
                                                            <span style="font-size:11px;">B.Tech. CSE</span>
                                                        </h4>
                                                        <p class ="text-justify">It was a nice learning experience with prwatech. The classes were well scheduled and managed.
Verma has good understanding of the topics taught and catered to our problems and doubts very patiently. The best thing about him was that he handled the situations accordingly, when needed a friend, he became one and also a teacher to always guide us.</p>
                                                       
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6 ">
                                                <div class="pv-20 ph-20 feature-box-2 boxed shadow object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
                                                    <div class="col-md-3 testimonial-image"><img src="../images/testimonial-4.PNG" /></div> 
                                                    <div class="body">
                                                        <h4 class="title">Varun Shivashanmugum
                                                        
                                                        <br>
                                                            <span style="font-size:11px;">Associate Consultant, ITC Infotech Ltd</span>
                                                        </h4>
                                                        
                                                        <p class ="text-justify">“Faculty is good,Verma takes keen interest and personnel care in improvising skills in students. And most importantly,Verma will be available and clears doubts at any time apart from class hours. And he always keeps boosting and trying to increase confidence in his students which adds extra attribute to him and organization as well. and organization as well.</p>
                                                       
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6 ">
                                                <div class="pv-20 ph-20 feature-box-2 boxed shadow object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
                                                    <div class="col-md-3 testimonial-image"><img src="../images/testimonial-5.jpg" /></div> 
                                                    <div class="body">
                                                        <h4 class="title">Mayank Srivastava <br>
                                                        <span style="font-size:11px;">Hadoop Developer, L&T, Bangalore</span>
                                                        </h4>
                                                        <p class ="text-justify">“Really good course content and labs, patient enthusiastic instructor. Good instructor, with in depth skills…Very relevant practicals allowed me to put theory into practice.”</p>
                                                       
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                                 <div class="col-md-3" style="position:relative;top:-10px;">
                                     <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center" >
                                         <div class="overlay-container">
                                             <img src="../images/icon/curse-d1.png" alt="">
                                         </div>
                                         <div class="body">
                                             <h3 class="heading-font">Rs. 14,000 + Tax </h3>
                                             <span class="small-font">per 2 weeks</span>
                                             <div class="separator"></div>
                                             <p class="heading-font"></p>
                                             <table class="table table-striped ">
                                                 <tbody>
                                                     <tr><th><center>35 Hours</center></th></tr>
                                                 <tr><th><center>15 Seats</center></th></tr>
                                                 <tr><th><center>Course Badge</center></th></tr>
                                                 <tr><th><center>Course Certificate</center></th></tr>
                                                 <tr><th><center> <a  onclick="openNav()" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Enroll Now<i class="fa fa-arrow-right pl-10"></i></a></center></th></tr>
                                                 </tbody>
                                             </table>
                                            <?php include '../includes/demo-popup.php';?>
                                                
                                         </div>
                                     </div>
                                      <h3 class="heading-font text-center" style="background:#4c4c4c;color:white;padding-top:10px;padding-bottom:10px">Suggested Courses</h3> 
                                      <div class="owl-carousel content-slider">
                                          <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
                                              <div class="overlay-container">
                                                  <img src="../images/por-1.png" alt="">
                                                  <div class="overlay-to-top">
                                                      <p class="small margin-clear">&nbsp;</p>
                                                  </div>
                                              </div>
                                              <div class="body">
                                                  <h3 class="heading-font">Big Data Hadoop Training</h3>
                                                  <div class="separator"></div>
                                                  <p class="heading-font">Advanced Program for Top-notch Developers </p>
                                                  <a href="big-data-hadoop-training.php"  target="_blank" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read More<i class="fa fa-arrow-right pl-10"></i></a>												
                                              </div>
                                          </div>
                                          <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
                                              <div class="overlay-container">
                                                  <img src="../images/por-2.png" alt="">
                                                  <div class="overlay-to-top">
                                                      <p class="small margin-clear">&nbsp;</p>
                                                  </div>
                                              </div>
                                              <div class="body">
                                                  <h3 class="heading-font">Hadoop Admin Training</h3>
                                                  <div class="separator"></div>
                                                  <p class="heading-font">Advanced Program for Top-notch Developers </p>
                                                  <a href="hadoop-admin.php" target="_blank" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read More<i class="fa fa-arrow-right pl-10"></i></a>												
                                              </div>
                                          </div>
                                          <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
                                              <div class="overlay-container">
                                                  <img src="../images/por-3.png" alt="">
                                                  <div class="overlay-to-top">
                                                      <p class="small margin-clear">&nbsp;</p>
                                                  </div>
                                              </div>
                                              <div class="body">
                                                  <h3 class="heading-font">Apache Spark Scala Training</h3>
                                                  <div class="separator"></div>
                                                  <p class="heading-font">Apache Spark Scala Combo Training </p>
                                                  <a href="apache-spark-scala-training.php" target="_blank" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read More<i class="fa fa-arrow-right pl-10"></i></a>												
                                              </div>
                                          </div>
                                          <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
                                              <div class="overlay-container">
                                                  <img src="../images/por-4.png" alt="">
                                                  <div class="overlay-to-top">
                                                      <p class="small margin-clear">&nbsp;</p>
                                                  </div>
                                              </div>
                                              <div class="body">
                                                  <h3 class="heading-font">Data Science Training</h3>
                                                  <div class="separator"></div>
                                                  <p class="heading-font">Find out the truth about what Data Science is. </p>
                                                  <a href="data-science-course-training-bangalore.php" target="_blank" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read More<i class="fa fa-arrow-right pl-10"></i></a>												
                                              </div>
                                          </div>
                                          <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
                                              <div class="overlay-container">
                                                  <img src="../images/por-5.png" alt="">
                                                  <div class="overlay-to-top">
                                                      <p class="small margin-clear">&nbsp;</p>
                                                  </div>
                                              </div>
                                              <div class="body">
                                                  <h3 class="heading-font">R Programming Training</h3>
                                                  <div class="separator"></div>
                                                  <p class="heading-font">Business Analytics With R Training</p>
                                                  <a href="r-programing.php" target="_blank" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read More<i class="fa fa-arrow-right pl-10"></i></a>												
                                              </div>
                                          </div>
                                          <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
                                              <div class="overlay-container">
                                                  <img src="../images/por-6.png" alt="">
                                                  <div class="overlay-to-top">
                                                      <p class="small margin-clear">&nbsp;</p>
                                                  </div>
                                              </div>
                                              <div class="body">
                                                  <h3 class="heading-font"> Tableau Training & Certification</h3>
                                                  <div class="separator"></div>
                                                  <p class="heading-font">Speed on concepts of data visualization</p>
                                                  <a href="tableau.php" target="_blank" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read More<i class="fa fa-arrow-right pl-10"></i></a>												
                                              </div>
                                          </div>
                                          <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
                                              <div class="overlay-container">
                                                  <img src="../images/por-7.png" alt="">
                                                  <div class="overlay-to-top">
                                                      <p class="small margin-clear">&nbsp;</p>
                                                  </div>
                                              </div>
                                              <div class="body">
                                                  <h3 class="heading-font">Python Programming</h3>
                                                  <div class="separator"></div>
                                                  <p class="heading-font">Advanced Concepts for Top-notch Developers  </p>
                                                  <a href="python.php" target="_blank" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">Read More<i class="fa fa-arrow-right pl-10"></i></a>												
                                              </div>
                                          </div>
                                      </div> 
                                      <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center" >
                                         <div class="body">
                                             <div itemscope itemtype="http://schema.org/Product"style="text-align: left">
                                                        <span itemprop="name">Big Data Hadoop Training</span><br>
                                                        <div itemprop="aggregateRating" itemscope itemtype="http://schema.org/AggregateRating">
                                                            Average Rating: <span itemprop="ratingValue">4.79</span><br>
                                                            Votes: <span itemprop="ratingCount">822</span><br>
                                                            Reviews: <span itemprop="reviewCount">822</span>
                                                        </div>
                                            </div>
                                         </div>
                                     </div>
                                 </div> 
                            </div> 
                            <p>&nbsp;</p>
                        </section>
                        
			<!-- section end -->
                    
			<div class="clearfix"></div>
			
			<!-- ================ -->
			
			<!-- section end -->
                        <section class="pv-30 dark-bg padding-bottom-clear clearfix object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
					<br>
					<div class="container">
						<div class="row">
							<div class="col-md-3 ">
								<div class="ph-20 feature-box text-center object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
									<span class="icon default-bg circle"><i class="fa fa-institution"></i></span>
									<h3>Live classes</h3>
									<div class="separator clearfix"></div>
                                                                        <p class="heading-font ">Live online and interactive classes conducted by instructor</p>
									
								</div>
							</div>
							<div class="col-md-3 ">
								<div class="ph-20 feature-box text-center object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
									<span class="icon default-bg circle"><i class="fa fa-user"></i></span>
									<h3>Expert instructions</h3>
									<div class="separator clearfix"></div>
									<p class="heading-font">Learn from our Experts and get Real-Time Guidance</p>
									
								</div>
							</div>
							<div class="col-md-3 ">
								<div class="ph-20 feature-box text-center object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
									<span class="icon default-bg circle"><i class="icon-signal"></i></span>
									<h3>24 X 7 Support</h3>
									<div class="separator clearfix"></div>
									<p class="heading-font">Personalized Guidance from our 24X7 Support Team</p>
									
								</div>
							</div>
							<div class="col-md-3 ">
								<div class="ph-20 feature-box text-center object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
									<span class="icon default-bg circle"><i class="icon-calendar"></i></span>
									<h3>Flexible schedule</h3>
									<div class="separator clearfix"></div>
									<p class="heading-font">Reschedule your Batch/Class at Your Convenience</p>
									
								</div>
							</div>
						</div>
					</div>
					<br>
				</section>
			
			<!-- section end -->

			<!-- section -->
			<!-- ================ -->
			 <section class="pv-30  padding-bottom-clear dark-translucent-bg parallax" id="row-testimonials"style="background:url('../images/bg/451.jpg'); box-shadow:inset 0 2px 7px rgba(0,0,0, 0.25);">
				
				<div class="space-bottom">
					
					<div class="owl-carousel content-slider">
						<div class="container">
							<div class="row">
								<div class="col-md-8 col-md-offset-2">
									<div class="testimonial text-center">
										<div class="testimonial-image">
                                                                                    <img src="../images/testimonial-1.png" alt="" title="" class="img-circle">
										</div>
										
										<div class="testimonial-body">
                                                                                    <p>&nbsp;</p>
												<p id="cpara">It was a nice learning experience with prwatech. The classes were well scheduled and managed.
Verma has good understanding of the topics taught and catered to our problems and doubts very patiently. The best thing about him was that he handled the situations accordingly, when needed a friend, he became one and also a teacher to always guide us.</p>
											
											<div class="testimonial-info-1">Anukanksha Garg</div>
											<div class="testimonial-info-2">B.TECH- CS</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="container">
							<div class="row">
								<div class="col-md-8 col-md-offset-2">
									<div class="testimonial text-center">
										<div class="testimonial-image">
											<img src="../images/testimonial-2.jpg" alt="" title="" class="img-circle">
										</div>
										
										<div class="testimonial-body">
											 <p>&nbsp;</p>
												<p id="cpara">Excellent course and instructor. I learnt a lot in a short period. Good value for money. Instructor took us through Advanced Hadoop Development in depth.</p>
											
											<div class="testimonial-info-1">Anupam Khamparia</div>
											<div class="testimonial-info-2">Consultant, Cognizant Technology Solutions, Bangalore</div>
										</div>
									</div>
								</div>
							</div>
						</div>
                                            <div class="container">
							<div class="row">
								<div class="col-md-8 col-md-offset-2">
									<div class="testimonial text-center">
										<div class="testimonial-image">
											<img src="../images/testimonial-3.jpg" alt="" title="" class="img-circle">
										</div>
										
										<div class="testimonial-body">
											<p>&nbsp;</p>
												<p id="cpara">Excellent course and instructor. I learnt a lot in a short period. Good value for money. Instructor took us through Advanced Hadoop Development in depth.</p>
											<div class="testimonial-info-1">Kumar Waibhav</div>
                                                                                        <div class="testimonial-info-2">Software Engineer, Cognizant Technology Solutions,<br> Philadelphia, Pennsylvania, USA</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					
				</div>
			</section>
			<!-- section end -->

			<!-- section start -->
			<!-- ================ -->
                        
			                   
			<!-- section end -->
			<!-- footer start (Add "dark" class to #footer in order to enable dark footer) -->
			<!-- ================ -->
			<?php include '../includes/footer.php'; ?>
			<!-- footer end -->
			
		</div>
		<!-- page-wrapper end -->
<div class="modal fade" id="demoVideoPopUp" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header ">
                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                <h4 class="modal-title" id="myModalLabel">Demo Session at Prwatech</h4>
                            </div>
                            <div class="modal-body">
                               <iframe width="570" height="350" src="https://www.youtube.com/embed/xA5TAalZt40" frameborder="0" allowfullscreen></iframe> 
                              
                            </div>
                            <div class="modal-footer">
                                
                            </div>
                        </div>
                    </div>
                </div>
		<?php include '../includes/jslinks.php';?>
        <?php include '../includes/quick-query.php';?> 
         <?php include '../includes/UserSignup.php';?>
        <script type="text/javascript" src="../plugins/magnific-popup/jquery.magnific-popup.min.js"></script>
	</body>
        <script>
            $(document).ready(function(){
                onlad(1);
            });  
        </script>


</html>
<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

