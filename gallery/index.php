<?php include '../includes/session.php'; ?>          
<!DOCTYPE html>
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
	<!--<![endif]-->
        <head>
            <meta charset="utf-8">
            <title>PrwaTech | Gallery </title>
            <meta name="title" content="Big Data Hadoop Online Training | Hadoop Certification Course | Prwatech" />
            <meta name="keywords" content="hadoop training, online hadoop training, hadoop training classes, hadoop course online, big data training, big data course, big data online course, hadoop tutorial, HDFS training, Yarn training, MapReduce training, Pig training, Hive training, HBase training" />
            <meta name="description" content="Our Big Data Hadoop Certification Training helps you master HDFS, Yarn, MapReduce, Pig, Hive, HBase with use cases on Retail, Social Media, Aviation, Finance, Tourism domain" />
            <meta name="description" content="PrwaTech provides big data Hadoop Training Classes for beginners and developers with versatile carrier options. Know more about courses visit our website.">
            <meta name="author" content="prwatech">
            <meta name="google-site-verification" content="fSzBVSEUMu0l5lnD0qBVGv5F_16zaI6xiUZFMm-iMqQ" /> 
            <?php include '../includes/csslinks.php'; ?>
            <script type="text/javascript">
                category={}
                $(document).ready(function() {
                     $.ajax({ 
                        type:"post",
                        url:"../server/controller/SelImageCategory.php",
                        success: function(data) { 
//                                   alert(data.trim());
                                    var duce = jQuery.parseJSON(data); //here data is passed to js object
                                    $.each(duce, function (index, article){ 
                                        $('#gallery_display').find('#gallery_categories')
                                                        .append($('<li>')
                                                            .append($('<a>').attr({'href':'#','data-filter':"."+article.name}).append(article.name)));  
                                                
    
                                    });//$.each() closed
                        } // Success closed
                    }); // $.ajax closed
                 });  // $(document).ready  closed
                           
                
                 
            </script>
            <script type="text/javascript">
                $(document).ready(function() {
                     $.ajax({ 
                        type:"post",
                        url:"../server/controller/SelGallery.php",
                        success: function(data) { 
                                    var duce = jQuery.parseJSON(data); //here data is passed to js object
                                    $.each(duce, function (index, article){ 
                                        $('#gallery_grid').find('#gallery_pics')
                                                        .append($('<div>').addClass('col-sm-6 col-md-3 isotope-item all').addClass(article.icat)
                                                        .append($('<div>').addClass('image-box text-center')
                                                        .append($('<div>').addClass('image-box shadow text-center')
                                                        .append($('<div>').addClass('overlay-container overlay-visible').css({'height':'160px','box-shadow':'0 6px 10px rgba(0,0,0, 0.25)'})
                                                         .append($('<img>').attr({'src':'../server/controller/'+article.ipath}))
                                                         .append($('<a>').attr({'href':'../server/controller/'+article.ipath}).addClass('popup-img overlay-link').append($('<i>').addClass('icon-plus-1')))
                                                        )
                                                        )
                                                       )
                                                       );
                                                            
                                                
    
                                    });//$.each() closed
                                        $.get('../plugins/magnific-popup/jquery.magnific-popup.min.js').done(function (){
                                        $.get('../js/template.js').done(function (){
                                        
                                        })
                                    }) // For Late Binding 
                                    }// Success closed
                    }); // $.ajax closed
                 });  // $(document).ready  closed
                           
              
                 
            </script>
        </head>

	<!-- body classes:  -->
	<!-- "boxed": boxed layout mode e.g. <body class="boxed"> -->
	<!-- "pattern-1 ... pattern-9": background patterns for boxed layout mode e.g. <body class="boxed pattern-1"> -->
	<!-- "transparent-header": makes the header transparent and pulls the banner to top -->
	<body class="no-trans front-page transparent-header ">
            <!-- scrollToTop -->
		<!-- ================ -->
		<div class="scrollToTop circle"><i class="icon-up-open-big"></i></div>
		
		<!-- page wrapper start -->
		<!-- ================ -->
		<div class="page-wrapper">
		
			<!-- header-container start -->
			<?php include '../includes/header.php' ;  ?>
			<!-- header-container end -->
		
			<!-- banner start -->
			<!-- ================ -->
			<div class="banner dark-translucent-bg" style="background-image:url('../images/page-about-banner-2.png'); background-position: 50% 27%;">
				<!-- breadcrumb start -->
				<!-- ================ -->
				<div class="breadcrumb-container object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
					<div class="container">
						<ol class="breadcrumb">
							<li><i class="fa fa-home pr-10"></i><a class="link-dark" href="index.html">Home</a></li>
							<li class="active">Gallery</li>
							
						</ol>
					</div>
				</div>
				<!-- breadcrumb end -->
				<div class="container">
					
				</div>
			</div>
			<!-- banner end -->
			
			<div id="page-start"></div>

			<!-- section start -->
			<!-- ================ -->
			     <section class="light-gray-bg pv-30 clearfix"  style="background:#f8f8f8 url('../images/bg/overlay.png') repeat;box-shadow:inset 0 2px 7px rgba(0,0,0, 0.25);">
				<div class="container">
					<div class="row">
						<div class="col-md-8 col-md-offset-2 text-center">
							<h2 class="text-center heading-font" style="text-transform:none;">Glimpses of <strong>Corporate Training</strong>  </h2>
							<div class="separator"></div>						
						</div>
					</div>
					<!-- isotope filters start -->
					<div class="filters text-center" id="gallery_display">
						<ul class="nav nav-pills style-2" id="gallery_categories">
                                                     <!--Here The List of Gallery Category-->   
						</ul>
					</div>
					<!-- isotope filters end -->
					<div class="isotope-container row grid-space-0" id="gallery_grid">
                                            <div class=" row" id="gallery_pics">
                                                
                                            </div>
						
					</div>
					
				</div>
				
				
			</section>
                       
                        
			<!-- section end -->
                    
			<div class="clearfix"></div>
			
			<!-- ================ -->
			
			                   
			<!-- section end -->
			<!-- footer start (Add "dark" class to #footer in order to enable dark footer) -->
			<!-- ================ -->
			<?php include '../includes/footer.php'; ?>
			<!-- footer end -->
			
		</div>
		<!-- page-wrapper end -->

                <?php include '../includes/jslinks.php';?>
                <!-- Color Switcher End -->
                <?php include '../includes/quick-query.php';?>
                <?php include '../includes/UserSignup.php';?>
                
                
	</body>


</html>
