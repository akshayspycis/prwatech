-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Dec 08, 2016 at 09:40 AM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `prwahadoop`
--

-- --------------------------------------------------------

--
-- Table structure for table `blog_category_details`
--

CREATE TABLE IF NOT EXISTS `blog_category_details` (
  `blog_category_details_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `category` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`blog_category_details_id`) USING BTREE
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `blog_category_details`
--

INSERT INTO `blog_category_details` (`blog_category_details_id`, `category`) VALUES
(3, 'Hadoop Software Box 2'),
(4, 'Jobs'),
(5, 'Career'),
(6, 'Big Data '),
(7, 'Big data and Hadoop Dump Questions'),
(10, 'Data Science '),
(11, 'FLUME (1)'),
(12, 'Hadoop Shell Command (1)');

-- --------------------------------------------------------

--
-- Table structure for table `blog_details`
--

CREATE TABLE IF NOT EXISTS `blog_details` (
  `blog_detail_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` varchar(200) DEFAULT NULL,
  `title` varchar(200) DEFAULT NULL,
  `short_description` varchar(200) DEFAULT NULL,
  `long_description` varchar(200) DEFAULT NULL,
  `date` varchar(200) DEFAULT NULL,
  `category_id` varchar(200) DEFAULT NULL,
  `status` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`blog_detail_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `blog_rply_details`
--

CREATE TABLE IF NOT EXISTS `blog_rply_details` (
  `blog_rply_details_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `blog_id` varchar(255) DEFAULT NULL,
  `user_id` varchar(255) DEFAULT NULL,
  `comment` varchar(255) DEFAULT NULL,
  `receiver_id` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `date` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`blog_rply_details_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `course_batch_details`
--

CREATE TABLE IF NOT EXISTS `course_batch_details` (
  `course_batch_details_id` int(11) NOT NULL AUTO_INCREMENT,
  `course_id` varchar(45) NOT NULL,
  `date` varchar(45) NOT NULL,
  `day_type` varchar(45) NOT NULL,
  `day` varchar(45) NOT NULL,
  `time` varchar(45) NOT NULL,
  `fees` varchar(45) NOT NULL,
  `status` varchar(45) NOT NULL,
  `discount` varchar(45) NOT NULL,
  PRIMARY KEY (`course_batch_details_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

--
-- Dumping data for table `course_batch_details`
--

INSERT INTO `course_batch_details` (`course_batch_details_id`, `course_id`, `date`, `day_type`, `day`, `time`, `fees`, `status`, `discount`) VALUES
(1, '1', '2016-12-07', 'Week Day', 'Mon', '12:12 PM', '12200', 'Enable', '0'),
(2, '2', '0111-11-12', 'Week Day', 'Tue', '01:00 PM', '12000', 'Enable', '0'),
(3, '3', '2016-12-07', 'Week Day', 'Web', '02:22 PM', '15000', 'Enable', '0'),
(4, '4', '2016-12-07', 'Week Day', 'Thur', '12:01 PM', '10200', 'Enable', '0'),
(5, '7', '1211-12-24', 'Week End', 'Sun', '06:00 AM', '15000', 'Enable', '0'),
(6, '7', '0121-12-26', 'Week End', 'Sun', '12:12 PM', '15000', 'Enable', '0'),
(7, '6', '2016-12-16', 'Week Day', 'Mon', '10:12 Am', '15000', 'Enable', '0'),
(8, '5', '2016-12-31', 'Week Day', 'Mon', '12:12 PM', '15000', 'Enable', '1500'),
(9, '1', '2016-12-09', 'Week Day', 'Mon', '03:10 PM', '15000', 'Enable', '0'),
(10, '1', '2016-12-17', 'Week Day', 'Web', '05:54 PM', '10050', 'Enable', '0'),
(11, '2', '2016-12-17', 'Week End', 'Sun', '04:54 PM', '10100', 'Enable', '0'),
(12, '6', '121212-12-29', 'Week Day', 'Thur', '06:00 PM', '15900', 'Enable', '0'),
(13, '2', '2016-12-17', 'Week End', 'Sat', '03:01 AM', '15800', 'Enable', '0'),
(14, '5', '2016-12-30', 'Week Day', 'Fri', '3:15 am', '15000', 'Enable', '0');

-- --------------------------------------------------------

--
-- Table structure for table `login_details`
--

CREATE TABLE IF NOT EXISTS `login_details` (
  `user_id` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `security_ques` varchar(255) DEFAULT NULL,
  `answer` varchar(255) DEFAULT NULL,
  `user_type` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `quick_query`
--

CREATE TABLE IF NOT EXISTS `quick_query` (
  `quick_query_id` int(255) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `contact` varchar(20) DEFAULT NULL,
  `subject` varchar(255) DEFAULT NULL,
  `message` varchar(255) DEFAULT NULL,
  `date` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`quick_query_id`) USING BTREE
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `quick_query`
--

INSERT INTO `quick_query` (`quick_query_id`, `name`, `email`, `contact`, `subject`, `message`, `date`) VALUES
(1, 'lalit', 'lalit.infopark@gmail.com', '7773070823', 'Bigdata Training Regarding', 'Hello, is there any vacant seat....I wanna training on bigdata in weekend days', '2016-12-07 '),
(3, 'Lalit', 'lalit.infopark@gmail.com', '7773070823', 'BIgdata', 'Hello Testing', 'Thu, 08 Dec 2016'),
(4, 'Ankit', 'ankit@gmail.com', '7773070823', 'BIgdata', 'Hello Testing', 'Thu, 08 Dec 2016');

-- --------------------------------------------------------

--
-- Table structure for table `user_details`
--

CREATE TABLE IF NOT EXISTS `user_details` (
  `user_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_name` varchar(255) DEFAULT NULL,
  `dob` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `contact_no` varchar(255) DEFAULT NULL,
  `date` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`user_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `user_profile_details`
--

CREATE TABLE IF NOT EXISTS `user_profile_details` (
  `user_profile_details_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` varchar(255) DEFAULT NULL,
  `pic` varchar(255) DEFAULT NULL,
  `date` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`user_profile_details_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
